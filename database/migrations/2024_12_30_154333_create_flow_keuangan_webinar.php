<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('flow_keuangan_webinar', function (Blueprint $table) {
            $table->id();
            $table->datetime('tanggal_pengajuan')->nullable();
            $table->datetime('tanggal_validasi')->nullable();
            $table->string('jumlah')->nullable();
            $table->string('status')->nullable();
            $table->foreignId('webinar_id')->constrained('webinar')->onDelete('cascade');  // Relasi dengan tabel webinar
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('flow_keuangan_webinar');
    }
};

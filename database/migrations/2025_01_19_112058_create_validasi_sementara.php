<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('validasi_sementara', function (Blueprint $table) {
            $table->id();
            $table->string('jenis')->nullable();
            $table->string('parameter')->nullable();
            $table->string('id_barang')->nullable();
            $table->string('id_pengajuan')->nullable();
            $table->string('status')->nullable();
            $table->longText('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('validasi_sementara');
    }
};

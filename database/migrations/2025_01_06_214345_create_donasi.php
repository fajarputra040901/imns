<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('donasi', function (Blueprint $table) {
            $table->id();
            $table->string('id_transaksi')->nullable();
            $table->string('nama')->nullable();
            $table->string('nohp')->nullable();
            $table->string('emial')->nullable();
            $table->string('donasi')->nullable();
            $table->string('jenis')->nullable();
            $table->string('parameter')->nullable();
            $table->string('status')->nullable();
            $table->string('snaptoken')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('donasi');
    }
};

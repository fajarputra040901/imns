<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pencairan', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->datetime('tanggal_pengajuan')->nullable();
            $table->datetime('tanggal_validasi')->nullable();
            $table->string('jumlah')->nullable();
            $table->string('status')->nullable();
            $table->string('jenis')->nullable();
            $table->string('parameter')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pencairan');
    }
};

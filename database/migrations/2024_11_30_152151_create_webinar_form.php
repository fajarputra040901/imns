<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('webinar_form', function (Blueprint $table) {
            $table->id();
            $table->json('form')->nullable();  // Form dalam format JSON, nullable jika tidak ada
            $table->foreignId('webinar_id')->constrained('webinar')->onDelete('cascade');  // Relasi dengan tabel webinar
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('webinar_form');
    }
};

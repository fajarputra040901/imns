<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('webinar', function (Blueprint $table) {
            $table->id();
            $table->string('judul')->nullable();  
            $table->text('tempat')->nullable();  
            $table->date('tanggal_mulai')->nullable();  
            $table->date('tanggal_selesai')->nullable();  
            $table->string('jenis')->nullable();  
            $table->string('peserta')->nullable();  
            $table->string('skp')->nullable();  
            $table->longText('deskripsi')->nullable(); 
            $table->string('slug')->nullable();  
            $table->string('cover')->nullable();  
            $table->json('cost')->nullable(); 
            $table->json('form')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('webinar');
    }
};

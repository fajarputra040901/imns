<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => "admnin",
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('indonesia'),  // Menggunakan Hash::make untuk menyimpan password dengan aman
        ]);
    }
}

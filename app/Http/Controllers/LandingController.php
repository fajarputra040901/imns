<?php

namespace App\Http\Controllers;

use App\Models\Giveawayokuli;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Http\Request;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Builder as HtmlBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\DataTables;

class LandingController extends Controller
{
    public function okuli()
    {
        return view('okuli');
    }
    public function giveaway_okuli()
    {
        return view('giveaway');
    }
    public function giveaway_post(Request $r)
    {
        $validExtensions = ['jpg', 'jpeg', 'png'];
        $gambar = $r->file('follow');
        $ext = $gambar->getClientOriginalExtension();
        $maxSize = 2 * 1024; // 2MB
        $cek = Giveawayokuli::where('email', $r->email)->count();
        $cek2 = Giveawayokuli::where('ig', $r->ig)->count();
        if (!in_array($ext, $validExtensions)) {
            Alert::error('Gagal', 'Ekstensi Gambar Screenshoot .jpg, .jpeg, atau .png');
            return back();
        } elseif ($cek != 0) {
            Alert::error('Gagal', 'Email Sudah Mengikuti Giveaway Ini');
            return back();
        } elseif ($cek2 != 0) {
            Alert::error('Gagal', 'USer Ig Ini Sudah Mengikuti Giveaway Ini');
            return back();
        } else {
            if ($r->hasFile('follow')) {
                $path = $r->file('follow')->store('upload', 'public');
            } else {
                $path = '';
            }

            $give = new Giveawayokuli;
            $give->nama = $r->nama;
            $give->email = $r->email;
            $give->nohp = $r->nohp;
            $give->ig = $r->ig;
            $give->sumber = $r->sumber;
            $give->per1 = $r->per1;
            $give->per2 = $r->per2;
            $give->per3 = $r->per3;
            $give->folow = $path;
            $give->save();
            return redirect("/finish_giveaway_okuli/$give->id");
        }
    }
    public function finish_giveaway_okuli($id)
    {
        $data = Giveawayokuli::where('id', $id)->first();
        if ($data) {
            return view('finish_giveaway_okuli', compact(['data']));
        } else {
            abort(404);
        }
    }
    public function download_instastory()
    {
        $path = public_path('assets/img/instastory-okuli.png');

        if (file_exists($path)) {
            return response()->download($path);
        } else {
            abort(404);
        }
    }
    public function admin()
    {
        return view('panel.giveaway_okuli');
    }
    public function responden()
    {
        return view('panel.responden');
    }
    public function kandidat()
    {
        return view('panel.kandidat');
    }
    public function gugur()
    {
        return view('panel.gugur');
    }
    public function list_responden()
    {
        $data = Giveawayokuli::get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('nama', function ($data) {
                return '' . $data->nama . '';
            })
            ->addColumn('ig', function ($data) {
                return '' . $data->ig . '';
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('waktu', function ($data) {
                $tgl = Carbon::parse($data->created_at)->format('d-m-Y | H:i');
                return '' . $tgl . '';
            })
            ->addColumn('tombol', function ($data) {
                if ($data->status == null) {
                    return "
                <a tittle='Lolos' type='button' data-id='$data->id' class='btn btn-success lolos'><i style='color:#fff' class='ti-check'></i></a>               
                 <a tittle='Gugur' type='button' data-id='$data->id' class='btn btn-danger gugur'><i style='color:#fff'  class='ti-close'></i></a>                           
               ";
                } elseif ($data->status == 'lolos') {
                    return "<p>Responden Lolos Menjadi Kandidat</p>";
                } elseif ($data->status == 'gugur') {
                    return "<p>Responden Gugur Menjadi Kandidat</p>";
                }
            })
            ->addColumn('detail', function ($data) {
                return "
                <a tittle='Detail' type='button' data-id='$data->id' class='btn btn-primary detail'><i style='color:#fff'  class='ti-eye'></i></a>               
             
               ";
            })
            ->rawColumns(['tombol', 'detail'])
            ->make(true);
    }
    public function lolos($id)
    {
        $data = Giveawayokuli::where('id', $id)->first();
        $data->status = 'lolos';
        $data->update();
        return response()->json(['data' => 'berhasil']);
    }
    public function gugurkan($id)
    {
        $data = Giveawayokuli::where('id', $id)->first();
        $data->status = 'gugur';
        $data->update();
        return response()->json(['data' => 'berhasil']);
    }
    public function detail($id)
    {
        $give = Giveawayokuli::where('id', $id)->first();
        $data = [
            'nama' => $give->nama,
            'email' => $give->email,
            'nohp' => $give->nohp,
            'ig' => $give->ig,
            'folow' => asset("storage/$give->folow"),
            'per1' => $give->per1,
            'per2' => $give->per2,
            'per3' => $give->per3,
            'sumber' => $give->sumber,
        ];
        return response()->json($data);
    }
    public function list_kandidat()
    {
        $data = Giveawayokuli::where('status', 'lolos')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('nama', function ($data) {
                return '' . $data->nama . '';
            })
            ->addColumn('ig', function ($data) {
                return '' . $data->ig . '';
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('waktu', function ($data) {
                $tgl = Carbon::parse($data->created_at)->format('d-m-Y | H:i');
                return '' . $tgl . '';
            })
            ->addColumn('tombol', function ($data) {

                return "
                
                 <a tittle='Gugur' type='button' data-id='$data->id' class='btn btn-danger gugur'><i style='color:#fff'  class='ti-close'></i></a>                           
               ";
            })
            ->addColumn('detail', function ($data) {
                return "
                <a tittle='Detail' type='button' data-id='$data->id' class='btn btn-primary detail'><i style='color:#fff'  class='ti-eye'></i></a>               
             
               ";
            })
            ->rawColumns(['tombol', 'detail'])
            ->make(true);
    }
    public function list_gugur()
    {
        $data = Giveawayokuli::where('status', 'gugur')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('nama', function ($data) {
                return '' . $data->nama . '';
            })
            ->addColumn('ig', function ($data) {
                return '' . $data->ig . '';
            })
            ->addColumn('nohp', function ($data) {
                return '' . $data->nohp . '';
            })
            ->addColumn('waktu', function ($data) {
                $tgl = Carbon::parse($data->created_at)->format('d-m-Y | H:i');
                return '' . $tgl . '';
            })
            ->addColumn('tombol', function ($data) {

                return "
                  <a tittle='Lolos' type='button' data-id='$data->id' class='btn btn-success lolos'><i style='color:#fff' class='ti-check'></i></a>     
                                         
               ";
            })
            ->addColumn('detail', function ($data) {
                return "
                <a tittle='Detail' type='button' data-id='$data->id' class='btn btn-primary detail'><i style='color:#fff'  class='ti-eye'></i></a>               
             
               ";
            })
            ->rawColumns(['tombol', 'detail'])
            ->make(true);
    }
}

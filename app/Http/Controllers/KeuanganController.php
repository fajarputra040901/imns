<?php

namespace App\Http\Controllers;

use App\Models\Pencairan;
use App\Models\Webinar;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KeuanganController extends Controller
{
    public function keuangan()
    {
        return view('portal.keuangan.index');
    }
    public function permintaan_pencairan()
    {
        return view('portal.keuangan.permintaan');
    }
    public function list_permintaan_pencairan()
    {
        $data = Pencairan::get();

        return datatables()->of($data)
            ->addColumn('tanggal_pengajuan', function ($row) {
                $tanggal = Carbon::parse($row->tanggal_pengajuan);
                return '
                <span>' . $tanggal->format('d-m-Y') . '</span><br>
                <span>' . $tanggal->format('H:i') . '</span>
            ';
            })
            ->addColumn('tanggal_validasi', function ($row) {
                if ($row->tanggal_validasi) {
                    $tanggal = Carbon::parse($row->tanggal_validasi);
                    return '
                    <span>' . $tanggal->format('d-m-Y') . '</span><br>
                    <span>' . $tanggal->format('H:i') . '</span>
                ';
                }
                return '<span>-</span>';
            })
            ->addColumn('jumlah', function ($row) {
                return "$row->jumlah";
            })
            ->addColumn('keterangan', function ($row) {
                return "$row->keterangan";
            })
            ->addColumn('jenis', function ($row) {
                return "$row->jenis";
            })
            ->addColumn('parameter', function ($row) {
                if ($row->jenis == "Webinar") {
                    $webinar = Webinar::where('id', $row->parameter)->first();
                    $judul = $webinar->judul;
                } else {
                    $judul = "Tidak Terdefinisi";
                }
                return "$judul";
            })
            ->addColumn('status', function ($row) {
                $status = '';
                $background = '';
                switch ($row->status) {
                    case 'N':
                        $status = 'Diajukan';
                        $background = 'bg-primary';
                        break;
                    case 'P':
                        $status = 'Diproses';
                        $background = 'bg-warning';
                        break;
                    case 'T':
                        $status = 'Ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'Y':
                        $status = 'Dikirim';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('aksi', function ($row) {
                $detailBtn = '<button class="btn btn-info btn-sm detail_pengajuan" title="Detail" data-id=' . $row->id . '>
                            <i class="bx bx-show"></i>
                          </button>';



                return $detailBtn;
            })
            ->rawColumns(['tanggal_pengajuan', 'tanggal_validasi', 'status', 'aksi'])
            ->make(true);
    }
    public function detail_pengajuan($id)
    {
        $data = Pencairan::where('id', $id)->first();
        $webinar = Webinar::where('id', $data->parameter)->first();
        $judul = $webinar->judul;
        return response()->json([
            'success' => true,
            'data' => $data,
            'parameter' => $judul,
        ]);
    }
    public function submit_pencairan(Request $r)
    {
        $data = Pencairan::where('id', $r->id_pencairan)->first();
        if (!$data) {
            return response()->json([
                'success' => false,
                'id' => $r->id_pencairan,
                'message' => 'Pencairan tidak ditemukan.',
            ], 404);
        }
        $data->status = $r->status;
        if ($r->status == "Y") {
            $data->tanggal_validasi = Carbon::now();
        }
        $data->keterangan_bendahara = $r->keterangan_bendahara;
        $data->bukti = $r->bukti;
        $data->save();
        return response()->json([
            'success' => true,

        ]);
    }
}

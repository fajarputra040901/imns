<?php

namespace App\Http\Controllers;

use App\Models\Webinar;
use App\Models\WebinarForm;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ExcelController extends Controller
{
    public function export_excel_peserta_webinar(Request $r)
    {
        $query = WebinarForm::query();

        if ($r->status != 'semua') {
            $query = $query->where('status', $r->status);
        }

        $data = $query->where('webinar_id', $r->id)->get();
        $webinar = Webinar::where('id', $r->id)->first();
        $formConfig = json_decode($webinar->form, true);
        $paket = json_decode($webinar->cost, true);

        $headers = ['ID Transaksi', 'Paket', 'Status'];
        foreach ($formConfig as $config) {
            $headers[] = $config['name'];
        }

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $column = 'A';
        foreach ($headers as $header) {
            $sheet->setCellValue($column . '1', ucfirst(str_replace('_', ' ', $header)));
            $sheet->getColumnDimension($column)->setAutoSize(true);  // Menyesuaikan lebar kolom dengan isi
            $column++;
        }

        $row = 2;
        foreach ($data as $item) {
            $column = 'A';

            $sheet->setCellValue($column . $row, $item->id_transaksi);
            $column++;

            $paketNama = '';
            foreach ($paket as $p) {
                if ($p['id'] === $item->paket) {
                    $paketNama = $p['namapaket'];
                    break;
                }
            }
            $sheet->setCellValue($column . $row, $paketNama);
            $column++;

            $sheet->setCellValue($column . $row, ucfirst($item->status));
            $column++;

            $formData = json_decode($item->form, true);
            foreach ($formConfig as $config) {
                $name = $config['name'];
                // Ambil nilai data dan ubah menjadi string
                $value = isset($formData[$name]) ? $formData[$name] : '';
                $sheet->setCellValueExplicit($column . $row, strval($value), \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                $column++;
            }


            $row++;
        }
        $judul = $webinar->judul;
        $fileName = 'Export_Peserta_Webinar_(' . $judul . ').xlsx';  // Menambahkan $judul ke nama file

        $writer = new Xlsx($spreadsheet);

        return response()->stream(
            function () use ($writer) {
                $writer->save('php://output');
            },
            200,
            [
                'Content-Type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                'Content-Disposition' => 'attachment;filename="' . $fileName . '"',  // Gunakan $fileName di sini
                'Cache-Control' => 'max-age=0',
                'Cache-Control' => 'max-age=1',
                'Pragma' => 'public',
            ]
        );
    }
}

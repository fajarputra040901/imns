<?php

namespace App\Http\Controllers;

use App\Models\Donasi;
use App\Models\Webinar;
use App\Models\WebinarForm;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function callback(Request $request)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        $server_key = config('midtrans.server_key');
        $hashed = hash("sha512", $request->order_id . $request->status_code . $request->gross_amount . $server_key);
        if ($hashed == $request->signature_key) {
            $data = WebinarForm::where('id_transaksi', $request->order_id)->first();

            if ($data) {
                // Jika data ditemukan di WebinarForm, panggil fungsi handleWebinarForm
                return $this->handleWebinarForm($data, $request);
            }

            // Jika tidak ditemukan di WebinarForm, cari di Donasi
            $donasi = Donasi::where('id_transaksi', $request->order_id)->first();

            if ($donasi) {
                // Jika data ditemukan di Donasi, panggil fungsi handleDonasi
                return $this->handleDonasi($donasi, $request);
            }

            // Jika data tidak ditemukan di keduanya
            return response()->json(['error' => 'Data not found'], 404);
        }

        return response()->json(['success' => true], 200);
    }
    public function sendWhatsAppMessage($number, $pesan, $appkey, $authkey)
    {
        if ($number) {
            $responses = [];
            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => [
                    'appkey' => $appkey,
                    'authkey' => $authkey,
                    'to' => $number,
                    'message' => $pesan,
                    'sandbox' => 'false',
                ],
            ]);

            $response = curl_exec($curl);
            curl_close($curl);
            $decodedResponse = json_decode($response, true);

            // Push to responses array
            if (isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success') {
                $responses[] = [
                    'number' => $number,
                    'status' => 'Success',
                    'details' => $decodedResponse['data'] ?? [],
                ];
            } else {
                $responses[] = [
                    'number' => $number,
                    'status' => 'Failed',
                    'details' => $decodedResponse['data'] ?? [],
                ];
            }

            return $responses;
        }

        return null;
    }
    private function handleWebinarForm($data, $request)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        $cek = Webinar::where('id', $data->webinar_id)->first();
        $isi_form = json_decode($data->form, true); // Form yang ada pada webinar
        $formConfig = json_decode($cek->form, true);

        // Ambil field untuk WhatsApp
        $notif_wa_fields = collect($formConfig)
            ->where('notif_wa', 'true')
            ->pluck('name')
            ->toArray();

        // Ambil nilai-nilai yang ada di form yang berhubungan dengan WA
        $wa_values = [];
        foreach ($notif_wa_fields as $field) {
            if (isset($isi_form[$field])) {
                $wa_values[$field] = $isi_form[$field];
            }
        }

        // Ambil nomor HP
        $number = isset($wa_values['nohp']) ? $wa_values['nohp'] : null;
        if ($number && strpos($number, '0') === 0) {
            $number = '62' . substr($number, 1);
        }

        // Tentukan pesan sesuai status transaksi
        $pesan = $this->generateWebinarMessage($request, $data, $number);

        // Kirim pesan WhatsApp
        $responses = $this->sendWhatsAppMessage($number, $pesan, $appkey, $authkey);

        // Update status dan simpan data
        $this->updateTransactionStatus($data, $request->transaction_status);

        return response()->json(['success' => true], 200);
    }
    private function handleDonasi($data, $request)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        // Ambil informasi donor
        $donor = json_decode($data->donor_data, true); // Asumsi data donor dalam format JSON
        $number = isset($data->nohp) ? $data->nohp : null;
        if ($number && strpos($number, '0') === 0) {
            $number = '62' . substr($number, 1);
        }

        // Tentukan pesan sesuai status transaksi
        $pesan = $this->generateDonasiMessage($request, $data, $number);

        // Kirim pesan WhatsApp
        $responses = $this->sendWhatsAppMessage($number, $pesan, $appkey, $authkey);

        // Update status dan simpan data
        $this->updateTransactionStatus($data, $request->transaction_status);

        return response()->json(['success' => true], 200);
    }
    private function generateWebinarMessage($request, $data)
    {
        if ($request->transaction_status == 'capture' || $request->transaction_status == 'settlement') {
            return "Pembayaran Anda dengan ID Transaksi *{$data->id_transaksi}* telah berhasil diproses. Terima kasih atas pembayaran Anda!\n\nBerikut adalah informasi Pembayaran Anda:\n\n*Jumlah Total:* Rp" . number_format($data->jumlah, 0, ',', '.') . "\n\nJika Anda membutuhkan informasi lebih lanjut atau memiliki pertanyaan, jangan ragu untuk menghubungi Contact Person kami yang tertera di web.\n\nUnduh Invoice dan informasi lain di link berikut: " . url('/w/t/' . $data->id_transaksi);
        } elseif ($request->transaction_status == 'deny' || $request->transaction_status == 'cancel' || $request->transaction_status == 'expire') {
            return "Pembayaran Anda dengan ID Transaksi *{$data->id_transaksi}* sudah kadaluarsa, harap melakukan pembayaran ulang untuk melanjutkan proses pendaftaran. Anda dapat mengunjungi tautan berikut: " . url('/w/t/' . $data->id_transaksi);
        } elseif ($request->transaction_status == 'pending') {
            return null;
        }
    }
    private function generateDonasiMessage($request, $data)
    {
        if ($request->transaction_status == 'capture' || $request->transaction_status == 'settlement') {
            return "Terima kasih banyak atas donasi Anda dengan ID Transaksi *{$data->id_transaksi}*! Donasi Anda telah berhasil diproses dan akan sangat berarti bagi kami.\n\nBerikut adalah rincian donasi Anda:\n\n*Jumlah Donasi:* Rp" . number_format($data->jumlah, 0, ',', '.') . "\n\nAnda dapat melihat informasi lebih lanjut atau mengunduh tanda terima donasi di tautan berikut: " . url('/d/' . $data->id_transaksi) . "\n\nKami sangat menghargai dukungan Anda. Jika ada pertanyaan, silakan hubungi kami melalui kontak yang tertera di web.";
        } elseif ($request->transaction_status == 'deny' || $request->transaction_status == 'cancel' || $request->transaction_status == 'expire') {
            return "Donasi Anda dengan ID Transaksi *{$data->id_transaksi}* tidak berhasil diproses. Jika Anda masih ingin berdonasi, silakan melakukan proses ulang pada tautan berikut: " . url('/d/' . $data->id_transaksi) . "\n\nTerima kasih atas niat baik Anda, kami sangat menghargainya.";
        } elseif ($request->transaction_status == 'pending') {
            return null;
        }
    }
    private function updateTransactionStatus($data, $status)
    {
        if ($status == 'capture' || $status == 'settlement') {
            $data->status = 'paid';
        } elseif ($status == 'pending') {
            $data->status = 'pending';
        } else {
            $data->status = 'expired';
        }
        $data->save();
    }
}

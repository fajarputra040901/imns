<?php

namespace App\Http\Controllers;

use App\Models\ItemRAB;
use App\Models\ItemRABSementara;
use App\Models\Logistik;
use App\Models\Pengadaan;
use App\Models\PengadaanSementara;
use App\Models\PengajuanPengadaan;
use App\Models\RABBaksos;
use App\Models\Pencairan;
use App\Models\ValidasiSementara;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class BaksosController extends Controller
{
    public function index()
    {
        return view('portal.baksos.index');
    }
    public function config_baksos($id)
    {
        // URL API
        $apiBaseUrl = config('services.api_ivcs.base_url'); // Mengambil dari .env
        $apiUrl = "$apiBaseUrl/api/detail_baksos/$id";

        try {
            // Permintaan ke API menggunakan Laravel HTTP Client
            $response = Http::get($apiUrl);

            // Periksa jika respons sukses
            if ($response->successful()) {
                $data = $response->json(); // Parse respons JSON
                if ($data['success']) {
                    $baksos = $data['data']; // Ambil data baksos dari respons


                    return view('portal.baksos.config', compact('baksos'));
                } else {
                    return back()->with('error', 'Data tidak ditemukan.');
                }
            } else {
                return back()->with('error', 'Gagal menghubungi API.');
            }
        } catch (\Exception $e) {
            // Tangani jika terjadi error
            return back()->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }
    public function pengadaan_barang($id)
    {
        $apiBaseUrl = config('services.api_ivcs.base_url'); // Mengambil dari .env
        $apiUrl = "$apiBaseUrl/api/detail_baksos/$id";
        try {
            // Permintaan ke API menggunakan Laravel HTTP Client
            $response = Http::get($apiUrl);

            // Periksa jika respons sukses
            if ($response->successful()) {
                $data = $response->json(); // Parse respons JSON
                if ($data['success']) {
                    $baksos = $data['data']; // Ambil data baksos dari respons


                    return view('portal.baksos.pengadaan', compact('baksos'));
                } else {
                    return back()->with('error', 'Data tidak ditemukan.');
                }
            } else {
                return back()->with('error', 'Gagal menghubungi API.');
            }
        } catch (\Exception $e) {
            // Tangani jika terjadi error
            return back()->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }
    public function table_barang_pengadaan()
    {
        $query = Logistik::query();

        // Filter based on form inputs


        $data = $query->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data) {
                return $data->stok;
            })
            ->addColumn('jenis', function ($data) {
                return $data->jenis;
            })
            ->addColumn('detail', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }

                $html = '<span>' . e($jenis) . '</span><br>';
                $html .= '<span>' . e($data->detail) . '</span><br>';
                $html .= '<span>' . e($data->keterangan) . '</span>';

                return $html;
            })




            ->addColumn('aksi', function ($data) {
                return "<div class='btn-group p-0' role='group' aria-label='First group'>
                            <button type='button' class='btn btn-sm btn-primary pilih_barang'
                                data-id='{$data->id}'
                                data-nama='{$data->nama}'
                                data-stok='{$data->stok}'
                                data-jenis='{$data->jenis}'
                                data-detail='{$data->detail}'
                                data-keterangan='{$data->keterangan}'
                                style='cursor: pointer;'
                                title='Pilih Barang'
                                >
                                <i class='bx bx-send'></i>
                            </button>
                        </div>";
            })




            ->rawColumns(['aksi', 'status', 'detail'])
            ->make(true);
    }
    public function table_barang_terpilih($id)
    {
        // Ambil data dari PengadaanSementara
        $pengadaan = PengadaanSementara::where('jenis', 'baksos')
            ->where('parameter', $id)
            ->get();

        // Ambil semua id_barang sebagai array
        $id_barang = $pengadaan->pluck('id_barang');

        // Query Logistik berdasarkan id_barang
        $query = Logistik::query();
        $data = $query->whereIn('id', $id_barang)->get();


        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data) {
                $st = PengadaanSementara::where('id_barang', $data->id)->first();
                return $st->stok;
            })
            ->addColumn('jenis', function ($data) {
                return $data->jenis;
            })
            ->addColumn('detail', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }

                $html = '<span>' . e($jenis) . '</span><br>';
                $html .= '<span>' . e($data->detail) . '</span><br>';
                $html .= '<span>' . e($data->keterangan) . '</span>';

                return $html;
            })




            ->addColumn('aksi', function ($data) {
                $st = PengadaanSementara::where('id_barang', $data->id)->first();
                return "<div class='btn-group p-0' role='group' aria-label='Action group'>
                            <!-- Tombol Ubah Stok -->
                            <button type='button' class='btn btn-sm btn-warning ubah_stok'
                                data-id='{$st->id}'
                                data-nama='{$data->nama}'
                                data-stok='{$data->stok}'
                                data-jenis='{$data->jenis}'
                                data-detail='{$data->detail}'
                                data-keterangan='{$data->keterangan}'
                                title='Ubah Stok Barang'
                                style='cursor: pointer;'>
                                <i class='bx bx-edit'></i>
                            </button>

                            <!-- Tombol Hapus -->
                            <button type='button' class='btn btn-sm btn-danger hapus_barang'
                                data-id='{$st->id}'
                                title='Hapus Barang'
                                style='cursor: pointer;'>
                                <i class='bx bx-trash'></i>
                            </button>
                        </div>";
            })





            ->rawColumns(['aksi', 'status', 'detail'])
            ->make(true);
    }
    public function tambah_pengadaan_sementara(Request $r)
    {
        $cek = PengadaanSementara::where('id_barang', $r->id_barang)->count();
        if ($cek > 0) {
            $barang = Logistik::where('id', $r->id_barang)->first();
            $tersimpan = PengadaanSementara::where('id_barang', $r->id_barang)->first();
            $renc = intval($r->jumlah_stok) + intval($tersimpan->stok);
            $sisa = intval($barang->stok) - $renc;

            if ($renc > intval($barang->stok)) {
                return response()->json(['success' => false, 'message' => 'Barang Tersebut Sudah Masuk Sebanyak ' . $tersimpan->stok . ' Dan Tersisa ' . (intval($barang->stok) - intval($tersimpan->stok))]);
            } else {
                $tersimpan->stok = $renc;
                $tersimpan->save();
                return response()->json(['success' => true, 'message' => 'Barang berhasil ditambah']);
            }
        } else {
            $data = new PengadaanSementara;
            $data->id_barang = $r->id_barang;
            $data->stok = $r->jumlah_stok;
            $data->jenis = "baksos";
            $data->parameter = $r->id_kegiatan;
            $data->save();
            return response()->json(['success' => true, 'message' => 'Barang berhasil ditambah']);
        }
    }
    public function ubah_pengadaan_sementara(Request $r)
    {
        $data = PengadaanSementara::where('id', $r->id_barang)->first();
        $data->stok = $r->jumlah_stok;
        $data->save();

        return response()->json(['success' => true, 'message' => 'Barang berhasil ditambah']);
    }
    public function scanner_pengadaan_barang_sementara(Request $r)
    {
        $barang = Logistik::where('code', $r->input_scanner)->first();

        if ($barang) {
            $cek = PengadaanSementara::where('id_barang', $barang->id)->count();
            if ($cek > 0) {

                $tersimpan = PengadaanSementara::where('id_barang', $barang->id)->first();
                $renc = intval($tersimpan->stok) + 1;
                $sisa = intval($barang->stok) - $renc;

                if ($renc > intval($barang->stok)) {
                    return response()->json(['success' => false, 'message' => 'Barang Tersebut Sudah Masuk Sebanyak ' . $tersimpan->stok . ' Dan Tersisa ' . (intval($barang->stok) - intval($tersimpan->stok))]);
                } else {
                    $tersimpan->stok = $renc;
                    $tersimpan->save();
                    return response()->json(['success' => true, 'message' => 'Barang berhasil ditambah']);
                }
            } else {
                $data = new PengadaanSementara;
                $data->id_barang = $barang->id;
                $data->stok = 1;
                $data->jenis = "baksos";
                $data->parameter = $r->parameter;
                $data->save();
                return response()->json(['success' => true, 'message' => 'Barang berhasil ditambah']);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Barang tidak ditemukan']);
        }
    }
    public function hapus_pengadaan_sementara(Request $r)
    {
        PengadaanSementara::where('id', $r->id)->delete();
        return response()->json(['success' => true, 'message' => 'Barang berhasil dihapus']);
    }
    public function simpan_pengadaan_baksos(Request $r)
    {
        $pengajuan = new PengajuanPengadaan;
        $pengajuan->jenis = 'baksos';
        $pengajuan->judul = 'Pengajuan Pengadaan Barang Tersedia u/ Baksos';
        $pengajuan->parameter = $r->id_kegiatan;
        $pengajuan->status = "diajukan";
        $pengajuan->indikator = "tersedia";
        $pengajuan->tanggal_pengajuan = Carbon::now();
        $pengajuan->save();
        $data = PengadaanSementara::where('jenis', 'baksos')->where('parameter', $r->id_kegiatan)->get();
        foreach ($data as $data) {
            $barang = Logistik::where('id', $data->id_barang)->first();
            $pengadaan = new Pengadaan;
            $pengadaan->jenis = "baksos";
            $pengadaan->parameter = $r->id_kegiatan;
            $pengadaan->id_barang = $data->id_barang;
            $pengadaan->stok = $data->stok;
            $pengadaan->code = $barang->code;
            $pengadaan->status = "diajukan";
            $pengadaan->id_pengajuan = $pengajuan->id;
            $pengadaan->save();


            $barang->stok = intval($barang->stok) - intval($pengadaan->stok);
            $barang->save();
            PengadaanSementara::where('id', $data->id)->delete();
        }
        return response()->json(['success' => true, 'message' => 'Pengadaan Barang Berhasil Diajukan']);
    }
    public function table_riwayat_pengadaan($id)
    {
        $data = PengajuanPengadaan::where('jenis', 'baksos')->where('parameter', $id)->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('tanggal_pengajuan', function ($data) {
                // Cek jika tanggal_pengajuan kosong atau null
                if (empty($data->tanggal_pengajuan)) {
                    return "-"; // Tampilkan tanda minus jika kosong atau null
                }
                // Format tanggal dan waktu
                $tanggal = \Carbon\Carbon::parse($data->tanggal_pengajuan)->format('d-m-Y');
                $waktu = \Carbon\Carbon::parse($data->tanggal_pengajuan)->format('H:i');
                return "<span>{$tanggal}</span><br><span>{$waktu}</span>";
            })
            ->addColumn('tanggal_validasi', function ($data) {
                // Cek jika tanggal_validasi kosong atau null
                if (empty($data->tanggal_validasi)) {
                    return "-"; // Tampilkan tanda minus jika kosong atau null
                }
                // Format tanggal dan waktu validasi
                $tanggal = \Carbon\Carbon::parse($data->tanggal_validasi)->format('d-m-Y');
                $waktu = \Carbon\Carbon::parse($data->tanggal_validasi)->format('H:i:s');
                return "<span>{$tanggal}</span><br><span>{$waktu}</span>";
            })

            ->addColumn('status', function ($data) {
                $proses = ValidasiSementara::where('id_pengajuan', $data->id)->count();
                if ($proses > 0) {
                    $indikator = 'diproses';
                } else {
                    $indikator = $data->status;
                }

                $status = '';
                $background = '';
                switch ($indikator) {
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('parameter', function ($data) {
                return $data->parameter;
            })
            ->addColumn('aksi', function ($data) {
                $proses = ValidasiSementara::where('id_pengajuan', $data->id)->count();
                if ($proses > 0) {
                    $status = 'diproses';
                } else {
                    $status = $data->status;
                }


                if ($status === 'diajukan') {
                    // Menampilkan kedua tombol jika status adalah "diajukan"
                    return "<div class='btn-group p-0' role='group' aria-label='Action group'>
                                <!-- Tombol Ubah Stok -->
                                <button type='button' class='btn btn-sm btn-success detail_pengajuan'
                                    data-id='{$data->id}'
                                    title='Detail'
                                    style='cursor: pointer;'>
                                    <i class='bx bx-edit'></i>
                                </button>

                                <!-- Tombol Hapus -->
                                <button type='button' class='btn btn-sm btn-danger batalkan_pengajuan'
                                    data-id='{$data->id}'
                                    title='Batalkan Pengajuan'
                                    style='cursor: pointer;'>
                                    <i class='bx bx-trash'></i>
                                </button>
                            </div>";
                } else {
                    // Menampilkan hanya tombol "Detail" jika status selain "diajukan"
                    return "<div class='btn-group p-0' role='group' aria-label='Action group'>
                                <!-- Tombol Ubah Stok -->
                                <button type='button' class='btn btn-sm btn-success detail_pengajuan'
                                    data-id='{$data->id}'
                                    title='Detail'
                                    style='cursor: pointer;'>
                                    <i class='bx bx-edit'></i>
                                </button>
                            </div>";
                }
            })

            ->rawColumns(['aksi', 'status', 'tanggal_pengajuan', 'tanggal_validasi'])
            ->make(true);
    }
    public function detail_pengajuan_pengadaan_baksos(Request $r)
    {
        $data = PengajuanPengadaan::where('id', $r->id)->first();
        return response()->json([
            'success' => true,
            'data' => $data,
            'message' => 'Pengadaan Barang Berhasil Diajukan'
        ]);
    }
    public function table_barang_diajukan($id)
    {
        // Ambil data dari PengadaanSementara
        $pengadaan = Pengadaan::where('jenis', 'baksos')
            ->where('id_pengajuan', $id)
            ->get();

        // Ambil semua id_barang sebagai array
        $id_barang = $pengadaan->pluck('id_barang');

        // Query Logistik berdasarkan id_barang
        $query = Logistik::query();
        $data = $query->whereIn('id', $id_barang)->get();


        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data)  use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();

                return $st->stok;
            })
            ->addColumn('jenis', function ($data) {
                return $data->jenis;
            })
            ->addColumn('status', function ($data)  use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();

                $status = '';
                $background = '';
                switch ($st->status) {
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('keterangan', function ($data)  use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();

                // Mengecek apakah keterangan null atau kosong
                return !empty($st->keterangan) ? $st->keterangan : '-';
            })

            ->addColumn('detail', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }

                $html = '<span>' . e($jenis) . '</span><br>';
                $html .= '<span>' . e($data->detail) . '</span><br>';
                $html .= '<span>' . e($data->keterangan) . '</span>';

                return $html;
            })




            ->addColumn('aksi', function ($data) use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();
                return "<div class='btn-group p-0' role='group' aria-label='Action group'>

                            <button type='button' class='btn btn-sm btn-danger hapus_barang_diajukan'
                                data-id='{$st->id}'
                                data-status='{$st->status}'
                                title='Hapus Barang'
                                style='cursor: pointer;'>
                                <i class='bx bx-trash'></i>
                            </button>
                        </div>";
            })





            ->rawColumns(['aksi', 'status', 'detail'])
            ->make(true);
    }
    public function hapus_barang_diajukan(Request $r)
    {
        $data = Pengadaan::where('id', $r->id)->first();
        $barang = Logistik::where('id', $data->id_barang)->first();
        $barang->stok = intval($barang->stok) + intval($data->stok);
        $barang->save();
        $data->delete();


        return response()->json([
            'success' => true,

            'message' => 'Barang Berhasil Dihapus'
        ]);
    }
    public function batalkan_pengajuan_barang_baksos(Request $r)
    {
        $data = PengajuanPengadaan::where('id', $r->id)->first();
        $masuk = Pengadaan::where('id_pengajuan', $r->id)->get();
        foreach ($masuk as $in) {
            $barang = Logistik::where('id', $in->id_barang)->first();
            $barang->stok = intval($barang->stok) + intval($in->stok);
            $barang->save();
            Pengadaan::where('id', $in->id)->delete();
        }

        $data->delete();


        return response()->json([
            'success' => true,

            'message' => 'Pengajuan Berhasil Dibatalkan'
        ]);
    }
    public function rab($id)
    {
        // URL API
        $apiBaseUrl = config('services.api_ivcs.base_url'); // Mengambil dari .env
        $apiUrl = "$apiBaseUrl/api/detail_baksos/$id";

        try {
            // Permintaan ke API menggunakan Laravel HTTP Client
            $response = Http::get($apiUrl);

            // Periksa jika respons sukses
            if ($response->successful()) {
                $data = $response->json(); // Parse respons JSON
                if ($data['success']) {
                    $baksos = $data['data']; // Ambil data baksos dari respons


                    return view('portal.baksos.rab', compact('baksos'));
                } else {
                    return back()->with('error', 'Data tidak ditemukan.');
                }
            } else {
                return back()->with('error', 'Gagal menghubungi API.');
            }
        } catch (\Exception $e) {
            // Tangani jika terjadi error
            return back()->with('error', 'Terjadi kesalahan: ' . $e->getMessage());
        }
    }
    public function buat_rab_baksos(Request $r)
    {
        $data = new RABBaksos;
        $data->id_baksos = $r->id_kegiatan;
        $data->status = 'draft';
        $data->judul = 'Rencana Anggaran Biaya Baksos';
        do {
            $randomNumber = str_pad(mt_rand(0, 99999), 5, '0', STR_PAD_LEFT);
            $rabId = 'RABB-' . $randomNumber;
        } while (RABBaksos::where('code', $rabId)->exists()); // Check if the code already exists

        $data->code = $rabId; // Assign the unique rab_id
        $data->tanggal_pembuatan =  Carbon::now();
        $data->save();
        return response()->json([
            'success' => true,

            'message' => 'RAB Berhasil Dibuat'
        ]);
    }
    public function table_rab_baksos()
    {
        $query = RABBaksos::query();
        $data = $query->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('pembuatan', function ($data) {
                // Cek jika tanggal_pengajuan kosong atau null
                if (empty($data->tanggal_pembuatan)) {
                    return "-"; // Tampilkan tanda minus jika kosong atau null
                }
                // Format tanggal dan waktu
                $tanggal = \Carbon\Carbon::parse($data->tanggal_pembuatan)->format('d-m-Y');
                $waktu = \Carbon\Carbon::parse($data->tanggal_pembuatan)->format('H:i');
                return "<span>{$tanggal}</span><br><span>{$waktu}</span>";
            })
            ->addColumn('judul', function ($data) {
                return $data->judul;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('status', function ($data) {

                $status = '';
                $background = '';
                switch ($data->status) {
                    case 'draft':
                        $status = 'draft';
                        $background = 'bg-secondary';
                        break;
                    case 'disimpan':
                        $status = 'disimpan';
                        $background = 'bg-secondary';
                        break;
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('biaya', function ($data) {
                $cek = ItemRAB::where('jenis', 'baksos')->where('parameter', $data->id)->count();

                if ($cek > 0) {
                    // Ambil biaya dari model ItemRAB
                    $totalBiaya = ItemRAB::where('jenis', 'baksos')->where('parameter', $data->id)->sum('biaya');

                    // Format biaya dalam Rupiah
                    return 'Rp ' . number_format($totalBiaya, 0, ',', '.');
                } else {
                    // Jika tidak ada data, tampilkan 0
                    return 'Rp 0';
                }
            })
            ->addColumn('aksi', function ($data) {
                return "<div class='btn-group p-0' role='group' aria-label='First group'>
                            <button type='button' class='btn btn-sm btn-primary detail'
                                data-id='{$data->id}'
                                style='cursor: pointer;'
                                title='Detail'
                                >
                                <i class='bx bx-send'></i>
                            </button>
                        </div>";
            })




            ->rawColumns(['aksi', 'status', 'pembuatan'])
            ->make(true);
    }
    public function table_item_rab_baksos_sementara($id)
    {
        $query = ItemRAB::query();
        $data = $query->where('jenis', 'baksos')->where('parameter', $id)->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })

            ->addColumn('anggaran', function ($data) {
                return $data->anggaran;
            })
            ->addColumn('status', function ($data) {

                $status = '';
                $background = '';
                switch ($data->status) {
                    case 'draft':
                        $status = 'draft';
                        $background = 'bg-secondary';
                        break;
                    case 'disimpan':
                        $status = 'disimpan';
                        $background = 'bg-secondary';
                        break;
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('biaya', function ($data) {
                return 'Rp ' . number_format($data->biaya, 0, ',', '.');
            })
            ->addColumn('aksi', function ($data) {
                return "<div class='btn-group p-0' role='group' aria-label='First group'>
                <!-- Tombol Ubah -->
                <button type='button' class='btn btn-sm btn-primary ubah'
                    data-id='{$data->id}'
                    style='cursor: pointer;'
                    title='Ubah'>
                    <i class='bx bx-edit-alt' ></i>
                </button>

                <!-- Tombol Hapus -->
                <button type='button' class='btn btn-sm btn-danger hapus'
                    data-id='{$data->id}'
                    data-status='{$data->status}'
                    style='cursor: pointer;'
                    title='Hapus'>
                    <i class='bx bx-trash'></i>
                </button>
            </div>";
            })




            ->rawColumns(['aksi', 'status', 'pembuatan'])
            ->make(true);
    }
    public function tambah_rencana_rab_baksos(Request $r)
    {
        $data = new ItemRAB;
        $data->jenis = 'baksos';
        $data->parameter = $r->id_rab;
        $data->anggaran = $r->anggaran;
        $data->biaya = $r->biaya;
        $data->status = "draft";
        $data->save();
        return response()->json([
            'success' => true,
            'message' => 'Item Berhasil Ditambahkan'
        ]);
    }
    public function resume_rencana_rab_baksos(Request $r)
    {
        $biaya_rencana = ItemRAB::where('jenis', 'baksos')->where('parameter', $r->id_baksos)->sum('biaya');
        $item_rencana =  ItemRAB::where('jenis', 'baksos')->where('parameter', $r->id_baksos)->count();

        $biaya = ItemRAB::where('jenis', 'baksos')->where('parameter', $r->id_baksos)->where('status', 'dikirim')->sum('biaya');
        $item =  ItemRAB::where('jenis', 'baksos')->where('parameter', $r->id_baksos)->where('status', 'dikirim')->count();
        return response()->json([
            'success' => true,

            'biaya' => $biaya,
            'item' => $item,
            'biaya_rencana' => $biaya_rencana,
            'item_rencana' => $item_rencana,
        ]);
    }
    public function detail_rab_baksos($id)
    {
        $data = ItemRAB::where('id', $id)->first();
        return response()->json([
            'success' => true,
            'data' => $data

        ]);
    }
    public function hapus_rencana_rab_baksos(Request $r)
    {
        ItemRAB::where('id', $r->id)->delete();
        return response()->json([
            'success' => true,


        ]);
    }
    public function fix_rab_baksos(Request $r)
    {
        if ($r->pengajuan == "Y") {
            $item = ItemRAB::where('jenis', 'baksos')->where('parameter', $r->id_rab)->get();
            foreach ($item as $tr) {
                $tr->status = 'diajukan';
                $tr->save();
            }
            $pencairan = new Pencairan;
            $pencairan->tanggal_pengajuan = Carbon::now();
            $pencairan->status = 'N';
            $pencairan->jenis = 'rab_baksos';
            $pencairan->parameter = $r->id_rab;
            $pencairan->jumlah = '';
            $pencairan->save();
            $judul = RABBaksos::where('id', $r->id_rab)->first();
            $judul->status = 'diajukan';
            $judul->save();
        } else {
            $item = ItemRAB::where('jenis', 'baksos')->where('parameter', $r->id_rab)->get();
            foreach ($item as $tr) {
                $tr->status = 'disimpan';
                $tr->save();
            }

            $judul = RABBaksos::where('id', $r->id_rab)->first();
            $judul->status = 'disimpan';
            $judul->save();
        }
        return response()->json([
            'success' => true,


        ]);
    }
}

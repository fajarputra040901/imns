<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Logistik;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Log;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Pengadaan;
use App\Models\PengadaanSementara;
use App\Models\PengajuanPengadaan;
use App\Models\ValidasiSementara;

class LogistikController extends Controller
{
    public function index()
    {
        return view('portal.logistik.index');
    }
    public function barang()
    {
        return view('portal.logistik.barang');
    }
    public function inventaris()
    {
        return view('portal.logistik.inventaris');
    }
    public function tambah_barang(Request $r)
    {
        $codeOtomatis = filter_var($r->code_otomatis, FILTER_VALIDATE_BOOLEAN);

        // Jika code_otomatis adalah false, lakukan pengecekan pada kode
        if (!$codeOtomatis) {
            $cek = Logistik::where('code', $r->code)->count();

            if ($cek > 0) {
                return response()->json([
                    'success' => false,
                    'message' => 'Kode Sudah Terpakai.'
                ]);
            } else {
                $kode = $r->code;
            }
        } else {
            $kode = $this->code_barang();
        }
        $data = new Logistik;
        $data->code = $kode;
        $data->nama = $r->nama;
        $data->stok = $r->stok;
        $data->detail = $r->detail;
        $data->keterangan = $r->keterangan;
        $data->jenis = 'barang';

        $data->parameter = $r->jenis;
        $data->jenis_lainnya = $r->jenis_lainnya;
        $data->save();
        return response()->json([
            'success' => true,
            'message' => 'Barang berhasil ditambahkan.'
        ]);
    }
    public function tambah_inventaris(Request $r)
    {
        $codeOtomatis = filter_var($r->code_otomatis, FILTER_VALIDATE_BOOLEAN);

        // Jika code_otomatis adalah false, lakukan pengecekan pada kode
        if (!$codeOtomatis) {
            $cek = Logistik::where('code', $r->code)->count();

            if ($cek > 0) {
                return response()->json([
                    'success' => false,
                    'message' => 'Kode Sudah Terpakai.'
                ]);
            } else {
                $kode = $r->code;
            }
        } else {
            $kode = $this->code_inventaris();
        }
        $data = new Logistik;
        $data->code = $kode;
        $data->nama = $r->nama;
        $data->stok = $r->stok;
        $data->detail = $r->detail;
        $data->keterangan = $r->keterangan;
        $data->jenis = 'inventaris';

        $data->parameter = $r->jenis;
        $data->jenis_lainnya = $r->jenis_lainnya;
        $data->save();
        return response()->json([
            'success' => true,
            'message' => 'Barang berhasil ditambahkan.'
        ]);
    }
    private function code_barang()
    {
        do {
            // Generate kode acak sebanyak 7 karakter (kombinasi angka dan huruf)
            $kodeRandom = strtoupper(bin2hex(random_bytes(4))); // menghasilkan 8 karakter
            $kodeRandom = substr($kodeRandom, 0, 7); // Membatasi panjang kode menjadi 7 karakter

            // Format kode menjadi B-xxxxxxx
            $kode = 'B-' . $kodeRandom;
        } while (Logistik::where('code', $kode)->exists()); // Cek apakah kode sudah ada

        return $kode;
    }
    private function code_inventaris()
    {
        do {
            // Generate kode acak sebanyak 7 karakter (kombinasi angka dan huruf)
            $kodeRandom = strtoupper(bin2hex(random_bytes(4))); // menghasilkan 8 karakter
            $kodeRandom = substr($kodeRandom, 0, 7); // Membatasi panjang kode menjadi 7 karakter

            // Format kode menjadi B-xxxxxxx
            $kode = 'I-' . $kodeRandom;
        } while (Logistik::where('code', $kode)->exists()); // Cek apakah kode sudah ada

        return $kode;
    }
    public function table_barang(Request $request)
    {
        $query = Logistik::query();

        // Filter based on form inputs
        if ($request->filled('tanggal_awal')) {
            $tanggal_awal = Carbon::parse($request->tanggal_awal)->startOfDay();
            $query->whereDate('tanggal', '>=', $tanggal_awal);
        }

        if ($request->filled('tanggal_akhir')) {
            $tanggal_akhir = Carbon::parse($request->tanggal_akhir)->endOfDay();
            $query->whereDate('tanggal', '<=', $tanggal_akhir);
        }

        if ($request->filled('nama')) {
            $query->where('nama_kegiatan', 'like', '%' . $request->nama . '%');
        }

        if ($request->filled('tempat')) {
            $query->where('tempat', 'like', '%' . $request->tempat . '%');
        }

        if ($request->filled('desa')) {
            $query->where('desa', 'like', '%' . $request->desa . '%');
        }

        if ($request->filled('kab')) {
            $query->where('kab', 'like', '%' . $request->kab . '%');
        }

        if ($request->filled('prov')) {
            $query->where('prov', 'like', '%' . $request->prov . '%');
        }

        $data = $query->where('jenis', 'barang')->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data) {
                return $data->stok;
            })
            ->addColumn('jenis', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }
                return $jenis;
            })
            ->addColumn('detail', function ($data) {
                return $data->detail;
            })
            ->addColumn('keterangan', function ($data) {
                return $data->keterangan;
            })



            ->addColumn('aksi', function ($data) {
                return "<div class='btn-group p-0' role='group' aria-label='First group'>
                            <a href='#' id='load' title='Detail'>
                                <button type='button' class='btn btn-primary detail' data-id='$data->id' style='cursor: pointer;'>
                                    <i class='bx bx-cog'></i>
                                </button>
                            </a>
                        </div>";
            })


            ->rawColumns(['aksi', 'status', 'region'])
            ->make(true);
    }
    public function table_inventaris(Request $request)
    {
        $query = Logistik::query();

        // Filter based on form inputs
        if ($request->filled('tanggal_awal')) {
            $tanggal_awal = Carbon::parse($request->tanggal_awal)->startOfDay();
            $query->whereDate('tanggal', '>=', $tanggal_awal);
        }

        if ($request->filled('tanggal_akhir')) {
            $tanggal_akhir = Carbon::parse($request->tanggal_akhir)->endOfDay();
            $query->whereDate('tanggal', '<=', $tanggal_akhir);
        }

        if ($request->filled('nama')) {
            $query->where('nama_kegiatan', 'like', '%' . $request->nama . '%');
        }

        if ($request->filled('tempat')) {
            $query->where('tempat', 'like', '%' . $request->tempat . '%');
        }

        if ($request->filled('desa')) {
            $query->where('desa', 'like', '%' . $request->desa . '%');
        }

        if ($request->filled('kab')) {
            $query->where('kab', 'like', '%' . $request->kab . '%');
        }

        if ($request->filled('prov')) {
            $query->where('prov', 'like', '%' . $request->prov . '%');
        }

        $data = $query->where('jenis', 'inventaris')->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data) {
                return $data->stok;
            })
            ->addColumn('jenis', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }
                return $jenis;
            })
            ->addColumn('detail', function ($data) {
                return $data->detail;
            })
            ->addColumn('keterangan', function ($data) {
                return $data->keterangan;
            })



            ->addColumn('aksi', function ($data) {
                return "<div class='btn-group p-0' role='group' aria-label='First group'>
                            <a href='#' id='load' title='Detail'>
                                <button type='button' class='btn btn-primary detail' data-id='$data->id' style='cursor: pointer;'>
                                    <i class='bx bx-cog'></i>
                                </button>
                            </a>
                        </div>";
            })


            ->rawColumns(['aksi', 'status', 'region'])
            ->make(true);
    }
    public function detail_barang($id)
    {
        $data = Logistik::where('id', $id)->first();
        return response()->json([
            'success' => true,
            'data' => $data
        ]);
    }
    public function hapus_barang(Request $r)
    {
        $data = Logistik::where('id', $r->id_barang)->delete();
        return response()->json([
            'success' => true,

        ]);
    }
    public function sertifikat()
    {
        return view('portal.sertifikat');
    }
    public function import_barang(Request $r)
    {
        $file = $r->file('file_import');
        if (!$file) {
            return response()->json(['success' => false, 'message' => 'File tidak ditemukan']);
        }

        try {
            // Load file Excel
            $spreadsheet = IOFactory::load($file->getPathname());
            $sheet = $spreadsheet->getActiveSheet();
            $rows = $sheet->toArray();

            // Loop melalui data (dimulai dari baris kedua)
            for ($i = 1; $i < count($rows); $i++) {
                $row = $rows[$i];

                // Ambil data dari setiap kolom
                $code = trim($row[0]); // Kolom A
                $nama = trim($row[1]); // Kolom B
                $parameter = trim($row[2]); // Kolom C
                $jenis_lainnya = trim($row[3]); // Kolom D
                $detail = trim($row[4]); // Kolom E
                $keterangan = trim($row[5]); // Kolom F
                $stok = trim($row[6]);


                // Validasi wajib untuk kolom nama dan stok
                if (empty($nama) || empty($stok)) {
                    return response()->json([
                        'success' => false,
                        'message' => "Baris ke-" . ($i + 1) . " memiliki data yang tidak lengkap.",
                    ]);
                }

                // Periksa apakah kolom kode kosong
                if (empty($code)) {
                    $code = $this->code_barang(); // Generate kode baru
                } else {
                    // Periksa apakah kode sudah ada
                    if (Logistik::where('code', $code)->exists()) {
                        return response()->json([
                            'success' => false,
                            'message' => "Kode pada baris ke-" . ($i + 1) . " sudah digunakan.",
                        ]);
                    }
                }

                // Simpan data ke model Logistik
                $logistik = new Logistik();
                $logistik->code = $code;
                $logistik->nama = $nama;
                $logistik->parameter = $parameter;
                $logistik->jenis_lainnya = $jenis_lainnya;
                $logistik->detail = $detail;
                $logistik->jenis = "barang";
                $logistik->keterangan = $keterangan;
                $logistik->stok = (int)$stok;
                $logistik->save();
            }

            return response()->json(['success' => true, 'message' => 'Data berhasil diimpor']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
    public function import_inventaris(Request $r)
    {
        $file = $r->file('file_import');
        if (!$file) {
            return response()->json(['success' => false, 'message' => 'File tidak ditemukan']);
        }

        try {
            // Load file Excel
            $spreadsheet = IOFactory::load($file->getPathname());
            $sheet = $spreadsheet->getActiveSheet();
            $rows = $sheet->toArray();

            // Loop melalui data (dimulai dari baris kedua)
            for ($i = 1; $i < count($rows); $i++) {
                $row = $rows[$i];

                // Ambil data dari setiap kolom
                $code = trim($row[0]); // Kolom A
                $nama = trim($row[1]); // Kolom B
                $parameter = trim($row[2]); // Kolom C

                $detail = trim($row[3]); // Kolom E
                $keterangan = trim($row[4]); // Kolom F
                $stok = trim($row[5]);


                // Validasi wajib untuk kolom nama dan stok
                if (empty($nama) || empty($stok)) {
                    return response()->json([
                        'success' => false,
                        'message' => "Baris ke-" . ($i + 1) . " memiliki data yang tidak lengkap.",
                    ]);
                }

                // Periksa apakah kolom kode kosong
                if (empty($code)) {
                    $code = $this->code_inventaris(); // Generate kode baru
                } else {
                    // Periksa apakah kode sudah ada
                    if (Logistik::where('code', $code)->exists()) {
                        return response()->json([
                            'success' => false,
                            'message' => "Kode pada baris ke-" . ($i + 1) . " sudah digunakan.",
                        ]);
                    }
                }

                // Simpan data ke model Logistik
                $logistik = new Logistik();
                $logistik->code = $code;
                $logistik->nama = $nama;
                $logistik->parameter = $parameter;

                $logistik->detail = $detail;
                $logistik->jenis = "inventaris";
                $logistik->keterangan = $keterangan;
                $logistik->stok = (int)$stok;
                $logistik->save();
            }

            return response()->json(['success' => true, 'message' => 'Data berhasil diimpor']);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()]);
        }
    }
    public function permintaan_pengadaan()
    {
        return view('portal.logistik.permintaan_pengadaan');
    }
    public function table_permintaan_barang()
    {
        $data = PengajuanPengadaan::get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('tanggal_pengajuan', function ($data) {
                // Cek jika tanggal_pengajuan kosong atau null
                if (empty($data->tanggal_pengajuan)) {
                    return "-"; // Tampilkan tanda minus jika kosong atau null
                }
                // Format tanggal dan waktu
                $tanggal = \Carbon\Carbon::parse($data->tanggal_pengajuan)->format('d-m-Y');
                $waktu = \Carbon\Carbon::parse($data->tanggal_pengajuan)->format('H:i');
                return "<span>{$tanggal}</span><br><span>{$waktu}</span>";
            })
            ->addColumn('tanggal_validasi', function ($data) {
                // Cek jika tanggal_validasi kosong atau null
                if (empty($data->tanggal_validasi)) {
                    return "-"; // Tampilkan tanda minus jika kosong atau null
                }
                // Format tanggal dan waktu validasi
                $tanggal = \Carbon\Carbon::parse($data->tanggal_validasi)->format('d-m-Y');
                $waktu = \Carbon\Carbon::parse($data->tanggal_validasi)->format('H:i:s');
                return "<span>{$tanggal}</span><br><span>{$waktu}</span>";
            })

            ->addColumn('status', function ($data) {
                $proses = ValidasiSementara::where('id_pengajuan', $data->id)->count();
                if ($proses > 0) {
                    $indikator = 'diproses';
                } else {
                    $indikator = $data->status;
                }

                $status = '';
                $background = '';
                switch ($indikator) {
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('detail', function ($data) {
                return $data->detail;
            })
            ->addColumn('aksi', function ($data) {

                return "<div class='btn-group p-0' role='group' aria-label='Action group'>
                            <!-- Tombol Ubah Stok -->
                            <button type='button' class='btn btn-sm btn-primary detail_pengajuan'
                                data-id='{$data->id}'
                                data-indikator='{$data->indikator}'
                                data-judul='{$data->judul}'
                                data-tanggal_pengajuan='{$data->tanggal_pengajuan}'
                                data-tanggal_validasi='{$data->tanggal_validasi}'
                                data-parameter='{$data->parameter}'
                                data-jenis='{$data->jenis}'
                                data-status='{$data->status}'
                                data-keterangan='{$data->keterangan}'
                                title='Detail'
                                style='cursor: pointer;'>
                                <i class='bx bx-edit'></i>
                            </button>

                            <!-- Tombol Hapus -->

                        </div>";
            })
            ->rawColumns(['aksi', 'status', 'tanggal_pengajuan', 'tanggal_validasi'])
            ->make(true);
    }
    public function table_permintaan($id)
    {
        // Ambil data dari PengadaanSementara
        $pengadaan = Pengadaan::where('jenis', 'baksos')
            ->where('id_pengajuan', $id)
            ->where('status', 'diajukan')
            ->get();

        // Ambil semua id_barang sebagai array
        $id_barang = $pengadaan->pluck('id_barang');

        // Query Logistik berdasarkan id_barang
        $query = Logistik::query();
        $data = $query->whereIn('id', $id_barang)->get();


        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data) use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();

                return $st->stok;
            })
            ->addColumn('jenis', function ($data) {
                return $data->jenis;
            })
            ->addColumn('status', function ($data) use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();

                $status = '';
                $background = '';
                switch ($st->status) {
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('keterangan', function ($data) use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();

                // Mengecek apakah keterangan null atau kosong
                return !empty($st->keterangan) ? $st->keterangan : '-';
            })

            ->addColumn('detail', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }

                $html = '<span>' . e($jenis) . '</span><br>';
                $html .= '<span>' . e($data->detail) . '</span><br>';
                $html .= '<span>' . e($data->keterangan) . '</span>';

                return $html;
            })




            ->addColumn('aksi', function ($data) use ($id) {
                $st = Pengadaan::where('id_barang', $data->id)->where('id_pengajuan', $id)->first();
                return "<div class='btn-group p-0' role='group' aria-label='Action group'>

                            <button type='button' class='btn btn-sm btn-danger validasi_permintaan_barang'
                                data-id='{$st->id}'
                                data-status='{$st->status}'
                                data-keterangan='{$st->keterangan}'
                                data-indikator='validasi'
                                title='Validasi Barang'
                                style='cursor: pointer;'>
                                <i class='bx bx-send'></i>
                            </button>
                        </div>";
            })





            ->rawColumns(['aksi', 'status', 'detail'])
            ->make(true);
    }
    public function table_validasi($id)
    {
        // Ambil data dari PengadaanSementara
        $pengadaan = ValidasiSementara::where('id_pengajuan', $id)

            ->get();

        // Ambil semua id_barang sebagai array
        $id_barang = $pengadaan->pluck('id_barang');

        // Query Logistik berdasarkan id_barang
        $query = Logistik::query();
        $data = $query->whereIn('id', $id_barang)->get();


        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('no', function ($data) {
                static $no = 0;
                return ++$no;
            })
            ->addColumn('code', function ($data) {
                return $data->code;
            })
            ->addColumn('nama', function ($data) {
                return $data->nama;
            })
            ->addColumn('stok', function ($data) {
                $st = Pengadaan::where('id_barang', $data->id)->first();

                return $st->stok;
            })
            ->addColumn('jenis', function ($data) {
                return $data->jenis;
            })
            ->addColumn('status', function ($data) {
                $st = ValidasiSementara::where('id_barang', $data->id)->first();

                $status = '';
                $background = '';
                switch ($st->status) {
                    case 'diajukan':
                        $status = 'diajukan';
                        $background = 'bg-warning';
                        break;
                    case 'diproses':
                        $status = 'diproses';
                        $background = 'bg-primary';
                        break;
                    case 'ditolak':
                        $status = 'ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'divalidasi':
                        $status = 'divalidasi';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('keterangan', function ($data) {
                $st = ValidasiSementara::where('id_barang', $data->id)->first();

                // Mengecek apakah keterangan null atau kosong
                return !empty($st->keterangan) ? $st->keterangan : '-';
            })

            ->addColumn('detail', function ($data) {
                if ($data->parameter == "lainnya") {
                    $jenis = $data->jenis_lainnya;
                } else {
                    $jenis = $data->parameter;
                }

                $html = '<span>' . e($jenis) . '</span><br>';
                $html .= '<span>' . e($data->detail) . '</span><br>';
                $html .= '<span>' . e($data->keterangan) . '</span>';

                return $html;
            })




            ->addColumn('aksi', function ($data) {
                $st = ValidasiSementara::where('id_barang', $data->id)->first();
                return "<div class='btn-group p-0' role='group' aria-label='Action group'>

                            <button type='button' class='btn btn-sm btn-danger validasi_permintaan_barang'
                                data-id='{$st->id}'
                                data-status='{$st->status}'
                                data-keterangan='{$st->keterangan}'
                                title='Ubah Validasi Barang'
                                style='cursor: pointer;'>
                               <i class='bx bx-reset'></i>
                            </button>
                        </div>";
            })





            ->rawColumns(['aksi', 'status', 'detail'])
            ->make(true);
    }
    public function validasi_barang(Request $r)
    {
        $data = Pengadaan::where('id', $r->id)->first();
        $cek = ValidasiSementara::where('id_pengadaan', $data->id)->where('id_barang', $data->id_barang)->count();
        if ($cek > 0) {
            $temp = ValidasiSementara::where('id_pengadaan', $data->id)->where('id_barang', $data->id_barang)->first();
        } else {
            $temp = new ValidasiSementara;
        }

        $temp->id_barang = $data->id_barang;
        $temp->id_pengadaan = $data->id;
        $temp->id_pengajuan = $data->id_pengajuan;
        $temp->status = $r->status;
        $temp->keterangan = $r->keterangan;
        if ($r->status != "diajukan") {
            $temp->save();
            $data->status = 'diproses';
            $data->save();
        }

        return response()->json(['success' => true, 'message' => 'Berhasil']);
    }
    public function ubah_validasi_barang(Request $r)
    {


        $temp = ValidasiSementara::where('id', $r->id)->first();
        $data = Pengadaan::where('id', $temp->id_pengadaan)->first();
        if ($r->status == "diajukan") {
            $data->status = 'diajukan';
            $data->save();
            $temp->delete();
        } else {
            $temp->status = $r->status;
            $temp->keterangan = $r->keterangan;
            $temp->save();
        }


        return response()->json(['success' => true, 'message' => 'Berhasil']);
    }
    public function validator_permintaan(Request $r)
    {

        if ($r->indikator == "validasi") {

            $pengadaan = Pengadaan::where('id_pengajuan', $r->id)->get();
            $cek = ValidasiSementara::where('id_pengajuan', $r->id)->count();
            if ($cek > 0) {
                foreach ($pengadaan as $dua) {
                    $dua->status = 'diproses';
                    $dua->save();
                    $temp = ValidasiSementara::where('id_pengadaan', $dua->id)->get();
                    foreach ($temp as $dump) {
                        $dump->status = 'divalidasi';
                        $dump->keterangan = $r->keterangan;
                        $dump->save();
                    }
                }
            } else {
                foreach ($pengadaan as $satu) {
                    $satu->status = 'diproses';
                    $satu->save();
                    $temp = new ValidasiSementara;
                    $temp->id_barang = $satu->id_barang;
                    $temp->id_pengadaan = $satu->id;
                    $temp->id_pengajuan = $satu->id_pengajuan;
                    $temp->status = 'divalidasi';
                    $temp->keterangan = $r->keterangan;
                    $temp->save();
                }
            }

            return response()->json(['success' => true, 'message' => 'Berhasil']);
        }
        if ($r->indikator == "tolak") {

            $pengadaan = Pengadaan::where('id_pengajuan', $r->id)->get();
            $cek = ValidasiSementara::where('id_pengajuan', $r->id)->count();
            if ($cek > 0) {
                foreach ($pengadaan as $dua) {
                    $dua->status = 'diproses';
                    $dua->save();
                    $temp = ValidasiSementara::where('id_pengadaan', $dua->id)->get();
                    foreach ($temp as $dump) {
                        $dump->status = 'ditolak';
                        $dump->keterangan = $r->keterangan;
                        $dump->save();
                    }
                }
            } else {
                foreach ($pengadaan as $dua) {
                    $dua->status = 'diproses';
                    $dua->save();
                    $int = new ValidasiSementara;
                    $int->id_barang = $dua->id_barang;
                    $int->id_pengadaan = $dua->id;
                    $int->id_pengajuan = $dua->id_pengajuan;
                    $int->status = 'ditolak';
                    $int->keterangan = $r->keterangan;
                    $int->save();
                }
            }

            return response()->json(['success' => true, 'message' => 'Berhasil']);
        }
        if ($r->indikator == "reset") {

            $pengadaan = Pengadaan::where('id_pengajuan', $r->id)->get();
            foreach ($pengadaan as $tiga) {
                $tiga->status = 'diajukan';
                $tiga->save();
                $temp = ValidasiSementara::where('id_pengadaan', $tiga->id)->delete();
            }
            return response()->json(['success' => true, 'message' => 'Berhasil']);
        }
    }
    public function fix_simpan_validasi(Request $r)
    {
        $temp = ValidasiSementara::where('id_pengajuan', $r->id)->get();
        foreach ($temp as $data) {
            if ($data->status == 'ditolak') {
                $stokis = Pengadaan::where('id', $data->id_pengadaan)->first();
                $barang = Logistik::where('id', $data->id_barang)->first();

                $barang->stok = intval($barang->stok) + intval($stokis->stok);
                $barang->save();
            }
            $pengadaan = Pengadaan::where('id_barang', $data->id_barang)->where('id_pengajuan', $data->id_pengajuan)->first();

            $pengadaan->status = $data->status;
            $pengadaan->keterangan = $data->keterangan;
            $pengadaan->save();
            ValidasiSementara::where('id', $data->id)->delete();
        }
        $pengajuan = PengajuanPengadaan::where('id', $r->id)->first();

        $pengajuan->tanggal_validasi = Carbon::now();
        $pengajuan->keterangan = $r->keterangan;
        $pengajuan->status = $r->status;
        $pengajuan->save();
        return response()->json(['success' => true, 'message' => 'Berhasil Divalidasi']);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Donasi;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Midtrans\Config;
use Midtrans\Snap;
use Midtrans\Notification;

class DonasiController extends Controller
{
    public function __construct()
    {
        // Set konfigurasi Midtrans
        Config::$serverKey = config('midtrans.server_key');
        Config::$clientKey = config('midtrans.client_key');
        Config::$isProduction = config('midtrans.is_production');
        Config::$isSanitized = true;
        Config::$is3ds = true;
    }
    public function index()
    {
        return view('donasi.index');
    }
    public function submit_donasi(Request $request)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        $request->validate([
            'nama' => 'required|string|max:255',
            'nohp' => 'required|string|max:15',
            'email' => 'required|email|max:255',
            'donasi' => 'required|numeric',  // Memastikan donasi adalah angka
        ]);

        // Membuat ID Transaksi dengan format D/ diikuti 7 angka acak
        $id_transaksi = $this->unik_donasi();

        // Menyimpan data donasi
        $donasi = new Donasi();

        // Set data donasi
        $donasi->nama = $request->input('nama');
        $donasi->nohp = $request->input('nohp');
        $donasi->email = $request->input('email');

        // Pastikan donasi hanya angka, hapus simbol non-digit jika ada
        $donasi_amount = preg_replace('/\D/', '', $request->input('donasi'));
        $donasi->donasi = (int) $donasi_amount;

        // Membuat transaksi ID dan jenis
        $donasi->id_transaksi = $id_transaksi;
        $donasi->jenis = 'donasi';
        $donasi->status = 'unpaid';  // Status donasi

        // Perhitungan harga (termasuk biaya layanan)
        $harga = (int) $donasi_amount + 2000; // Menambahkan biaya layanan 2000
        $donasi->jumlah = $harga;
        $donasi->keterangan = $request->keterangan;

        // Menyiapkan parameter untuk Midtrans
        $params = [
            'transaction_details' => [
                'order_id' => $id_transaksi,
                'gross_amount' => $harga,  // Total harga
            ],
            'customer_details' => [
                'first_name' => $request->nama,
                'email' => $request->input('email'),
                'phone' => $request->input('nohp'),
            ],
            'item_details' => [
                [
                    'id' => 'donasi',
                    'price' => $donasi_amount,  // Harga donasi yang benar
                    'quantity' => 1,
                    'name' => 'Donasi',
                ],
                [
                    'id' => 'admin',
                    'price' => 2000,
                    'quantity' => 1,
                    'name' => 'Biaya Layanan',
                ],
            ],
        ];

        // Memanggil Midtrans API untuk mendapatkan Snap Token
        try {
            $snapToken = Snap::getSnapToken($params);
            $donasi->snaptoken = $snapToken;  // Menyimpan Snap Token di database
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Gagal membuat Snap Token.', 'error' => $e->getMessage()], 500);
        }

        // Menyimpan data donasi ke database
        $donasi->save();
        $number = $request->input('nohp');

        // Cek apakah nomor dimulai dengan 0
        if (strpos($number, '0') === 0) {
            $number = '62' . substr($number, 1); // Ganti 0 dengan 62
        }

        $pesan = "Halo, {$request->nama}!\n\nKami melihat bahwa proses donasi Anda dengan ID Transaksi *{$donasi->id_transaksi}* belum selesai. Mohon segera selesaikan pembayaran melalui metode yang Anda pilih agar donasi Anda dapat kami proses.\n\nBerikut adalah detail donasi Anda:\n\n*Jumlah Donasi:* Rp" . number_format($donasi->jumlah, 0, ',', '.') . "\n\nKlik tautan berikut untuk melanjutkan pembayaran:\nhttps://indonesiamelihat.org/d/{$donasi->id_transaksi}\n\nJika ada pertanyaan, jangan ragu untuk menghubungi kami melalui kontak yang tersedia di website. Terima kasih banyak atas niat baik dan dukungan Anda!";


        $responses = [];
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'appkey' => $appkey,
                'authkey' => $authkey,
                'to' => $number,
                'message' => $pesan,
                'sandbox' => 'false',
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        $decodedResponse = json_decode($response, true);
        if (isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success') {
            $responses[] = [
                'number' => $number,
                'status' => 'Success',
                'details' => $decodedResponse['data'] ?? [],
            ];
        } else {
            $responses[] = [
                'number' => $number,
                'status' => 'Failed',
                'details' => $decodedResponse['data'] ?? [],
            ];
        }

        return response()->json([
            'success' => true,
            'message' => 'Formulir berhasil disubmit.',

            'snaptoken' => $snapToken,
            'result' => $responses,
            'data' => $donasi
        ]);
        // Response sukses

    }

    private function unik_donasi()
    {
        $id_transaksi = 'D-' . strtoupper(Str::random(7));

        // Cek apakah id_transaksi sudah ada di database
        while (Donasi::where('id_transaksi', $id_transaksi)->exists()) {
            // Jika sudah ada, generate ID baru
            $id_transaksi = 'D-' . strtoupper(Str::random(7));
        }

        return $id_transaksi;
    }
    public function user_donasi($id)
    {
        $data = Donasi::where('id_transaksi', $id)->first();
        return view('donasi.payment', compact('data'));
    }
    public function get_tr_donasi($id)
    {
        $data = Donasi::where('id_transaksi', $id)->first();
        return response()->json([
            'success' => true,
            'data' => $data

        ], 200);
    }
    // Laravel Controller
    public function getCountryCodes()
    {
        try {
            $response = Http::timeout(60) // Timeout diperpanjang
                ->get('https://restcountries.com/v3.1/all?fields=name,idd');

            if ($response->failed()) {
                return response()->json(['error' => 'Unable to fetch country codes'], 500);
            }

            $countries = collect($response->json())->map(function ($country) {
                return [
                    'name' => $country['name']['common'],
                    'code' => $country['idd']['root'] . ($country['idd']['suffixes'][0] ?? '')
                ];
            });

            return response()->json($countries->all());
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}

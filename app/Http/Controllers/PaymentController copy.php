<?php

namespace App\Http\Controllers;

use App\Models\Webinar;
use App\Models\WebinarForm;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    public function callback(Request $request)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        $server_key = config('midtrans.server_key');
        $hashed = hash("sha512", $request->order_id . $request->status_code . $request->gross_amount . $server_key);
        if ($hashed == $request->signature_key) {
            $data = WebinarForm::where('id_transaksi', $request->order_id)->first();
            $cek = Webinar::where('id', $data->webinar_id)->first();
            $isi_form = json_decode($data->form, true); // Form yang ada pada webinar
            $formConfig = json_decode($cek->form, true);

            // Ambil field untuk WhatsApp
            $notif_wa_fields = collect($formConfig)
                ->where('notif_wa', 'true')
                ->pluck('name')
                ->toArray(); // Ambil field yang berhubungan dengan WA

            // Ambil nilai-nilai yang ada di form yang berhubungan dengan WA
            $wa_values = [];
            foreach ($notif_wa_fields as $field) {
                if (isset($isi_form[$field])) {
                    $wa_values[$field] = $isi_form[$field]; // Ambil nilai untuk WhatsApp
                }
            }

            // Ambil nomor HP
            $number = isset($wa_values['nohp']) ? $wa_values['nohp'] : null;

            // Cek apakah nomor dimulai dengan 0
            if ($number && strpos($number, '0') === 0) {
                $number = '62' . substr($number, 1); // Ganti 0 dengan 62
            }

            // Pesan default


            // Tentukan pesan sesuai dengan status transaksi
            if ($request->transaction_status == 'capture' || $request->transaction_status == 'settlement') {
                // Jika status transaksi capture atau settlement
                $pesan = "Hai!\n\nPesanan Anda dengan ID Transaksi *{$data->id_transaksi}* telah berhasil diproses. Terima kasih atas pembayaran Anda!\n\nBerikut adalah informasi pesanan Anda:\n\n*Jumlah Total:* Rp" . number_format($data->jumlah, 0, ',', '.') . "\n\nJika Anda membutuhkan informasi lebih lanjut atau memiliki pertanyaan, jangan ragu untuk menghubungi Contact Person kami yang tertera di web.";
            } elseif ($request->transaction_status == 'deny' || $request->transaction_status == 'cancel' || $request->transaction_status == 'expire') {
                // Jika status transaksi deny, cancel, atau expire
                $pesan = "Pesanan Anda dengan ID Transaksi *{$data->id_transaksi}* telah dibatalkan.\n\nJika Anda membutuhkan informasi lebih lanjut, hubungi kami.";
            } elseif ($request->transaction_status == 'pending') {
                // Jika status transaksi pending
                $pesan = "Pesanan Anda dengan ID Transaksi *{$data->id_transaksi}* masih dalam status pending.\n\nKami akan memberi informasi lebih lanjut secepatnya.";
            }

            // Kirim pesan WhatsApp
            $responses = $this->sendWhatsAppMessage($number, $pesan, $appkey, $authkey);

            // Update status dan simpan data
            if ($request->transaction_status == 'capture' || $request->transaction_status == 'settlement') {
                $data->status = 'paid';
            } else {
                $data->status = 'expired';
            }
            $data->save();
        }

        return response()->json(['success' => true], 200);
    }
    public function sendWhatsAppMessage($number, $pesan, $appkey, $authkey)
    {
        if ($number) {
            $responses = [];
            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => [
                    'appkey' => $appkey,
                    'authkey' => $authkey,
                    'to' => $number,
                    'message' => $pesan,
                    'sandbox' => 'false',
                ],
            ]);

            $response = curl_exec($curl);
            curl_close($curl);
            $decodedResponse = json_decode($response, true);

            // Push to responses array
            if (isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success') {
                $responses[] = [
                    'number' => $number,
                    'status' => 'Success',
                    'details' => $decodedResponse['data'] ?? [],
                ];
            } else {
                $responses[] = [
                    'number' => $number,
                    'status' => 'Failed',
                    'details' => $decodedResponse['data'] ?? [],
                ];
            }

            return $responses;
        }

        return null;
    }
}

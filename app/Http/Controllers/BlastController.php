<?php

namespace App\Http\Controllers;

use App\Mail\SendWebinarEmail;
use App\Mail\SendWebinarEmailWithFile;
use App\Models\Email;
use App\Models\Webinar;
use App\Models\WebinarForm;
use Illuminate\Http\Request;
use Symfony\Component\Mime\Part\TextPart;
use Symfony\Component\Mime\Part\HtmlPart;
use Illuminate\Support\Facades\Mail;

class BlastController extends Controller
{
    public function pesan_wa_webinar(Request $r)
    {
        $webinar = Webinar::where('id', $r->id)->first();
        $formConfig = json_decode($webinar->form, true);
        $notif_wa = collect($formConfig)
            ->where('notif_wa', 'true')
            ->pluck('name');

        $data = WebinarForm::where('webinar_id', $r->id)
            ->where('status', $r->status)
            ->get();

        $result = $data->map(function ($item) use ($notif_wa) {
            $formData = json_decode($item->form, true);

            // Filter hanya field yang ada di $notif_wa
            $filteredData = collect($formData)
                ->only($notif_wa)
                ->values() // Ambil hanya nilai (tanpa key)
                ->map(function ($value) {
                    // Ubah awalan 0 menjadi 62 jika nilai adalah string dimulai dengan 0
                    if (is_string($value) && preg_match('/^0/', $value)) {
                        return preg_replace('/^0/', '62', $value);
                    }
                    return $value;
                })
                ->toArray();

            return $filteredData;
        });

        $responses = [];

        // Jika jenis pesan adalah Text, kirim pesan
        if ($r->jenis_pesan == 'Teks') {
            $message = $r->isi_pesan;

            // Mengganti <b>, <i>, <u> dengan format WhatsApp
            $message = preg_replace('/<b>(.*?)<\/b>/', '*$1*', $message);  // Bold
            $message = preg_replace('/<i>(.*?)<\/i>/', '_$1_', $message);  // Italic
            $message = preg_replace('/<u>(.*?)<\/u>/', '~$1~', $message);  // Underline

            // Mengganti <br> dengan newline (\n)
            $message = preg_replace('/<br\s*\/?>/', "\n", $message);

            // Mengganti <ul> dan <li> untuk membuat daftar
            $message = preg_replace('/<ul[^>]*>/', '', $message);  // Menghapus <ul> tag
            $message = preg_replace('/<\/ul>/', '', $message);  // Menghapus </ul> tag
            $message = preg_replace('/<li[^>]*>/', '• ', $message);  // Menambahkan bullet point untuk <li>

            // Menghapus <span> tag karena tidak diperlukan, hanya menjaga teks di dalamnya
            $message = preg_replace('/<span[^>]*>/', '', $message);
            $message = preg_replace('/<\/span>/', '', $message);

            // Menghapus tag HTML lainnya yang tidak diperlukan
            $message = strip_tags($message);

            // Kirim pesan ke semua nomor yang diambil
            foreach ($result as $numbers) {
                foreach ($numbers as $number) {
                    // Kirim pesan menggunakan CURL
                    $response = $this->sendMessageToWA($number, $message);
                    // Menyimpan response untuk setiap pengiriman
                    $responses[] = $response;
                }
            }
        }

        // Jika jenis pesan adalah file, kirim file
        if ($r->jenis_pesan == 'File') {
            $message = $r->isi_pesan;

            // Mengganti <b>, <i>, <u> dengan format WhatsApp
            $message = preg_replace('/<b>(.*?)<\/b>/', '*$1*', $message);  // Bold
            $message = preg_replace('/<i>(.*?)<\/i>/', '_$1_', $message);  // Italic
            $message = preg_replace('/<u>(.*?)<\/u>/', '~$1~', $message);  // Underline

            // Mengganti <br> dengan newline (\n)
            $message = preg_replace('/<br\s*\/?>/', "\n", $message);

            // Mengganti <ul> dan <li> untuk membuat daftar
            $message = preg_replace('/<ul[^>]*>/', '', $message);  // Menghapus <ul> tag
            $message = preg_replace('/<\/ul>/', '', $message);  // Menghapus </ul> tag
            $message = preg_replace('/<li[^>]*>/', '• ', $message);  // Menambahkan bullet point untuk <li>

            // Menghapus <span> tag karena tidak diperlukan, hanya menjaga teks di dalamnya
            $message = preg_replace('/<span[^>]*>/', '', $message);
            $message = preg_replace('/<\/span>/', '', $message);

            // Menghapus tag HTML lainnya yang tidak diperlukan
            $message = strip_tags($message);

            // Proses multiple file
            $fileUrls = [];
            foreach ($r->file as $file) {
                // Menyimpan file ke folder public/gambar_pesan
                $fileName = time() . '-' . $file->getClientOriginalName();
                $destinationPath = public_path('gambar_pesan'); // Folder tujuan
                $file->move($destinationPath, $fileName); // Pindahkan file

                // URL untuk mengakses file
                $fileUrl = asset('gambar_pesan/' . $fileName);
                $fileUrls[] = $fileUrl;
            }

            // Kirim pesan dengan file ke semua nomor
            foreach ($result as $numbers) {
                foreach ($numbers as $number) {
                    foreach ($fileUrls as $fileUrl) {
                        $response = $this->sendFileToWA($number, $message, $fileUrl); // Kirim dengan isi pesan dari $r->isi_pesan
                        $responses[] = $response;
                    }
                }
            }

            // Hapus file setelah pengiriman
            foreach ($fileUrls as $fileUrl) {
                // Mendapatkan path file berdasarkan URL
                $filePath = public_path(str_replace(asset('gambar_pesan'), 'gambar_pesan', $fileUrl));
                if (file_exists($filePath)) {
                    unlink($filePath);  // Hapus file
                }
            }
        }

        // Mengembalikan hasil pemrosesan dan response pesan dalam bentuk JSON
        return response()->json([
            'success' => true,
            'data' => $result,
            'responses' => $responses
        ]);
    }

    // Fungsi untuk mengirim pesan WA menggunakan CURL
    private function sendMessageToWA($receiverNumber, $message)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'appkey' => 'eb5db2ad-20fd-46ce-922a-c952c789f4a2',
                'authkey' => 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG',
                'to' => $receiverNumber,
                'message' => $message,
                'sandbox' => 'false'
            ),
        ));

        // Eksekusi cURL dan ambil respons
        $response = curl_exec($curl);

        // Tutup cURL
        curl_close($curl);

        // Mengembalikan response dari cURL
        return $response;
    }

    // Fungsi untuk mengirim pesan WA dengan file menggunakan CURL
    private function sendFileToWA($receiverNumber, $message, $fileUrl)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => array(
                'appkey' => 'eb5db2ad-20fd-46ce-922a-c952c789f4a2',
                'authkey' => 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG',
                'to' => $receiverNumber,
                'message' => $message, // Menggunakan isi pesan dari $r->isi_pesan
                'file' => $fileUrl,
                'sandbox' => 'false'
            ),
        ));

        // Eksekusi cURL dan ambil respons
        $response = curl_exec($curl);

        // Tutup cURL
        curl_close($curl);

        // Mengembalikan response dari cURL
        return $response;
    }

    public function get_email()
    {
        $data = Email::all();
        return response()->json([
            'success' => true,
            'data' => $data,

        ]);
    }
    public function kirim_pesan_email_webinar(Request $r)
    {
        $webinar = Webinar::where('id', $r->id)->first();
        $formConfig = json_decode($webinar->form, true);
        $notif_email = collect($formConfig)
            ->where('notif_email', 'true')
            ->pluck('name');

        $data = WebinarForm::where('webinar_id', $r->id)
            ->where('status', $r->status_email)
            ->get();

        $result = $data->map(function ($item) use ($notif_email) {
            $formData = json_decode($item->form, true);

            $filteredData = collect($formData)
                ->only($notif_email)
                ->values()
                ->map(function ($value) {
                    if (is_string($value) && preg_match('/^0/', $value)) {
                        return preg_replace('/^0/', '62', $value);
                    }
                    return $value;
                })
                ->toArray();

            return $filteredData;
        });

        $data_email = Email::where('id', $r->email)->first();
        $email_pengirim = $data_email->email;
        $password = $data_email->password;
        $username = $data_email->username;

        // Set SMTP konfigurasi dinamis
        config([
            'mail.mailers.smtp.host' => 'mail.indonesiamelihat.org',  // Sesuaikan dengan host SMTP Anda
            'mail.mailers.smtp.port' => 465,                  // Sesuaikan dengan port SMTP Anda
            'mail.mailers.smtp.username' => $email_pengirim,        // Username email pengirim
            'mail.mailers.smtp.password' => $password,        // Password email pengirim
            'mail.mailers.smtp.encryption' => 'tls',          // Sesuaikan dengan enkripsi SMTP Anda
        ]);

        if ($r->jenis_pesan_email == 'Teks') {
            foreach ($result as $recipient) {
                Mail::to($recipient)->send(new SendWebinarEmail(
                    $r->subject,
                    $r->isi_pesan_email,
                    $recipient,
                    $email_pengirim,
                    $password,
                    $username,
                    []  // Tidak ada lampiran jika jenis pesan hanya teks
                ));
            }
        } elseif ($r->jenis_pesan_email == 'File') {
            // Proses pengiriman email dengan file lampiran
            $uploadedFiles = $this->handleFileUpload($r);

            foreach ($result as $recipient) {
                Mail::to($recipient)->send(new SendWebinarEmailWithFile(
                    $r->subject,
                    $r->isi_pesan_email,
                    $recipient,
                    $email_pengirim,
                    $username,
                    $password,
                    $uploadedFiles

                ));
            }

            // Hapus file setelah pengiriman
            $this->removeUploadedFiles($uploadedFiles);
        }

        return response()->json([
            'success' => true,
            'message' => 'Pesan berhasil dikirim.'
        ]);
    }
    private function handleFileUpload($r)
    {
        $uploadedFiles = [];
        if ($r->hasFile('file_email')) {
            $files = $r->file('file_email');
            foreach ($files as $file) {
                // Mengenerate nama file yang unik berdasarkan timestamp dan random
                $filename = 'file_' . now()->format('Y_m_d_H_i_s') . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                // Pindahkan file ke public/gambar_pesan dengan nama yang sudah di-generate
                $file->move(public_path('gambar_pesan'), $filename);
                // Simpan path file yang telah dipindahkan
                $uploadedFiles[] = $filename;
            }
        }
        return $uploadedFiles;
    }

    // Fungsi untuk menghapus file setelah pengiriman
    private function removeUploadedFiles($uploadedFiles)
    {
        foreach ($uploadedFiles as $file) {
            $filePath = public_path('gambar_pesan/' . $file);
            if (file_exists($filePath)) {
                unlink($filePath);  // Hapus file
            }
        }
    }
    public function blast()
    {
        return view('portal.blast.index');
    }
    public function blast_pesan(Request $request)
    {
        $validated = $request->validate([
            'jenis' => 'required',
            'email' => 'required',
            'isi_pesan' => 'required',
            'kontak' => 'required|string',
            'kontak.*.nama' => 'required|string',
            'kontak.*.telp' => 'required|string',
        ]);

        // Lakukan pengiriman pesan sesuai jenis
        $jenis = $request->jenis;
        $isi_pesan = $request->isi_pesan;


        // Proses untuk mengganti tag HTML dengan simbol pesan
        $isi_pesan = preg_replace('/<b>(.*?)<\/b>/', '*$1*', $isi_pesan);  // Bold
        $isi_pesan = preg_replace('/<i>(.*?)<\/i>/', '_$1_', $isi_pesan);  // Italic
        $isi_pesan = preg_replace('/<u>(.*?)<\/u>/', '~$1~', $isi_pesan);  // Underline

        // Mengganti <br> dengan newline (\n)
        $isi_pesan = preg_replace('/<br\s*\/?>/', "\n", $isi_pesan);

        // Mengganti <ul> dan <li> untuk membuat daftar
        $isi_pesan = preg_replace('/<ul[^>]*>/', '', $isi_pesan);  // Menghapus <ul> tag
        $isi_pesan = preg_replace('/<\/ul>/', '', $isi_pesan);  // Menghapus </ul> tag
        $isi_pesan = preg_replace('/<li[^>]*>/', '• ', $isi_pesan);  // Menambahkan bullet point untuk <li>

        // Menghapus <span> tag karena tidak diperlukan, hanya menjaga teks di dalamnya
        $isi_pesan = preg_replace('/<span[^>]*>/', '', $isi_pesan);
        $isi_pesan = preg_replace('/<\/span>/', '', $isi_pesan);
        // Mengganti beberapa spasi menjadi satu spasi
        $isi_pesan = str_replace('&nbsp;', ' ', $isi_pesan);  // Mengganti &nbsp; dengan spasi biasa
        $isi_pesan = str_replace('&quot;', '"', $isi_pesan);
        // Menghapus tag HTML lainnya yang tidak diperlukan
        $isi_pesan = strip_tags($isi_pesan);

        // Menghapus karakter entitas seperti &nbsp; dan lainnya

        $kontak = json_decode($request->kontak, true);
        $responses = [];
        if ($jenis === 'wa') {
            foreach ($kontak as $contact) {
                $receiverNumber = $this->formatPhoneNumber($contact['telp']);
                $response = $this->sendMessageToWA($receiverNumber, $isi_pesan);
                $responses[] = [

                    'response' => json_decode($response, true), // Dekode respons cURL jika formatnya JSON
                ];
            }
        } elseif ($jenis === 'email') {
            
            $data_email = Email::where('id', $request->email)->first();
            $email_pengirim = $data_email->email;
            $password = $data_email->password;
            $username = $data_email->username;

           
            config([
                'mail.mailers.smtp.host' => 'mail.indonesiamelihat.org',  // Sesuaikan dengan host SMTP Anda
                'mail.mailers.smtp.port' => 465,                  // Sesuaikan dengan port SMTP Anda
                'mail.mailers.smtp.username' => $email_pengirim,        // Username email pengirim
                'mail.mailers.smtp.password' => $password,        // Password email pengirim
                'mail.mailers.smtp.encryption' => 'tls',          // Sesuaikan dengan enkripsi SMTP Anda
            ]);
            $uploadedFiles = $this->handleFileUpload($request);
            foreach ($kontak as $contact) {
                $email = $contact['telp'];
                Mail::to($email)->send(new SendWebinarEmailWithFile(
                    $request->subjek,
                    $request->isi_pesan,
                    $email,
                    $email_pengirim,
                    $username,
                    $password,
                    $uploadedFiles

                )); 
            }
           
        }

        return response()->json(['sukses' => true, 'responses' => $responses]);
    }
    private function formatPhoneNumber($phoneNumber)
    {
        // Jika nomor diawali dengan 0, ganti dengan 62
        if (substr($phoneNumber, 0, 1) === '0') {
            $phoneNumber = '62' . substr($phoneNumber, 1);
        }

        return $phoneNumber;
    }
}

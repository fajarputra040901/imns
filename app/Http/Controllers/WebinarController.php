<?php

namespace App\Http\Controllers;

use App\Models\FlowWebinar;
use App\Models\Pencairan;
use Illuminate\Http\Request;
use App\Models\Webinar;
use App\Models\WebinarForm;
use App\Models\WebinarPayment;
use Carbon\Carbon;
use Midtrans\Config;
use Yajra\DataTables\DataTables;
use Midtrans\Snap;
use Illuminate\Support\Str;
use Midtrans\Notification;

class WebinarController extends Controller
{
    public function __construct()
    {
        // Set konfigurasi Midtrans
        Config::$serverKey = config('midtrans.server_key');
        Config::$clientKey = config('midtrans.client_key');
        Config::$isProduction = config('midtrans.is_production');
        Config::$isSanitized = true;
        Config::$is3ds = true;
    }
    public function webinar()
    {
        return view('portal.webinar');
    }
    public function tes_sertifikat()
    {
        return view('portal.sertifikat');
    }
    public function tambah_webinar(Request $request)
    {
        $tanggalMulai = Carbon::createFromFormat('d-m-Y', $request->tanggal_mulai)->format('Y-m-d');
        $tanggalSelesai = Carbon::createFromFormat('d-m-Y', $request->tanggal_selesai)->format('Y-m-d');
        $webinar = new Webinar();
        $webinar->judul = $request->judul;

        $webinar->tanggal_mulai = $tanggalMulai;
        $webinar->tanggal_selesai = $tanggalSelesai;
        $webinar->jenis = $request->jenis;
        $webinar->peserta = $request->peserta;
        $webinar->skp = $request->skp;
        $webinar->deskripsi = $request->deskripsi;
        $slug = Str::slug($request->judul) . '-' . Str::random(6); // Menggabungkan slug dari judul dan 6 karakter acak

        // Pastikan slug unik
        while (Webinar::where('slug', $slug)->exists()) {
            $slug = Str::slug($request->judul) . '-' . Str::random(6); // Cek dan buat ulang jika slug sudah ada
        }

        $webinar->slug = $slug;


        $webinar->save();
        return response()->json([
            'status' => 'success',
            'message' => 'Webinar berhasil ditambah!',

        ], 200);
    }
    public function perbaharui_webinar(Request $request)
    {
        $validated = $request->validate([

            'cover' => 'nullable|string', // Bisa null, tapi jika ada harus berupa string
        ]);
        $tanggalMulai = Carbon::createFromFormat('d-m-Y', $request->tanggal_mulai)->format('Y-m-d');
        $tanggalSelesai = Carbon::createFromFormat('d-m-Y', $request->tanggal_selesai)->format('Y-m-d');
        $webinar = Webinar::where('id', $request->id_webinar)->first();

        // Data Webinar yang tidak berhubungan dengan gambar
        $webinar->judul = $request->judul;
        $webinar->cakupan = $request->cakupan;
        $webinar->tempat = $request->tempat;
        $webinar->tanggal_mulai = $tanggalMulai;
        $webinar->tanggal_selesai = $tanggalSelesai;
        $webinar->jenis = $request->jenis;
        $webinar->peserta = $request->peserta;
        $webinar->skp = $request->skp;
        $webinar->deskripsi = $request->deskripsi;
        $webinar->link_wa = $request->link_wa;
        $webinar->visabilitas = $request->visabilitas;
        $webinar->contact_person = $request->contact_person;
        $webinar->slug = $request->slug;
        $webinar->lms = $request->lms;
        $webinar->dedlen = Carbon::createFromFormat('d-m-Y', $request->dedlen)->format('Y-m-d');

        // Menangani Cover (file gambar)
        if ($request->has('cover') && !empty($request->cover)) {
            $webinar->cover = $request->cover;
        }

        // Simpan perubahan Webinar
        $webinar->save();

        return response()->json([
            'success' => true,
            'message' => 'Webinar berhasil diperbaharui!',
        ], 200);
    }


    public function list_webinar()
    {
        // Mengambil semua webinar
        $webinars = Webinar::orderBy('id', 'desc')->get();

        // Mengembalikan response JSON
        return response()->json([
            'status' => 'success',
            'data' => $webinars
        ], 200);
    }
    public function config($id)
    {
        $data = Webinar::where('id', $id)->first();
        return view('portal.config_webinar', compact('data'));
    }
    public function view_webinar($id)
    {
        $data = Webinar::where('id', $id)->first();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ], 200);
    }
    public function isi_form_webinar(Request $request)
    {

        $webinar = Webinar::where('id', $request->id_webinar)->first();
        $webinar->form = $request->data;
        $webinar->save();
        return response()->json([
            'success' => true,
            'message' => 'Form berhasil diperbaharui!',

        ], 200);
    }
    public function tes_wa()
    {
        return view('portal.tes_wa');
    }
    public function kirim_wa(Request $request)
    {
        $validatedData = $request->validate([
            'numbers' => 'required|array',
            'numbers.*' => 'required|string',
            'message' => 'required|string',
        ]);

        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';

        $responses = [];
        foreach ($validatedData['numbers'] as $number) {
            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => [
                    'appkey' => $appkey,
                    'authkey' => $authkey,
                    'to' => $number,
                    'message' => $validatedData['message'],
                    'sandbox' => 'false',
                ],
            ]);

            $response = curl_exec($curl);
            curl_close($curl);

            // Decode response from API
            $decodedResponse = json_decode($response, true);

            // Push to responses array
            if (isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success') {
                $responses[] = [
                    'number' => $number,
                    'status' => 'Success',
                    'details' => $decodedResponse['data'] ?? [],
                ];
            } else {
                $responses[] = [
                    'number' => $number,
                    'status' => 'Failed',
                    'details' => $decodedResponse['data'] ?? [],
                ];
            }
        }

        return response()->json([
            'success' => true,
            'results' => $responses,
        ]);
    }

    public function tes_form($id)
    {
        $data = Webinar::where('id', $id)->first();
        return view('portal.tes_form', compact('data'));
    }
    public function get_form($id)
    {

        $webinar = Webinar::where('id', $id)->first();
        $data = $webinar->form;

        return response()->json([
            'success' => true,
            'data' => $data

        ], 200);
    }
    public function submit_form(Request $r)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        $cek = Webinar::find($r->id);
        if (!$cek) {
            return response()->json(['success' => false, 'message' => 'Webinar tidak ditemukan.'], 404);
        }
        $data = Webinar::where('id', $r->id)->first();
        $kuota = $data->peserta;
        $dedlen = $data->dedlen;
        $hitung = WebinarForm::where('webinar_id', $r->id)->where('status', 'paid')->count();
        if ($hitung >= $kuota) {
            return response()->json([
                'success' => false,
                'message' => 'Kuota webinar sudah penuh'
            ]);
        }

        $today = Carbon::today(); // Mendapatkan tanggal hari ini

        // Memeriksa apakah hari ini lebih besar atau sama dengan tanggal deadline
        if ($today->greaterThanOrEqualTo(Carbon::parse($dedlen))) {
            return response()->json([
                'success' => false,
                'message' => 'Pendaftaran sudah ditutup'
            ]);
        }

        // Decode JSON form config dan filter primary fields
        $formConfig = json_decode($cek->form, true);
        $primaryFields = collect($formConfig)
            ->where('primary', 'true')
            ->pluck('name')
            ->toArray();

        $notif_wa = collect($formConfig)
            ->where('notif_wa', 'true')
            ->pluck('name');
        $notif_wemail = collect($formConfig)
            ->where('notif_email', 'true')
            ->pluck('name');

        $waValues = $notif_wa->map(function ($field) use ($r) {
            return $r->input($field);
        })->filter()->first();


        // Ambil nilai dari $r berdasarkan nama-nama di $notif_email
        $emailValues = $notif_wemail->map(function ($field) use ($r) {
            return $r->input($field);
        })->filter()->first(); // Ambil nilai pertama yang valid


        // return response()->json(['success' => false, 'nohp' => $waValues, 'email' => $emailValues], 404);

        // Validasi primary fields untuk mencegah duplikasi
        foreach ($primaryFields as $field) {
            if ($r->has($field)) {
                $existingForm = WebinarForm::whereJsonContains('form', [$field => $r->$field])->first();
                if ($existingForm) {
                    return response()->json([
                        'success' => false,
                        'message' => "Formulir dengan '$field' tersebut sudah terdaftar.",
                    ]);
                }
            }
        }

        // Filter dan simpan data form
        $formData = $r->except(['_token', 'id', 'paket', 'harga', 'donasi', 'harga_paket', 'donasi_paket']);
        $jsonData = json_encode($formData);

        do {
            $idTransaksi = random_int(10000000, 99999999); // 8 digit angka
        } while (WebinarForm::where('id_transaksi', $idTransaksi)->exists());



        $donasi = $r->donasi_paket ?? 0;

        $webinar = new WebinarForm();
        $webinar->form = $jsonData;
        $webinar->webinar_id = $r->id;
        $webinar->paket = $r->paket;
        $webinar->donasi = $donasi;
        $webinar->harga = $r->harga_paket;
        $webinar->jumlah = (float)$r->harga_paket + (float)$donasi + 2000;
        $webinar->id_transaksi = $idTransaksi;



        // Decode dan cari paket di dalam cost
        $paketList = json_decode($cek->cost, true);
        if (!is_array($paketList)) {
            return response()->json(['success' => false, 'message' => 'Data paket tidak valid.'], 400);
        }

        $selectedPaket = collect($paketList)->firstWhere('id', $r->paket);
        if (!$selectedPaket) {
            return response()->json(['success' => false, 'message' => 'Paket tidak ditemukan.'], 404);
        }

        // Isi properti tambahan berdasarkan paket
        $webinar->baksos = $selectedPaket['baksos'];
        $webinar->volunter = isset($selectedPaket['volunter']) && $selectedPaket['volunter'] === "true";

        $params = [
            'transaction_details' => [
                'order_id' => $idTransaksi,
                'gross_amount' => $webinar->jumlah, // Total harga
            ],
            'customer_details' => [
                'first_name' => $r->nama,   // Nama dari form
                'email' => $emailValues,       // Email dari form
                'phone' => $waValues,        // Nomor HP dari form
            ],
            'item_details' => [
                [
                    'id' => $r->paket,
                    'price' => $r->harga_paket,
                    'quantity' => 1,
                    'name' => $selectedPaket['namapaket'],
                ],
                [
                    'id' => 'donasi',
                    'price' => $donasi,
                    'quantity' => 1,
                    'name' => 'Donasi',
                ],
                [
                    'id' => 'admin',
                    'price' => 2000,
                    'quantity' => 1,
                    'name' => 'Biaya Layanan',
                ],
            ],
        ];

        try {
            $snapToken = Snap::getSnapToken($params);
            $webinar->snaptoken = $snapToken; // Simpan Snap Token
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Gagal membuat Snap Token.', 'error' => $e->getMessage()], 500);
        }

        $webinar->status = "unpaid";
        $webinar->save();
        $nama = isset($r->nama) && !empty($r->nama) ? $r->nama : '';
        $number = $waValues;

        // Cek apakah nomor dimulai dengan 0
        if (strpos($number, '0') === 0) {
            $number = '62' . substr($number, 1); // Ganti 0 dengan 62
        }

        $pesan = "Hai, {$nama}!\n\nPembayaran untuk pesanan Anda dengan ID Transaksi *{$webinar->id_transaksi}* belum selesai. Segera selesaikan pembayaran melalui metode yang Anda pilih agar pesanan Anda bisa segera diproses.\n\nBerikut adalah jumlah pembayaran Anda:\n\n*Jumlah Total:* Rp" . number_format($webinar->jumlah, 0, ',', '.') . "\n\nKlik link berikut untuk melanjutkan pembayaran:\nhttps://indonesiamelihat.org/w/t/{$webinar->id_transaksi}\n\nJika ada pertanyaan, jangan ragu untuk menghubungi Contact Person kami yang tertera di web.";

        $responses = [];
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'appkey' => $appkey,
                'authkey' => $authkey,
                'to' => $number,
                'message' => $pesan,
                'sandbox' => 'false',
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        $decodedResponse = json_decode($response, true);

        // Push to responses array
        if (isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success') {
            $responses[] = [
                'number' => $number,
                'status' => 'Success',
                'details' => $decodedResponse['data'] ?? [],
            ];
        } else {
            $responses[] = [
                'number' => $number,
                'status' => 'Failed',
                'details' => $decodedResponse['data'] ?? [],
            ];
        }

        return response()->json([
            'success' => true,
            'message' => 'Formulir berhasil disubmit.',
            'id_transaksi' => $idTransaksi,
            'snaptoken' => $snapToken,
            'result' => $responses,
        ]);
    }
    public function submit_form_undangan(Request $r)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';
        $cek = Webinar::find($r->id);
        if (!$cek) {
            return response()->json(['success' => false, 'message' => 'Webinar tidak ditemukan.'], 404);
        }

        // Decode JSON form config dan filter primary fields
        $formConfig = json_decode($cek->form, true);
        $primaryFields = collect($formConfig)
            ->where('primary', 'true')
            ->pluck('name')
            ->toArray();

        $notif_wa = collect($formConfig)
            ->where('notif_wa', 'true')
            ->pluck('name');
        $notif_wemail = collect($formConfig)
            ->where('notif_email', 'true')
            ->pluck('name');

        $waValues = $notif_wa->map(function ($field) use ($r) {
            return $r->input($field);
        })->filter()->first(); // Ambil nomor HP pertama yang valid


        // Ambil nilai dari $r berdasarkan nama-nama di $notif_email
        $emailValues = $notif_wemail->map(function ($field) use ($r) {
            return $r->input($field);
        })->filter()->first(); // Ambil nilai pertama yang valid


        // return response()->json(['success' => false, 'nohp' => $waValues, 'email' => $emailValues], 404);

        // Validasi primary fields untuk mencegah duplikasi
        foreach ($primaryFields as $field) {
            if ($r->has($field)) {
                $existingForm = WebinarForm::whereJsonContains('form', [$field => $r->$field])->first();
                if ($existingForm) {
                    return response()->json([
                        'success' => false,
                        'message' => "Formulir dengan '$field' yang berisi '{$r->$field}' sudah terdaftar.",
                    ]);
                }
            }
        }

        // Filter dan simpan data form
        $formData = $r->except(['_token', 'id', 'paket', 'harga', 'donasi', 'harga_paket', 'donasi_paket']);
        $jsonData = json_encode($formData);

        do {
            $idTransaksi = random_int(10000000, 99999999); // 8 digit angka
        } while (WebinarForm::where('id_transaksi', $idTransaksi)->exists());





        $webinar = new WebinarForm();
        $webinar->form = $jsonData;
        $webinar->webinar_id = $r->id;
        $webinar->paket = null;
        $webinar->status = 'undangan';
        $webinar->donasi = 0;
        $webinar->harga = 0;
        $webinar->jumlah = 0;
        $webinar->id_transaksi = $idTransaksi;



        $webinar->save();
        $nama = isset($r->nama) && !empty($r->nama) ? $r->nama : '';
        $number = $waValues;
        $data = Webinar::where('id', $r->id)->first();
        $nama_webinar = $data->judul;
        $grup = $data->link_wa;
        // Cek apakah nomor dimulai dengan 0
        if (strpos($number, '0') === 0) {
            $number = '62' . substr($number, 1); // Ganti 0 dengan 62
        }

        $pesan = "Hai {$nama}, Anda berhasil mendaftar webinar {$nama_webinar} melalui jalur undangan, Anda dapat masuk ke dalam grup peserta webinar tersebut melalui link ini \n\n{$grup}\n\nApabila ada pertanyaan dapat ditanyakan kepada contact person yang berada di tautan pendaftaran jalur undangan. Terima kasih";


        $responses = [];
        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'appkey' => $appkey,
                'authkey' => $authkey,
                'to' => $number,
                'message' => $pesan,
                'sandbox' => 'false',
            ],
        ]);

        $response = curl_exec($curl);

        curl_close($curl);
        $decodedResponse = json_decode($response, true);

        // Push to responses array
        if (isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success') {
            $responses[] = [
                'number' => $number,
                'status' => 'Success',
                'details' => $decodedResponse['data'] ?? [],
            ];
        } else {
            $responses[] = [
                'number' => $number,
                'status' => 'Failed',
                'details' => $decodedResponse['data'] ?? [],
            ];
        }

        return response()->json([
            'success' => true,
            'message' => 'Formulir berhasil disubmit.',
            'id_transaksi' => $idTransaksi,

            'result' => $responses,
        ]);
    }

    public function simpan_biaya(Request $r)
    {
        $webinar = Webinar::where('id', $r->id_webinar)->first();
        $webinar->cost = $r->data;
        $webinar->save();
        return response()->json([
            'success' => true,
            'message' => 'Biaya berhasil Disimpan.'
        ]);
    }
    public function get_biaya($id)
    {

        $webinar = Webinar::where('id', $id)->first();
        $data = $webinar->cost;

        return response()->json([
            'success' => true,
            'data' => $data

        ], 200);
    }
    public function tes_payment($id)
    {
        $data = WebinarForm::where('id_transaksi', $id)->first();

        if ($data) {
            $webinar = Webinar::where('id', $data->webinar_id)->first();
            $paketId = $data->paket;

            // Decode JSON untuk memproses array
            $paketList = json_decode($webinar->cost, true);

            // Filter paket berdasarkan ID
            $paket = collect($paketList)->firstWhere('id', $paketId);

            // Return view dengan data yang sesuai
            return view('portal.tes_payment', compact('data', 'webinar', 'paket'));
        } else {
            // Jika data tidak ditemukan, arahkan ke halaman 404
            abort(404, 'Pembayaran tidak ditemukan atau sudah berganti hubungi contact person apabila mengalami kesulitan.');
        }
    }

    public function get_tr($id)
    {
        $data = WebinarForm::where('id_transaksi', $id)->first();
        return response()->json([
            'success' => true,
            'data' => $data

        ], 200);
    }
    public function user_webinar($slug)
    {
        $data = Webinar::where('slug', $slug)->where('visabilitas', 'Y')->first();
        if ($data) {
            return view('webinar.detail_webinar', compact('data'));
        } else {
            return view('webinar.index');
        }
    }
    public function user_webinar_undangan($slug)
    {
        $data = Webinar::where('slug', $slug)->where('undangan', 'Y')->first();
        if ($data) {
            return view('webinar.detail_webinar_undangan', compact('data'));
        } else {
            return view('webinar.index');
        }
    }
    public function user_webinar_2()
    {
        return view('webinar.index');
    }

    //  public function peserta_webinar($id)
    // {
    //     $data = Webinar::where('id',$id)->first();
    //     return view('portal.peserta_webinar', compact('data'));

    // }

    public function peserta_webinar($id_webinar)
    {
        // Ambil data webinar berdasarkan ID
        $webinar = Webinar::findOrFail($id_webinar);

        // Ambil form data (field-form) yang tersimpan dalam webinar
        $formData = json_decode($webinar->form, true);  // Form data sebagai array
        $costData = json_decode($webinar->cost, true);  // Cost data sebagai array

        // Mengirim data ke view
        return view('portal.peserta_webinar', compact('webinar', 'formData', 'costData'));
    }


    public function getData($id_webinar, Request $r)
    {
        $list = WebinarForm::query();
        if ($r->status) {
            $list->where('status', $r->status);
        }
        $list->where('webinar_id', $id_webinar);
        $data = $list->get()->map(function ($item) {
            // Decode form data
            $decodedForm = html_entity_decode($item->form);
            $item->form = json_decode($decodedForm, true);
            return $item;
        });

        // Mengambil data webinar dan biaya
        $webinar = Webinar::find($id_webinar);
        $costData = json_decode($webinar->cost, true);

        // Menyediakan response dalam format DataTables (seluruh data tanpa filter)
        return DataTables::of($data)
            ->addColumn('paket', function ($data) use ($costData) {
                $cost = collect($costData)->firstWhere('id', $data->paket);
                return $cost ? $cost['namapaket'] : '-';
            })
            ->addColumn('jumlah', function ($data) {
                return 'Rp ' . number_format($data->jumlah - 2000, 0, ',', '.');
            })
            ->addColumn('id_transaksi', function ($data) {
                return "$data->id_transaksi";
            })
            ->addColumn('status', function ($data) {
                $status = $data->status;
                switch (strtolower($status)) {
                    case 'paid':
                        $color = 'green';
                        break;
                    case 'undangan':
                        $color = 'green';
                        break;
                    case 'unpaid':
                        $color = 'red';
                        break;
                    case 'expire':
                        $color = 'red';
                        break;
                    case 'pending':
                        $color = 'yellow';
                        break;
                    default:
                        $color = 'gray';
                }
                return '<span class="badge" style="background-color: ' . $color . ';">' . ucfirst($status) . '</span>';
            })
            ->addColumn('aksi', function ($data) {
                return '<button data-id="' . $data->id . '" class="btn btn-primary detail-btn">Detail</button>';
            })
            ->rawColumns(['aksi', 'status'])
            ->make(true);
    }
    public function get_webinar_user()
    {
        $webinars = Webinar::paginate(6); // Ambil 6 item per halaman
        return response()->json($webinars);
    }
    public function cek_peserta(Request $r)
    {
        // Jika id_transaksi diisi
        if ($r->id_transaksi) {
            $data = WebinarForm::where('id_transaksi', $r->id_transaksi)->count();
            $dump = WebinarForm::where('id_transaksi', $r->id_transaksi)->first();

            if ($data > 0) {
                return response()->json([
                    'success' => true,
                    'message' => 'Data ditemukan berdasarkan No Transaksi.',
                    'id_transaksi' => $r->id_transaksi,
                    'status' => $dump->status,
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Data tidak ditemukan dengan No Transaksi tersebut.',
                ]);
            }
        }

        // Jika id_transaksi tidak diisi, ambil semua data kecuali id_transaksi dan token
        $inputFields = $r->except(['id_transaksi', '_token']);
        $webinarData = WebinarForm::where('webinar_id', $r->id)->first(); // Ambil kolom form

        if (!$webinarData) {
            return response()->json([
                'success' => false,
                'message' => 'Webinar tidak ditemukan.',
            ]);
        }
        $cek = Webinar::find($r->id);
        // Dekode JSON dari kolom form
        $formConfig = json_decode($cek->form, true);
        $primaryFields = collect($formConfig)
            ->pluck('name')
            ->toArray();


        // Iterasi setiap field dan periksa apakah input ada
        foreach ($primaryFields as $field) {
            // Jika ada input untuk field tersebut
            if ($r->has($field)) {
                // Cek apakah ada data yang cocok di database
                $existingForm = WebinarForm::whereJsonContains('form', [$field => $r->$field])->first();
                if ($existingForm) {
                    // Jika ditemukan, return response sukses atau sesuai kebutuhan
                    return response()->json([
                        'success' => true,
                        'status' => $existingForm->status,
                        'message' => "Data Ditemukan dengan $field tersebut, ",
                        'id_transaksi' => $existingForm->id_transaksi
                    ]);
                } else {
                    return response()->json([
                        'success' => false,

                        'message' => "Data Peserta Tidak Ditemukan ",

                    ]);
                }
            }
        }
    }
    public function re_payment($id)
    {
        // Ambil data webinar pertama kali
        $webinar = WebinarForm::where('id_transaksi', $id)->first();
        if (!$webinar) {
            return response()->json(['success' => false, 'message' => 'Webinar tidak ditemukan.'], 404);
        }

        $cek = Webinar::where('id', $webinar->webinar_id)->first();
        if (!$cek) {
            return response()->json(['success' => false, 'message' => 'Webinar terkait tidak ditemukan.'], 404);
        }

        // Ambil data paket dan form webinar
        $paketList = json_decode($cek->cost, true);
        $formConfig = json_decode($cek->form, true);
        $isi_form = json_decode($webinar->form, true);

        // Validasi jika data paket tidak valid
        if (!is_array($paketList)) {
            return response()->json(['success' => false, 'message' => 'Data paket tidak valid.'], 400);
        }

        // Cari paket yang sesuai
        $selectedPaket = collect($paketList)->firstWhere('id', $webinar->paket);
        if (!$selectedPaket) {
            return response()->json(['success' => false, 'message' => 'Paket tidak ditemukan.'], 404);
        }

        // Ambil data dari form
        $nama = $isi_form['nama'] ?? '';
        $nohp = $isi_form['nohp'] ?? '';
        $email = $isi_form['email'] ?? '';

        // Cek format nohp
        if (strpos($nohp, '0') === 0) {
            $nohp = '62' . substr($nohp, 1); // Ganti 0 dengan 62
        } else {
            $nohp = '62' . substr($nohp, 1); // Ganti 0 dengan 62
        }

        // Buat ID Transaksi Baru yang unik
        do {
            $idTransaksi = random_int(10000000, 99999999); // 8 digit angka
        } while (WebinarForm::where('id_transaksi', $idTransaksi)->exists());

        // Update ID transaksi dan status
        try {
            $webinar->id_transaksi = $idTransaksi;
            $webinar->status = "unpaid";
            $webinar->save();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Gagal menyimpan data transaksi baru.', 'error' => $e->getMessage()], 500);
        }

        // Persiapkan data untuk transaksi
        $params = [
            'transaction_details' => [
                'order_id' => $idTransaksi,
                'gross_amount' => $webinar->jumlah,
            ],
            'customer_details' => [
                'first_name' => $nama,
                'email' => $email,
                'phone' => $nohp,
            ],
            'item_details' => [
                [
                    'id' => $webinar->paket,
                    'price' => $webinar->harga,
                    'quantity' => 1,
                    'name' => $selectedPaket['namapaket'],
                ],
                [
                    'id' => 'donasi',
                    'price' => $webinar->donasi,
                    'quantity' => 1,
                    'name' => 'Donasi',
                ],
                [
                    'id' => 'admin',
                    'price' => 2000,
                    'quantity' => 1,
                    'name' => 'Biaya Layanan',
                ],
            ],
        ];

        // Ambil Snap Token
        try {
            $snapToken = Snap::getSnapToken($params);
            $webinar->snaptoken = $snapToken;
            $webinar->save();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => 'Gagal membuat Snap Token.', 'error' => $e->getMessage()], 500);
        }

        // Membuat pesan notifikasi
        $pesan = $this->createRePaymentMessage($nama, $webinar->id_transaksi, $webinar->jumlah);

        // Kirim notifikasi WhatsApp
        $responses = $this->sendWhatsAppNotification($nohp, $pesan);

        return response()->json([
            'success' => true,
            'message' => 'Pembayaran dapat dilanjutkan',
            'token' => $snapToken,
            'id' => $idTransaksi,
            'result' => $responses,
        ]);
    }

    // Helper untuk membuat pesan re-payment
    private function createRePaymentMessage($nama, $idTransaksi, $jumlah)
    {
        return "Hai, {$nama}!\n\nPembayaran untuk pesanan Anda dengan ID Transaksi *{$idTransaksi}* belum selesai. Kami melihat Anda telah melakukan permintaan pembayaran ulang. Segera selesaikan pembayaran melalui metode yang Anda pilih agar pesanan Anda bisa segera diproses.\n\nBerikut adalah jumlah pembayaran Anda:\n\n*Jumlah Total:* Rp" . number_format($jumlah, 0, ',', '.') . "\n\nKlik link berikut untuk melanjutkan pembayaran:\nhttps://indonesiamelihat.org/w/t/{$idTransaksi}\n\nJika ada pertanyaan, jangan ragu untuk menghubungi Contact Person kami yang tertera di web.";
    }

    // Helper untuk mengirim pesan WhatsApp
    private function sendWhatsAppNotification($number, $pesan)
    {
        $appkey = 'eb5db2ad-20fd-46ce-922a-c952c789f4a2';
        $authkey = 'zSH11VyL1F7lFkyVCVgk68ZqHN4vsAgpk9eO3Pc5xtmkGaxvqG';

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'https://app.saungwa.com/api/create-message',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => [
                'appkey' => $appkey,
                'authkey' => $authkey,
                'to' => $number,
                'message' => $pesan,
                'sandbox' => 'false',
            ],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        $decodedResponse = json_decode($response, true);

        $status = isset($decodedResponse['message_status']) && $decodedResponse['message_status'] === 'Success' ? 'Success' : 'Failed';

        return [
            'number' => $number,
            'status' => $status,
            'details' => $decodedResponse['data'] ?? [],
        ];
    }
    public function detail_peserta($id)
    {
        // Ambil data webinar berdasarkan ID
        $data = WebinarForm::where('id', $id)->first();
        $webinar = Webinar::where('id', $data->webinar_id)->first();



        // Ambil form data (field-form) yang tersimpan dalam webinar
        $formData = json_decode($data->form, true);  // Form data sebagai array
        $formconfig = json_decode($webinar->form, true);
        $formcost = json_decode($webinar->cost, true);

        // Mengirim data ke view
        return response()->json([
            'success' => true,

            'formconfig' => $formconfig,
            'formdata' => $formData,
            'formcost' => $formcost,
            'data' => $data,

        ]);
    }
    public function hapus_peserta($id)
    {
        WebinarForm::where('id', $id)->delete();
        return response()->json([
            'success' => true,



        ]);
    }
    public function edit_peserta($id, Request $r)
    {
        $formData = $r->except(['_token', 'id', 'status', 'harga', 'donasi', 'harga_paket', 'donasi_paket']);
        $jsonData = json_encode($formData);

        $webinar = WebinarForm::where('id', $id)->first();
        $webinar->form = $jsonData;
        $webinar->status = $r->status;
        $webinar->save();
        return response()->json([
            'success' => true,



        ]);
    }
    public function count_peserta($id)
    {
        $data = Webinar::where('id', $id)->first();
        $kuota = $data->peserta;
        $slug = $data->slug;
        $undangan = $data->undangan;
        $peserta = WebinarForm::where('webinar_id', $id)->count();
        $peserta_paid = WebinarForm::where('webinar_id', $id)->where('status', 'paid')->count();
        $peserta_unpaid = WebinarForm::where('webinar_id', $id)->where('status', 'unpaid')->count();
        $peserta_pending = WebinarForm::where('webinar_id', $id)->where('status', 'pending')->count();
        $peserta_expired = WebinarForm::where('webinar_id', $id)->where('status', 'expired')->count();
        $peserta_undangan = WebinarForm::where('webinar_id', $id)->where('status', 'undangan')->count();

        return response()->json([
            'success' => true,
            'kuota' => $kuota,
            'slug' => $slug,
            'undangan' => $undangan,
            'peserta' => $peserta,
            'peserta_paid' => $peserta_paid,
            'peserta_unpaid' => $peserta_unpaid,
            'peserta_pending' => $peserta_pending,
            'peserta_expired' => $peserta_expired,
            'peserta_undangan' => $peserta_undangan,



        ]);
    }
    public function update_undangan($id, Request $r)
    {
        $data = Webinar::where('id', $id)->first();
        $data->undangan = $r->undangan;
        $data->save();
        //json_nih
        return response()->json([
            'success' => true,



        ]);
    }
    public function keuangan_webinar($id)
    {
        $data = Webinar::where('id', $id)->first();
        return view('portal.keuangan_webinar', compact('data'));
    }
    // public function info_keuangan_webinar($id)
    // {
    //     $data = Webinar::where('id', $id)->first();
    //     $paket = json_decode($data->cost, true); // Decode JSON dari $data->cost
    //     $form = WebinarForm::where('webinar_id', $id)->get();
    //     $keuangan = Pencairan::where('jenis', 'Webinar')->where('parameter', $id)->get();

    //     $formPaid = $form->where('status', 'paid');

    //     // Hitung jumlah pembayaran
    //     $jumlahPembayaran = $formPaid->sum(function ($item) {
    //         return $item->jumlah; // Kurangi 2000 dari setiap jumlah
    //     });
    //     $dapat_ditarik = $formPaid->sum(function ($item) {
    //         return $item->jumlah - 2000; // Kurangi 2000 dari setiap jumlah
    //     });

    //     // Hitung biaya layanan
    //     $biayaLayanan = $formPaid->count() * 2000;
    //     $jumlah = $formPaid->count();

    //     // Hitung total donasi
    //     $donasi = $formPaid->sum('donasi');

    //     // Inisialisasi array untuk hasil klasifikasi
    //     $klasifikasiPaket = [];

    //     // Looping melalui setiap paket
    //     foreach ($paket as $item) {
    //         // Filter $form untuk id paket yang cocok
    //         $filteredForm = $formPaid->where('paket', $item['id']);

    //         // Jumlahkan harga dari form yang sesuai
    //         $totalHarga = $filteredForm->sum('harga');
    //         $jumlahTerjual = $filteredForm->count();

    //         // Tambahkan hasil ke dalam klasifikasi
    //         $klasifikasiPaket[] = [
    //             'namapaket' => $item['namapaket'],
    //             'total_harga' => $totalHarga,
    //             'jumlah_terjual' => $jumlahTerjual,
    //         ];
    //     }

    //     // Hitung jumlah penarikan dari $keuangan
    //     $jumlah_penarikan = $keuangan->sum('jumlah');

    //     // Kembalikan hasil sebagai respons JSON
    //     return response()->json([
    //         'klasifikasi_paket' => $klasifikasiPaket,
    //         'uang_masuk' => $jumlahPembayaran,
    //         'donasi' => $donasi,
    //         'biaya_layanan' => $biayaLayanan,
    //         'uang_terkumpul' => $dapat_ditarik,
    //         'histori_penarikan' => $jumlah_penarikan,
    //     ]);
    // }
    public function info_keuangan_webinar($id)
    {
        $data = Webinar::where('id', $id)->first();
        $paket = json_decode($data->cost, true); // Decode JSON dari $data->cost
        $form = WebinarForm::where('webinar_id', $id)->get();
        $keuangan = Pencairan::where('jenis', 'Webinar')->where('parameter', $id)->get();

        $formPaid = $form->where('status', 'paid');

        // Hitung jumlah pembayaran
        $jumlahPembayaran = $formPaid->sum(function ($item) {
            return $item->jumlah; // Kurangi 2000 dari setiap jumlah
        });
        $dapat_ditarik = $formPaid->sum(function ($item) {
            return $item->jumlah - 2000; // Kurangi 2000 dari setiap jumlah
        });

        // Hitung biaya layanan
        $biayaLayanan = $formPaid->count() * 2000;
        $jumlah = $formPaid->count();

        // Hitung total donasi
        $donasi = $formPaid->sum('donasi');

        // Inisialisasi array untuk hasil klasifikasi
        $klasifikasiPaket = [];
        $uangpokokkeseluruhan = 0; // Tambahkan variabel ini
        $uanglebihkeseluruhan = 0; // Tambahkan variabel ini

        // Looping melalui setiap paket
        foreach ($paket as $item) {
            // Filter $form untuk id paket yang cocok
            $filteredForm = $formPaid->where('paket', $item['id']);

            // Jumlahkan harga dari form yang sesuai
            $totalHarga = $filteredForm->sum('harga');
            $jumlahTerjual = $filteredForm->count();

            // Jika paket memiliki minimalharga:true
            $hargapokok = 0;
            $uanglebih = 0;

            if (isset($item['minimalharga']) && $item['minimalharga'] === true) {
                $hargapokok = $filteredForm->count() * $item['hargapaket'];
                $uanglebih = $totalHarga - $hargapokok;
            } else {
                $hargapokok = $filteredForm->count() *  $item['hargapaket'];
            }
            $uangpokokkeseluruhan += $hargapokok;
            $uanglebihkeseluruhan += $uanglebih;
            // Tambahkan hasil ke dalam klasifikasi
            $klasifikasiPaket[] = [
                'namapaket' => $item['namapaket'],
                'total_harga' => $totalHarga,
                'jumlah_terjual' => $jumlahTerjual,
                'hargapokok' => $hargapokok,
                'uanglebih' => $uanglebih,
            ];
        }

        // Hitung jumlah penarikan dari $keuangan
        $jumlah_penarikan = $keuangan->sum('jumlah');

        // Kembalikan hasil sebagai respons JSON
        return response()->json([
            'klasifikasi_paket' => $klasifikasiPaket,
            'uang_masuk' => $jumlahPembayaran,
            'donasi' => $donasi,
            'biaya_layanan' => $biayaLayanan,
            'uang_terkumpul' => $dapat_ditarik,
            'histori_penarikan' => $jumlah_penarikan,
            'uangpokokkeseluruhan' => $uangpokokkeseluruhan,
            'uanglebihkeseluruhan' => $uanglebihkeseluruhan,
        ]);
    }

    public function pengajuan_pencairan_webinar(Request $r)
    {
        $keuangan = Pencairan::where('jenis', 'Webinar')->where('parameter', $r->id)->get();
        $jumlah_penarikan = $keuangan->sum('jumlah');
        $form = WebinarForm::where('webinar_id', $r->id)->get();
        $formPaid = $form->where('status', 'paid');
        $dapat_ditarik = $formPaid->sum(function ($item) {
            return $item->jumlah - 2000; // Kurangi 2000 dari setiap jumlah
        });
        $sisa = $dapat_ditarik - $jumlah_penarikan;
        if ($r->jumlah > $sisa) {
            return response()->json([
                'success' => false,
                'message' => "Jumlah Pengajuan Melebihi Jumlah Uang Yang Dapat Ditarik, Informasi Selengkapnya Dapat Dilihat Di Info Keuangan "



            ]);
        }
        $data = new Pencairan;
        $data->tanggal_pengajuan = Carbon::now();
        $data->status = 'N';
        $data->jumlah = $r->jumlah;
        $data->parameter = $r->id;
        $data->keterangan = $r->keterangan;
        $data->jenis = 'Webinar';
        $data->save();
        return response()->json([
            'success' => true,
            'message' => "Pengajuan Berhasil Dikirim"



        ]);
    }
    public function list_pengajuan_webinar($id)
    {
        $data = Pencairan::where('jenis', 'Webinar')
            ->where('parameter', $id)
            ->get();

        return datatables()->of($data)
            ->addColumn('tanggal_pengajuan', function ($row) {
                $tanggal = Carbon::parse($row->tanggal_pengajuan);
                return '
                <span>' . $tanggal->format('d-m-Y') . '</span><br>
                <span>' . $tanggal->format('H:i') . '</span>
            ';
            })
            ->addColumn('tanggal_validasi', function ($row) {
                if ($row->tanggal_validasi) {
                    $tanggal = Carbon::parse($row->tanggal_validasi);
                    return '
                    <span>' . $tanggal->format('d-m-Y') . '</span><br>
                    <span>' . $tanggal->format('H:i') . '</span>
                ';
                }
                return '<span>-</span>';
            })
            ->addColumn('jumlah', function ($row) {
                return "$row->jumlah";
            })
            ->addColumn('status', function ($row) {
                $status = '';
                $background = '';
                switch ($row->status) {
                    case 'N':
                        $status = 'Diajukan';
                        $background = 'bg-primary';
                        break;
                    case 'P':
                        $status = 'Diproses';
                        $background = 'bg-warning';
                        break;
                    case 'T':
                        $status = 'Ditolak';
                        $background = 'bg-danger';
                        break;
                    case 'Y':
                        $status = 'Dikirim';
                        $background = 'bg-success';
                        break;
                }
                return '<span class="badge ' . $background . '">' . $status . '</span>';
            })
            ->addColumn('aksi', function ($row) {
                $detailBtn = '<button class="btn btn-info btn-sm detail_pengajuan me-2" title="Detail" data-id="' . $row->id . '">
                                <i class="bx bx-show"></i>
                              </button>';

                $batalkanBtn = '<button class="btn btn-danger btn-sm batal_pengajuan" title="Batalkan" data-id="' . $row->id . '">
                                  <i class="bx bx-x"></i>
                                </button>';

                switch ($row->status) {
                    case 'N':
                        return '<div class="d-flex">' . $detailBtn . $batalkanBtn . '</div>';
                    case 'P':
                    case 'T':
                    case 'Y':
                        return '<div class="d-flex">' . $detailBtn . '</div>';
                    default:
                        return '';
                }
            })

            ->rawColumns(['tanggal_pengajuan', 'tanggal_validasi', 'status', 'aksi'])
            ->make(true);
    }
    public function batal_pengajuan($id)
    {
        Pencairan::where('id', $id)->delete();
        return response()->json([
            'success' => true,



        ]);
    }
}

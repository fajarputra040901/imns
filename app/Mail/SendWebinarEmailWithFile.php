<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendWebinarEmailWithFile extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $isiPesanEmail;
    public $recipient;
    public $emailPengirim;
    public $username;
    public $password;
    public $uploadedFiles;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $isiPesanEmail, $recipient, $emailPengirim, $username, $password, $uploadedFiles)
    {
        $this->subject = $subject;
        $this->isiPesanEmail = $isiPesanEmail;
        $this->recipient = $recipient;
        $this->emailPengirim = $emailPengirim;
        $this->username = $username;
        $this->password = $password;
        $this->uploadedFiles = $uploadedFiles;
    }

    /**
     * Build the message.
     *
     * @return \Illuminate\Mail\Message
     */
    public function build()
    {
        // Membuat email
        $email = $this->from($this->emailPengirim, $this->username)
                      ->subject($this->subject)
                      ->view('emails.webinar')  // View untuk isi email
                      ->with([
                          'isiPesanEmail' => $this->isiPesanEmail,
                      ]);
    
        // Menambahkan lampiran file jika ada
        foreach ($this->uploadedFiles as $file) {
            // Menambahkan lampiran
            $email->attach(public_path('gambar_pesan/' . $file));
        }
    
        // Kembalikan objek email
        return $email;
    }
    
}

<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendWebinarEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $isiPesanEmail;
    public $recipient;
    public $emailPengirim;
    public $password;
    public $username;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $isiPesanEmail, $recipient, $emailPengirim, $password, $username)
    {
        $this->subject = $subject;
        $this->isiPesanEmail = $isiPesanEmail;
        $this->recipient = $recipient;
        $this->emailPengirim = $emailPengirim;
        $this->password = $password;
        $this->username = $username;
    }

    /**
     * Build the message.
     *
     * @return \Illuminate\Mail\Message
     */
    public function build()
    {
        return $this->from($this->emailPengirim, $this->username)
            ->subject($this->subject)
            ->view('emails.webinar')  // View untuk isi email
            ->with([
                'isiPesanEmail' => $this->isiPesanEmail,
            ]);
    }
}

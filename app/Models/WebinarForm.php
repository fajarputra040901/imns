<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebinarForm extends Model
{
    use HasFactory;
    protected $table = 'webinar_form';

    // Jika menggunakan $guarded, ini untuk menentukan kolom yang tidak boleh diisi melalui mass-assignment.
    protected $guarded = [];
}

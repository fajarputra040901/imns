<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Giveawayokuli extends Model
{
    use HasFactory;
    protected $table = 'giveaway_okuli';
    protected $guarded = [];
}

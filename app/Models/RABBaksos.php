<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RABBaksos extends Model
{
    use HasFactory;
    protected $table = 'rab_baksos';
    protected $guarded = [];
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ItemRABSementara extends Model
{
    use HasFactory;
    protected $table = 'item_rab_sementara';
    protected $guarded = [];
}

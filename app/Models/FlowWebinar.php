<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FlowWebinar extends Model
{
    use HasFactory;
    protected $table = 'flow_keuangan_webinar';
    protected $guarded = [];
}

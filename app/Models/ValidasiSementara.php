<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ValidasiSementara extends Model
{
    use HasFactory;
    protected $table = 'validasi_sementara';
    protected $guarded = [];
}

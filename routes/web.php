<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/w/{slug}', [App\Http\Controllers\WebinarController::class, 'user_webinar']);
Route::get('/w', [App\Http\Controllers\WebinarController::class, 'user_webinar_2']);
Route::post('/cek_peserta', [App\Http\Controllers\WebinarController::class, 'cek_peserta']);
Route::get('/re_payment/{id}', [App\Http\Controllers\WebinarController::class, 're_payment']);
Route::get('/tes_form/{id}', [App\Http\Controllers\WebinarController::class, 'tes_form']);
Route::get('/w/t/{id}', [App\Http\Controllers\WebinarController::class, 'tes_payment']);
Route::get('/get_tr/{id}', [App\Http\Controllers\WebinarController::class, 'get_tr']);
Route::get('/get_webinar_user', [App\Http\Controllers\WebinarController::class, 'get_webinar_user']);

Route::get('/w/u/{slug}', [App\Http\Controllers\WebinarController::class, 'user_webinar_undangan']);

Route::get('/get_form/{id}', [App\Http\Controllers\WebinarController::class, 'get_form']);
Route::get('/get_biaya/{id}', [App\Http\Controllers\WebinarController::class, 'get_biaya']);
Route::post('/submit_form', [App\Http\Controllers\WebinarController::class, 'submit_form']);
Route::post('/simpan_biaya', [App\Http\Controllers\WebinarController::class, 'simpan_biaya']);
Route::post('/submit_form_undangan', [App\Http\Controllers\WebinarController::class, 'submit_form_undangan']);

Route::get('/donasi', [App\Http\Controllers\DonasiController::class, 'index']);
Route::post('/submit_donasi', [App\Http\Controllers\DonasiController::class, 'submit_donasi']);
Route::get('/d/{id}', [App\Http\Controllers\DonasiController::class, 'user_donasi']);
Route::get('/get-country-codes', [App\Http\Controllers\DonasiController::class, 'getCountryCodes']);
Route::get('/get_tr_donasi/{id}', [App\Http\Controllers\DonasiController::class, 'get_tr_donasi']);





Route::middleware(['auth'])->group(
    function () {

        Route::get('/webinar', [App\Http\Controllers\WebinarController::class, 'webinar']);
        Route::post('/tambah_webinar', [App\Http\Controllers\WebinarController::class, 'tambah_webinar']);
        Route::post('/perbaharui_webinar', [App\Http\Controllers\WebinarController::class, 'perbaharui_webinar']);
        Route::post('/isi_form_webinar', [App\Http\Controllers\WebinarController::class, 'isi_form_webinar']);
        Route::get('/list_webinar', [App\Http\Controllers\WebinarController::class, 'list_webinar']);
        Route::get('/tes_sertifikat', [App\Http\Controllers\WebinarController::class, 'tes_sertifikat']);
        Route::get('/view_webinar/{id}', [App\Http\Controllers\WebinarController::class, 'view_webinar']);
        Route::get('/config_webinar/{id}', [App\Http\Controllers\WebinarController::class, 'config']);
        Route::get('/peserta_webinar/{id}', [App\Http\Controllers\WebinarController::class, 'peserta_webinar'])->name('webinars.index');
        Route::get('/detail_peserta/{id}', [App\Http\Controllers\WebinarController::class, 'detail_peserta']);
        Route::post('/hapus_peserta/{id}', [App\Http\Controllers\WebinarController::class, 'hapus_peserta']);
        Route::post('/edit_peserta/{id}', [App\Http\Controllers\WebinarController::class, 'edit_peserta']);
        Route::get('/count_peserta/{id}', [App\Http\Controllers\WebinarController::class, 'count_peserta']);
        Route::post('/update_undangan/{id}', [App\Http\Controllers\WebinarController::class, 'update_undangan']);
        Route::get('/webinars/{id_webinar}/data', [App\Http\Controllers\WebinarController::class, 'getData'])->name('webinars.data');
        Route::get('/keuangan_webinar/{id}', [App\Http\Controllers\WebinarController::class, 'keuangan_webinar']);
        Route::get('/info_keuangan_webinar/{id}', [App\Http\Controllers\WebinarController::class, 'info_keuangan_webinar']);
        Route::get('/list_pengajuan_webinar/{id}', [App\Http\Controllers\WebinarController::class, 'list_pengajuan_webinar']);
        Route::post('/pengajuan_pencairan_webinar', [App\Http\Controllers\WebinarController::class, 'pengajuan_pencairan_webinar']);
        Route::post('/batal_pengajuan/{id}', [App\Http\Controllers\WebinarController::class, 'batal_pengajuan']);

        Route::get('/keuangan', [App\Http\Controllers\KeuanganController::class, 'keuangan']);
        Route::get('/permintaan_pencairan', [App\Http\Controllers\KeuanganController::class, 'permintaan_pencairan']);
        Route::get('/list_permintaan_pencairan', [App\Http\Controllers\KeuanganController::class, 'list_permintaan_pencairan']);
        Route::get('/detail_pengajuan/{id}', [App\Http\Controllers\KeuanganController::class, 'detail_pengajuan']);
        Route::post('/submit_pencairan', [App\Http\Controllers\KeuanganController::class, 'submit_pencairan']);


        Route::post('/pesan_wa_webinar', [App\Http\Controllers\BlastController::class, 'pesan_wa_webinar']);
        Route::post('/kirim_pesan_email_webinar', [App\Http\Controllers\BlastController::class, 'kirim_pesan_email_webinar']);
        Route::get('/get_email', [App\Http\Controllers\BlastController::class, 'get_email']);


        Route::get('/export_excel_peserta_webinar', [App\Http\Controllers\ExcelController::class, 'export_excel_peserta_webinar']);


        Route::get('/blast', [App\Http\Controllers\BlastController::class, 'blast']);
        Route::post('/blast_pesan', [App\Http\Controllers\BlastController::class, 'blast_pesan']);



        Route::get('/tes_wa', [App\Http\Controllers\WebinarController::class, 'tes_wa']);
        Route::post('/kirim_wa', [App\Http\Controllers\WebinarController::class, 'kirim_wa']);

        // Logistik
        Route::get('/logistik', [App\Http\Controllers\LogistikController::class, 'index']);
        Route::get('/logistik/barang', [App\Http\Controllers\LogistikController::class, 'barang']);
        Route::get('/logistik/inventaris', [App\Http\Controllers\LogistikController::class, 'inventaris']);
        Route::get('/table_barang', [App\Http\Controllers\LogistikController::class, 'table_barang']);
        Route::get('/table_inventaris', [App\Http\Controllers\LogistikController::class, 'table_inventaris']);
        Route::get('/detail_barang/{id}', [App\Http\Controllers\LogistikController::class, 'detail_barang']);
        Route::post('/tambah_barang', [App\Http\Controllers\LogistikController::class, 'tambah_barang']);
        Route::post('/tambah_inventaris', [App\Http\Controllers\LogistikController::class, 'tambah_inventaris']);
        Route::post('/hapus_barang', [App\Http\Controllers\LogistikController::class, 'hapus_barang']);
        Route::post('/import_barang', [App\Http\Controllers\LogistikController::class, 'import_barang']);
        Route::post('/import_inventaris', [App\Http\Controllers\LogistikController::class, 'import_inventaris']);
        Route::get('/logistik/permintaan_pengadaan', [App\Http\Controllers\LogistikController::class, 'permintaan_pengadaan']);
        Route::get('/table_permintaan_barang', [App\Http\Controllers\LogistikController::class, 'table_permintaan_barang']);
        Route::get('/table_permintaan_logistik/{id}', [App\Http\Controllers\LogistikController::class, 'table_permintaan']);
        Route::get('/table_validasi_logistik/{id}', [App\Http\Controllers\LogistikController::class, 'table_validasi']);
        Route::post('/validasi_barang', [App\Http\Controllers\LogistikController::class, 'validasi_barang']);
        Route::post('/ubah_validasi_barang', [App\Http\Controllers\LogistikController::class, 'ubah_validasi_barang']);
        Route::post('/validator_permintaan', [App\Http\Controllers\LogistikController::class, 'validator_permintaan']);
        Route::post('/fix_simpan_validasi', [App\Http\Controllers\LogistikController::class, 'fix_simpan_validasi']);



        Route::get('/sertifikat', [App\Http\Controllers\LogistikController::class, 'sertifikat']);
        // Baksos
        Route::get('/baksos', [App\Http\Controllers\BaksosController::class, 'index']);
        Route::get('/config_baksos/{id}', [App\Http\Controllers\BaksosController::class, 'config_baksos']);
        Route::get('/config_baksos/pengadaan_barang/{id}', [App\Http\Controllers\BaksosController::class, 'pengadaan_barang']);
        Route::get('/table_barang_pengadaan', [App\Http\Controllers\BaksosController::class, 'table_barang_pengadaan']);
        Route::get('/table_barang_terpilih/{id}', [App\Http\Controllers\BaksosController::class, 'table_barang_terpilih']);
        Route::post('/tambah_pengadaan_sementara', [App\Http\Controllers\BaksosController::class, 'tambah_pengadaan_sementara']);
        Route::post('/ubah_pengadaan_sementara', [App\Http\Controllers\BaksosController::class, 'ubah_pengadaan_sementara']);
        Route::post('/hapus_pengadaan_sementara', [App\Http\Controllers\BaksosController::class, 'hapus_pengadaan_sementara']);
        Route::post('/scanner_pengadaan_barang_sementara', [App\Http\Controllers\BaksosController::class, 'scanner_pengadaan_barang_sementara']);
        Route::post('/simpan_pengadaan_baksos', [App\Http\Controllers\BaksosController::class, 'simpan_pengadaan_baksos']);
        Route::get('/table_riwayat_pengadaan/{id}', [App\Http\Controllers\BaksosController::class, 'table_riwayat_pengadaan']);
        Route::get('/table_barang_diajukan/{id}', [App\Http\Controllers\BaksosController::class, 'table_barang_diajukan']);
        Route::get('/detail_pengajuan_pengadaan_baksos', [App\Http\Controllers\BaksosController::class, 'detail_pengajuan_pengadaan_baksos']);
        Route::post('/hapus_barang_diajukan', [App\Http\Controllers\BaksosController::class, 'hapus_barang_diajukan']);
        Route::post('/batalkan_pengajuan_barang_baksos', [App\Http\Controllers\BaksosController::class, 'batalkan_pengajuan_barang_baksos']);
        Route::get('/config_baksos/rab/{id}', [App\Http\Controllers\BaksosController::class, 'rab']);
        Route::post('/buat_rab_baksos', [App\Http\Controllers\BaksosController::class, 'buat_rab_baksos']);
        Route::get('/table_rab_baksos/{id}', [App\Http\Controllers\BaksosController::class, 'table_rab_baksos']);
        Route::get('/table_item_rab_baksos_sementara/{id}', [App\Http\Controllers\BaksosController::class, 'table_item_rab_baksos_sementara']);
        Route::post('/tambah_rencana_rab_baksos', [App\Http\Controllers\BaksosController::class, 'tambah_rencana_rab_baksos']);
        Route::get('/resume_rencana_rab_baksos', [App\Http\Controllers\BaksosController::class, 'resume_rencana_rab_baksos']);
        Route::post('/fix_rab_baksos', [App\Http\Controllers\BaksosController::class, 'fix_rab_baksos']);
        Route::post('/hapus_rencana_rab_baksos', [App\Http\Controllers\BaksosController::class, 'hapus_rencana_rab_baksos']);
        Route::get('/detail_rab_baksos/{id}', [App\Http\Controllers\BaksosController::class, 'detail_rab_baksos']);


        Route::get('/panel', [App\Http\Controllers\PortalController::class, 'index']);
    }
);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/okuli', [App\Http\Controllers\LandingController::class, 'okuli'])->name('okuli');
Route::get('/giveaway_okuli', [App\Http\Controllers\LandingController::class, 'giveaway_okuli'])->name('giveaway_okuli');
Route::get('/download_instastory', [App\Http\Controllers\LandingController::class, 'download_instastory'])->name('download_instastory');
Route::get('/finish_giveaway_okuli/{id}', [App\Http\Controllers\LandingController::class, 'finish_giveaway_okuli'])->name('finish_giveaway_okuli');
Route::post('/giveaway_post', [App\Http\Controllers\LandingController::class, 'giveaway_post'])->name('giveaway_post');
Route::get('/linkstorage', function () {
    $targetFolder = base_path() . '/storage/app/public';
    $linkFolder = $_SERVER['DOCUMENT_ROOT'] . '/storage';
    symlink($targetFolder, $linkFolder);
});
Route::get('/generate', function () {
    \Illuminate\Support\Facades\Artisan::call('storage:link');
    echo 'ok';
});

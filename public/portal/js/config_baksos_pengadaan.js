function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}
function table_barang(url) {
    table_barang_ins = $("#table_barang").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: "code",
                searchable: true,
            },
            {
                data: "nama",
                searchable: true,
            },
            {
                data: "jenis",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },

            {
                data: "stok",
                searchable: true,
            },
            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function table_barang_terpilih(url) {
    table_barang_terpilih_ins = $("#table_barang_terpilih").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: "code",
                searchable: true,
            },
            {
                data: "nama",
                searchable: true,
            },
            {
                data: "jenis",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },

            {
                data: "stok",
                searchable: true,
            },
            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function table_riwayat(url) {
    table_riwayat_ins = $("#table_riwayat").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: null, // Kolom untuk nomor urut
                render: function (data, type, row, meta) {
                    // Gunakan meta.settings._iDisplayStart untuk menghitung nomor urut yang berkelanjutan
                    return meta.settings._iDisplayStart + meta.row + 1; // Menampilkan nomor urut yang berkelanjutan
                },
            },
            {
                data: "tanggal_pengajuan",
                searchable: true,
            },
            {
                data: "tanggal_validasi",
                searchable: true,
            },
            {
                data: "judul",
                searchable: true,
            },
           

            {
                data: "status",
                searchable: true,
            },
            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reload_table_barang() {
    if (table_barang_ins) {
        table_barang_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function reload_table_barang_terpilih() {
    if (table_barang_terpilih_ins) {
        table_barang_terpilih_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function reload_table_riwayat() {
    if (table_riwayat_ins) {
        table_riwayat_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function table_barang_diajukan(url) {
    table_barang_diajukan_ins = $("#table_barang_diajukan").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: "code",
                searchable: true,
            },
            {
                data: "nama",
                searchable: true,
            },
            {
                data: "jenis",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },
            {
                data: "stok",
                searchable: true,
            },
            {
                data: "keterangan",
                searchable: true,
            },
            {
                data: "status",
                searchable: true,
            },
           

            
            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reload_table_diajukan() {
    if (table_barang_diajukan_ins) {
        table_barang_diajukan_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
$(document).ready(function () {
    
    table_riwayat("/table_riwayat_pengadaan/" + id_kegiatan)
    $("#barang_tersedia").on("click", function () {
        
        if ($.fn.dataTable.isDataTable('#table_barang')) {
            // Jika sudah ada, hancurkan (destroy) DataTable yang lama
            $('#table_barang').DataTable().clear().destroy();
        }  
        
        table_barang("/table_barang_pengadaan");
        if ($.fn.dataTable.isDataTable('#table_barang_terpilih')) {
            // Jika sudah ada, hancurkan (destroy) DataTable yang lama
            $('#table_barang_terpilih').DataTable().clear().destroy();
        }  
        table_barang_terpilih("/table_barang_terpilih/" + id_kegiatan);
        $("#modal_barang_tersedia").modal("show"); 
    });
    $("#modal_barang_tersedia").on("shown.bs.modal", function () {
        $("#input_scanner").focus();
    });
    $(document).on("click", ".pilih_barang", function () {
        var id = $(this).data("id");
        var nama = $(this).data("nama");
        var stok = $(this).data("stok");
        var jenis = $(this).data("jenis");
        var detail = $(this).data("detail");
        var keterangan = $(this).data("keterangan");

        // Isi tabel dengan data dari tombol
        $("#namaBarang").text(nama);
        $("#jenisBarang").text(jenis);
        $("#detailBarang").text(detail);
        $("#keteranganBarang").text(keterangan);
        $("#stokBarang").text(stok);

        $("#labeljumlah").text("Masukkan Jumlah Stok");
        $("#modalPilihBarangLabel").text("Pilih Barang");

        // Isi input hidden dan placeholder jumlah stok
        $("#barangId").val(id);
        $("#jenis_form").val("tambah");
        $("#jumlahStok").attr("placeholder", `Maksimal: ${stok}`);

        // Tampilkan modal

        $("#modalPilihBarang").modal("show");
        
    });
    $('#modalPilihBarang').on('shown.bs.modal', function () {
        $("#jumlahStok").focus();
    });
    $(document).on("click", ".ubah_stok", function () {
        var id = $(this).data("id");
        var nama = $(this).data("nama");
        var stok = $(this).data("stok");
        var jenis = $(this).data("jenis");
        var detail = $(this).data("detail");
        var keterangan = $(this).data("keterangan");

        // Isi tabel dengan data dari tombol
        $("#namaBarang").text(nama);
        $("#jenisBarang").text(jenis);
        $("#detailBarang").text(detail);
        $("#keteranganBarang").text(keterangan);
        $("#stokBarang").text(stok);

        $("#labeljumlah").text("Masukkan Update Jumlah Stok");
        $("#modalPilihBarangLabel").text("Ubah Stok Barang");

        $("#barangId").val(id);
        $("#jenis_form").val("ubah");
        $("#jumlahStok").attr("placeholder", `Maksimal: ${stok}`);

        // Tampilkan modal

        $("#modalPilihBarang").modal("show");
        $("#jumlahStok").focus();
    });
    // Form submit handler
    $("#formPilihBarang").on("submit", function (e) {
        e.preventDefault();
        loader(true);
        var stokMaksimal = parseInt($("#stokBarang").text()); // Ambil stok dari tabel
        var jumlahStok = parseInt($("#jumlahStok").val()); // Ambil nilai input stok

        // Validasi jumlah stok
        if (jumlahStok > stokMaksimal) {
            loader(false);
            $("#jumlahStok").val("");
            Swal.fire({
                icon: "error",
                title: "Stok Melebihi Batas",
                text: `Jumlah stok tidak boleh lebih dari ${stokMaksimal}.`,
            });
            return;
        }

        // Jika validasi lolos, kirim data
        var formData = $(this).serialize();
        formData += "&id_kegiatan=" + id_kegiatan;

        var jenis_form = $("#jenis_form").val();
        var url = null;
        if (jenis_form === "tambah") {
            url = "/tambah_pengadaan_sementara";
        } else {
            url = "/ubah_pengadaan_sementara";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
            },
            success: function (response) {
                $("#jumlahStok").val("");
                loader(false);
                if (response.success) {
                    $("#modalPilihBarang").modal("hide");
                    reload_table_barang_terpilih();
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Terjadi Kesalahan",
                        text: response.message,
                    });
                }
            },
            error: function (error) {
                $("#jumlahStok").val("");
                loader(false);
                Swal.fire({
                    icon: "error",
                    title: "Terjadi Kesalahan",
                    text: "Ada masalah saat menyimpan data.",
                });
            },
        });
    });
    $("#scanner_form").on("submit", function (e) {
        e.preventDefault(); // Mencegah submit default form
        loader(true);
        var formData = $(this).serialize(); // Ambil data form

        $.ajax({
            type: "POST",
            url: "/scanner_pengadaan_barang_sementara",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // Tambahkan CSRF token
            },
            success: function (response) {
                loader(false);
                if (response.success) {
                    reload_table_barang_terpilih();
                    $("#input_scanner").val("");
                    $("#input_scanner").focus();
                } else {
                    $("#input_scanner").val("");
                    $("#input_scanner").focus();
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: response.message,
                    });
                }
            },
            error: function (xhr) {
                loader(false);
                $("#input_scanner").val("");
                $("#input_scanner").focus();
                Swal.fire({
                    icon: "error",
                    title: "Kesalahan",
                    text: "Terjadi kesalahan saat memproses permintaan.",
                });
                console.error(xhr.responseText);
            },
        });
    });
    $(document).on("click", ".hapus_barang", function () {
        var idBarang = $(this).data("id"); // Ambil ID dari atribut data-id

        Swal.fire({
            title: "Yakin ingin menghapus barang ini?",
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                loader(true);
                // Kirim AJAX jika user mengonfirmasi
                $.ajax({
                    type: "POST",
                    url: "/hapus_pengadaan_sementara",
                    data: {
                        id: idBarang,
                    },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ), // CSRF token
                    },
                    success: function (response) {
                        loader(false);
                        if (response.success) {
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil",
                                text: response.message,
                            });
                            reload_table_barang_terpilih();
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal",
                                text: response.message,
                            });
                        }
                    },
                    error: function (xhr) {
                        loader(false);
                        Swal.fire({
                            icon: "error",
                            title: "Kesalahan",
                            text: "Terjadi kesalahan saat memproses permintaan.",
                        });
                        console.error(xhr.responseText);
                    },
                });
            }
        });
    });
    $("#simpan_pengadaan").on("click", function () {
        // Menampilkan konfirmasi menggunakan SweetAlert2
        Swal.fire({
            title: 'Apakah Anda yakin?',
            text: 'Data pengadaan akan diajukan.',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yaaa!',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                loader(true);
                // Jika pengguna mengklik "Ya, simpan!"
                $.ajax({
                    url: "/simpan_pengadaan_baksos", // URL tujuan
                    type: "POST", // Metode request
                    data: {
                        id_kegiatan: id_kegiatan, // Kirim id_kegiatan
                    },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
                    },
                    success: function (response) {
                        loader(false);
                        if (response.success) {
                            $("#modalPilihBarang").modal("hide");
                            reload_table_riwayat();
                            reload_table_barang();
                            reload_table_barang_terpilih();
                            $("#modal_barang_tersedia").modal("hide");
                            Swal.fire({
                                icon: "success",
                                title: "Simpan Berhasil!",
                                text: response.message,
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Terjadi kesalahan",
                                text: "Gagal menyimpan pengadaan.",
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        loader(false);
                        // Jika terjadi error, tampilkan pesan error
                        Swal.fire({
                            icon: "error",
                            title: "Terjadi kesalahan",
                            text: "Gagal menyimpan pengadaan.",
                        });
                    }
                });
            } else {
                // Jika pengguna memilih untuk batal
                loader(false);
                Swal.fire({
                    icon: 'info',
                    title: 'Proses dibatalkan',
                    text: 'Data pengadaan tidak disimpan.',
                });
            }
        });
    });
    $(document).on("click", ".detail_pengajuan", function () {
        var id = $(this).data("id"); // Ambil id dari data-id

        $.ajax({
            url: "/detail_pengajuan_pengadaan_baksos", // URL tujuan
            type: "GET", // Metode request (gunakan GET jika hanya mengambil data)
            data: {
                id: id // Kirimkan id
            },
            success: function (response) {
                if (response.success) {
                    // Jika data berhasil didapat, masukkan data ke dalam modal
                    var data = response.data;
                    var tanggal_pengajuan = data.tanggal_pengajuan ? moment(data.tanggal_pengajuan).format('DD-MM-YYYY') + '<br>' + moment(data.tanggal_pengajuan).format('HH:mm') : '-';
                    var tanggal_validasi = data.tanggal_validasi ? moment(data.tanggal_validasi).format('DD-MM-YYYY') + '<br>' + moment(data.tanggal_validasi).format('HH:mm') : '-';

                    $("#detail_pengajuan_div").empty();
                    // Membuat tabel dengan data
                    var tableContent = `
                        <table class="table table-bordered">
                            <tr>
                                <th>Judul</th>
                                <td>${data.judul}</td>
                            </tr>
                             <tr>
                                <th>Tanggal Pengajuan</th>
                                <td>${tanggal_pengajuan}</td>
                            </tr>
                            <tr>
                                <th>Tanggal Validasi</th>
                                <td>${tanggal_validasi}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>${data.status}</td>
                            </tr>
                            <tr>
                                <th>Keterangan</th>
                                <td>${data.keterangan ? data.keterangan : '-'}</td>
                            </tr>
                           
                           
                        </table>
                    `;

                    // Masukkan tabel ke dalam modal body
                    $("#detail_pengajuan_div").html(tableContent);
                    $("#id_pengajuan_barang").val(data.id);

                    // Tampilkan modal
                    $("#modal_detail_pengajuan").modal("show");
                } else {
                    // Jika gagal, tampilkan pesan error
                    Swal.fire({
                        icon: "error",
                        title: "Terjadi kesalahan",
                        text: "Gagal memuat detail pengajuan.",
                    });
                }
            },
            error: function (xhr, status, error) {
                // Jika terjadi error, tampilkan pesan error
                Swal.fire({
                    icon: "error",
                    title: "Terjadi kesalahan",
                    text: "Gagal memuat detail pengajuan.",
                });
            }
        });
    });
    
    $("#detail_barang_pengajuan").on("click", function () {
        var id_pengajuan = $("#id_pengajuan_barang").val();
        
        // Cek apakah DataTable sudah ada pada table_barang_diajukan
        if ($.fn.dataTable.isDataTable('#table_barang_diajukan')) {
            // Jika sudah ada, hancurkan (destroy) DataTable yang lama
            $('#table_barang_diajukan').DataTable().clear().destroy();
        }
    
        // Panggil fungsi untuk menginisialisasi DataTable
        table_barang_diajukan("/table_barang_diajukan/" + id_pengajuan);
    
        // Tampilkan modal
        $("#modal_barang_tersedia_detail").modal("show");
    });
    
    $(document).on("click", ".hapus_barang_diajukan", function () {
        var id = $(this).data("id"); // Ambil ID dari data-id
        var status = $(this).data("status"); // Ambil status dari data-status
        loader(true);
        // Cek apakah status barang adalah 'diajukan'
        if (status !== 'diajukan') {
            loader(false);
            // Jika status bukan 'diajukan', tampilkan peringatan bahwa barang sudah tidak bisa dihapus
            Swal.fire({
                icon: 'error',
                title: 'Gagal Menghapus',
                text: 'Barang ini sudah tidak bisa dihapus.',
            });
        } else {
            // Jika status 'diajukan', tampilkan konfirmasi penghapusan
            loader(false);
            Swal.fire({
                title: 'Apakah Anda yakin?',
                text: "Anda tidak dapat mengembalikan aksi ini!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    // Jika dikonfirmasi, kirimkan request AJAX untuk menghapus barang
                    $.ajax({
                        url: "/hapus_barang_diajukan", // URL tujuan
                        type: "POST", // Metode request
                        data: {
                            id: id, // Kirimkan ID
                        },
                        headers: {
                            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
                        },
                        success: function (response) {
                            loader(false);
                            if (response.success) {
                                reload_table_diajukan()
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Barang Dihapus',
                                    text: response.message,
                                });
                                
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Terjadi Kesalahan',
                                    text: 'Gagal menghapus barang.',
                                });
                            }
                        },
                        error: function (xhr, status, error) {
                            loader(false);
                            Swal.fire({
                                icon: 'error',
                                title: 'Terjadi Kesalahan',
                                text: 'Gagal menghapus barang.',
                            });
                        }
                    });
                }
            });
        }
    });
    $(document).on("click", ".batalkan_pengajuan", function () {
        var id = $(this).data("id"); // Ambil ID dari data-id

        Swal.fire({
            title: 'Apakah Anda yakin?',
            text: "Pengajuan ini akan dibatalkan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Ya, batalkan!',
            cancelButtonText: 'Batal',
        }).then((result) => {
            if (result.isConfirmed) {
                loader(true);
                $.ajax({
                    url: "/batalkan_pengajuan_barang_baksos", // URL tujuan
                    type: "POST", // Metode request
                    data: {
                        id: id, // Kirimkan ID
                    },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
                    },
                    success: function (response) {
                        loader(false);
                        if (response.success) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Pengajuan Dibatalkan',
                                text: response.message,
                            });
                            // Reload atau update halaman setelah pembatalan berhasil
                            reload_table_riwayat(); // Misalnya, reload tabel
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Terjadi Kesalahan',
                                text: 'Gagal membatalkan pengajuan.',
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        loader(false);
                        Swal.fire({
                            icon: 'error',
                            title: 'Terjadi Kesalahan',
                            text: 'Gagal membatalkan pengajuan.',
                        });
                    }
                });
            }
        });
    });
});

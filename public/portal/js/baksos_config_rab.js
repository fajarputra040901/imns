function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}
function table_rab(url) {
    table_rab_ins = $("#table_rab").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: null, // Kolom untuk nomor urut
                render: function (data, type, row, meta) {
                    // Gunakan meta.settings._iDisplayStart untuk menghitung nomor urut yang berkelanjutan
                    return meta.settings._iDisplayStart + meta.row + 1; // Menampilkan nomor urut yang berkelanjutan
                },
            },
            {
                data: "code",
                searchable: true,
            },
            {
                data: "pembuatan",
                searchable: true,
            },
            {
                data: "judul",
                searchable: true,
            },
            {
                data: "biaya",
                searchable: true,
            },
            {
                data: "status",
                searchable: true,
            },

            {
                data: "aksi",
                searchable: false,
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reload_table_rab() {
    if (table_rab_ins) {
        table_rab_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function table_item_rab_sementara(url) {
    table_item_rab_sementara_ins = $("#table_rencana_rab").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: null, // Kolom untuk nomor urut
                render: function (data, type, row, meta) {
                    // Gunakan meta.settings._iDisplayStart untuk menghitung nomor urut yang berkelanjutan
                    return meta.settings._iDisplayStart + meta.row + 1; // Menampilkan nomor urut yang berkelanjutan
                },
            },
            {
                data: "anggaran",
                searchable: true,
            },

            {
                data: "biaya",
                searchable: true,
            },
            {
                data: "status",
                searchable: true,
            },

            {
                data: "aksi",
                searchable: false,
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reload_table_item_rab_sementara() {
    if (table_item_rab_sementara_ins) {
        table_item_rab_sementara_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function resume_rab_baksos(id) {
    $.ajax({
        type: "GET",
        url: "/resume_rencana_rab_baksos", // Endpoint sesuai rute
        data: { id_baksos: id }, // Kirim parameter id
        success: function (response) {
            if (response.success) {
                // Kosongkan div sebelum menambahkan data baru
                $("#div_rencana_rab_baksos").empty();

                // Format biaya menjadi currency Rupiah
                const formatter = new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                    minimumFractionDigits: 0, // Hilangkan desimal
                    maximumFractionDigits: 0, // Pastikan tidak ada angka setelah titik
                });
                const formattedBiaya = formatter.format(response.biaya);
                const formattedBiayarencana = formatter.format(
                    response.biaya_rencana
                );

                // Tambahkan data ke dalam div
                $("#div_rencana_rab_baksos").append(`
                     <p><strong>Jumlah Item direncanakan :</strong> ${response.item_rencana}</p>
                    <p><strong>Jumlah Biaya direncanakan :</strong> ${formattedBiayarencana}</p>
                    <hr>
                    <p><strong>Jumlah Item divalidasi / diterima:</strong> ${response.item}</p>
                    <p><strong>Jumlah Biaya divalidasi / dikirim:</strong> ${formattedBiaya}</p>
                `);
            } else {
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: "Data tidak ditemukan",
                });
            }
        },
        error: function (xhr, status, error) {
            console.error("Error:", error);
            Swal.fire({
                icon: "error",
                title: "Error",
                text: "Ada kesalahan pada server.",
            });
        },
    });
}
$(document).ready(function () {
    table_rab("/table_rab_baksos/" + id_kegiatan);
    const formatRupiah = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 0, // Hilangkan desimal
        maximumFractionDigits: 0, // Pastikan tidak ada angka setelah titik
    });

    $("#biaya").on("input", function () {
        let inputVal = $(this).val().replace(/[^\d]/g, ""); // Hanya angka
        if (inputVal) {
            // Format dan tampilkan hasil tanpa ",00"
            $(this).val(formatRupiah.format(inputVal));
        } else {
            $(this).val("");
        }
    });

    $("#buat_rab").on("click", function () {
        // Ambil data yang ingin dikirimkan (misalnya anggaran dan biaya)

        // Kirim data ke server menggunakan AJAX
        $.ajax({
            url: "/buat_rab_baksos",
            method: "POST",
            data: {
                id_kegiatan: id_baksos,
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
            },
            success: function (response) {
                if (response.success) {
                    reload_table_rab();
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil",
                        text: response.message,
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: response.message,
                    });
                }
            },
            error: function (xhr, status, error) {
                console.error("Error:", xhr, status, error);
                Swal.fire({
                    icon: "error",
                    title: "Terjadi kesalahan",
                    text: "Silakan coba lagi.",
                });
            },
        });
    });
    $(document).on("click", ".detail", function () {
        var idRab = $(this).data("id"); // Ambil ID yang dibawa dari data-id

        // Isi input #id_rab dengan nilai idRab
        $("#id_rab").val(idRab);

        // Tampilkan modal
        if ($.fn.dataTable.isDataTable("#table_rencana_rab")) {
            $("#table_rencana_rab").DataTable().clear().destroy();
        }
        table_item_rab_sementara("/table_item_rab_baksos_sementara/" + idRab);
        resume_rab_baksos(idRab);
        $("#modal_isi_rab").modal("show");
    });
    $(document).on("click", "#simpanRab", function (e) {
        e.preventDefault(); // Mencegah form submit default
        loader(true);
        // Ambil data dari form
        var anggaran = $("#anggaran").val();
        var id_rab = $("#id_rab").val();
        var biayaFormatted = $("#biaya").val();

        // Pastikan data tidak kosong
        if (!anggaran.trim() || !biayaFormatted.trim()) {
            loader(false);
            Swal.fire({
                icon: "error",
                title: "Error",
                text: "Anggaran dan Biaya harus diisi!",
            });
            return;
        }

        // Menghapus simbol mata uang dan tanda koma pada biaya
        var biaya = parseInt(biayaFormatted.replace(/[^\d]/g, ""));

        if (isNaN(biaya)) {
            loader(false);
            Swal.fire({
                icon: "error",
                title: "Error",
                text: "Biaya tidak valid!",
            });
            return;
        }

        // Kirim data ke server dengan AJAX
        $.ajax({
            url: "/tambah_rencana_rab_baksos",
            method: "POST",
            data: {
                anggaran: anggaran,
                id_rab: id_rab,
                biaya: biaya, // Kirim biaya dalam format angka
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token jika menggunakan Laravel
            },
            success: function (response) {
                loader(false);
                if (response.success) {
                    reload_table_item_rab_sementara();
                    reload_table_rab();
                    resume_rab_baksos(id_rab);
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil",
                        text: response.message,
                    });
                    // Bersihkan form setelah berhasil
                    $("#anggaran").val("");
                    $("#biaya").val("");
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: response.message,
                    });
                }
            },
            error: function (xhr, status, error) {
                loader(false);
                Swal.fire({
                    icon: "error",
                    title: "Terjadi Kesalahan",
                    text: "Silakan coba lagi.",
                });
                console.error("Error:", xhr, status, error);
            },
        });
    });
    $(document).on("click", "#fix_rab", function () {
        // Tampilkan modal konfirmasi
        $("#modal_konfirmasi").modal("show");
    });

    $(document).on("click", "#lanjutkan_pengajuan", function () {
        // Periksa apakah checkbox dicentang
        var pengajuan = $("#pengajuan").is(":checked") ? "Y" : "N";
        var id_rab = $("#id_rab").val();
        // Kirim data melalui AJAX
        $.ajax({
            type: "POST",
            url: "/fix_rab_baksos",
            data: {
                id_rab: id_rab,
                pengajuan: pengajuan,
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token jika menggunakan Laravel
            },
            success: function (response) {
                if (response.success) {
                    $("#modal_konfirmasi").modal("hide");
                    $("#modal_isi_rab").modal("hide");
                    reload_table_rab();
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: response.message,
                    });
                    // Tutup modal dan reload halaman jika diperlukan
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: response.message,
                    });
                }
            },
            error: function (xhr, status, error) {
                console.error("Error:", error);
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: "Ada kesalahan pada server.",
                });
            },
        });
    });
    $(document).on("click", ".ubah", function () {
        var id = $(this).data("id"); // Get the ID from data-id attribute

        // Send AJAX request to the server
        $.ajax({
            url: "/detail_rab_baksos/" + id, // The URL with the ID
            type: "GET",
            success: function (response) {
                // Assuming the response contains data in the format you provided
                var data = response.data;

                // Format 'biaya' as currency (Rupiah)
                var formattedBiaya = new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "IDR",
                }).format(data.biaya);

                // Populate the form fields in the modal
                $("#anggaranedit").val(data.anggaran);
                $("#biayaedit").val(formattedBiaya);

                // Set the status badge class based on the status value
                var statusElement = $("#header");
                switch (data.status) {
                    case "draft":
                        statusElement
                            .text("Draft")
                            .removeClass()
                            .addClass("bg-secondary");
                        break;
                    case "disimpan":
                        statusElement
                            .text("Disimpan")
                            .removeClass()
                            .addClass("bg-secondary");
                        break;
                    case "diajukan":
                        statusElement
                            .text("Diajukan")
                            .removeClass()
                            .addClass("bg-warning");
                        break;
                    case "diproses":
                        statusElement
                            .text("Diproses")
                            .removeClass()
                            .addClass("bg-primary");
                        break;
                    case "dikirim":
                        statusElement
                            .text("Dikirim")
                            .removeClass()
                            .addClass("bg-success");
                        break;
                    case "ditolak":
                        statusElement
                            .text("Ditolak")
                            .removeClass()
                            .addClass("bg-danger");
                        break;
                    default:
                        statusElement
                            .text("Unknown")
                            .removeClass()
                            .addClass("bg-secondary");
                }

                // Show the modal
                $("#modal_detail_rab").modal("show");
            },
            error: function (xhr, status, error) {
                console.error("An error occurred:", error);
            },
        });
    });
    $(document).on("click", ".hapus", function () {
        // Ambil data-id dan data-status dari tombol
        var id = $(this).data("id");
        var status = $(this).data("status");
        var id_rab = $("#id_rab").val();
        // Periksa apakah status bukan 'disimpan' atau 'draft'
        if (status !== "disimpan" && status !== "draft") {
            Swal.fire({
                icon: "error",
                title: "Tidak Dapat Dihapus",
                text: "Item sudah tidak bisa dihapus.",
            });
            return;
        }

        // Konfirmasi penghapusan dengan SweetAlert
        Swal.fire({
            title: "Apakah Anda yakin?",
            text: "Data akan dihapus secara permanen.",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "Ya, Hapus",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                // Kirim AJAX request untuk menghapus data
                $.ajax({
                    type: "POST",
                    url: `/hapus_rencana_rab_baksos`,
                    data: {
                        id: id,
                    },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ), // CSRF token jika menggunakan Laravel
                    },
                    success: function (response) {
                        if (response.success) {
                            reload_table_item_rab_sementara();
                            reload_table_rab();
                            resume_rab_baksos(id_rab);
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil",
                                text: "Data berhasil dihapus.",
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal",
                                text:
                                    response.message ||
                                    "Ada kesalahan saat menghapus data.",
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        console.error("Error:", error);
                        Swal.fire({
                            icon: "error",
                            title: "Error",
                            text: "Ada kesalahan pada server.",
                        });
                    },
                });
            }
        });
    });
});

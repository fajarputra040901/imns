window.loader = function (show) {
    if (show) {
        $("#preloader").fadeIn(); // Tampilkan preloader dengan efek fade
    } else {
        $("#preloader").fadeOut(); // Sembunyikan preloader dengan efek fade
    }
};

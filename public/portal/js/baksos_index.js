function Data(url) {
    $("#table").DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: url,

        columnDefs: [
            {
                searchable: false,
                orderable: false,
                targets: 0, // Kolom nomor
            },
        ],
        order: [[0, "asc"]],
        columns: [
            {
                data: null,
                orderable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                },
            },
            {
                data: "id_kegiatan",
            },

            {
                data: "tanggal",
            },
            {
                data: "kegiatan",
            },
            {
                data: "tempat",
            },
            {
                data: "region",
            },
            {
                data: "status",
            },

            {
                data: "tombol",
                name: "tombol",
                orderable: false,
                searchable: false,
            },
        ],
    });
}
function detail(id) {
    $.ajax({
        url: "/detail_kegiatan/" + id,
        method: "GET",
        success: function (res) {
            if (res.status === "ada") {
                $("#lapkosong").css("display", "none");
                $("#lap").css("display", "");
                var parts = res.tanggal.split("-");
                var tanggal = parts[2] + "-" + parts[1] + "-" + parts[0];
                $("#tanggaledit").val(tanggal);
                $("#namaedit").val(res.nama);
                if (editorDeskripsiEdit) {
                    editorDeskripsiEdit.setData(res.deskripsi);
                }
                $("#tempatedit").val(res.tempat);
                $("#desaedit").val(res.desa);
                $("#jenis_kegiatanedit").val(res.jenis_kegiatan);
                if (res.jenis_kegiatan === "Berbayar") {
                    $("#div_biayaedit").show();
                } else {
                    $("#div_biayaedit").hide();
                }
                $("#biaya_edit").val(res.biaya);
                $("#image_preview_edit").empty();
                var imagePath = res.cover;
                var defaultImagePath = "/aset_me/default_image.jpeg";
                var fullImagePath = imagePath
                    ? "/" + imagePath
                    : defaultImagePath;
                $("#image_preview_edit").html(`
                    <a href="${fullImagePath}" data-fancybox="gallery">
                        <img src="${fullImagePath}" alt="Preview Image" style="display: block; margin-left: auto; margin-right: auto; max-width: 100%; cursor: pointer;">
                    </a>
                `);
                $("#kabedit").val(res.kab);
                $("#provedit").val(res.prov);
                $("#idkegiatan").val(res.id_kegiatan);
                $("#pasien_terdaftar").text(res.pasien_terdaftar);
                $("#kunjungan_pasien").text(res.kunjungan_pasien);
                $("#batal_periksa").text(res.batal_periksa);
                $("#pasien_valid").text(res.pasien_valid);
                $("#pasien_l").text(res.pasien_l);
                $("#pasien_p").text(res.pasien_p);
                $("#persen_l").text(res.persen_l + " %");
                $("#persen_p").text(res.persen_p + " %");
                $("#balita").text(res.balita);
                $("#anak").text(res.anak);
                $("#remaja_awal").text(res.remaja_awal);
                $("#remaja_akhir").text(res.remaja_akhir);
                $("#dewasa_awal").text(res.dewasa_awal);
                $("#dewasa_akhir").text(res.dewasa_akhir);
                $("#lansia_awal").text(res.lansia_awal);
                $("#lansia_akhir").text(res.lansia_akhir);
                $("#manula").text(res.manula);
                $("#emmetropiaod").text(res.emmetropiaod);
                $("#miopiaod").text(res.miopiaod);
                $("#hipermetropiaod").text(res.hipermetropiaod);
                $("#amcod").text(res.amcod);
                $("#amsod").text(res.amsod);
                $("#ahcod").text(res.ahcod);
                $("#ahsod").text(res.ahsod);
                $("#amod").text(res.amod);
                $("#organikod").text(res.organikod);
                $("#emmetropiaos").text(res.emmetropiaos);
                $("#miopiaos").text(res.miopiaos);
                $("#hipermetropiaos").text(res.hipermetropiaos);
                $("#amcos").text(res.amcos);
                $("#amsos").text(res.amsos);
                $("#ahcos").text(res.ahcos);
                $("#ahsos").text(res.ahsos);
                $("#amos").text(res.amos);
                $("#organikos").text(res.organikos);
                $("#presbiopya").text(res.presbiopya);
                $("#presbioptidak").text(res.presbioptidak);
                $("#rujukan").text(res.rujukan);
                $("#persen_balita").text(res.persen_balita + " %");
                $("#persen_anak").text(res.persen_anak + " %");
                $("#persen_remaja_awal").text(res.persen_remaja_awal + " %");
                $("#persen_remaja_akhir").text(res.persen_remaja_akhir + " %");
                $("#persen_dewasa_awal").text(res.persen_dewasa_awal + " %");
                $("#persen_dewasa_akhir").text(res.persen_dewasa_akhir + " %");
                $("#persen_lansia_awal").text(res.persen_lansia_awal + " %");
                $("#persen_lansia_akhir").text(res.persen_lansia_akhir + " %");
                $("#persen_manula").text(res.persen_manula + " %");
                $("#persen_emmetropiaod").text(res.persen_emmetropiaod + " %");
                $("#persen_miopiaod").text(res.persen_miopiaod + " %");
                $("#persen_hipermetropiaod").text(
                    res.persen_hipermetropiaod + " %"
                );
                $("#persen_amcod").text(res.persen_amcod + " %");
                $("#persen_amsod").text(res.persen_amsod + " %");
                $("#persen_ahcod").text(res.persen_ahcod + " %");
                $("#persen_ahsod").text(res.persen_ahsod + " %");
                $("#persen_amod").text(res.persen_amod + " %");
                $("#persen_organikod").text(res.persen_organikod + " %");
                $("#persen_emmetropiaos").text(res.persen_emmetropiaos + " %");
                $("#persen_miopiaos").text(res.persen_miopiaos + " %");
                $("#persen_hipermetropiaos").text(
                    res.persen_hipermetropiaos + " %"
                );
                $("#persen_amcos").text(res.persen_amcos + " %");
                $("#persen_amsos").text(res.persen_amsos + " %");
                $("#persen_ahcos").text(res.persen_ahcos + " %");
                $("#persen_ahsos").text(res.persen_ahsos + " %");
                $("#persen_amos").text(res.persen_amos + " %");
                $("#persen_organikos").text(res.persen_organikos + " %");
                $("#persen_presbiopya").text(res.persen_presbiopya + " %");
                $("#persen_presbioptidak").text(
                    res.persen_presbioptidak + " %"
                );
                $("#total_jk").text(res.pasien_p + res.pasien_l);
                $("#total_persen_jk").text(res.persen_p + res.persen_l + " %");
                $("#total_usia").text(
                    res.balita +
                        res.anak +
                        res.remaja_awal +
                        res.remaja_akhir +
                        res.dewasa_awal +
                        res.dewasa_akhir +
                        res.lansia_awal +
                        res.lansia_akhir +
                        res.manula
                );
                $("#total_persen_usia").text(
                    res.persen_balita +
                        res.persen_anak +
                        res.persen_remaja_awal +
                        res.persen_remaja_akhir +
                        res.persen_dewasa_awal +
                        res.persen_dewasa_akhir +
                        res.persen_lansia_awal +
                        res.persen_lansia_akhir +
                        res.persen_manula +
                        " %"
                );
                $("#total_diagnosaod").text(
                    res.emmetropiaod +
                        res.miopiaod +
                        res.hipermetropiaod +
                        res.amcod +
                        res.amsod +
                        res.ahcod +
                        res.ahsod +
                        res.amod +
                        res.organikod
                );
                $("#total_persen_diagnosaod").text(
                    res.persen_emmetropiaod +
                        res.persen_miopiaod +
                        res.persen_hipermetropiaod +
                        res.persen_amcod +
                        res.persen_amsod +
                        res.persen_ahcod +
                        res.persen_ahsod +
                        res.persen_amod +
                        res.persen_organikod +
                        " %"
                );
                $("#total_diagnosaos").text(
                    res.emmetropiaos +
                        res.miopiaos +
                        res.hipermetropiaos +
                        res.amcos +
                        res.amsos +
                        res.ahcos +
                        res.ahsos +
                        res.amos +
                        res.organikos
                );
                $("#total_persen_diagnosaos").text(
                    res.persen_emmetropiaos +
                        res.persen_miopiaos +
                        res.persen_hipermetropiaos +
                        res.persen_amcos +
                        res.persen_amsos +
                        res.persen_ahcos +
                        res.persen_ahsos +
                        res.persen_amos +
                        res.persen_organikos +
                        " %"
                );
                $("#total_presbiopia").text(res.presbiopya + res.presbioptidak);
                $("#total_persen_presbiopia").text(
                    res.persen_presbiopya + res.persen_presbioptidak + " %"
                );
                terlibat(res.id_kegiatan);
                struktur(res.id_kegiatan);
            } else if (res.status === "tidak") {
                $("#lapkosong").css("display", "");
                $("#lap").css("display", "none");
                var parts = res.tanggal.split("-");
                var tanggal = parts[2] + "-" + parts[1] + "-" + parts[0];
                $("#tanggaledit").val(tanggal);
                $("#namaedit").val(res.nama);
                if (editorDeskripsiEdit) {
                    editorDeskripsiEdit.setData(res.deskripsi);
                }
                $("#jenis_kegiatanedit").val(res.jenis_kegiatan);
                if (res.jenis_kegiatan === "Berbayar") {
                    $("#div_biayaedit").show();
                } else {
                    $("#div_biayaedit").hide();
                }

                $("#biayaedit").val(res.biaya);
                $("#image_preview_edit").empty();
                var imagePath = res.cover;
                var defaultImagePath = "/aset_me/default_image.jpeg";
                var fullImagePath = imagePath
                    ? "/" + imagePath
                    : defaultImagePath;
                $("#image_preview_edit").html(`
                    <a href="${fullImagePath}" data-fancybox="gallery">
                        <img src="${fullImagePath}" alt="Preview Image" style="display: block; margin-left: auto; margin-right: auto; max-width: 100%; cursor: pointer;">
                    </a>
                `);
                $("#tempatedit").val(res.tempat);
                $("#desaedit").val(res.desa);
                $("#kabedit").val(res.kab);
                $("#provedit").val(res.prov);
                $("#idkegiatan").val(res.id_kegiatan);
            }
            if (res.peserta === "T") {
                $("#tanpaBatasanedit").prop("checked", true);
                $("#pesertaedit").val("").prop("readonly", true);
            } else {
                $("#tanpaBatasanedit").prop("checked", false);
                $("#pesertaedit").val(res.peserta).prop("readonly", false);
            }

            // Set Minimal Usia
            if (res.min_usia === "T") {
                $("#tanpaMinimaledit").prop("checked", true);
                $("#min_usiaedit").val("").prop("readonly", true);
            } else {
                $("#tanpaMinimaledit").prop("checked", false);
                $("#min_usiaedit").val(res.min_usia).prop("readonly", false);
            }

            // Set Maksimal Usia
            if (res.max_usia === "T") {
                $("#tanpaMaksimaledit").prop("checked", true);
                $("#max_usiaedit").val("").prop("readonly", true);
            } else {
                $("#tanpaMaksimaledit").prop("checked", false);
                $("#max_usiaedit").val(res.max_usia).prop("readonly", false);
            }

            // Set Tanpa Batasan Usia
            if (res.min_usia === "T" && res.max_usia === "T") {
                $("#tanpaBatasanUsiaedit").prop("checked", true);
                $("#min_usiaedit").prop("readonly", true);
                $("#max_usiaedit").prop("readonly", true);
            } else {
                $("#tanpaBatasanUsiaedit").prop("checked", false);
            }
        },
    });
}
$(document).ready(function () {
    Data("http://localhost:8001/api/list_baksos");

    $(document).on("click", ".lihatlah", function (e) {
        e.preventDefault(); // Mencegah aksi default
        const id = $(this).data("id"); // Ambil ID dari data-id
        const urli = `/config_baksos/${id}`; // URL tujuan
        window.open(urli, "_blank"); // Membuka URL di tab baru
    });
});

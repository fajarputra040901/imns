$(document).ready(function () {
    let inputCount = 0;
    let editingId = null;
    function viewWebinar() {
        // Mengirimkan permintaan GET via AJAX
        $.ajax({
            url: "/view_webinar/" + id_webinar, // URL untuk mendapatkan data webinar
            method: "GET", // Menggunakan metode GET
            dataType: "json", // Format data yang diterima adalah JSON
            success: function (response) {
                // Mengecek apakah request berhasil
                if (response.status === "success") {
                    var webinarData = response.data;
                    if (webinarData.cover) {
                        var coverData = [];
                        try {
                            coverData = JSON.parse(webinarData.cover);
                        } catch (e) {
                            console.error("Error parsing cover data:", e);
                        }

                        if (Array.isArray(coverData)) {
                            var previewHTML = "";
                            coverData.forEach(function (item) {
                                if (
                                    item.src &&
                                    item.src.startsWith("data:image")
                                ) {
                                    previewHTML += `
                                        <div class="image-preview-container" data-id="${item.id}">
                                            <img src="${item.src}" alt="Cover Image" width="100" height="100" style="object-fit: cover;"/>
                                            <button type="button" class="delete-btn" style="cursor: pointer;">X</button>
                                        </div>`;
                                } else {
                                    console.warn(
                                        "Invalid image source:",
                                        item.src
                                    );
                                }
                            });

                            // Tampilkan gambar di #review_cover
                            $("#riview_cover").html(previewHTML);

                            // Tambahkan event klik untuk tombol hapus dan Fancybox
                            $(".delete-btn").click(function () {
                                var container = $(this).closest(
                                    ".image-preview-container"
                                );
                                var idToDelete = container.data("id");

                                // Hapus gambar dari preview
                                container.remove();

                                // Update JSON di textarea #cover
                                var existingData = JSON.parse(
                                    $("#cover").val() || "[]"
                                );
                                var updatedData = existingData.filter(function (
                                    item
                                ) {
                                    return item.id !== idToDelete;
                                });

                                // Update textarea dengan JSON yang baru
                                $("#cover").val(JSON.stringify(updatedData));
                            });

                            $(".image-preview-container img").on(
                                "click",
                                function () {
                                    var imageUrl = $(this).attr("src");
                                    Fancybox.show([
                                        { src: imageUrl, type: "image" },
                                    ]);
                                }
                            );

                            // Pastikan textarea #cover diupdate dengan data JSON yang baru
                            $("#cover").val(JSON.stringify(coverData));
                        } else {
                            console.error(
                                "Invalid cover data structure:",
                                webinarData.cover
                            );
                            $("#riview_cover").empty();
                        }
                    } else {
                        // Jika tidak ada gambar cover, pastikan riview_cover kosong
                        $("#riview_cover").empty();
                    }

                    // Mengisi form dengan data yang diterima
                    $('input[name="judul"]').val(webinarData.judul);
                    $('input[name="cakupan"]').val(webinarData.cakupan);
                    $('input[name="tempat"]').val(webinarData.tempat);
                    $('input[name="tanggal_mulai"]').val(
                        webinarData.tanggal_mulai
                            ? moment(webinarData.tanggal_mulai).format(
                                  "DD-MM-YYYY"
                              )
                            : ""
                    );
                    $('input[name="tanggal_selesai"]').val(
                        webinarData.tanggal_selesai
                            ? moment(webinarData.tanggal_selesai).format(
                                  "DD-MM-YYYY"
                              )
                            : ""
                    );
                    $('input[name="dedlen"]').val(
                        webinarData.dedlen
                            ? moment(webinarData.dedlen).format("DD-MM-YYYY")
                            : ""
                    );
                    $('select[name="jenis"]').val(webinarData.jenis);
                    $('input[name="peserta"]').val(webinarData.peserta);
                    $('input[name="skp"]').val(webinarData.skp);
                    $('textarea[name="deskripsi"]').val(webinarData.deskripsi);
                    $('input[name="link_wa"]').val(webinarData.link_wa);
                    $('input[name="slug"]').val(webinarData.slug);
                    var fullUrl =
                        window.location.origin + "/w/" + webinarData.slug;

                    // Menyisipkan URL lengkap ke href dan teks dari elemen dengan id link_webinar
                    $("#link_webinar")
                        .attr("href", fullUrl) // Set href ke domain + /w/slug
                        .text(fullUrl) // Set teks dari link sesuai dengan URL lengkap
                        .attr("target", "_blank");
                    $('input[name="lms"]').val(webinarData.lms);
                    $(
                        'input[name="visabilitas"][value="' +
                            webinarData.visabilitas +
                            '"]'
                    ).prop("checked", true);

                    // Menangani data Contact Person
                    var contactPersons = JSON.parse(webinarData.contact_person);
                    var cpTableBody = $("#tabel_cp tbody");
                    cpTableBody.empty(); // Kosongkan tabel sebelum menambahkan data baru

                    // Menambahkan setiap Contact Person ke dalam tabel
                    contactPersons.forEach(function (cp, index) {
                        var row = `<tr data-id="${index + 1}">
                            <td>${index + 1}</td>
                            <td>${cp.nama}</td>
                            <td>${cp.no_cp}</td>
                            <td>${
                                cp.status === "Y" ? "Anonim" : "Tidak Anonim"
                            }</td>
                            <td><button type="button" class="btn btn-danger btn-sm hapus_cp" >Hapus</button></td>
                        </tr>`;
                        cpTableBody.append(row);
                    });
                    if (webinarData.form) {
                        // Pastikan webinarData.form adalah array
                        var formData = Array.isArray(webinarData.form)
                            ? webinarData.form
                            : JSON.parse(webinarData.form);

                        var jumlah_input =
                            formData.length > 0 ? formData.length : 0;
                        inputCount = jumlah_input;
                        // Iterasi data form
                        formData.forEach(function (item) {
                            var validations = Array.isArray(item.validations)
                                ? item.validations
                                : [];
                            var newInput;
                            // Buat elemen input berdasarkan tipe
                            if (item.type === "text") {
                                newInput = `<input type="text" class="form-control" notif_wa='${
                                    item.notif_wa
                                }' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }"  data-validations="${validations.join(
                                    ","
                                )}">`;
                            } else if (item.type === "number") {
                                newInput = `<input type="number" class="form-control" notif_wa='${
                                    item.notif_wa
                                }' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }"  data-validations="${validations.join(
                                    ","
                                )}">`;
                            } else if (item.type === "date") {
                                newInput = `<input type="date" class="form-control" name="${
                                    item.name
                                }" notif_wa='${item.notif_wa}' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }" data-validations="${validations.join(
                                    ","
                                )}">`;
                            } else if (item.type === "textarea") {
                                newInput = `<textarea class="form-control" notif_wa='${
                                    item.notif_wa
                                }' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }"  data-validations="${validations.join(
                                    ","
                                )}"></textarea>`;
                            } else if (item.type === "file") {
                                newInput = `<input type="file" class="form-control" name="${
                                    item.name
                                }" notif_wa='${item.notif_wa}' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }"  data-validations="${validations.join(
                                    ","
                                )}">`;
                            } else if (item.type === "email") {
                                newInput = `<input type="email" class="form-control" notif_wa='${
                                    item.notif_wa
                                }' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }"  data-validations="${validations.join(
                                    ","
                                )}">`;
                            } else if (item.type === "select") {
                                // Pastikan opsi ada di dalam form options
                                var opsiList = Array.isArray(item.options)
                                    ? item.options
                                    : [];
                                var optionsHTML = opsiList
                                    .map(function (opsi) {
                                        return `<option value="${opsi}">${opsi}</option>`;
                                    })
                                    .join("");

                                newInput = `<select class="form-select" name="${
                                    item.name
                                }" notif_wa='${item.notif_wa}' notif_email='${
                                    item.notif_email
                                }' keterangan='${item.keterangan}' primary='${
                                    item.primary
                                }' name="${item.name}" placeholder="${
                                    item.placeholder
                                }" data-validations="${validations.join(
                                    ","
                                )}" data-opsi='${JSON.stringify(
                                    item.options
                                )}'>${optionsHTML}</select>`;
                            }

                            // Menambahkan input baru ke div #review_input_form

                            var newInputHTML = `
                                <div class="mb-3 input-item" id="${item.id}">
                                    <label class="form-label">${
                                        item.label
                                    }</label>
                                    ${newInput}
                                    <small class="form-text text-muted">${
                                        item.keterangan
                                    }</small>
                                    ${
                                        validations.length > 0
                                            ? `<p class="text-muted">Validasi: ${validations.join(
                                                  ", "
                                              )}</p>`
                                            : ""
                                    }
                                    <button type="button" class="btn btn-warning btn-sm edit-input-btn" data-id="${
                                        item.id
                                    }">Edit</button>
                                    <button type="button" class="btn btn-danger btn-sm delete-input-btn" data-id="${
                                        item.id
                                    }">Hapus</button>
                                </div>
                            `;

                            // Menambahkan input baru ke dalam #review_input_form
                            $("#review_input_form").append(newInputHTML);
                        });

                        // Memperbarui JSON setelah template dimuat
                        updateJSON();
                    }
                } else {
                    alert("Gagal memuat data webinar");
                }
            },
            error: function (xhr, status, error) {
                // Menangani jika terjadi kesalahan dalam permintaan AJAX
                alert("Terjadi kesalahan: " + error);
            },
        });
    }
    viewWebinar();
    $("#tambah_cp").click(function (event) {
        event.preventDefault(); // Mencegah refresh atau aksi default lainnya
        $("#form_cp").toggle(); // Menampilkan atau menyembunyikan form_cp
    });

    $("#submit_cp").click(function (event) {
        event.preventDefault(); // Mencegah form melakukan submit

        // Mengambil nilai dari input form
        var nama = $("#nama").val();
        var noCp = $("#no_cp").val();
        var anonim = $("#anonim").prop("checked") ? "Anonim" : "Tidak Anonim"; // Memeriksa apakah checkbox dicentang

        // Menghitung nomor urut berdasarkan jumlah baris yang ada di tabel
        var no = $("#tabel_cp tbody tr").length + 1;

        // Memasukkan data ke dalam tabel
        var newRow = `<tr data-id="${no}">
                        <td>${no}</td>
                        <td>${nama}</td>
                        <td>${noCp}</td>
                        <td>${anonim}</td>
                        <td><button class="btn btn-danger btn-sm hapus_cp">Hapus</button></td>
                    </tr>`;

        // Menambahkan baris baru ke dalam tabel
        $("#tabel_cp tbody").append(newRow);

        // Reset form setelah data ditambahkan (opsional)
        $("#form_cp").hide(); // Sembunyikan form
        $("#nama").val(""); // Reset field Nama
        $("#no_cp").val(""); // Reset field No Telp
        $("#anonim").prop("checked", true); // Reset checkbox Anonim ke checked
    });

    // Menangani klik pada tombol hapus untuk menghapus baris
    $("#tabel_cp").on("click", ".hapus_cp", function () {
        // Menghapus baris yang mengandung tombol hapus yang diklik
        $(this).closest("tr").remove();

        // Mengupdate nomor urut setelah penghapusan
        $("#tabel_cp tbody tr").each(function (index) {
            $(this)
                .find("td:first")
                .text(index + 1);
        });
    });

    $("#submit_form").click(function (event) {
        event.preventDefault(); // Mencegah form melakukan submit

        // Mengambil data dari form
        var formData = new FormData($("#form_edit_webinar")[0]); // Menggunakan FormData untuk mengirimkan file

        var coverData = $("#cover").val();
        formData.append("cover", coverData);

        // Mengambil data Contact Person dan mengubahnya menjadi array
        var contactPersonData = [];
        $("#tabel_cp tbody tr").each(function () {
            var row = $(this);
            var contactPerson = {
                id: row.attr("data-id"), // Mengambil ID unik (nomor urut)
                nama: row.find("td:eq(1)").text(),
                no_cp: row.find("td:eq(2)").text(),
                status: row.find("td:eq(3)").text() == "Anonim" ? "Y" : "N", // Status Anonim atau Tidak Anonim
            };
            contactPersonData.push(contactPerson); // Menambahkan data Contact Person ke array
        });

        var deskripsi = CKEDITOR.instances.deskripsi.getData(); // Mengambil data dari CKEditor

        // Menambahkan Deskripsi ke form data
        formData.append("deskripsi", deskripsi);

        // Menambahkan Contact Person ke form data sebagai JSON
        formData.append("contact_person", JSON.stringify(contactPersonData));

        // Mengirim data melalui AJAX
        $.ajax({
            url: "/perbaharui_webinar", // URL untuk mengirim data
            method: "POST", // Metode POST
            data: formData, // Data yang akan dikirimkan
            dataType: "json",
            processData: false, // Jangan proses data (karena ada file)
            contentType: false, // Jangan set contentType secara manual
            beforeSend: function (xhr) {
                // Menyertakan CSRF token di header
                xhr.setRequestHeader(
                    "X-CSRF-TOKEN",
                    $('meta[name="csrf-token"]').attr("content")
                );
            },
            success: function (response) {
                if (response.success) {
                    // Panggil viewWebinar() untuk memperbarui tampilan setelah data berhasil diperbarui
                    viewWebinar();

                    // Menampilkan SweetAlert untuk konfirmasi
                    Swal.fire({
                        icon: "success",
                        title: "Data Berhasil Diperbaharui!",
                        text: "Webinar telah berhasil diperbaharui.",
                        confirmButtonText: "OK",
                    });
                } else {
                    // Menampilkan SweetAlert untuk error
                    Swal.fire({
                        icon: "error",
                        title: "Terjadi Kesalahan",
                        text: "Silakan coba lagi.",
                        confirmButtonText: "OK",
                    });
                }
            },
            error: function (xhr, status, error) {
                // Menampilkan SweetAlert untuk kesalahan server
                Swal.fire({
                    icon: "error",
                    title: "Terjadi Kesalahan",
                    text: "Terjadi kesalahan teknis. Silakan coba lagi.",
                    confirmButtonText: "OK",
                });
            },
        });
    });

    $("#tambah_input").click(function () {
        // Menampilkan modal dengan ID modalForm
        resetForm();
        $("#modalForm").modal("show");
    });
    $("#jenisInput").change(function () {
        var jenis = $(this).val();

        // Menampilkan atau menyembunyikan validasi file jika jenis input adalah 'file'
        if (jenis === "file") {
            $("#fileValidationFields").show();
        } else {
            $("#fileValidationFields").hide();
        }

        if (jenis === "select") {
            $("#jenis_select").show();
        } else {
            $("#jenis_select").hide();
        }
    });
    $("#tambah_input_form").click(function () {
        if (editingId === null) {
            // Kondisi hanya menambah jika tidak dalam mode edit
            // Ambil data dari form
            var namaInput = $("#namaInput").val();
            var jenisInput = $("#jenisInput").val();
            var placeholderInput = $("#placeholderInput").val();
            var keteranganInput = $("#keteranganInput").val();
            var labelInput = $("#labelInput").val(); // Menambahkan pengambilan data label

            var primer = false;
            if ($("#primary").prop("checked")) {
                primer = true;
            } else {
                primer = false;
            }
            var notif_wa = false;
            if ($("#notif_wa").prop("checked")) {
                notif_wa = true;
            } else {
                notif_wa = false;
            }
            var notif_email = false;
            if ($("#notif_email").prop("checked")) {
                notif_email = true;
            } else {
                notif_email = false;
            }
            // Ambil validasi yang dipilih
            var validasi = [];
            if ($("#validasiRequired").prop("checked"))
                validasi.push("required");
            if ($("#validasiNumber").prop("checked")) validasi.push("number");
            if ($("#validasiDate").prop("checked")) validasi.push("date");
            if ($("#validasiFile").prop("checked")) validasi.push("file");

            // Validasi file jika jenis input adalah file
            var fileDetails = "";
            var maxFileSize = null;
            var allowedFileTypes = [];
            if (jenisInput === "file" && $("#validasiFile").prop("checked")) {
                maxFileSize = $("#ukuranFile").val(); // Ambil ukuran maksimal file
                // Ambil jenis file yang dipilih
                $(".form-check-input:checked").each(function () {
                    allowedFileTypes.push($(this).val());
                });
                fileDetails = {
                    maxFileSize: maxFileSize,
                    allowedFileTypes: allowedFileTypes,
                };
            }

            // Membuat elemen input sesuai dengan jenis input yang dipilih
            var newInput;
            if (jenisInput === "text") {
                newInput = `<input type="text" class="form-control" name="${namaInput}" placeholder="${placeholderInput}" required notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "textarea") {
                newInput = `<textarea class="form-control" name="${namaInput}" placeholder="${placeholderInput}" required notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-validations="${validasi.join(
                    ","
                )}"></textarea>`;
            } else if (jenisInput === "date") {
                newInput = `<input type="date" class="form-control" name="${namaInput}" required notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "number") {
                newInput = `<input type="number" class="form-control" name="${namaInput}" placeholder="${placeholderInput}" required notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "file") {
                newInput = `<input type="file" class="form-control" name="${namaInput}" required notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "email") {
                newInput = `<input type="email" class="form-control" name="${namaInput}" placeholder="${placeholderInput}" required notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "select") {
                // Ambil opsi dari list_opsi
                var opsiList = [];
                $("#list_opsi .ui-state-default").each(function () {
                    opsiList.push($(this).find("span").text());
                });

                if (opsiList.length === 0) {
                    alert("Tambahkan opsi terlebih dahulu!");
                    return;
                }

                // Buat elemen select dengan opsi
                try {
                    // Ubah opsiList menjadi JSON string
                    var opsiListJSON = JSON.stringify(opsiList);

                    // Buat elemen select dengan opsi
                    var optionsHTML = opsiList
                        .map(
                            (opsi) => `<option value="${opsi}">${opsi}</option>`
                        )
                        .join("");
                    newInput = `<select class="form-select" name="${namaInput}" notif_wa='${notif_wa}' notif_email='${namaInput}' keterangan='${keteranganInput}' primary='${primer}' data-opsi='${opsiListJSON}' data-validations="${validasi.join(
                        ","
                    )}" required>${optionsHTML}</select>`;
                } catch (e) {
                    console.error(
                        "Gagal mengubah opsi menjadi JSON:",
                        e.message
                    );
                    alert("Terjadi kesalahan saat memproses opsi. Coba lagi.");
                    return;
                }
            }

            // Menambahkan input baru ke div #review_input_form dengan tombol edit dan hapus
            var uniqueId = "input_" + inputCount++;
            var newInputHTML = `
                <div class="mb-3 input-item" id="${uniqueId}">
                    <label class="form-label">${labelInput}</label>
                    ${newInput}
                    <small class="form-text text-muted">${keteranganInput}</small>
                    ${
                        validasi.length > 0
                            ? `<p class="text-muted">Validasi: ${validasi.join(
                                  ", "
                              )}</p>`
                            : ""
                    }
                    ${
                        fileDetails
                            ? `<p class="text-muted">File Details: Max Size: ${
                                  fileDetails.maxFileSize
                              } MB, Allowed Types: ${fileDetails.allowedFileTypes.join(
                                  ", "
                              )}</p>`
                            : ""
                    }
                    <button type="button" class="btn btn-warning btn-sm edit-input-btn" data-id="${uniqueId}">Edit</button>
                   <button type="button" class="btn btn-danger btn-sm delete-input-btn" data-id="${uniqueId}">Hapus</button>
                </div>
            `;

            // Menambahkan input baru di dalam #review_input_form
            $("#review_input_form").append(newInputHTML);

            // Memperbarui JSON
            updateJSON();

            // Tutup modal setelah input berhasil disimpan
            $("#modalForm").modal("hide");

            // Kosongkan form setelah menambah input
            resetForm();
        } else {
            // Jika dalam mode edit, lakukan update pada elemen input yang dipilih
            saveEdit();
        }

        // Aktifkan drag and drop
    });
    $("#review_input_form").sortable({
        items: ".input-item",
        update: function () {
            updateJSON(); // Update JSON setelah posisi input berubah
        },
    });

    // Edit Button
    $(document).on("click", ".edit-input-btn", function () {
        var uniqueId = $(this).data("id");
        var inputDiv = $("#" + uniqueId);

        // Dapatkan nilai yang ada dalam input untuk diubah
        var label = inputDiv.find("label").text();
        var placeholder = inputDiv
            .find("input, textarea, select")
            .attr("placeholder");
        var keterangan = inputDiv
            .find("input, textarea, select")
            .attr("keterangan");
        var primary = inputDiv.find("input, textarea, select").attr("primary");
        var notif_wa = inputDiv
            .find("input, textarea, select")
            .attr("notif_wa");
        var notif_email = inputDiv
            .find("input, textarea, select")
            .attr("notif_email");
        var type =
            inputDiv.find("input, textarea, select").attr("type") || "select"; // Tambahkan untuk select
        var validationsData = inputDiv
            .find("input, textarea, select")
            .data("validations");

        // Berikan default nilai jika data-validations tidak ada
        var validasi = validationsData ? validationsData.split(",") : [];

        // Isi modal dengan data yang ada
        $("#labelInput").val(label);
        $("#namaInput").val(
            inputDiv.find("input[name], select[name]").attr("name")
        );
        $("#placeholderInput").val(placeholder || "");
        $("#jenisInput").val(type);
        $("#keteranganInput").val(keterangan);
        if (primary === "true") {
            $("#primary").prop("checked", true); // Checkbox di-check
        } else {
            $("#primary").prop("checked", false); // Checkbox di-uncheck
        }

        if (notif_wa === "true") {
            $("#notif_wa").prop("checked", true); // Checkbox di-check
        } else {
            $("#notif_wa").prop("checked", false); // Checkbox di-uncheck
        }
        if (notif_email === "true") {
            $("#notif_email").prop("checked", true); // Checkbox di-check
        } else {
            $("#notif_email").prop("checked", false); // Checkbox di-uncheck
        }

        // Set validasi checkbox
        $("#validasiRequired").prop("checked", validasi.includes("required"));
        $("#validasiNumber").prop("checked", validasi.includes("number"));
        $("#validasiDate").prop("checked", validasi.includes("date"));
        $("#validasiFile").prop("checked", validasi.includes("file"));

        // Jika tipe adalah select, ambil opsi dari data-opsi
        if (type === "select") {
            $("#jenis_select").show();
            $("#list_opsi").empty(); // Kosongkan daftar sebelumnya

            var dataOpsi = inputDiv.find("select").attr("data-opsi"); // Ambil atribut data asli
            var opsiList = [];
            try {
                opsiList = JSON.parse(dataOpsi || "[]");
            } catch (error) {
                console.error(
                    "Gagal mem-parse data-opsi:",
                    error.message,
                    dataOpsi
                );
                alert("Data opsi tidak valid.");
                return;
            }

            opsiList.forEach((opsi) => {
                $("#list_opsi").append(
                    `<div class="d-flex justify-content-between align-items-center mb-2 ui-state-default" id="item_${Date.now()}">
                <span>${opsi}</span>
                <button type="button" class="btn btn-sm btn-danger hapus-opsi">Hapus</button>
            </div>`
                );
            });
        } else {
            $("#jenis_select").hide();
        }

        // Simpan ID unik yang akan diedit
        editingId = uniqueId;

        // Tampilkan modal untuk edit
        $("#modalForm").modal("show");
    });

    // Menyimpan perubahan input setelah edit
    function saveEdit() {
        if (editingId) {
            var namaInput = $("#namaInput").val();
            var jenisInput = $("#jenisInput").val();
            var placeholderInput = $("#placeholderInput").val();
            var keteranganInput = $("#keteranganInput").val();
            var labelInput = $("#labelInput").val(); // Menambahkan pengambilan data label
            var primer = false;
            if ($("#primary").prop("checked")) {
                primer = true;
            } else {
                primer = false;
            }
            var notif_wa = false;
            if ($("#notif_wa").prop("checked")) {
                notif_wa = true;
            } else {
                notif_wa = false;
            }
            var notif_email = false;
            if ($("#notif_email").prop("checked")) {
                notif_email = true;
            } else {
                notif_email = false;
            }
            // Ambil validasi yang dipilih
            var validasi = [];
            if ($("#validasiRequired").prop("checked"))
                validasi.push("required");
            if ($("#validasiNumber").prop("checked")) validasi.push("number");
            if ($("#validasiDate").prop("checked")) validasi.push("date");
            if ($("#validasiFile").prop("checked")) validasi.push("file");

            // Validasi file jika jenis input adalah file
            var fileDetails = "";
            var maxFileSize = null;
            var allowedFileTypes = [];
            if (jenisInput === "file" && $("#validasiFile").prop("checked")) {
                maxFileSize = $("#ukuranFile").val(); // Ambil ukuran maksimal file
                // Ambil jenis file yang dipilih
                $(".form-check-input:checked").each(function () {
                    allowedFileTypes.push($(this).val());
                });
                fileDetails = {
                    maxFileSize: maxFileSize,
                    allowedFileTypes: allowedFileTypes,
                };
            }

            // Membuat elemen input sesuai dengan jenis input yang dipilih
            var newInput;
            if (jenisInput === "text") {
                newInput = `<input type="text" class="form-control"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" placeholder="${placeholderInput}" required data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "textarea") {
                newInput = `<textarea class="form-control"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" placeholder="${placeholderInput}" required data-validations="${validasi.join(
                    ","
                )}"></textarea>`;
            } else if (jenisInput === "date") {
                newInput = `<input type="date" class="form-control"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" required data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "number") {
                newInput = `<input type="number" class="form-control"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" placeholder="${placeholderInput}" required data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "file") {
                newInput = `<input type="file" class="form-control"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" required data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "email") {
                newInput = `<input type="email" class="form-control"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" placeholder="${placeholderInput}" required data-validations="${validasi.join(
                    ","
                )}">`;
            } else if (jenisInput === "select") {
                // Ambil opsi dari list_opsi
                var opsiList = [];
                $("#list_opsi .ui-state-default").each(function () {
                    opsiList.push($(this).find("span").text());
                });

                if (opsiList.length === 0) {
                    alert("Tambahkan opsi terlebih dahulu!");
                    return;
                }

                // Buat elemen select dengan opsi
                try {
                    // Ubah opsiList menjadi JSON string
                    var opsiListJSON = JSON.stringify(opsiList);

                    // Buat elemen select dengan opsi
                    var optionsHTML = opsiList
                        .map(
                            (opsi) => `<option value="${opsi}">${opsi}</option>`
                        )
                        .join("");
                    newInput = `<select class="form-select"  keterangan='${keteranganInput}' notif_wa='${notif_wa}' notif_email='${notif_email}' primary='${primer}' name="${namaInput}" data-opsi='${opsiListJSON}' data-validations="${validasi.join(
                        ","
                    )}" required>${optionsHTML}</select>`;
                } catch (e) {
                    console.error(
                        "Gagal mengubah opsi menjadi JSON:",
                        e.message
                    );
                    alert("Terjadi kesalahan saat memproses opsi. Coba lagi.");
                    return;
                }
            }

            var inputDiv = $("#" + editingId);
            inputDiv.html(`
                <label class="form-label">${labelInput}</label>
                ${newInput}
                <small class="form-text text-muted">${keteranganInput}</small>
                ${
                    validasi.length > 0
                        ? `<p class="text-muted">Validasi: ${validasi.join(
                              ", "
                          )}</p>`
                        : ""
                }
                ${
                    fileDetails
                        ? `<p class="text-muted">File Details: Max Size: ${
                              fileDetails.maxFileSize
                          } MB, Allowed Types: ${fileDetails.allowedFileTypes.join(
                              ", "
                          )}</p>`
                        : ""
                }
                <button type="button" class="btn btn-warning btn-sm edit-input-btn" data-id="${editingId}">Edit</button>
                <button type="button" class="btn btn-danger btn-sm delete-input-btn" data-id="${editingId}">Hapus</button>
            `);

            // Memperbarui JSON setelah perubahan
            updateJSON();

            // Tutup modal setelah perubahan
            $("#modalForm").modal("hide");
            resetForm();
            // Reset editingId
            editingId = null;
        }
    }
    $(document).on("click", ".delete-input-btn", function () {
        var uniqueId = $(this).data("id");
        $("#" + uniqueId).remove();
        updateJSON(); // Update JSON setelah input dihapus
    });

    // Reset form
    function resetForm() {
        $("#labelInput").val("");

        $("#namaInput").val("");
        $("#keteranganInput").val("");
        $("#placeholderInput").val("");
        $("#jenisInput").val("");
        $("#validasiRequired").prop("checked", false);
        $("#validasiNumber").prop("checked", false);
        $("#validasiDate").prop("checked", false);
        $("#validasiFile").prop("checked", false);
        $("#primary").prop("checked", false);
        $("#notif_wa").prop("checked", false);
        $("#notif_email").prop("checked", false);
    }

    // Update JSON yang ada di textarea
    function updateJSON() {
        var inputsData = [];
        $("#review_input_form .input-item").each(function () {
            var input = $(this);
            var inputData = {
                id: input.attr("id"),
                label: input.find("label").text(),
                name: input.find("input, textarea, select").attr("name"),
                keterangan: input
                    .find("input, textarea, select")
                    .attr("keterangan"),
                primary: input.find("input, textarea, select").attr("primary"),
                notif_wa: input
                    .find("input, textarea, select")
                    .attr("notif_wa"),
                notif_email: input
                    .find("input, textarea, select")
                    .attr("notif_email"),
                placeholder: input.find("input, textarea").attr("placeholder"),
                type: input.find("input, textarea, select").attr("type"),
                validations: input
                    .find("input, textarea, select")
                    .data("validations")
                    ? input
                          .find("input, textarea, select")
                          .data("validations")
                          .split(",")
                    : [],
            };

            if (input.find("select").length > 0) {
                inputData.type = "select";
                inputData.options = input.find("select").data("opsi");
            }

            inputsData.push(inputData);
        });
        $("#isi_form").val(JSON.stringify(inputsData, null, 2));
    }
    $(document).on("click", "#template_input", function () {
        // Memuat file JSON template
        $.getJSON(
            "/portal/js/template_form_pendaftaran.json",
            function (templateJSON) {
                // Iterasi dan buat input berdasarkan templateJSON
                templateJSON.forEach(function (item) {
                    // Buat elemen input berdasarkan tipe
                    var newInput;
                    if (item.type === "text") {
                        newInput = `<input type="text" class="form-control"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" placeholder="${
                            item.placeholder
                        }" required data-validations="${item.validations.join(
                            ","
                        )}">`;
                    } else if (item.type === "number") {
                        newInput = `<input type="number" class="form-control"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" placeholder="${
                            item.placeholder
                        }" required data-validations="${item.validations.join(
                            ","
                        )}">`;
                    } else if (item.type === "date") {
                        newInput = `<input type="date" class="form-control"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" required data-validations="${item.validations.join(
                            ","
                        )}">`;
                    } else if (item.type === "textarea") {
                        newInput = `<textarea class="form-control"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" placeholder="${
                            item.placeholder
                        }" required data-validations="${item.validations.join(
                            ","
                        )}"></textarea>`;
                    } else if (item.type === "file") {
                        newInput = `<input type="file" class="form-control"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" required data-validations="${item.validations.join(
                            ","
                        )}">`;
                    } else if (item.type === "email") {
                        newInput = `<input type="email" class="form-control"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" placeholder="${
                            item.placeholder
                        }" required data-validations="${item.validations.join(
                            ","
                        )}">`;
                    } else if (item.type === "select") {
                        // Buat elemen select dengan opsi
                        var optionsHTML = item.options
                            .map(
                                (option) =>
                                    `<option value="${option}">${option}</option>`
                            )
                            .join("");
                        newInput = `<select class="form-select"  notif_wa='${
                            item.notif_wa
                        }' notif_email='${item.notif_email}' keterangan='${
                            item.keterangan
                        }' primary='${item.primary}' name="${
                            item.name
                        }" data-opsi='${JSON.stringify(
                            item.options
                        )}' required data-validations="${item.validations.join(
                            ","
                        )}">${optionsHTML}</select>`;
                    }

                    // Menambahkan input baru ke div #review_input_form
                    var uniqueId = item.id || "input_" + Date.now();
                    var newInputHTML = `
                    <div class="mb-3 input-item" id="${uniqueId}">
                        <label class="form-label">${item.label}</label>
                        ${newInput}
                        <small class="form-text text-muted">${
                            item.keterangan || ""
                        }</small>
                        ${
                            item.validations.length > 0
                                ? `<p class="text-muted">Validasi: ${item.validations.join(
                                      ", "
                                  )}</p>`
                                : ""
                        }
                        <button type="button" class="btn btn-warning btn-sm edit-input-btn" data-id="${uniqueId}">Edit</button>
                        <button type="button" class="btn btn-danger btn-sm delete-input-btn" data-id="${uniqueId}">Hapus</button>
                    </div>
                    `;

                    // Menambahkan input baru ke dalam #review_input_form
                    $("#review_input_form").append(newInputHTML);
                });

                // Memperbarui JSON setelah template dimuat
                updateJSON();
            }
        ).fail(function () {
            alert("Gagal memuat template JSON.");
        });
    });

    $(document).on("click", "#simpan_input", function () {
        // Ambil data JSON dari textarea dengan ID #isi_form
        var formData = $("#isi_form").val();

        // Cek apakah data yang diambil adalah JSON yang valid
        try {
            var parsedData = JSON.parse(formData);
        } catch (e) {
            alert("Data JSON tidak valid!");
            return;
        }

        // Ambil CSRF token dari meta tag atau dari cookie
        var csrfToken = $("meta[name='csrf-token']").attr("content");

        // Jika tidak ada CSRF token, tampilkan pesan error
        if (!csrfToken) {
            alert("Token CSRF tidak ditemukan!");
            return;
        }

        // Kirimkan data JSON ke server menggunakan AJAX
        $.ajax({
            url: "/isi_form_webinar", // Endpoint server yang akan menerima data
            type: "POST",
            data: {
                _token: csrfToken, // CSRF token
                data: parsedData, // Data JSON yang dikirimkan
                id_webinar: id_webinar, // Tambahkan id_webinar
            },
            success: function (response) {
                // Tanggapan sukses
                alert("Data berhasil disimpan!");
                console.log(response);
            },
            error: function (xhr, status, error) {
                // Tanggapan error
                alert("Gagal mengirim data. Silakan coba lagi.");
                console.error(error);
            },
        });
    });

    $("#tambah_opsi").click(function (e) {
        e.preventDefault();

        var opsi = $("#opsi").val().trim();
        if (opsi === "") {
            alert("Masukkan opsi terlebih dahulu!");
            return;
        }

        // Elemen opsi baru dengan ID unik
        var listItem = `
            <div class="d-flex justify-content-between align-items-center mb-2 ui-state-default" id="item_${Date.now()}">
                <span>${opsi}</span>
                <button class="btn btn-danger btn-sm hapus-opsi">Hapus</button>
            </div>
        `;
        $("#list_opsi").append(listItem);
        $("#opsi").val("");
    });

    // Fungsi untuk menghapus opsi
    $("#list_opsi").on("click", ".hapus-opsi", function () {
        $(this).parent().remove();
    });

    // Aktifkan sortable pada #list_opsi
    $("#list_opsi").sortable({
        placeholder: "ui-state-highlight", // Tambahkan gaya saat drag
        stop: function (event, ui) {
            console.log("Posisi opsi diubah!");
        },
    });

    // Opsional: Tambahkan gaya ke elemen yang diseret
    $("#list_opsi").disableSelection();
    // $(document).on("click", "#list_opsi",".hapus_opsi", function (event) {
    //     event.preventDefault(); // Mencegah perilaku default tombol
    //     // $(this).parent().remove();
    //     $(this).parent().remove();
    // });
    const rupiahFormatter = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });
    $(".harga").on("input", function () {
        var value = $(this).val();
        // Menghapus karakter selain angka
        value = value.replace(/[^0-9]/g, "");
        // Mengubah nilai menjadi format Rupiah dan set kembali ke input
        $(this).val(rupiahFormatter.format(value));
    });
});

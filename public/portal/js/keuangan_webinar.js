function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}
function initializeDataTable() {
    $("#table").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/list_pengajuan_webinar/" + id,
            type: "GET",
        },
        columns: [
            { data: "id", name: "id" }, // Kolom untuk nomor urut
            { data: "tanggal_pengajuan", name: "tanggal_pengajuan" }, // Tanggal Pengajuan
            { data: "tanggal_validasi", name: "tanggal_validasi" }, // Tanggal Validasi
            {
                data: "jumlah",
                name: "jumlah",
                render: function (data) {
                    return new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0,
                    }).format(data);
                },
            }, // Format jumlah dengan currency
            { data: "status", name: "status" }, // Status
            {
                data: "aksi",
                name: "aksi",
                orderable: false,
                searchable: false,
            }, // Kolom untuk aksi (button atau link)
        ],
        order: [[1, "desc"]], // Urutkan berdasarkan Tanggal Pengajuan
    });
}

// Fungsi untuk reload tabel
function reloadTable() {
    $("#table").DataTable().ajax.reload(null, false); // Reload tanpa mengubah posisi paging
}
$(document).ready(function () {
    Fancybox.bind('[data-fancybox="gallery"]', {
        on: {
            ready: (fancybox) => {
                fancybox.$container.style.zIndex = "99999";
            },
        },
    });

    initializeDataTable();
    $("#info_peserta").on("click", function () {
        // Tampilkan preloader atau indikasi proses

        // Lakukan AJAX
        $.ajax({
            url: "/info_keuangan_webinar/" + id,
            type: "GET",
            success: function (response) {
                $("#infonya").empty();
                const formatCurrency = (value) =>
                    new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 0,
                    }).format(value);

                // Ambil data dari response
                const klasifikasiPaket = response.klasifikasi_paket;
                const uangMasuk = formatCurrency(response.uang_masuk);
                const donasi = formatCurrency(response.donasi);
                const biayaLayanan = formatCurrency(response.biaya_layanan);
                const uangTerkumpul = formatCurrency(response.uang_terkumpul);
                const uangPokokKeseluruhan = formatCurrency(
                    response.uangpokokkeseluruhan
                );
                const uangLebihKeseluruhan = formatCurrency(
                    response.uanglebihkeseluruhan
                );
                const historiPenarikan = formatCurrency(
                    response.histori_penarikan
                );
                const sisaUangYangDapatDitarik =
                    response.uang_terkumpul - response.histori_penarikan;

                // Tabel untuk klasifikasi paket
                let klasifikasiTable = `
    <h5>Klasifikasi Paket</h5>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Nama Paket</th>
                <th>Uang Pokok</th>
                <th>Uang Lebih</th>
                <th>Total Harga</th>
                <th>Jumlah Terjual</th>

            </tr>
        </thead>
        <tbody>
`;

                klasifikasiPaket.forEach((paket) => {
                    // Cek jika uang pokok atau uang lebih tidak 0
                    const uangPokok = paket.hargapokok || 0;
                    const uangLebih = paket.uanglebih || 0;

                    klasifikasiTable += `
        <tr>
            <td>${paket.namapaket}</td>

            <td>${
                uangPokok !== 0
                    ? `<span >${formatCurrency(uangPokok)}</span>`
                    : "-"
            }</td>
            <td>${
                uangLebih !== 0
                    ? `<span >${formatCurrency(uangLebih)}</span>`
                    : "-"
            }</td>
             <td>${formatCurrency(paket.total_harga)}</td>
            <td>${paket.jumlah_terjual}</td>
        </tr>
    `;
                });

                klasifikasiTable += `
        </tbody>
    </table>
`;

                // Tabel untuk data lainnya
                const infoTable = `
                <h5>Informasi Keuangan</h5>

                <!-- Tabel 1 -->
                <table class="table table-bordered" style="width: 100%; table-layout: fixed;">
                    <tbody>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Uang Masuk</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${uangMasuk}</td>
                        </tr>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Donasi (optional)</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${donasi}</td>
                        </tr>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Biaya Layanan</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${biayaLayanan}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- Tabel 2 -->
                <table class="table table-bordered" style="width: 100%; table-layout: fixed; margin-top: 15px;">
                    <tbody>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Uang Terkumpul Dan Dapat Ditarik</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${uangTerkumpul}</td>
                        </tr>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Uang Pokok Keseluruhan</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${uangPokokKeseluruhan}</td>
                        </tr>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Uang Lebih</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${uangLebihKeseluruhan}</td>
                        </tr>
                    </tbody>
                </table>

                <!-- Tabel 3 -->
                <table class="table table-bordered" style="width: 100%; table-layout: fixed; margin-top: 15px;">
                    <tbody>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Sisa Uang Yang Dapat Ditarik</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${formatCurrency(
                                sisaUangYangDapatDitarik
                            )}</td>
                        </tr>
                        <tr>
                            <th style="width: 50%; text-align: left; padding: 10px;">Histori Penarikan</th>
                            <td style="width: 50%; text-align: left; padding: 10px;">${historiPenarikan}</td>
                        </tr>
                    </tbody>
                </table>
            `;

                // Masukkan tabel ke div dengan id 'info'
                $("#infonya").html(klasifikasiTable + infoTable);
                $("#info_keuangan_modal").modal("show");
            },
            error: function (xhr, status, error) {
                console.error("Error:", error);

                Swal.fire({
                    icon: "error",
                    title: "Oops...",
                    text: "Terjadi kesalahan saat memuat data. Silakan coba lagi.",
                });
            },
        });
    });

    $("#kirimPesanCard").on("click", function () {
        $("#pengajuan_penarikan_modal").modal("show");
    });

    $("#jumlah").on("input", function () {
        let value = this.value.replace(/[^0-9]/g, ""); // Hanya angka
        if (value) {
            this.value = new Intl.NumberFormat("id-ID", {
                style: "currency",
                currency: "IDR",
                minimumFractionDigits: 0,
                maximumFractionDigits: 0,
            }).format(value);
        } else {
            this.value = "";
        }
    });

    $("#form_pengajuan_penarikan").on("submit", function (e) {
        e.preventDefault(); // Mencegah submit form secara default

        const jumlah = $("#jumlah")
            .val()
            .replace(/[^0-9]/g, ""); // Ambil nilai jumlah tanpa format
        const keterangan = $("#keterangan").val(); // Ambil nilai keterangan
        const csrfToken = $('meta[name="csrf-token"]').attr("content"); // Ambil CSRF token

        $.ajax({
            url: "/pengajuan_pencairan_webinar", // Endpoint tujuan
            method: "POST",
            data: {
                _token: csrfToken, // Sertakan CSRF token
                id: id,
                jumlah: jumlah,
                keterangan: keterangan,
            },
            beforeSend: function () {
                Swal.fire({
                    title: "Sedang Mengajukan...",
                    text: "Mohon tunggu",
                    didOpen: () => Swal.showLoading(),
                });
            },
            success: function (response) {
                reloadTable();
                if (response.success) {
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil",
                        text:
                            response.message || "Pengajuan berhasil dilakukan",
                    });
                    $("#pengajuan_penarikan_modal").modal("hide");
                    $("#form_pengajuan_penarikan")[0].reset();
                    $("#jumlah").trigger("input");
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: response.message || "Pengajuan Gagal dilakukan",
                    });
                }
            },
            error: function (xhr) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: xhr.responseJSON?.message || "Terjadi kesalahan",
                });
            },
        });
    });
    $(document).on("click", ".batal_pengajuan", function () {
        let id = $(this).data("id"); // Ambil id dari data-id
        let url = `/batal_pengajuan/${id}`; // Susun URL dengan id

        Swal.fire({
            title: "Apakah Anda yakin?",
            text: "Pengajuan ini akan dibatalkan!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, batalkan!",
            cancelButtonText: "Tidak, kembali",
        }).then((result) => {
            if (result.isConfirmed) {
                loader(true);
                $.ajax({
                    url: url,
                    type: "POST",
                    data: {
                        _token: $('meta[name="csrf-token"]').attr("content"), // CSRF Token
                    },
                    success: function (response) {
                        loader(false);
                        reloadTable();
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Pengajuan berhasil dibatalkan.",
                        });
                    },
                    error: function (xhr) {
                        loader(false);
                        Swal.fire({
                            icon: "error",
                            title: "Gagal",
                            text: "Terjadi kesalahan saat membatalkan pengajuan.",
                        });
                    },
                });
            }
        });
    });
    $(document).on("click", ".detail_pengajuan", function () {
        let id = $(this).data("id"); // Ambil ID dari data-id
        let url = `/detail_pengajuan/${id}`; // Buat URL dengan ID

        $("#detailPengajuanContent").empty();
        $("#form_validasi").empty();
        loader(true);

        $.ajax({
            url: url,
            type: "GET",
            success: function (response) {
                if (response.success) {
                    const data = response.data;

                    // Format jumlah menggunakan Intl.NumberFormat
                    const formattedJumlah = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0, // Menghapus desimal
                        maximumFractionDigits: 0,
                    }).format(data.jumlah);

                    // Konversi status
                    const statusMapping = {
                        N: "Diajukan",
                        T: "Ditolak",
                        Y: "Dikirim",
                        P: "Diproses",
                    };
                    const statusText =
                        statusMapping[data.status] || "Tidak Diketahui";

                    // Isi konten modal
                    const content = `
                            <table class="table table-bordered">
                                <tr>
                                    <th>Tanggal Pengajuan</th>
                                    <td>${data.tanggal_pengajuan}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Validasi</th>
                                    <td>${
                                        data.tanggal_validasi ||
                                        "Belum Validasi"
                                    }</td>
                                </tr>
                                <tr>
                                    <th>Jenis</th>
                                    <td>${data.jenis}</td>
                                </tr>
                                <tr>
                                    <th>Parameter</th>
                                    <td>${response.parameter}</td>
                                </tr>
                                <tr>
                                    <th>Keterangan</th>
                                    <td>${data.keterangan}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah</th>
                                    <td>${formattedJumlah}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>${statusText}</td>
                                </tr>
                                <tr>
                                    <th>Keterangan Bendahara</th>
                                    <td>${
                                        data.keterangan_bendahara || "Belum Ada"
                                    }</td>
                                </tr>
                            </table>
                            ${
                                data.bukti
                                    ? `
                                <a href="data:image/jpeg;base64,${data.bukti}" data-fancybox="gallery">
                                    <img src="data:image/jpeg;base64,${data.bukti}" alt="Bukti" class="img-thumbnail" style="max-width: 200px;">
                                </a>
                            `
                                    : '<p class="text-muted">Belum ada bukti tersedia.</p>'
                            }
                        `;

                    $("#detailPengajuanContent").html(content);

                    $("#detailPengajuanContent").html(content);

                    // Isi form validasi

                    // Tampilkan modal
                    $("#modal_detail_pengajuan").modal("show");
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: "Data tidak ditemukan.",
                    });
                }

                // Sembunyikan preloader (opsional)
                loader(false);
            },
            error: function (xhr) {
                console.error(xhr);

                // Sembunyikan preloader (opsional)
                loader(false);

                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: "Terjadi kesalahan saat memuat detail pengajuan.",
                });
            },
        });
    });
});

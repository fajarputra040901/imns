var configData = []; // Array untuk menyimpan data konfigurasi kotak teks
var selectedTextBox = null; // Menyimpan kotak teks yang dipilih untuk pengaturan

// Event listener untuk form upload gambar
document.getElementById('upload-form').addEventListener('submit', function(e) {
    e.preventDefault();
    var fileInput = document.getElementById('certificate_template').files[0];

    if (fileInput) {
        var reader = new FileReader(); // Membaca gambar

        reader.onloadend = function() {
            var img = document.getElementById('certificate-image');
            img.src = reader.result;
        };

        // Membaca gambar sebagai data URL (base64)
        reader.readAsDataURL(fileInput);
    }
});

// Event listener untuk menambahkan kotak teks
document.getElementById('add-text-box').addEventListener('click', function() {
    var label = prompt("Enter label (e.g., Name, Date):");

    // Membuat objek konfigurasi kotak teks
    var box = {
        x: 50,
        y: 50,
        width: 200,
        height: 50,
        label: label,
        font: "20px Arial",
        color: "black",
        text: label
    };

    // Simpan kotak teks ke dalam array konfigurasi
    configData.push(box);

    // Membuat elemen div untuk kotak teks
    var textBoxDiv = document.createElement('div');
    textBoxDiv.classList.add('text-box');
    textBoxDiv.contentEditable = true;
    textBoxDiv.style.position = 'absolute';
    textBoxDiv.style.left = box.x + 'px';
    textBoxDiv.style.top = box.y + 'px';
    textBoxDiv.style.width = box.width + 'px';
    textBoxDiv.style.height = box.height + 'px';
    textBoxDiv.style.border = '1px solid black';
    textBoxDiv.style.font = box.font;
    textBoxDiv.style.color = box.color;
    textBoxDiv.textContent = label;

    // Tambahkan kotak teks ke dalam container sertifikat
    document.getElementById('certificate-container').appendChild(textBoxDiv);

    // Gunakan interact.js untuk membuat kotak teks bisa di drag dan resize
    interact(textBoxDiv)
        .draggable({
            onmove(event) {
                var x = box.x + event.dx;
                var y = box.y + event.dy;

                // Update posisi kotak teks
                box.x = x;
                box.y = y;

                // Update posisi div
                textBoxDiv.style.left = x + 'px';
                textBoxDiv.style.top = y + 'px';

                // Gambar ulang konfigurasi
                updateConfig();
            }
        })
        .resizable({
            edges: { top: true, left: true, bottom: true, right: true },
            onmove(event) {
                var width = event.rect.width;
                var height = event.rect.height;

                // Update ukuran kotak teks
                box.width = width;
                box.height = height;

                // Update ukuran div
                textBoxDiv.style.width = width + 'px';
                textBoxDiv.style.height = height + 'px';

                // Gambar ulang konfigurasi
                updateConfig();
            }
        });

    // Update konfigurasi dan tampilkan dalam textarea
    updateConfig();

    // Tampilkan panel pengaturan dan pilih kotak teks
    selectedTextBox = textBoxDiv;
    showTextSettings(box);
});

// Fungsi untuk memperbarui dan menampilkan konfigurasi dalam textarea
function updateConfig() {
    document.getElementById('config').value = JSON.stringify(configData, null, 2);
}

// Fungsi untuk menampilkan pengaturan teks berdasarkan kotak yang dipilih
function showTextSettings(box) {
    document.getElementById('text-settings').style.display = 'block';

    // Mengisi pengaturan berdasarkan box yang dipilih
    document.getElementById('font-family').value = box.font.split(' ')[1]; // Ambil font name
    document.getElementById('font-size').value = parseInt(box.font.split(' ')[0]); // Ambil font size
    document.getElementById('font-color').value = box.color;
    document.getElementById('text-align').value = 'left'; // Default alignment
    document.getElementById('text-content').value = box.text;
}

// Event listener untuk menerapkan pengaturan teks
document.getElementById('apply-settings').addEventListener('click', function() {
    if (selectedTextBox) {
        var fontFamily = document.getElementById('font-family').value;
        var fontSize = document.getElementById('font-size').value + "px";
        var fontColor = document.getElementById('font-color').value;
        var textAlign = document.getElementById('text-align').value;
        var textContent = document.getElementById('text-content').value;

        // Mengupdate konfigurasi kotak teks
        selectedTextBox.style.font = fontSize + " " + fontFamily;
        selectedTextBox.style.color = fontColor;
        selectedTextBox.style.textAlign = textAlign;
        selectedTextBox.textContent = textContent;

        // Update kotak teks dalam array konfigurasi
        var index = configData.findIndex(function(box) {
            return box.label === selectedTextBox.textContent; // Cocokkan label
        });

        if (index !== -1) {
            configData[index].font = fontSize + " " + fontFamily;
            configData[index].color = fontColor;
            configData[index].text = textContent;
        }

        // Gambar ulang konfigurasi
        updateConfig();
    }
});

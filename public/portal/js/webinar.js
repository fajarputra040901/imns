var webinars = []; // Array untuk menyimpan semua webinar
var currentPage = 1; // Halaman saat ini
var itemsPerPage = 5;
function loadWebinars() {
    loader(true);
    $.ajax({
        url: "/list_webinar", // Ganti dengan URL API yang sesuai
        method: "GET",
        dataType: "json",
        success: function (response) {
            if (response.status === "success") {
                webinars = response.data; // Menyimpan semua data webinar
                displayWebinars(); // Menampilkan webinar pertama kali
            }
        },
        error: function () {
            Swal.fire({
                icon: "error",
                title: "Galat",
                text: "Terjadi kesalahan saat memuat data webinar.",
            });
        },
    });
}

// Fungsi untuk menampilkan webinar di halaman
function displayWebinars() {
    $("#list_webinar").empty();
    var filteredWebinars = webinars; // Data webinar yang telah difilter berdasarkan pencarian
    var search = $("#pencarian_webinar").val().toLowerCase();

    // Filter webinar berdasarkan pencarian
    if (search) {
        filteredWebinars = webinars.filter(function (webinar) {
            return (
                webinar.judul.toLowerCase().includes(search) ||
                webinar.jenis.toLowerCase().includes(search)
            );
        });
    }

    // Paginate filtered webinars
    var totalItems = filteredWebinars.length;
    var totalPages = Math.ceil(totalItems / itemsPerPage);
    var start = (currentPage - 1) * itemsPerPage;
    var end = start + itemsPerPage;
    var pageWebinars = filteredWebinars.slice(start, end);

    // Menampilkan card webinar
    var output = "";
    if (pageWebinars.length > 0) {
        pageWebinars.forEach(function (webinar) {
            let startDate = moment(webinar.tanggal_mulai).format("DD-MM-YYYY");
            let endDate = moment(webinar.tanggal_selesai).format("DD-MM-YYYY");
            let dateRange =
                startDate === endDate
                    ? startDate
                    : startDate + " s/d " + endDate;
            let totalDays =
                moment(webinar.tanggal_selesai).diff(
                    moment(webinar.tanggal_mulai),
                    "days"
                ) + 1;

            output += `
                    <div class="webinar-card mb-3 card_list_webinar" data-id="${webinar.id}" style="cursor: pointer;">
                <div class="webinar-card-body">
                    <div class="webinar-card-header d-flex justify-content-between">
                        <div>
                            <p class="mb-1">${dateRange} | ${totalDays} Hari</p>

                        </div>
                        <div class="webinar-card-icon">
                            <i class="bx bx-right-arrow-alt" style="font-size: 24px; color: #007bff;"></i>
                        </div>
                    </div>
                    <h5 class="webinar-card-title">${webinar.judul}</h5>
                    <p class="webinar-card-text text-muted">${webinar.jenis} | ${webinar.peserta} Peserta</p>
                </div>
            </div>

                `;
        });
    } else {
        output = `<p>Belum ada webinar / seminar yang terjadwal.</p>`;
    }

    // Menampilkan card ke div
    $("#list_webinar").html(output);
    loader(false);

    // Menampilkan pagination
    var pagination = "";
    if (totalPages > 1) {
        pagination += `<button class="btn btn-secondary" id="prevPage" ${
            currentPage === 1 ? "disabled" : ""
        }>Previous</button>`;
        pagination += `<span class="mx-2">Page ${currentPage} of ${totalPages}</span>`;
        pagination += `<button class="btn btn-secondary" id="nextPage" ${
            currentPage === totalPages ? "disabled" : ""
        }>Next</button>`;
    }

    $("#pagination").html(pagination);

    // Menangani klik pada card
    $(".card_list_webinar").on("click", function () {
        let webinarId = $(this).data("id");
        window.location.href = `/config_webinar/${webinarId}`; // Ganti dengan URL untuk melihat detail webinar
    });

    // Menangani pagination
    $("#prevPage").on("click", function () {
        if (currentPage > 1) {
            currentPage--;
            displayWebinars();
        }
    });
    $("#nextPage").on("click", function () {
        if (currentPage < totalPages) {
            currentPage++;
            displayWebinars();
        }
    });
}
function loader(show) {
    if (show) {
        $(".overlay").show(); // Menampilkan preloader
        $(".preloader").show();
    } else {
        $(".overlay").hide(); // Menyembunyikan preloader
        $(".preloader").hide(); // Menyembunyikan preloader
    }
}

$(document).ready(function () {
    loadWebinars();
    CKEDITOR.replace("deskripsi");
    $("#pencarian_webinar").on("keyup", function () {
        currentPage = 1; // Reset ke halaman pertama
        displayWebinars();
    });
    // Event listener untuk membuka modal
    $("#btn_tambah_webinar").on("click", function () {
        $("#modal_tambah_webinar").modal("show");
    });

    $('input[name="tanggal_mulai"], input[name="tanggal_selesai"]').datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true,
    });
    $('input[name="tanggal_mulai"], input[name="tanggal_selesai"]').on(
        "input",
        function () {
            var val = $(this).val();
            var formatted = val.replace(/[^0-9]/g, "").slice(0, 8); // Ambil hanya angka dan batasi panjangnya
            $(this).val(formatted.replace(/(\d{2})(\d{2})(\d{4})/, "$1-$2-$3")); // Format dengan '-'
        }
    );

    // Fungsi untuk memformat tanggal ke dd-mm-yyyy

    // Tangkap submit form
    $("#form_tambah_webinar").on("submit", function (e) {
        e.preventDefault(); // Mencegah form submit default

        // Validasi: Periksa apakah semua field terisi
        var valid = true;
        var errorMessage = "";

        // Validasi Tanggal Mulai dan Selesai (format dd-mm-yyyy)
        var tanggalMulai = $('input[name="tanggal_mulai"]').val();
        var tanggalSelesai = $('input[name="tanggal_selesai"]').val();
        var datePattern = /^(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[0-2])-\d{4}$/; // dd-mm-yyyy format

        if (!tanggalMulai || !tanggalSelesai) {
            valid = false;
            errorMessage = "Tanggal Mulai dan Tanggal Selesai harus diisi.";
        } else if (
            !datePattern.test(tanggalMulai) ||
            !datePattern.test(tanggalSelesai)
        ) {
            valid = false;
            errorMessage = "Format tanggal harus dd-mm-yyyy.";
        }

        // Validasi input lainnya
        var judul = $('input[name="judul"]').val();
        var jenisWebinar = $('select[name="jenis"]').val();
        var jumlahPeserta = $('input[name="peserta"]').val();
        var jumlahSkp = $('input[name="skp"]').val();
        var deskripsi = CKEDITOR.instances.deskripsi.getData(); // Ambil data dari CKEditor

        if (
            !judul ||
            !jenisWebinar ||
            !jumlahPeserta ||
            !jumlahSkp ||
            !deskripsi
        ) {
            valid = false;
            errorMessage = "Semua field harus diisi.";
        }

        // Jika validasi gagal, tampilkan SweetAlert error
        if (!valid) {
            Swal.fire({
                icon: "error",
                title: "Gagal",
                text: errorMessage,
            });
            return; // Hentikan eksekusi jika validasi gagal
        }

        // Jika validasi sukses, tampilkan SweetAlert konfirmasi
        Swal.fire({
            title: "Konfirmasi",
            text: "Apakah Anda yakin ingin menyimpan data ini?",
            icon: "question",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Simpan",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                // Kirim form dengan AJAX
                let formData = $(this).serialize(); // Ambil data form
                loader(true);
                $.ajax({
                    url: "/tambah_webinar", // Ganti dengan endpoint Anda
                    type: "POST",
                    data: formData,
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ), // Menambahkan CSRF token di header
                    },
                    success: function (response) {
                        loadWebinars();
                        loader(false);
                        // Tampilkan pesan sukses
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Data webinar berhasil disimpan!",
                        }).then(() => {
                            // Tutup modal dan reset form
                            $("#modal_tambah_webinar").modal("hide");
                            $("#form_tambah_webinar")[0].reset();
                            CKEDITOR.instances.deskripsi.setData(""); // Reset CKEditor
                        });
                    },
                    error: function (xhr) {
                        // Tampilkan pesan error
                        Swal.fire({
                            icon: "error",
                            title: "Gagal",
                            text: "Terjadi kesalahan saat menyimpan data!",
                        });
                        console.error(xhr.responseText);
                    },
                });
            }
        });
    });
});

function loader(show) {
    if (show) {
        $(".overlay").show(); // Menampilkan preloader
        $(".preloader").show();
    } else {
        $(".overlay").hide(); // Menyembunyikan preloader
        $(".preloader").hide(); // Menyembunyikan preloader
    }
}

function get_email() {
    loader(true);
    $.ajax({
        url: "/get_email",
        type: "GET",
        success: function(response) {
            loader(false);
            if (response.success) {
                // Mengisi select dengan ID 'name_email'
                let emailSelect = $("#name_email");
                emailSelect.empty(); // Kosongkan opsi sebelumnya

                // Menambahkan opsi 'Pilih' dengan val ""
                emailSelect.append(new Option("Pilih Email", ""));

                response.data.forEach(function(email) {
                    // Menambahkan option dengan id sebagai value dan email sebagai tampilan
                    emailSelect.append(new Option(email.email, email.id));
                });
            } else {
                console.error("Gagal mengambil data email");
            }
        },
        error: function(error) {
            loader(false);
            console.error("Terjadi kesalahan:", error);
        },
    });
}

$(document).ready(function() {
    get_email()
    $('#import_card').on('click', function() {
        $('#modal_import').modal('show');
    });
    $('#jenis').on('change', function() {
        if ($(this).val() === 'email') {
            $('#subjek_email').show(); 
            $('#div_email').show();
            $('#fileInputWrapperEmail').show();
        } else {
            $('#subjek_email').hide(); 
            $('#div_email').hide();
            $('#fileInputWrapperEmail').hide();
        }
    });
    $('#template_whatsapp').on('click', function() {

        const data = [
            ['Nama', 'No Telp'],
            ['Contoh Nama', '081234567890'],

        ];


        const worksheet = XLSX.utils.aoa_to_sheet(data);


        const colWidths = data[0].map(function(_, colIndex) {
            return {
                wch: Math.max(...data.map(row => String(row[colIndex] || '')
                    .length))
            };
        });


        worksheet['!cols'] = colWidths;


        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Template');


        XLSX.writeFile(workbook, 'template_whatsapp.xlsx');
    });
    $('#template_email').on('click', function() {

        const data = [
            ['Nama', 'Email'],
            ['Contoh Nama', 'contoh@gmail.com'],

        ];


        const worksheet = XLSX.utils.aoa_to_sheet(data);


        const colWidths = data[0].map(function(_, colIndex) {
            return {
                wch: Math.max(...data.map(row => String(row[colIndex] || '')
                    .length))
            };
        });


        worksheet['!cols'] = colWidths;


        const workbook = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(workbook, worksheet, 'Template');


        XLSX.writeFile(workbook, 'template_email.xlsx');
    });
    let currentPage = 1;
    const itemsPerPage = 5;
    let jsonData = [];

    // Fungsi untuk menampilkan data berdasarkan halaman
    function renderTable(page = 1, searchQuery = '') {
        const start = (page - 1) * itemsPerPage;
        const filteredData = jsonData.filter(row =>
            row[0]?.toLowerCase().includes(searchQuery.toLowerCase()) ||
            row[1]?.toLowerCase().includes(searchQuery.toLowerCase())
        );
        const paginatedData = filteredData.slice(start, start + itemsPerPage);
    
        let tableHTML = '<table class="table table-bordered" id="kontak_table">';
        tableHTML += '<thead><tr><th>Nama</th><th>No Telp / Email</th><th>Aksi</th></tr></thead><tbody>';
        paginatedData.forEach((row, index) => {
            tableHTML += `
            <tr data-index="${start + index}">
                <td>${row[0] || ''}</td>
                <td>${row[1] || ''}</td>
                <td><button class="btn btn-danger btn-sm hapus">Hapus</button></td>
            </tr>`;
        });
        tableHTML += '</tbody></table>';
    
        // Tampilkan tabel
        $('#list_kontak').html(tableHTML);
    
        // Tampilkan pagination
        renderPagination(filteredData.length, page);
    }

    // Fungsi untuk menampilkan pagination
   // Fungsi untuk menampilkan pagination
function renderPagination(totalItems, currentPage) {
    const totalPages = Math.ceil(totalItems / itemsPerPage);
    const maxPageLinks = 5; // Jumlah halaman yang akan ditampilkan

    let startPage = Math.max(1, currentPage - Math.floor(maxPageLinks / 2));
    let endPage = Math.min(totalPages, currentPage + Math.floor(maxPageLinks / 2));

    // Sesuaikan startPage dan endPage jika kurang dari maxPageLinks
    if (endPage - startPage < maxPageLinks - 1) {
        startPage = Math.max(1, endPage - maxPageLinks + 1);
    }

    let paginationHTML = '<nav><ul class="pagination justify-content-center">';

    // Tambahkan tombol Previous
    if (currentPage > 1) {
        paginationHTML += `<li class="page-item"><a class="page-link" href="#" data-page="${currentPage - 1}">&laquo;</a></li>`;
    }

    // Tampilkan halaman yang sesuai dengan batasan
    for (let i = startPage; i <= endPage; i++) {
        paginationHTML += `<li class="page-item ${i === currentPage ? 'active' : ''}">
            <a class="page-link" href="#" data-page="${i}">${i}</a>
        </li>`;
    }

    // Tambahkan tombol Next
    if (currentPage < totalPages) {
        paginationHTML += `<li class="page-item"><a class="page-link" href="#" data-page="${currentPage + 1}">&raquo;</a></li>`;
    }

    paginationHTML += '</ul></nav>';
    $('#pagination').html(paginationHTML);
}


    // Upload file
    $('#upload').on('click', function() {
        loader(true)
        const fileInput = $('#file')[0].files[0];

        if (!fileInput) {
            loader(false)
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Silakan pilih file terlebih dahulu!',
            });
            return;
        }

        const fileName = fileInput.name;
        const fileExtension = fileName.split('.').pop().toLowerCase();
        if (fileExtension !== 'xlsx') {
            loader(false)
            Swal.fire({
                icon: 'error',
                title: 'File tidak valid!',
                text: 'File harus berformat .xlsx.',
            });
            return;
        }

        const reader = new FileReader();
        reader.onload = function(e) {
            const data = new Uint8Array(e.target.result);
            const workbook = XLSX.read(data, {
                type: 'array'
            });

            const sheetName = workbook.SheetNames[0];
            const sheet = workbook.Sheets[sheetName];

            jsonData = XLSX.utils.sheet_to_json(sheet, {
                header: 1
            });
            jsonData.shift();

            if (jsonData.length > 0) {
                loader(false)
                renderTable(currentPage);
                Swal.fire({
                    icon: 'success',
                    title: 'Berhasil!',
                    text: 'Data berhasil diunggah dan ditampilkan.',
                });
            } else {
                loader(false)
                Swal.fire({
                    icon: 'warning',
                    title: 'Tidak ada data!',
                    text: 'File tidak memiliki data yang valid.',
                });
            }
        };

        reader.readAsArrayBuffer(fileInput);
    });

    // Tombol hapus
    $('#list_kontak').on('click', '.hapus', function() {
        loader(true)
        const index = $(this).closest('tr').data('index');
        jsonData.splice(index, 1);
        renderTable(currentPage);
        loader(false)
        Swal.fire({
            icon: 'success',
            title: 'Berhasil!',
            text: 'Data berhasil dihapus.',
        });
    });

    // Pagination klik
    $(document).on('click', '.page-link', function(e) {
        e.preventDefault();
        const page = $(this).data('page'); // Ambil nomor halaman dari data-page
    
        if (page) {
            currentPage = page;
            renderTable(currentPage, $('#search').val()); // Render table sesuai halaman
        }
    });

    // Kolom pencarian
    $('#search').on('input', function() {
        const searchQuery = $(this).val();
        renderTable(currentPage, searchQuery);
    });
    
    $('#kirim_pesan').on('click', function() {
        // Trigger submit form
        $('#form_blast').submit();
    });
    $("#form_blast").on("submit", function (e) {
        e.preventDefault();
        loader(true);
    
        // Ambil data dari form
        const jenis = $('#jenis').val();
        const email = $('#name_email').val();
        const subjek = $('#subjek').val();
        const isi_pesan = CKEDITOR.instances.isi_pesan.getData(); // Ambil data dari CKEditor
    
        // Ambil list kontak dari tabel #list_kontak
        const kontakList = [];
        $('#kontak_table tbody tr').each(function() {
            const nama = $(this).find('td').eq(0).text(); // Ambil nama
            const telp = $(this).find('td').eq(1).text(); // Ambil no telp
            if (nama && telp) {
                kontakList.push({
                    nama,
                    telp
                });
            }
        });
    
        // Validasi data
        if (!jenis || !isi_pesan || kontakList.length === 0) {
            loader(false);
            Swal.fire({
                icon: 'error',
                title: 'Data Tidak Lengkap',
                text: 'Pastikan semua data terisi dengan benar.',
            });
            return;
        }
    
        let formData = new FormData(this);
        formData.append(
            "isi_pesan",
            CKEDITOR.instances.isi_pesan.getData()
        )
       
        formData.append('kontak', JSON.stringify(kontakList)); // Kirim kontakList sebagai JSON
    
       
        for (let pair of formData.entries()) {
            console.log(pair[0] + ': ' + pair[1]);
        }
    
        $.ajax({
            type: 'POST',
            url: '/blast_pesan',
            data: formData,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // Ambil CSRF token dari meta tag
            },
            processData: false,  // Jangan proses data otomatis
            contentType: false,  // Jangan set contentType karena FormData akan menangani itu
            success: function(response) {
                loader(false);
                if (response.sukses) {
                    console.log(response);
                    Swal.fire({
                        icon: 'success',
                        title: 'Pesan Terkirim',
                        text: 'Pesan berhasil dikirimkan ke kontak.',
                    });
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Gagal Kirim Pesan',
                        text: response.error || 'Terjadi kesalahan.',
                    });
                }
            },
            error: function() {
                loader(false);
                Swal.fire({
                    icon: 'error',
                    title: 'Terjadi Kesalahan',
                    text: 'Pesan gagal dikirim.',
                });
            }
        });
    });
    
});
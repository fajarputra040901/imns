function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}
function initializeDataTable() {
    $("#table").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: "/list_permintaan_pencairan",
            type: "GET",
        },
        columns: [
            { data: "id", name: "id" }, // Kolom untuk nomor urut
            { data: "tanggal_pengajuan", name: "tanggal_pengajuan" }, // Tanggal Pengajuan
            { data: "tanggal_validasi", name: "tanggal_validasi" }, // Tanggal Validasi
            { data: "jenis", name: "jenis" }, // Status
            { data: "parameter", name: "parameter" }, // Status
            {
                data: "jumlah",
                name: "jumlah",
                render: function (data) {
                    return new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0,
                    }).format(data);
                },
            }, // Format jumlah dengan currency
            { data: "status", name: "status" }, // Status
            { data: "keterangan", name: "keterangan" }, // Status
            {
                data: "aksi",
                name: "aksi",
                orderable: false,
                searchable: false,
            }, // Kolom untuk aksi (button atau link)
        ],
        order: [[1, "desc"]], // Urutkan berdasarkan Tanggal Pengajuan
    });
}

// Fungsi untuk reload tabel
function reloadTable() {
    $("#table").DataTable().ajax.reload(null, false); // Reload tanpa mengubah posisi paging
}

$(document).ready(function () {
    Fancybox.bind('[data-fancybox="gallery"]', {
        on: {
            ready: (fancybox) => {
                fancybox.$container.style.zIndex = "99999";
            },
        },
    });

    initializeDataTable();

    $(document).on("click", ".detail_pengajuan", function () {
        let id = $(this).data("id"); // Ambil ID dari data-id
        let url = `/detail_pengajuan/${id}`; // Buat URL dengan ID

        $("#detailPengajuanContent").empty();
        $("#form_validasi").empty();
        loader(true);

        $.ajax({
            url: url,
            type: "GET",
            success: function (response) {
                if (response.success) {
                    const data = response.data;

                    // Format jumlah menggunakan Intl.NumberFormat
                    const formattedJumlah = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0, // Menghapus desimal
                        maximumFractionDigits: 0,
                    }).format(data.jumlah);

                    // Konversi status
                    const statusMapping = {
                        N: "Diajukan",
                        T: "Ditolak",
                        Y: "Dikirim",
                        P: "Diproses",
                    };
                    const statusText =
                        statusMapping[data.status] || "Tidak Diketahui";

                    // Isi konten modal
                    const content = `
                            <table class="table table-bordered">
                                <tr>
                                    <th>Tanggal Pengajuan</th>
                                    <td>${data.tanggal_pengajuan}</td>
                                </tr>
                                <tr>
                                    <th>Tanggal Validasi</th>
                                    <td>${
                                        data.tanggal_validasi ||
                                        "Belum Validasi"
                                    }</td>
                                </tr>
                                <tr>
                                    <th>Jenis</th>
                                    <td>${data.jenis}</td>
                                </tr>
                                <tr>
                                    <th>Parameter</th>
                                    <td>${response.parameter}</td>
                                </tr>
                                <tr>
                                    <th>Keterangan</th>
                                    <td>${data.keterangan}</td>
                                </tr>
                                <tr>
                                    <th>Jumlah</th>
                                    <td>${formattedJumlah}</td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>${statusText}</td>
                                </tr>
                                <tr>
                                    <th>Keterangan Bendahara</th>
                                    <td>${
                                        data.keterangan_bendahara || "Belum Ada"
                                    }</td>
                                </tr>
                            </table>
                        `;
                    $("#detailPengajuanContent").html(content);

                    $("#detailPengajuanContent").html(content);

                    // Isi form validasi
                    const formContent = `
                      <input type="hidden" id="id_pencairan" name="id_pencairan" value="${
                          data.id
                      }">

                    <div class="mb-3">
                        <label for="status" class="form-label">Status</label>
                        <select id="status" name="status" class="form-select">
                            <option value="N" ${
                                data.status === "N" ? "selected" : ""
                            }>Diajukan</option>
                            <option value="T" ${
                                data.status === "T" ? "selected" : ""
                            }>Ditolak</option>
                            <option value="Y" ${
                                data.status === "Y" ? "selected" : ""
                            }>Dikirim</option>
                            <option value="P" ${
                                data.status === "P" ? "selected" : ""
                            }>Diproses</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="keterangan_bendahara" class="form-label">Keterangan Bendahara</label>
                        <textarea id="keterangan_bendahara" name="keterangan_bendahara" class="form-control">${
                            data.keterangan_bendahara || ""
                        }</textarea>
                    </div>
                    <div class="mb-3">
                        <label for="bukti" class="form-label">Bukti</label>
                        <input type="file" id="bukti" name="bukti" class="form-control">
                    </div>
                    <div class="mb-3">
                        ${
                            data.bukti
                                ? `
                            <a href="data:image/jpeg;base64,${data.bukti}" data-fancybox="gallery">
                                <img src="data:image/jpeg;base64,${data.bukti}" alt="Bukti" class="img-thumbnail" style="max-width: 200px;">
                            </a>
                        `
                                : '<p class="text-muted">Belum ada bukti tersedia.</p>'
                        }
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                `;
                    $("#form_validasi").html(formContent);

                    // Tampilkan modal
                    $("#modal_detail_pengajuan").modal("show");
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: "Data tidak ditemukan.",
                    });
                }

                // Sembunyikan preloader (opsional)
                loader(false);
            },
            error: function (xhr) {
                console.error(xhr);

                // Sembunyikan preloader (opsional)
                loader(false);

                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: "Terjadi kesalahan saat memuat detail pengajuan.",
                });
            },
        });
    });
    $(document).on("submit", "#form_validasi", function (e) {
        e.preventDefault(); // Mencegah submit default

        // Ambil data dari form
        const formData = new FormData(this);
        const idPencairan = formData.get("id_pencairan");

        const status = formData.get("status");
        const keterangan_bendahara = formData.get("keterangan_bendahara");
        const buktiFile = formData.get("bukti");

        // CSRF token
        const csrfToken = $('meta[name="csrf-token"]').attr("content");

        console.log(formData.get("id_pencairan"));
        if (buktiFile && buktiFile.size > 0) {
            const reader = new FileReader();
            reader.onload = function (e) {
                const buktiBase64 = e.target.result.split(",")[1]; // Ambil hanya data base64 tanpa header
                submitData(
                    status,
                    keterangan_bendahara,
                    buktiBase64,
                    csrfToken,
                    idPencairan
                );
            };
            reader.readAsDataURL(buktiFile); // Konversi file ke base64
        } else {
            submitData(
                status,
                keterangan_bendahara,
                null,
                csrfToken,
                idPencairan
            );
        }
    });

    // Fungsi untuk mengirim data melalui AJAX
    function submitData(
        status,
        keterangan_bendahara,
        buktiBase64,
        csrfToken,
        idPencairan
    ) {
        console.log(idPencairan);
        loader(true);
        $.ajax({
            url: "/submit_pencairan",
            method: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            data: {
                id_pencairan: idPencairan,
                status: status,
                keterangan_bendahara: keterangan_bendahara,
                bukti: buktiBase64,
            },
            success: function (response) {
                loader(false);

                if (response.success) {
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil",
                        text: "Data telah dikirim!",
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Galat",
                        text: "Gagal mengirim data.",
                    });
                }

                // Reset form setelah submit berhasil
                reloadTable();
                $("#form_validasi")[0].reset();
                $("#modal_detail_pengajuan").modal("hide");
            },
            error: function (xhr) {
                loader(false);
                Swal.fire({
                    icon: "error",
                    title: "Galat",
                    text: "Gagal mengirim data.",
                });
                console.error(xhr.responseText);
            },
        });
    }
});

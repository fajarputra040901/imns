function table(url) {
    table_ins = $("#table").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: null, // Kolom untuk nomor urut
                render: function (data, type, row, meta) {
                    // Gunakan meta.settings._iDisplayStart untuk menghitung nomor urut yang berkelanjutan
                    return meta.settings._iDisplayStart + meta.row + 1; // Menampilkan nomor urut yang berkelanjutan
                },
            },
            {
                data: "tanggal_pengajuan",
                searchable: true,
            },
            {
                data: "tanggal_validasi",
                searchable: true,
            },
            {
                data: "judul",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },

            {
                data: "status",
                searchable: true,
            },
            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reloadDataTable() {
    if (table_ins) {
        table_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function table_permintaan(url) {
    table_permintaan_ins = $("#table_permintaan").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: "code",
                searchable: true,
            },
            {
                data: "nama",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },
            {
                data: "stok",
                searchable: true,
            },
            {
                data: "status",
                searchable: true,
            },

            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reload_table_permintaan() {
    if (table_permintaan_ins) {
        table_permintaan_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function table_validasi(url) {
    table_validasi_ins = $("#table_validasi").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: "code",
                searchable: true,
            },
            {
                data: "nama",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },
            {
                data: "stok",
                searchable: true,
            },
            {
                data: "status",
                searchable: true,
            },
            {
                data: "keterangan",
                searchable: true,
            },

            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reload_table_validasi() {
    if (table_validasi_ins) {
        table_validasi_ins.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}
$(document).ready(function () {
    table("/table_permintaan_barang");
    $(document).on("click", ".detail_pengajuan", function () {
        const id = $(this).data("parameter");
        const jenis = $(this).data("jenis");
        const keterangan = $(this).data("keterangan");
        const indikator = $(this).data("indikator");
        const judul = $(this).data("judul");
        const tanggalPengajuanRaw = $(this).data("tanggal_pengajuan");
        const tanggalValidasiRaw = $(this).data("tanggal_validasi");

        const tanggalPengajuan = tanggalPengajuanRaw
            ? moment(tanggalPengajuanRaw).format("DD-MM-YYYY <br> HH:mm")
            : "-";

        const tanggalValidasi = tanggalValidasiRaw
            ? moment(tanggalValidasiRaw).format("DD-MM-YYYY <br> HH:mm")
            : "-";
        const parameter = $(this).data("parameter");
        const status = $(this).data("status");

        if (status === "diajukan") {
            $("#tombol_validasi").html(`
            <button id="validasi_barang" class="btn btn-primary">Validasi Barang Permintaan</button>
        `);
        } else {
            $("#tombol_validasi").html(`
            <button id="barang_tervalidasi" class="btn btn-primary">Lihat Barang Permintaan</button>
        `);
        }
        const id_pengajuan = $(this).data("id");
        $("#id_pengajuan").val(id_pengajuan);
        // Jika jenis adalah 'baksos', kirimkan request AJAX
        if (jenis === "baksos") {
            $.ajax({
                url: `${api_ivcs}/api/detail_baksos_id/${id}`,
                type: "GET",
                beforeSend: function () {
                    // Tampilkan loader jika diperlukan
                    loader(true);
                },
                success: function (response) {
                    loader(false);

                    // Ambil data dari atribut tombol

                    // Ambil nama_kegiatan dari response
                    const namaKegiatan = response.data.nama_kegiatan;

                    // Isi tabel pertama
                    let tableContent = `
                        <table class="table">
                            <tr><th>Indikator</th><td>${indikator}</td></tr>
                            <tr><th>Judul</th><td>${judul}</td></tr>
                            <tr><th>Tanggal Pengajuan</th><td>${tanggalPengajuan}</td></tr>
                            <tr><th>Tanggal Validasi</th><td>${tanggalValidasi}</td></tr>
                            <tr><th>Parameter</th><td>${parameter}</td></tr>
                            <tr><th>Status</th><td>${status}</td></tr>
                             <tr><th>Keterangan</th><td>${keterangan}</td></tr>
                        </table>
                    `;

                    // Isi tabel kedua dari response
                    tableContent += `
                        <table class="table mt-3">
                            <tr><th>Nama Kegiatan</th><td>${namaKegiatan}</td></tr>
                        </table>
                    `;

                    // Masukkan ke dalam div dengan id 'div_detal'
                    $("#div_detal").html(tableContent);

                    // Buka modal
                    $("#modal_detal_permintaan").modal("show");
                },
                error: function (xhr) {
                    loader(false);
                    console.error(xhr);
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: "Gagal mengambil detail kegiatan.",
                    });
                },
            });
        }
    });
    $(document).on("click", "#validasi_barang", function () {
        var id_pengajuan = $("#id_pengajuan").val();
        if ($.fn.dataTable.isDataTable("#table_permintaan")) {
            $("#table_permintaan").DataTable().clear().destroy();
        }
        table_permintaan("/table_permintaan_logistik/" + id_pengajuan);

        if ($.fn.dataTable.isDataTable("#table_validasi")) {
            $("#table_validasi").DataTable().clear().destroy();
        }
        table_validasi("/table_validasi_logistik/" + id_pengajuan);
        $("#modal_validasi").modal("show");
    });
    $(document).on("click", ".validasi_permintaan_barang", function () {
        const id = $(this).data("id");
        const status = $(this).data("status");
        const keterangan = $(this).data("keterangan");
        const indikator = $(this).data("indikator");
        var urlajax = "";
        if (indikator === "validasi") {
            urlajax = "/validasi_barang";
        } else {
            urlajax = "/ubah_validasi_barang";
        }
        // Set default values in the modal
        $("#status").val(status || ""); // Set current status if available
        $("#keterangan").val(keterangan); // Clear the textarea

        // Show the modal
        $("#modal_validasi_barang").modal("show");

        // Handle save button click
        $("#simpan_validasi")
            .off("click")
            .on("click", function () {
                const newStatus = $("#status").val();
                const keterangan = $("#keterangan").val();

                if (!newStatus) {
                    alert("Status harus dipilih.");
                    return;
                }

                // Send data via AJAX
                $.ajax({
                    url: urlajax,
                    type: "POST",
                    data: {
                        id: id,
                        status: newStatus,
                        keterangan: keterangan,
                    },
                    headers: {
                        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr(
                            "content"
                        ), // CSRF token
                    },
                    success: function (response) {
                        Swal.fire({
                            icon: "success",
                            title: "Berhasil",
                            text: "Sip!",
                        });
                        reload_table_permintaan();
                        reload_table_validasi();
                        $("#modal_validasi_barang").modal("hide");
                    },
                    error: function (error) {
                        console.error(error);
                        Swal.fire({
                            icon: "error",
                            title: "Kesalahan",
                            text: "Terjadi kesalahan saat memproses data.",
                        });
                    },
                });
            });
    });
    let indikator = "";

    // Handle button clicks
    $(".validator").on("click", function () {
        indikator = $(this).data("indikator"); // Get indikator from data attribute
        let title = "";

        if (indikator === "validasi") {
            title = "Validasi Semua Permintaan";
        } else if (indikator === "tolak") {
            title = "Tolak Semua Permintaan";
        } else if (indikator === "reset") {
            title = "Reset Validasi";
        }

        // Confirm action with SweetAlert2
        Swal.fire({
            icon: "question",
            title: title,
            text: "Apakah Anda yakin ingin melanjutkan?",
            showCancelButton: true,
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                // Open modal for additional input
                $("#indikator_validator").val(indikator); // Set indikator in hidden input
                $("#modalValidator").modal("show");
            }
        });
    });

    $("#submitValidator").on("click", function () {
        const keterangan = $("#keterangan_validator").val(); // Get keterangan value
        const indikator = $("#indikator_validator").val(); // Get indikator value
        var id_pengajuan = $("#id_pengajuan").val();
        if (keterangan.trim() === "") {
            Swal.fire({
                icon: "warning",
                title: "Keterangan Kosong",
                text: "Harap masukkan keterangan sebelum melanjutkan.",
            });
            return;
        }
        if (indikator === "validasi") {
            title = "Validasi Semua Permintaan";
            successMessage = "Semua permintaan berhasil divalidasi!";
        } else if (indikator === "tolak") {
            title = "Tolak Semua Permintaan";
            successMessage = "Semua permintaan berhasil ditolak!";
        } else if (indikator === "reset") {
            title = "Reset Validasi";
            successMessage = "Validasi berhasil direset!";
        }

        // Send AJAX request
        $.ajax({
            url: "/validator_permintaan",
            type: "POST",
            data: {
                indikator: indikator, // Send indikator
                keterangan: keterangan, // Send keterangan
                id: id_pengajuan,
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
            },
            success: function (response) {
                $("#keterangan_validator").val("");
                reload_table_permintaan();
                reload_table_validasi();
                $("#modalValidator").modal("hide"); // Close modal
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: successMessage,
                });
            },
            error: function (error) {
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Kesalahan",
                    text: "Terjadi kesalahan saat memproses permintaan.",
                });
            },
        });
    });
    $("#simpan_semua_validasi").on("click", function () {
        Swal.fire({
            title: "Konfirmasi",
            text: "Apakah Anda yakin ingin menyimpan validasi ini?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Ya, Lanjutkan!",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                // Tampilkan modal keterangan
                $("#modalKeterangan").modal("show");
            }
        });
    });
    $("#submitKeterangan").on("click", function () {
        let keterangan = $("#keterangan_final").val();
        let status = $("#status_final").val();

        if (!keterangan.trim()) {
            Swal.fire({
                icon: "error",
                title: "Error",
                text: "Keterangan tidak boleh kosong!",
            });
            return;
        }
        if (!status.trim()) {
            Swal.fire({
                icon: "error",
                title: "Error",
                text: "Status Harus Dipilih!",
            });
            return;
        }

        // Tampilkan preloader jika ada
        loader(true); // Opsional: ganti dengan fungsi loader Anda
        var id_pengajuan = $("#id_pengajuan").val();
        // Kirim data ke server dengan AJAX
        $.ajax({
            url: "/fix_simpan_validasi",
            method: "POST",
            data: {
                id: id_pengajuan,
                keterangan: keterangan,
                status: status,
            },
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // CSRF token
            },
            success: function (response) {
                loader(false); // Sembunyikan preloader

                if (response.success) {
                    reloadDataTable();

                    Swal.fire({
                        icon: "success",
                        title: "Berhasil",
                        text: response.message,
                    });
                    $("#keterangan_final").val("");
                    $("#status_final").val("");

                    $("#modalKeterangan").modal("hide");
                    $("#modal_validasi").modal("hide");
                    $("#modal_detal_permintaan").modal("hide");
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text: response.message,
                    });
                }
            },
            error: function (xhr, status, error) {
                loader(false); // Sembunyikan preloader

                $("#modalKeterangan").modal("hide");
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: "Terjadi kesalahan. Silakan coba lagi.",
                });
                console.error("Error:", xhr, status, error);
            },
        });
    });
});

function table(url) {
    tableInstance = $("#table").DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url: url,
            type: "GET",
            dataSrc: function (json) {
                return json.data; // Data langsung dari server
            },
        },
        columns: [
            {
                data: null, // Kolom untuk nomor urut
                render: function (data, type, row, meta) {
                    // Gunakan meta.settings._iDisplayStart untuk menghitung nomor urut yang berkelanjutan
                    return meta.settings._iDisplayStart + meta.row + 1; // Menampilkan nomor urut yang berkelanjutan
                },
            },
            {
                data: "code",
                searchable: true,
            },
            {
                data: "nama",
                searchable: true,
            },
            {
                data: "detail",
                searchable: true,
            },
            {
                data: "keterangan",
                searchable: true,
            },
            {
                data: "jenis",
                searchable: true,
            },
            {
                data: "stok",
                searchable: true,
            },
            {
                data: "aksi",
                searchable: false, // Nonaktifkan pencarian untuk kolom 'aksi'
            },
        ],
        language: {
            search: "Cari: ",
        },
    });
}
function reloadDataTable() {
    if (tableInstance) {
        tableInstance.ajax.reload(null, false); // Reload tabel tanpa reset pagination
    } else {
        console.warn("DataTable belum diinisialisasi.");
    }
}
function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}

function generateBarcodeBwip(selector, value) {
    const canvas = document.getElementById(selector);

    if (!canvas) {
        console.error("Canvas untuk barcode tidak ditemukan");
        return;
    }

    try {
        // Gunakan bwip-js untuk menggambar barcode
        bwipjs.toCanvas(canvas, {
            bcid: "code128", // Jenis barcode (CODE128)
            text: value, // Nilai barcode
            scale: 2, // Skala ukuran barcode
            height: 8, // Tinggi barcode (dalam mm)
            includetext: true, // Sertakan teks di bawah barcode
            textxalign: "center", // Penyesuaian teks
        });
        console.log("Barcode berhasil dibuat dengan bwip-js");
    } catch (e) {
        console.error("Error saat membuat barcode dengan bwip-js:", e);
    }
}

// Contoh penggunaan

$(document).ready(function () {
    table("/table_inventaris");
    // Handle checkbox Otomatis
    $("#tambah_barang").on("click", function () {
        $("#modal_tambah_barang").modal("show"); // Menampilkan modal
    });
    $("#checkbox_otomatis").on("change", function () {
        if ($(this).is(":checked")) {
            $("#code").val("").prop("readonly", true); // Kosongkan dan jadikan readonly
        } else {
            $("#code").focus();
            $("#code").prop("readonly", false); // Hapus readonly
        }
    });

    // Handle form submit
    $("#form_tambah_barang").submit(function (e) {
        e.preventDefault(); // Mencegah form dari submit biasa

        // Konfirmasi dengan SweetAlert2 sebelum submit
        Swal.fire({
            title: "Apakah Anda yakin?",
            text: "Pastikan data yang Anda masukkan sudah benar!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonText: "Ya, Kirim!",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                // Mendapatkan CSRF token dari meta tag atau elemen HTML yang sesuai
                var csrfToken = $('meta[name="csrf-token"]').attr("content"); // Pastikan CSRF token ada di meta tag
                var codeOtomatis = $("#checkbox_otomatis").prop("checked")
                    ? true
                    : false;

                // Mengirimkan form dengan AJAX
                $.ajax({
                    url: "/tambah_inventaris",
                    method: "POST",

                    data:
                        $(this).serialize() +
                        "&_token=" +
                        csrfToken +
                        "&code_otomatis=" +
                        codeOtomatis,
                    success: function (response) {
                        if (response.success) {
                            Swal.fire({
                                title: "Sukses!",
                                text: "Barang berhasil ditambahkan.",
                                icon: "success",
                                confirmButtonText: "OK",
                            });
                            reloadDataTable();
                            $("#form_tambah_barang")[0].reset();
                            $("#jenis_lainnya").hide();
                            $("#modal_tambah_barang").modal("hide");
                        } else {
                            Swal.fire({
                                title: "error!",
                                text: response.message,
                                icon: "error",
                                confirmButtonText: "OK",
                            });
                        }
                    },
                    error: function (xhr, status, error) {
                        Swal.fire({
                            title: "Error!",
                            text: "Terjadi kesalahan saat menambahkan barang.",
                            icon: "error",
                            confirmButtonText: "OK",
                        });
                    },
                });
            }
        });
    });
    $("#jenis").change(function () {
        var jenisValue = $(this).val(); // Mendapatkan nilai yang dipilih

        // Jika nilai "lainnya" dipilih, tampilkan input #jenis_lainnya, jika tidak sembunyikan
        if (jenisValue === "lainnya") {
            $("#jenis_lainnya").show();
        } else {
            $("#jenis_lainnya").hide();
        }
    });
    $("#jenis_edit").change(function () {
        var jenis_edit = $(this).val(); // Mendapatkan nilai yang dipilih

        // Jika nilai "lainnya" dipilih, tampilkan input #jenis_lainnya, jika tidak sembunyikan
        if (jenis_edit === "lainnya") {
            $("#jenis_lainnya_edit").show();
        } else {
            $("#jenis_lainnya_edit").hide().val("");
        }
    });
    $(document).on("click", ".detail", function () {
        const id = $(this).data("id");
        loader(true); // Jika Anda memiliki fungsi preloader
        $.ajax({
            type: "GET",
            url: "/detail_barang/" + id,

            success: function (response) {
                loader(false);
                if (response.success) {
                    // Mengisi data ke dalam form
                    $("#id_barang").val(response.data.id);
                    $("#code_edit").val(response.data.code);
                    $("#nama_edit").val(response.data.nama);
                    $("#jenis_edit").val(response.data.parameter);
                    $("#stok_edit").val(response.data.stok);
                    $("#detail_edit").val(response.data.detail);
                    $("#keterangan_edit").val(response.data.keterangan);

                    // Logika untuk jenis "lainnya"
                    if (response.data.parameter === "lainnya") {
                        $("#jenis_lainnya_edit")
                            .show()
                            .val(response.data.jenis_lainnya);
                    } else {
                        $("#jenis_lainnya_edit").hide().val("");
                    }

                    // Tampilkan modal
                    $("#modal_detail").modal("show");
                    generateBarcodeBwip("barcode", response.data.code);
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: response.message || "Terjadi kesalahan.",
                    });
                }
            },
            error: function (error) {
                loader(false);
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: "Terjadi kesalahan pada server.",
                });
            },
        });
    });
    $(document).on("click", "#hapus_barang", function (e) {
        e.preventDefault();
        const id_barang = $("#id_barang").val(); // Ambil nilai ID Barang dari input hidden

        // Konfirmasi SweetAlert2
        Swal.fire({
            title: "Yakin ingin menghapus barang ini?",
            text: "Barang yang dihapus tidak bisa dikembalikan!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: "Ya, Hapus!",
            cancelButtonText: "Batal",
        }).then((result) => {
            if (result.isConfirmed) {
                // Kirim permintaan AJAX jika dikonfirmasi
                $.ajax({
                    type: "POST",
                    url: "/hapus_barang",
                    data: {
                        _token: $('meta[name="csrf-token"]').attr("content"), // Token CSRF dari meta tag
                        id_barang: id_barang,
                    },
                    success: function (response) {
                        if (response.success) {
                            $("#modal_detail").modal("hide");
                            reloadDataTable();
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil!",
                                text:
                                    response.message ||
                                    "Barang berhasil dihapus.",
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal!",
                                text:
                                    response.message ||
                                    "Terjadi kesalahan saat menghapus barang.",
                            });
                        }
                    },
                    error: function (xhr) {
                        Swal.fire({
                            icon: "error",
                            title: "Gagal!",
                            text: "Terjadi kesalahan pada server.",
                        });
                    },
                });
            }
        });
    });
    $("#import").on("click", function () {
        $("#modal_import").modal("show");
    });
    $("#template").on("click", function () {
        // Data untuk file Excel
        const data = [
            ["Code", "Nama Barang", "Jenis", "Detail", "Keterangan", "Stok"], // Header
            // Tambahkan baris kosong jika diperlukan
        ];

        // Buat workbook dan worksheet
        const wb = XLSX.utils.book_new();
        const ws = XLSX.utils.aoa_to_sheet(data);

        // Atur lebar kolom secara otomatis berdasarkan panjang header
        const columnWidths = [
            { wch: 10 }, // Code
            { wch: 20 }, // Nama Barang
            { wch: 15 }, // Jenis
            { wch: 20 }, // Jenis Lainnya
            { wch: 25 }, // Detail
            { wch: 20 }, // Keterangan
            { wch: 10 }, // Stok
        ];
        ws["!cols"] = columnWidths;

        // Tambahkan worksheet ke workbook
        XLSX.utils.book_append_sheet(wb, ws, "Template");

        // Simpan file Excel
        XLSX.writeFile(wb, "Template Import Inventaris.xlsx");
    });
    $("#form_import").on("submit", function (e) {
        e.preventDefault(); // Mencegah reload halaman

        // Ambil file dari input
        const fileData = new FormData(this);
        loader(true);

        // Kirim AJAX ke endpoint backend
        $.ajax({
            url: "/import_inventaris", // Ganti dengan endpoint Anda
            type: "POST",
            data: fileData,
            processData: false,
            contentType: false,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // Tambahkan CSRF token
            },
            success: function (response) {
                loader(false);
                if (response.success) {
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: response.message,
                    });
                    reloadDataTable();
                    $("#modal_import").modal("hide");

                    // Reset form
                    $("#form_import")[0].reset();
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal!",
                        text: response.message,
                    });
                }

                // Tutup modal
            },
            error: function (xhr) {
                loader(false);
                // Tampilkan notifikasi error
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text:
                        xhr.responseJSON?.message ||
                        "Terjadi kesalahan saat mengunggah file.",
                });
            },
        });
    });
});

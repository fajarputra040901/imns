function loadForm(id) {
    $.ajax({
        url: "/get_form/" + id, // Endpoint yang ingin Anda tuju
        type: "GET", // Metode HTTP GET
        success: function (response) {
            var data = JSON.parse(response.data);

            // Loop through each item in the data array
            data.forEach(function (item) {
                var newInput;
                var validations = item.validations || [];

                // Create the corresponding input field based on item type
                if (item.type === "text") {
                    newInput = `<input type="text" class="form-control" notif_wa="${
                        item.notif_wa
                    }" notif_email="${item.notif_email}" keterangan="${
                        item.keterangan
                    }" primary="${item.primary}" name="${
                        item.name
                    }" placeholder="${
                        item.placeholder
                    }"  data-validations="${validations.join(",")}">`;
                } else if (item.type === "number") {
                    newInput = `<input type="number" class="form-control" notif_wa="${
                        item.notif_wa
                    }" notif_email="${item.notif_email}" keterangan="${
                        item.keterangan
                    }" primary="${item.primary}" name="${
                        item.name
                    }" placeholder="${
                        item.placeholder
                    }"  data-validations="${validations.join(",")}">`;
                } else if (item.type === "date") {
                    newInput = `<input type="date" class="form-control" notif_wa="${
                        item.notif_wa
                    }" notif_email="${item.notif_email}" keterangan="${
                        item.keterangan
                    }" primary="${item.primary}" name="${
                        item.name
                    }"  data-validations="${validations.join(",")}">`;
                } else if (item.type === "textarea") {
                    newInput = `<textarea class="form-control" notif_wa="${
                        item.notif_wa
                    }" notif_email="${item.notif_email}" keterangan="${
                        item.keterangan
                    }" primary="${item.primary}" name="${
                        item.name
                    }" placeholder="${
                        item.placeholder
                    }"  data-validations="${validations.join(
                        ","
                    )}"></textarea>`;
                } else if (item.type === "file") {
                    newInput = `<input type="file" class="form-control" notif_wa="${
                        item.notif_wa
                    }" notif_email="${item.notif_email}" keterangan="${
                        item.keterangan
                    }" primary="${item.primary}" name="${
                        item.name
                    }"  data-validations="${validations.join(",")}">`;
                } else if (item.type === "email") {
                    newInput = `<input type="email" class="form-control" notif_wa="${
                        item.notif_wa
                    }" notif_email="${item.notif_email}" keterangan="${
                        item.keterangan
                    }" primary="${item.primary}" name="${
                        item.name
                    }" placeholder="${
                        item.placeholder
                    }"  data-validations="${validations.join(",")}">`;
                } else if (item.type === "select") {
                    // Pastikan opsi ada di dalam form options
                    var opsiList = Array.isArray(item.options)
                        ? item.options
                        : [];
                    var optionsHTML = opsiList
                        .map(function (opsi) {
                            return `<option value="${opsi}">${opsi}</option>`;
                        })
                        .join("");

                    newInput = `<select class="form-select" name="${
                        item.name
                    }" notif_wa='${item.notif_wa}' notif_email='${
                        item.notif_email
                    }' keterangan='${item.keterangan}' primary='${
                        item.primary
                    }' name="${item.name}" placeholder="${
                        item.placeholder
                    }" data-validations="${validations.join(
                        ","
                    )}">${optionsHTML}</select>`;
                }

                // Create the full HTML structure for the input
                var uniqueId = item.id || "input_" + Date.now();
                var newInputHTML = `
                    <div class="mb-3 input-item" id="${uniqueId}">
                        <label class="form-label">${item.label}</label>
                        ${newInput}
                        <small class="form-text text-muted">${
                            item.keterangan || ""
                        }</small>

                    </div>
                `;

                // Append the generated input to the form
                $("#formnya").append(newInputHTML);
            });
        },
        error: function (xhr, status, error) {
            // Jika terjadi error, tampilkan pesan kesalahan
            console.error("Error:", error);
            alert("Terjadi kesalahan dalam memuat form.");
        },
    });
}
function load_biaya(id) {
    $.ajax({
        url: "/get_biaya/" + id, // Endpoint untuk GET data
        method: "GET",
        success: function (response) {
            const data = JSON.parse(response.data);
            const select = $("#biaya");

            // Tambahkan options ke dalam select
            data.forEach((item) => {
                select.append(
                    `<option value="${item.id}">${item.namapaket}</option>`
                );
            });

            // Event listener untuk perubahan pilihan pada select
            select.on("change", function () {
                const selectedId = $(this).val();
                const selectedData = data.find(
                    (item) => item.id === selectedId
                );

                if (selectedData) {
                    // Tampilkan benefit di #keterangan
                    $("#keterangan").html(selectedData.benefit);

                    // Format harga menjadi currency Rupiah
                    const formattedHarga = new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "IDR",
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 0,
                    }).format(selectedData.hargapaket);

                    // Masukkan harga yang sudah diformat ke input #harga
                    const hargaInput = $("#harga");
                    hargaInput.val(formattedHarga);

                    // Logika untuk minimalharga
                    if (selectedData.minimalharga) {
                        // Tambahkan atribut min
                        hargaInput.attr("min", selectedData.hargapaket);
                        hargaInput.attr(
                            "data-validations",
                            "required,min:" + selectedData.hargapaket
                        );
                        hargaInput.attr("placeholder", "Masukkan donasi");
                        hargaInput.prop("readonly", false); // Pastikan input tidak readonly
                        hargaInput.val("");
                        $("#donasikan").val("");
                        // Tampilkan pesan minimal donasi
                        $("#keterangan_harga")
                            .text(`Minimal Donasi sebesar ${formattedHarga}`)
                            .show();

                        // Sembunyikan #donasikan
                        $("#donasikan").hide();
                    } else {
                        // Set harga sebagai readonly
                        hargaInput.attr("min", "");
                        hargaInput.prop("readonly", true); // Jadikan input readonly
                        hargaInput.val(formattedHarga); // Masukkan nilai harga yang sudah diformat
                        hargaInput.attr("data-validations", "");

                        // Sembunyikan pesan minimal donasi
                        $("#keterangan_harga").hide();

                        // Tampilkan #donasikan
                        $("#donasikan").show();
                        $("#donasikan").val("");
                    }
                } else {
                    // Reset jika tidak ada pilihan
                    $("#keterangan").html("");
                    $("#harga")
                        .val("")
                        .attr("min", "")
                        .prop("readonly", false)
                        .attr("placeholder", "");
                    $("#keterangan_harga").hide();
                    $("#donasikan").hide();
                }
            });
        },
        error: function (err) {
            console.error("Error fetching data:", err);
        },
    });
}

$(document).ready(function () {
    function loader(show) {
        if (show) {
            $("#preloader").show(); // Menampilkan preloader
        } else {
            $("#preloader").hide(); // Menyembunyikan preloader
        }
    }

    loadForm(idnya);

    $("#formulir_pendaftaran").on("submit", function (e) {
        e.preventDefault(); // Prevent the default form submission
        loader(true);
        var isValid = true; // Flag to check if the form is valid
        var invalidFields = []; // Array to collect invalid fields

        // Clear previous error messages and styles
        $(this).find(".is-invalid").removeClass("is-invalid");
        $(this).find(".invalid-feedback").remove();

        // Loop through each input field with data-validation attribute
        $(this)
            .find("input, textarea, select")
            .each(function () {
                var validations = $(this).attr("data-validations"); // Get the validations
                if (validations) {
                    var validationsArray = validations.split(","); // Split validations into an array
                    for (var i = 0; i < validationsArray.length; i++) {
                        var validation = validationsArray[i].trim();
                        var fieldValid = true;
                        var errorMessage = "";

                        // Check if validation is "required"
                        if (validation === "required" && !$(this).val()) {
                            fieldValid = false;
                            errorMessage = "Input Ini Harus Diisi";
                        }

                        // Check if validation is "number"
                        if (validation === "number" && isNaN($(this).val())) {
                            fieldValid = false;
                            errorMessage = "Input Harus Berupa Angka";
                        }
                        if (validation.startsWith("min:")) {
                            var minVal = parseInt(validation.split(":")[1], 10); // Extract the minimum value

                            // Ambil nilai dari input dan hilangkan karakter selain angka (termasuk simbol Rp dan tanda koma)
                            var inputValue = $(this).val();
                            var numericValue = parseFloat(
                                inputValue.replace(/[^\d]/g, "")
                            ); // Hapus semua karakter non-numerik

                            if (numericValue < minVal) {
                                fieldValid = false;
                                var formattedMinVal = new Intl.NumberFormat(
                                    "id-ID",
                                    {
                                        style: "currency",
                                        currency: "IDR",
                                        minimumFractionDigits: 0,
                                        maximumFractionDigits: 0,
                                    }
                                ).format(minVal);

                                errorMessage =
                                    "Donasi Harus lebih besar atau sama dengan " +
                                    formattedMinVal +
                                    ".";
                            }
                        }

                        // If the field is not valid, apply invalid class and show message
                        if (!fieldValid) {
                            isValid = false;
                            $(this).addClass("is-invalid");
                            var errorSpan = $("<span>")
                                .addClass("invalid-feedback")
                                .text(errorMessage);
                            $(this).after(errorSpan); // Append error message after the input
                            break;
                        }
                    }
                }
            });

        // If form is not valid, show SweetAlert2 message
        if (!isValid) {
            loader(false);

            Swal.fire({
                icon: "error",
                title: "Formulir Tidak Valid",
                text: "Tolong perbaiki input yang tidak valid.",
            });
            return; // Stop form submission
        }

        // Convert .harga input fields to numeric values

        // If form is valid, continue with AJAX submission
        var formData = new FormData(this); // Collect all form data, including CSRF token

        $.ajax({
            url: "/submit_form_undangan", // URL to send the data
            type: "POST", // Request method
            data: formData, // Data to send
            contentType: false, // Do not set content type for FormData
            processData: false, // Do not process data (FormData)
            success: function (response) {
                if (response.success) {
                    Swal.fire({
                        icon: "success",
                        title: "Success",
                        text: "Anda berhasil mendaftar di webinar ini melalui jalur undangan.",
                    });
                } else {
                    // Tampilkan pesan error jika sudah ada yang terdaftar
                    loader(false);
                    Swal.fire({
                        icon: "error",
                        title: "Error",
                        text: response.message,
                    });
                }
            },
            error: function (xhr, status, error) {
                // Handle AJAX error
                loader(false);
                console.error(error);
                Swal.fire({
                    icon: "error",
                    title: "Error",
                    text: "Terjadi kesalahan saat mengirim data.",
                });
            },
        });
    });

    $("#formulir_pendaftaran").on(
        "input",
        "input, textarea, select",
        function () {
            // Hapus class is-invalid dan pesan error saat pengguna mulai mengetik
            $(this).removeClass("is-invalid");
            $(this).siblings(".invalid-feedback").remove();
        }
    );
    const rupiahFormatter = new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR",
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    });

    function loader(show) {
        if (show) {
            $("#preloader").show(); // Menampilkan preloader
        } else {
            $("#preloader").hide(); // Menyembunyikan preloader
        }
    }
});

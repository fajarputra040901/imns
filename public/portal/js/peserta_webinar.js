$(document).ready(function() {
    $('#pesertaCard').click(function() {
      $('#modalContent').text('Anda memilih menu Peserta.');
      $('#menuModal').modal('show');
    });

    $('#pencarianCard').click(function() {
      $('#modalContent').text('Anda memilih menu Pencarian.');
      $('#menuModal').modal('show');
    });

    $('#kirimPesanCard').click(function() {
      $('#modalContent').text('Anda memilih menu Kirim Pesan.');
      $('#menuModal').modal('show');
    });
  });
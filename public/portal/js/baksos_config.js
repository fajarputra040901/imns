function loader(show) {
    if (show) {
        $("#preloader").show(); // Menampilkan preloader
    } else {
        $("#preloader").hide(); // Menyembunyikan preloader
    }
}
function formatTanggal(input) {
    var cleaned = input.replace(/\D/g, "");
    var formatted = "";
    if (cleaned.length > 0) {
        formatted = cleaned
            .match(/^(\d{1,2})(\d{0,2})?(\d{0,4})?/)
            .slice(1)
            .filter(Boolean)
            .join("-");
    }
    return formatted;
}
function detail(id) {
    $.ajax({
        url: "https://ivcs.indonesiamelihat.org/api/detail_kegiatan/" + id,
        method: "GET",
        success: function (res) {
            var parts = res.tanggal.split("-");
            var tanggal = parts[2] + "-" + parts[1] + "-" + parts[0];
            $("#tanggaledit").val(tanggal);
            $("#namaedit").val(res.nama);

            $("#tempatedit").val(res.tempat);
            if (CKEDITOR.instances.deskripsiedit) {
                CKEDITOR.instances.deskripsiedit.setData(res.deskripsi);
            }
            $("#desaedit").val(res.desa);
            $("#jenis_kegiatanedit").val(res.jenis_kegiatan);
            if (res.jenis_kegiatan === "Berbayar") {
                $("#div_biayaedit").show();
            } else {
                $("#div_biayaedit").hide();
            }
            $("#biaya_edit").val(res.biaya);
            $("#image_preview_edit").empty();
            var apiurl = "https://ivcs.indonesiamelihat.org/";
            var imagePath = res.cover;
            var defaultImagePath = "/aset_me/default_image.jpeg";
            var fullImagePath = imagePath
                ? apiurl + imagePath
                : defaultImagePath;
            $("#image_preview_edit").html(`
                    <a href="${fullImagePath}" data-fancybox="gallery">
                        <img src="${fullImagePath}" alt="Preview Image" style="display: block; margin-left: auto; margin-right: auto; max-width: 40%; cursor: pointer;">
                    </a>
                `);
            $("#kabedit").val(res.kab);
            $("#provedit").val(res.prov);
            $("#idkegiatan").val(res.id_kegiatan);

            if (res.peserta === "T") {
                $("#tanpaBatasanedit").prop("checked", true);
                $("#pesertaedit").val("").prop("readonly", true);
            } else {
                $("#tanpaBatasanedit").prop("checked", false);
                $("#pesertaedit").val(res.peserta).prop("readonly", false);
            }

            // Set Minimal Usia
            if (res.min_usia === "T") {
                $("#tanpaMinimaledit").prop("checked", true);
                $("#min_usiaedit").val("").prop("readonly", true);
            } else {
                $("#tanpaMinimaledit").prop("checked", false);
                $("#min_usiaedit").val(res.min_usia).prop("readonly", false);
            }

            // Set Maksimal Usia
            if (res.max_usia === "T") {
                $("#tanpaMaksimaledit").prop("checked", true);
                $("#max_usiaedit").val("").prop("readonly", true);
            } else {
                $("#tanpaMaksimaledit").prop("checked", false);
                $("#max_usiaedit").val(res.max_usia).prop("readonly", false);
            }

            // Set Tanpa Batasan Usia
            if (res.min_usia === "T" && res.max_usia === "T") {
                $("#tanpaBatasanUsiaedit").prop("checked", true);
                $("#min_usiaedit").prop("readonly", true);
                $("#max_usiaedit").prop("readonly", true);
            } else {
                $("#tanpaBatasanUsiaedit").prop("checked", false);
            }
        },
    });
}
$(document).ready(function () {
    detail(id_kegiatan);
    deskripsi_edit();
    $("#pengadaan").on("click", function () {
        var url = "/config_baksos/pengadaan_barang/" + id_kegiatan;
        window.open(url, "_blank"); // Membuka URL di tab baru
    });
    $("#rab").on("click", function () {
        var url = "/config_baksos/rab/" + id_kegiatan;
        window.open(url, "_blank"); // Membuka URL di tab baru
    });
    $("#jenis_kegiatanedit").change(function () {
        var selectedValue = $(this).val();

        if (selectedValue === "Berbayar") {
            $("#div_biayaedit").show(); // Menampilkan div jika jenis kegiatan "Berbayar"
        } else {
            $("#div_biayaedit").hide(); // Menyembunyikan div jika jenis kegiatan bukan "Berbayar"
        }
    });
    $("#coveredit").on("change", function (event) {
        var input = event.target;
        var imagePreview = $("#image_preview_edit");
        imagePreview.empty(); // Clear previous previews

        // Check if files are selected
        if (input.files && input.files[0]) {
            var file = input.files[0];
            var reader = new FileReader();

            reader.onload = function (e) {
                // Create an image element
                var imgElement = $("<a></a>", {
                    href: e.target.result,
                    "data-fancybox": "gallery",
                    html:
                        '<img src="' +
                        e.target.result +
                        '" style="display: block; margin-left: auto; margin-right: auto; max-width: 100%; cursor: pointer;">',
                });

                // Append the image to the preview div
                imagePreview.append(imgElement);
            };

            // Read the file as a data URL
            reader.readAsDataURL(file);
        }
    });
    function deskripsi_edit() {
        CKEDITOR.replace("deskripsiedit");
    }
    $("#tanpaMinimaledit").change(function () {
        if ($(this).is(":checked")) {
            $("#min_usiaedit").val("").prop("readonly", true);
        } else {
            $("#min_usiaedit").prop("readonly", false);
            $("#tanpaBatasanUsiaedit").prop("checked", false);
        }
    });
    $("#tanpaMaksimaledit").change(function () {
        if ($(this).is(":checked")) {
            $("#max_usiaedit").val("").prop("readonly", true);
        } else {
            $("#max_usiaedit").prop("readonly", false);
            $("#tanpaBatasanUsiaedit").prop("checked", false);
        }
    });
    $("#tanpaBatasanUsiaedit").change(function () {
        if ($(this).is(":checked")) {
            $("#min_usiaedit, #max_usiaedit").val("").prop("readonly", true);
            $("#tanpaMinimaledit, #tanpaMaksimaledit").prop("checked", true);
        } else {
            $("#min_usiaedit, #max_usiaedit").prop("readonly", false);
            $("#tanpaMinimaledit, #tanpaMaksimaledit").prop("checked", false);
        }
    });
    $("#tanpaBatasanedit").change(function () {
        if ($(this).is(":checked")) {
            $("#pesertaedit").val("").prop("readonly", true);
        } else {
            $("#pesertaedit").prop("readonly", false);
        }
    });
    $('[data-toggle="tanggaledit"]').datepicker({
        autoHide: true,
        zIndex: 2048,
        format: "dd-mm-yyyy",
    });
    $("#tanggaledit").on("input", function () {
        var nilaiInput = $(this).val();
        var tanggal = formatTanggal(nilaiInput);
        $(this).val(tanggal);
    });
    $("#edit").click(function () {
        Swal.fire({
            title: "Apakah Anda Yakin?",
            text: "Kegiatan Akan DI Diperbaharui!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            cancelButtonText: "Tidak",
            confirmButtonText: "Ya, Saya Yakin!",
        }).then((result) => {
            if (result.isConfirmed) {
                loader(true);

                var formData = new FormData();
                var id = $("#idkegiatan").val();
                var tgl = $("#tanggaledit").val();
                var parts = tgl.split("-");
                var tanggal = parts[2] + "-" + parts[1] + "-" + parts[0];
                var nama = $("#namaedit").val();
                var tempat = $("#tempatedit").val();
                var desa = $("#desaedit").val();
                var kab = $("#kabedit").val();
                var prov = $("#provedit").val();
                var jenis_kegiatan = $("#jenis_kegiatanedit").val();
                var biaya = $("#biayaedit").val();
                var deskripsi = CKEDITOR.instances.deskripsiedit.getData();
                var peserta = $("#tanpaBatasanedit").is(":checked")
                    ? "T"
                    : $("#pesertaedit").val();
                var min_usia =
                    $("#tanpaBatasanUsiaedit").is(":checked") ||
                    $("#tanpaMinimaledit").is(":checked")
                        ? "T"
                        : $("#min_usiaedit").val();
                var max_usia =
                    $("#tanpaBatasanUsiaedit").is(":checked") ||
                    $("#tanpaMaksimaledit").is(":checked")
                        ? "T"
                        : $("#max_usiaedit").val();
                var coverFile = $("#coveredit")[0].files[0]; // Ambil file gambar

                // Tambahkan data ke FormData
                formData.append("id", id);
                formData.append("tanggal", tanggal);
                formData.append("nama", nama);
                formData.append("tempat", tempat);
                formData.append("desa", desa);
                formData.append("kab", kab);
                formData.append("prov", prov);
                formData.append("deskripsi", deskripsi);
                formData.append("peserta", peserta);
                formData.append("jenis_kegiatan", jenis_kegiatan);
                formData.append("biaya", biaya);
                formData.append("min_usia", min_usia);
                formData.append("max_usia", max_usia);
                formData.append("cover", coverFile); // Tambahkan file gambar ke FormData
                formData.append(
                    "_token",
                    $('meta[name="csrf-token"]').attr("content")
                );

                $.ajax({
                    type: "POST",
                    url: "https://ivcs.indonesiamelihat.org/api/edit_kegiatan",
                    data: formData,
                    processData: false, // Jangan proses data secara otomatis
                    contentType: false, // Jangan atur tipe konten
                    success: function (response) {
                        loader(false);
                        if (response.pesan == "sukses") {
                            Swal.fire({
                                icon: "success",
                                title: "Berhasil",
                                text: "Kegiatan Berhasil Diperbaharui",
                            });
                        } else {
                            Swal.fire({
                                icon: "error",
                                title: "Gagal",
                                text: "Kegiatan Gagal Diperbaharui, Kemungkinan Koneksi Buruk Atau Apabila Masalah Ini Terus Berlanjut Hubungi Pengembang",
                            });
                        }
                        console.log(response);
                    },
                    error: function (xhr, status, error) {
                        loader(false);
                        console.error(xhr.responseText);
                    },
                });
            }
        });
    });
});

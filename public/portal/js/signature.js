var isDrawing = false;
var signatureData = null;

// Mode menggambar tanda tangan
document.getElementById('add-signature').addEventListener('click', function() {
    canvas.addEventListener('mousedown', startDrawing);
    canvas.addEventListener('mousemove', drawSignature);
    canvas.addEventListener('mouseup', stopDrawing);
});

function startDrawing(e) {
    isDrawing = true;
    ctx.beginPath();
    ctx.moveTo(e.offsetX, e.offsetY);
}

function drawSignature(e) {
    if (isDrawing) {
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
    }
}

function stopDrawing() {
    isDrawing = false;
    ctx.closePath();
}

// Fitur upload gambar tanda tangan
document.getElementById('upload-signature').addEventListener('change', function(e) {
    var file = e.target.files[0];
    var reader = new FileReader();
    reader.onloadend = function() {
        var img = new Image();
        img.src = reader.result;
        img.onload = function() {
            ctx.drawImage(img, 100, 300, 100, 50); // Menempelkan gambar tanda tangan di canvas
        };
    };
    reader.readAsDataURL(file);
});
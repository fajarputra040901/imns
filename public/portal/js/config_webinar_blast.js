function get_email() {
    loader(true);
    $.ajax({
        url: "/get_email",
        type: "GET",
        success: function (response) {
            loader(false);
            if (response.success) {
                // Mengisi select dengan ID 'name_email'
                let emailSelect = $("#name_email");
                emailSelect.empty(); // Kosongkan opsi sebelumnya
                response.data.forEach(function (email) {
                    // Menambahkan option dengan id sebagai value dan email sebagai tampilan
                    emailSelect.append(new Option(email.email, email.id));
                });
            } else {
                console.error("Gagal mengambil data email");
            }
        },
        error: function (error) {
            loader(false);
            console.error("Terjadi kesalahan:", error);
        },
    });
}

// Memanggil fungsi get_email

$(document).ready(function () {
    $("#kirimPesanCard").on("click", function () {
        get_email();
        $("#kirim_pesan_modal").modal("show");
    });
    $("#jenis_pesan").on("change", function () {
        if ($(this).val() === "File") {
            $("#fileInputWrapper").show();
        } else {
            $("#fileInputWrapper").hide();
        }
    });
    $("#jenis_pesan_email").on("change", function () {
        if ($(this).val() === "File") {
            $("#fileInputWrapperEmail").show();
        } else {
            $("#fileInputWrapperEmail").hide();
        }
    });
    $("#form_wa_blast").on("submit", function (e) {
        e.preventDefault(); // Mencegah form dari submit standar

        // Buat form data
        let formData = new FormData(this);
        formData.append("id", id);
        formData.append("isi_pesan", CKEDITOR.instances.isi_pesan.getData()); // Ambil data dari CKEditor

        loader(true);
        // Kirim dengan AJAX
        $.ajax({
            url: "/pesan_wa_webinar",
            method: "POST",
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // Ambil CSRF token dari meta tag
            },
            data: formData,
            processData: false,
            contentType: false,
            success: function (response) {
                loader(false);
                if (response.success) {
                    Swal.fire({
                        icon: "success",
                        title: "Berhasil",
                        text: response.message || "Pesan berhasil dikirim!",
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal",
                        text:
                            xhr.responseJSON?.message ||
                            "Terjadi kesalahan, silakan coba lagi.",
                    });
                }
            },
            error: function (xhr) {
                loader(false);
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text:
                        xhr.responseJSON?.message ||
                        "Terjadi kesalahan, silakan coba lagi.",
                });
            },
        });
    });
    $("#kirim_pesan_email").on("submit", function (e) {
        e.preventDefault(); // Mencegah form dari submit biasa
        loader(true);
        let formData = new FormData(this);
        formData.append("id", id);

        // Mengambil isi pesan dari CKEditor
        formData.append(
            "isi_pesan_email",
            CKEDITOR.instances.isi_pesan_email.getData()
        );

        $.ajax({
            url: "/kirim_pesan_email_webinar",
            type: "POST",
            data: formData,
            headers: {
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"), // Ambil CSRF token dari meta tag
            },
            processData: false, // Jangan mengubah data ke dalam format query string
            contentType: false, // Jangan menetapkan content-type
            success: function (response) {
                loader(false);
                if (response.success) {
                    Swal.fire({
                        icon: "success",
                        title: "Pesan Berhasil Dikirim!",
                        text: "Pesan Anda telah berhasil dikirim.",
                        confirmButtonText: "OK",
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Gagal Mengirim Pesan",
                        text: "Terdapat kesalahan dalam pengiriman pesan.",
                        confirmButtonText: "OK",
                    });
                }
            },
            error: function (error) {
                loader(false);
                console.error("Terjadi kesalahan:", error);
                alert("Terjadi kesalahan saat mengirim pesan.");
            },
        });
    });
});

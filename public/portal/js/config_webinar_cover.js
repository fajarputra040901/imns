$(document).ready(function () {
    // Ketika tombol "Tambah Cover" diklik, tampilkan modal
    $("#tambah_cover").click(function (event) {
        event.preventDefault(); // Mencegah form submit atau refresh halaman
        var modal = new bootstrap.Modal($("#modalcover"));
        modal.show();
    });

    // Ketika tombol "Simpan" diklik, proses data file dan ceklis
    $("#simpanCover").click(function () {
        var coverFile = $("#coverFile")[0].files[0]; // Ambil file yang dipilih
        var isThumbnail = $("#thumbnailsCheck").prop("checked"); // Cek apakah thumbnail dipilih

        if (coverFile) {
            // Tampilkan gambar di dalam div #riview_cover (scrollable)
            var reader = new FileReader();
            reader.onload = function (event) {
                // Membaca data URL dari file gambar
                var imageUrl = event.target.result;

                // Membuat ID unik untuk gambar menggunakan timestamp
                var uniqueId = new Date().getTime(); // ID unik berdasarkan timestamp

                // Membuat elemen gambar
                var imgElement = $("<img>", {
                    src: imageUrl,
                    width: 100, // Gambar tampil dengan ukuran 100x100px
                    height: 100, // Ukuran kotak gambar tetap
                    style: "object-fit: cover;", // Menjaga proporsi gambar dan terpotong dengan benar
                });

                // Membuat tombol hapus
                var deleteBtn = $("<button>", {
                    text: "X",
                    class: "delete-btn", // Memberikan class untuk styling
                    style: "cursor: pointer;", // Menambahkan cursor pointer pada tombol hapus
                });

                // Membungkus gambar dan tombol hapus dalam sebuah div
                var previewDiv = $("<div>", {
                    class: "image-preview-container",
                    "data-id": uniqueId, // Menambahkan data-id dengan ID unik untuk setiap gambar
                })
                    .append(imgElement)
                    .append(deleteBtn);

                // Menambahkan preview ke #riview_cover
                $("#riview_cover").append(previewDiv);

                // Menambahkan event untuk menghapus gambar
                deleteBtn.click(function () {
                    // Menghapus gambar dan tombol hapus dari tampilan
                    previewDiv.remove();

                    // Perbarui data JSON setelah gambar dihapus
                    var existingData = JSON.parse($("#cover").val() || "[]");

                    // Hapus gambar yang sesuai dengan ID unik yang dihapus
                    var updatedData = existingData.filter(function (item) {
                        return item.id !== uniqueId; // Hapus item yang id-nya sama dengan yang dihapus
                    });

                    // Perbarui JSON di textarea
                    $("#cover").val(JSON.stringify(updatedData));
                });

                // Menambahkan event untuk menampilkan gambar menggunakan Fancybox
                imgElement.on("click", function () {
                    Fancybox.show([
                        {
                            src: imageUrl, // Menampilkan gambar yang telah diupload
                            type: "image",
                        },
                    ]);
                });

                // Menyimpan data dalam format JSON ke textarea dengan ID unik
                var coverData = {
                    id: uniqueId, // Menyimpan ID unik
                    src: imageUrl, // Menyimpan URL data gambar
                    thumbnail: isThumbnail, // Menyimpan status thumbnail
                };

                // Ambil nilai dari textarea dan parse jika ada
                var existingData = JSON.parse($("#cover").val() || "[]");
                existingData.push(coverData);

                // Update textarea dengan JSON yang baru
                $("#cover").val(JSON.stringify(existingData));
            };
            reader.readAsDataURL(coverFile);

            // Tutup modal setelah simpan
            var modal = bootstrap.Modal.getInstance($("#modalcover"));
            modal.hide();
        }
    });
});

$(document).ready(function () {
    $(
        'input[name="tanggal_mulai"], input[name="tanggal_selesai"],input[name="dedlen"]'
    ).datepicker({
        format: "dd-mm-yyyy",
        autoclose: true,
        todayHighlight: true,
    });
    $(
        'input[name="tanggal_mulai"], input[name="tanggal_selesai"],input[name="dedlen"]'
    ).on("input", function () {
        var val = $(this).val();
        var formatted = val.replace(/[^0-9]/g, "").slice(0, 8); // Ambil hanya angka dan batasi panjangnya
        $(this).val(formatted.replace(/(\d{2})(\d{2})(\d{4})/, "$1-$2-$3")); // Format dengan '-'
    });
    function getBiaya() {
        $.ajax({
            url: "/get_biaya/" + id_webinar,
            method: "GET",
            success: function (response) {
                // Bersihkan kontainer sebelum menambahkan kartu baru

                // Iterasi data dan render setiap kartu
                let data = JSON.parse(response.data); // Parse string JSON menjadi array
                $("#isi_form_biaya").val(response.data);
                // Pastikan data adalah array sebelum diiterasi
                if (Array.isArray(data)) {
                    $("#review_input_biaya").empty(); // Bersihkan container sebelum render
                    data.forEach((item) => {
                        const cardHTML = renderCard(item);
                        $("#review_input_biaya").append(cardHTML);
                    });
                } else {
                    console.error("Data tidak berbentuk array:", data);
                }
            },
            error: function (xhr, status, error) {
                console.error(xhr.responseText);
            },
        });
    }
    getBiaya();
    $("#tambah_input_biaya").click(function () {
        // Menampilkan modal dengan ID modalForm
        resetForm();
        $("#modalpaket").modal("show");
    });
    function resetForm() {
        $("#namapaket").val("");

        $("#hargapaket").val("");
        $("#benefit").val("");
    }

    function generateUniqueId() {
        return "id-" + Math.random().toString(36).substr(2, 9);
    }

    let editModeId = null;

    // Event listener tombol tambah menggunakan jQuery
    $(document).on("click", "#tambah_input_form_biaya", function () {
        const namapaket = $("#namapaket").val();
        let hargapaket = $("#hargapaket").val();
        hargapaket = parseFloat(hargapaket.replace(/[^\d]/g, ""));
        const baksos = $("#baksos").val();
        const benefit = CKEDITOR.instances.benefit.getData();
        const minimalharga = $("#minimalharga").is(":checked"); // Periksa status checkbox
        const textarea = $("#isi_form_biaya");
        const existingData = textarea.val() ? JSON.parse(textarea.val()) : [];

        // Jika dalam mode edit
        if (editModeId) {
            const index = existingData.findIndex(
                (item) => item.id === editModeId
            );
            if (index !== -1) {
                existingData[index] = {
                    id: editModeId,
                    namapaket,
                    hargapaket,
                    baksos,
                    benefit,
                    minimalharga, // Tambahkan properti minimalharga
                };
                $(`#${editModeId}`).replaceWith(
                    renderCard(existingData[index])
                );
            }
            editModeId = null;
        } else {
            // Tambah data baru
            const data = {
                id: generateUniqueId(),
                namapaket,
                hargapaket,
                baksos,
                benefit,
                minimalharga, // Tambahkan properti minimalharga
            };
            existingData.push(data);
            $("#review_input_biaya").append(renderCard(data));
        }

        // Perbarui textarea dan reset form
        textarea.val(JSON.stringify(existingData));
        $("#formpaket")[0].reset();
        CKEDITOR.instances.benefit.setData("");
        $("#modalpaket").modal("hide");
    });

    // Event listener untuk tombol edit
    $(document).on("click", ".edit-button", function () {
        const id = $(this).data("id");
        const textarea = $("#isi_form_biaya");
        const existingData = JSON.parse(textarea.val());
        const data = existingData.find((item) => item.id === id);
        const hargapaket = new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
        }).format(data.hargapaket);
        if (data) {
            // Isi form dengan data yang dipilih
            $("#namapaket").val(data.namapaket);
            $("#hargapaket").val(hargapaket);
            $("#baksos").val(data.baksos);
            CKEDITOR.instances.benefit.setData(data.benefit);

            // Periksa dan atur status checkbox minimalharga
            if (data.minimalharga) {
                $("#minimalharga").prop("checked", true);
            } else {
                $("#minimalharga").prop("checked", false);
            }

            // Simpan ID untuk mode edit
            editModeId = id;
            $("#modalpaket").modal("show");
        }
    });

    // Event listener untuk tombol hapus
    $(document).on("click", ".delete-button", function () {
        const id = $(this).data("id");
        const textarea = $("#isi_form_biaya");
        const existingData = JSON.parse(textarea.val());
        const updatedData = existingData.filter((item) => item.id !== id);

        // Hapus data dan perbarui textarea
        textarea.val(JSON.stringify(updatedData));
        $(`#${id}`).remove();
    });
    function renderCard(data) {
        const formattedHarga = new Intl.NumberFormat("id-ID", {
            style: "currency",
            currency: "IDR",
            minimumFractionDigits: 0,
            maximumFractionDigits: 0,
        }).format(data.hargapaket);
        return `
            <div class="card mb-2" id="${data.id}">
                <div class="card-body">
                    <h5 class="card-title">${data.namapaket}</h5>
                    <p class="card-text"><strong>Harga:</strong> ${formattedHarga}</p>
                     <p class="card-text"><strong>Minimal Harga:</strong> ${
                         data.minimalharga ? "Ya" : "Tidak"
                     }</p>
                    ${
                        data.baksos
                            ? `<p class="card-text"><strong>Baksos:</strong> ${data.baksos}</p>`
                            : ""
                    }
                    <p class="card-text"><strong>Benefit:</strong> ${
                        data.benefit
                    }</p>

                    <button class="btn btn-warning btn-sm edit-button" data-id="${
                        data.id
                    }">Edit</button>
                    <button class="btn btn-danger btn-sm delete-button" data-id="${
                        data.id
                    }">Hapus</button>
                </div>
            </div>
        `;
    }

    $("#review_input_biaya")
        .sortable({
            update: function (event, ui) {
                // Ketika posisi diubah, perbarui JSON
                const newOrder = [];
                $("#review_input_biaya .card").each(function () {
                    const id = $(this).attr("id");
                    const textarea = $("#isi_form_biaya");
                    const existingData = JSON.parse(textarea.val());
                    const data = existingData.find((item) => item.id === id);
                    newOrder.push(data);
                });

                // Simpan urutan baru ke dalam textarea
                $("#isi_form_biaya").val(JSON.stringify(newOrder));
            },
        })
        .disableSelection();

    $("#simpan_input_biaya").on("click", function () {
        var isi_form_biaya = $("#isi_form_biaya").val();
        var csrfToken = $("meta[name='csrf-token']").attr("content");

        // Validasi jika textarea kosong
        if (!isi_form_biaya.trim()) {
            Swal.fire({
                icon: "warning",
                title: "Peringatan",
                text: "Form biaya tidak boleh kosong!",
            });
            return;
        }

        // Kirim data melalui AJAX
        $.ajax({
            url: "/simpan_biaya",
            method: "POST",
            data: {
                _token: csrfToken,
                id_webinar: id_webinar,
                data: isi_form_biaya,
            },
            success: function (response) {
                Swal.fire({
                    icon: "success",
                    title: "Berhasil",
                    text: "Data biaya berhasil disimpan!",
                });
            },
            error: function (xhr, status, error) {
                Swal.fire({
                    icon: "error",
                    title: "Gagal",
                    text: "Terjadi kesalahan saat menyimpan data!",
                });
                console.error(xhr.responseText);
            },
        });
    });
});

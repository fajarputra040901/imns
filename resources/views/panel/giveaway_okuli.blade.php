@extends('partial.admin')
@section('content')
    @php
        use App\Models\Giveawayokuli;
        $responden = Giveawayokuli::where('status', null)->count();
        $kandidat = Giveawayokuli::where('status', 'lolos')->count();
        $gugur = Giveawayokuli::where('status', 'gugur')->count();
    @endphp
    <h3 style="text-align: center">Giveaway Okuli</h3>
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-success border-success"></i>
                    </div>
                    <div class="stat-content dib">
                        <div class="stat-text">Responden</div>
                        <div class="stat-digit">{{ $responden }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-primary border-primary"></i>
                    </div>
                    <div class="stat-content dib">
                        <div class="stat-text">Kandidat</div>
                        <div class="stat-digit">{{ $kandidat }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-danger border-danger"></i>
                    </div>
                    <div class="stat-content dib">
                        <div class="stat-text">Gugur</div>
                        <div class="stat-digit">{{ $gugur }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

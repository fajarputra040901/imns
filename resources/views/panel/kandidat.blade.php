@extends('partial.admin')
@section('content')
    <h3 style="text-align: center">Kandidat Giveaway Okuli</h3>
    <table id="table" class="table table-stripped table-bordered">
        <thead>
            <tr>

                <th>Waktu Daftar</th>
                <th>Nama</th>
                <th>Username IG</th>
                <th>No WA</th>
                <th>Detail</th>
                <th>Aksi</th>
            </tr>
        </thead>
    </table>

    <div class="modal fade" id="detailres" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Detail Jawaban</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <table class="table ">
                        <tr>
                            <td>Nama</td>
                            <td id="nama"></td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td id="email"></td>
                        </tr>
                        <tr>
                            <td>Sumber</td>
                            <td id="sumber"></td>
                        </tr>
                        <tr>
                            <td>Username Ig</td>
                            <td id="ig"></td>
                        </tr>
                        <tr>
                            <td>No WA</td>
                            <td id="nohp"></td>
                        </tr>
                    </table>
                    <h6 style="text-align: center">Jawaban Pertanyaan</h6>
                    <p>Apa Tagline Indonesia Melihat</p>
                    <ul>
                        <li>Jawaban : <span id="per1"></span></li>
                    </ul><br>
                    <p>Siapa Ketua Dari Yayasan Indonesia Melihat</p>
                    <ul>
                        <li>Jawaban : <span id="per2"></span></li>
                    </ul><br>
                    <p>Titik Terjauh Kegiatan Yang Pernah Diselenggarakan
                        Indonesia Melihat</p>
                    <ul>
                        <li>Jawaban : <span id="per3"></span></li>
                    </ul><br>
                    <h6 style="text-align: center">Bukti Screenshoot</h6>
                    <img src="" id="folow" alt="">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            var dataTable = $('#table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: '/list_kandidat',
                "columnDefs": [{
                    "searchable": false,
                    "orderable": false,
                    "targets": 0 // Kolom nomor
                }],
                "order": [
                    [0, 'asc']
                ],
                columns: [{
                        data: 'waktu'
                    },
                    {
                        data: 'nama'
                    },
                    {
                        data: 'ig'
                    },
                    {
                        data: 'nohp'
                    },
                    {
                        data: 'detail',
                        name: 'detail',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'tombol',
                        name: 'tombol',
                        orderable: false,
                        searchable: false
                    }
                ]
            });
            $('#table').on('click', '.lolos', function() {
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah Anda Yakin?',
                    text: "Jadikan Responden Ini Menjadi Kandidat Pemenang!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Jadikan Kandidat!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: 'GET',
                            url: '/lolos/' + id,

                            success: function(res) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Responden Menjadi Kandidat!'

                                })
                                refreshDataTable();
                            },
                            error: function(error) {

                                console.error(error);
                            }

                        })
                    }
                })


            })

            $('#table').on('click', '.gugur', function() {
                var id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah Anda Yakin?',
                    text: "Menggugurkan Kandidat Ini!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Gugurkan!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            type: 'GET',
                            url: '/gugurkan/' + id,

                            success: function(res) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Responden Gugur Menjadi Kandidat!'

                                })
                                refreshDataTable();
                            },
                            error: function(error) {

                                console.error(error);
                            }

                        })
                    }
                })


            })
            $('#table').on('click', '.detail', function() {
                var id = $(this).data('id');

                $.ajax({
                    type: 'GET',
                    url: '/detail/' + id,

                    success: function(res) {
                        $('#nama').text(res.nama);
                        $('#email').text(res.email);
                        $('#nohp').text(res.nohp);
                        $('#ig').text(res.ig);
                        $('#folow').attr('src', res.folow);
                        $('#per1').text(res.per1);
                        $('#per2').text(res.per2);
                        $('#per3').text(res.per3);
                        $('#sumber').text(res.sumber);
                        $('#detailres').modal('show');
                    },
                    error: function(error) {

                        console.error(error);
                    }

                })


            })

            // Membuat fungsi untuk mengambil data ulang
            function refreshDataTable() {
                dataTable.ajax.reload(); // Ini akan memicu pengambilan data ulang dari sumber data Ajax
            }



            // Menghubungkan tombol refresh dengan fungsi refreshDataTable
            $('#refreshData').on('click', function() {
                refreshDataTable();
            });
        });
    </script>
@endsection

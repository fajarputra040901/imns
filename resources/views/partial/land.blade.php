<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @stack('meta')
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/hash.png">
    <link rel="icon" type="image/png" href="/assets/img/hash.png">
    <title>Indonesia Melihat</title>
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous"> --}}

    <link rel="stylesheet" href="{{ asset('aset_landing/vendors/animate.css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ asset('aset_landing/css/style.css') }}">
    <script src="{{ asset('aset_landing/vendors/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('aset_landing/js/loader.js') }}"></script>
    <meta name="google-adsense-account" content="ca-pub-9967513248230041">
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> --}}
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"> --}}
    </script>
    <link rel="stylesheet" href="{{ asset('date/css/datepicker.css') }}">
    {{-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script> --}}
    <script src="/sweet/sweetalert2.all.js"></script>
    <!-- Fancybox CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4/dist/fancybox.css" />
    <script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4/dist/fancybox.umd.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <link rel="stylesheet" type="text/css"
        href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css" />
    <script src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" href="{{ asset('portal/') }}/style/loader.css" />

</head>
<style>
    @keyframes blink {
        0% {
            opacity: 1;
        }

        50% {
            opacity: 0;
        }

        100% {
            opacity: 1;
        }
    }

    .blinking {
        animation: blink 1s infinite;
    }

    .material-symbols-rounded {
        font-variation-settings:
            'FILL' 0,
            'wght' 200,
            'GRAD' 200,
            'opsz' 20
    }

    .content {
        display: flex;
        justify-content: center;
        align-items: center;
        height: 100vh;
        padding: 1rem;

    }

    @media (max-width: 567px) {
        h1 {
            font-size: 7vw;
            text-align: center;
        }
    }


    /* Loader Styles start here */
</style>
@stack('style')

<body>
    <div id="preloader" style="display: none" class="preloader">

        <div class="loader"></div>

    </div>
    <div class="oleez-loader"></div>
    <header class="oleez-header">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="#">
                <img src="/assets/img/logo_imn_bg.png" class="naom" alt="Indonesia Melihat">
                {{-- Indonesia Melihat --}}
            </a>
            <ul class="nav nav-actions d-lg-none ml-auto">
                {{-- <li class="nav-item active">
                    <a class="nav-link" href="#!" data-toggle="searchModal">
                        <img src="assets/images/search.svg" alt="search">
                    </a>
                </li> --}}
                {{-- <li class="nav-item nav-item-cart">
                    <a class="nav-link" href="#!">
                        <span class="cart-item-count">0</span>
                        <img src="assets/images/shopping-cart.svg" alt="cart">
                    </a>
                </li> --}}
                {{-- <li class="nav-item dropdown d-none d-sm-block">
                    <a class="nav-link dropdown-toggle" href="#!" id="languageDropdown" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">ENG</a>
                    <div class="dropdown-menu" aria-labelledby="languageDropdown">
                        <a class="dropdown-item" href="#!">ARB</a>
                        <a class="dropdown-item" href="#!">FRE</a>
                    </div>
                </li> --}}
                <li class="nav-item">
                    <a class="nav-link" href="#!" data-toggle="offCanvasMenu">
                        <img src="{{ asset('aset_landing/images/social icon@2x.svg') }}" alt="social-nav-toggle">
                    </a>
                </li>
            </ul>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#oleezMainNav"
                aria-controls="oleezMainNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="oleezMainNav">
                <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                    {{-- <li class="nav-item active">
                        <a class="nav-link" href="index.html">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about.html">About</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#!" id="pagesDropdown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Pages</a>
                        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                            <a class="dropdown-item" href="shop.html">Shop</a>
                            <a class="dropdown-item" href="contact.html">Contact</a>
                            <a class="dropdown-item" href="404.html">404</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#!" id="portfolioDropdown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Portfolio</a>
                        <div class="dropdown-menu" aria-labelledby="portfolioDropdown">
                            <a class="dropdown-item" href="portfolio-list.html">Portfolio list</a>
                            <a class="dropdown-item" href="portfolio.html">Portfolio grid</a>
                            <a class="dropdown-item" href="portfolio-masonry.html">Portfolio masonry</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#!" id="blogDropdown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">Blog</a>
                        <div class="dropdown-menu" aria-labelledby="blogDropdown">
                            <a class="dropdown-item" href="blog-standard.html">Blog Standard</a>
                            <a class="dropdown-item" href="blog-grid.html">Blog grid</a>
                            <a class="dropdown-item" href="blog-single.html">Blog Post</a>
                        </div>
                    </li> --}}
                </ul>
                <ul class="navbar-nav d-none d-lg-flex">
                    {{-- <li class="nav-item active">
                        <a class="nav-link nav-link-btn" href="#!" data-toggle="searchModal">
                            <img src="assets/images/search.svg" alt="search">
                        </a>
                    </li>
                    <li class="nav-item nav-item-cart">
                        <a class="nav-link nav-link-btn" href="#!">
                            <span class="cart-item-count">0</span>
                            <img src="assets/images/shopping-cart.svg" alt="cart">
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle " href="#!" id="languageDropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ENG</a>
                        <div class="dropdown-menu" aria-labelledby="languageDropdown">
                            <a class="dropdown-item" href="#!">ARB</a>
                            <a class="dropdown-item" href="#!">FRE</a>
                        </div>
                    </li> --}}
                    <li class="nav-item ml-5">
                        <a class="nav-link pr-0 nav-link-btn" href="#!" data-toggle="offCanvasMenu">
                            <img src="{{ asset('aset_landing/images/social icon@2x.svg') }}" alt="social-nav-toggle">
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer class="oleez-footer wow fadeInUp">
        <div class="container">
            <div class="footer-content">
                <div class="row">
                    <div class="col-md-6">
                        <img src="/assets/img/logo_imn_wg.png" alt="Indonesia Melihat" style="width: 70%;">
                        <p class="footer-intro-text"><i>Everyone Can See Better!</i></p>
                        <nav class="footer-social-links">
                            <a target="_blank" href="https://www.instagram.com/indonesia.melihat/">Instagram</a>
                            {{-- <a href="#!">Tw</a>
                            <a href="#!">In</a>
                            <a href="#!">Be</a> --}}
                        </nav>
                    </div>
                    {{-- <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6 footer-widget-text">
                                <h6 class="widget-title">PHONE</h6>
                                <p class="widget-content">+80 (0)5 12 34 95 89</p>
                            </div>
                            <div class="col-md-6 footer-widget-text">
                                <h6 class="widget-title">ENQUIRUES</h6>
                                <p class="widget-content">info@oleez.site</p>
                            </div>
                            <div class="col-md-6 footer-widget-text">
                                <h6 class="widget-title">ADDRESS</h6>
                                <p class="widget-content">33 rue Burdeau 69089 <br> Paris France</p>
                            </div>
                            <div class="col-md-6 footer-widget-text">
                                <h6 class="widget-title">WORK HOURS</h6>
                                <p class="widget-content">Weekdays: 09:00 - 18:00 <br> Weekends: 11:00 - 17:00</p>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
            <div class="footer-text">
                <p class="mb-md-0">© 2024, Yayasan Indonesia Melihat. Made with passion by <a href="#"
                        target="_blank" rel="noopener noreferrer" class="text-reset">Fajar Putra</a>.</p>
                <p class="mb-0">All right reserved.</p>
            </div>
        </div>
    </footer>

    <!-- Modals -->
    <!-- Off canvas social menu -->
    <nav id="offCanvasMenu" class="off-canvas-menu">
        <button type="button" class="close" aria-label="Close" data-dismiss="offCanvasMenu">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul class="oleez-social-menu">

            <li>
                <a href="https://www.instagram.com/indonesia.melihat/" target="_blank"
                    class="oleez-social-menu-link">Instagram</a>
            </li>

        </ul>
    </nav>
    <!-- Full screen search box -->
    <div id="searchModal" class="search-modal">
        <button type="button" class="close" aria-label="Close" data-dismiss="searchModal">
            <span aria-hidden="true">&times;</span>
        </button>
        <form action="index.html" method="get" class="oleez-overlay-search-form">
            <label for="search" class="sr-only">Search</label>
            <input type="search" class="oleez-overlay-search-input" id="search" name="search"
                placeholder="Search here">
        </form>
    </div>
    <script src="{{ asset('aset_landing/vendors/popper.js/popper.min.js') }}"></script>
    <script src="{{ asset('aset_landing/vendors/wowjs/wow.min.js') }}"></script>
    <script src="{{ asset('aset_landing/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('aset_landing/vendors/slick-carousel/slick.min.js') }}"></script>
    <script src="{{ asset('aset_landing/js/main.js') }}"></script>
    <script>
        new WOW().init();
    </script>
</body>

</html>

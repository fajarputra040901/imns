@extends('partial.index')
@push('style')
    {{-- <style>
        .blog-post-card {
            display: flex;
            flex-direction: column;
            border: 1px solid #ddd;
            border-radius: 8px;
            min-height: 500px;
            overflow: hidden;
            height: 100%;
            position: relative;
        }

        .blog-post-thumbnail-wrapper {
            flex: 1;
            overflow: hidden;
        }

        .blog-post-thumbnail-wrapper img {
            width: 100%;
            height: auto;
        }

        .blog-post-date {
            font-size: 14px;
            color: #888;
        }

        .blog-post-title {
            font-size: 16px;
            margin: 10px 0;
        }

        .blog-post-address {
            font-size: 14px;
            color: #555;
            margin-bottom: 15px;
        }

        .blog-post-footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            text-align: right;
            padding: 10px;
            background: #f9f9f9;
            border-top: 1px solid #ddd;
        }

        .blog-post-footer .btn {
            margin: 0;
        }
    </style> --}}
@endpush
@section('content')
    <main class="blog-grid-page">
        <div class="container">
            <h1 class="oleez-page-title wow fadeInUp">Webinar / Seminar Yayasan Indonesia Melihat</h1>
            <p class="wow fadeInUp">Ayo daftarkan diri anda pada webinar / seminar </p>
            <input type="text" class="wow fadeInUp" id="search" placeholder="Cari webinar / seminar..."
                style="margin-bottom: 20px; width: 100%; padding: 10px;">
            <div id="webinar" class="row"></div>
            <nav aria-label="Page navigation">
                <ul class="pagination justify-content-end" id="pagination"></ul>
            </nav>

        </div>

        </div>
    </main>


    <script>
        const itemsPerPage = 6;
        let currentPage = 1;

        function loadWebinars(page = 1) {
            $.ajax({
                url: "/get_webinar_user",
                type: "GET",
                data: {
                    page: page
                },
                beforeSend: function() {
                    $("#webinar").html('<p class="text-center">Loading...</p>');
                },
                success: function(response) {
                    $("#webinar").empty();
                    response.data.forEach(function(webinar) {
                        if (webinar.visabilitas === "Y") {
                            let thumbnailSrc = "placeholder.png";
                            if (webinar.cover) {
                                let coverData = JSON.parse(webinar.cover);
                                let thumbnailCover = coverData.find(item => item.thumbnail === true);
                                if (thumbnailCover) {
                                    thumbnailSrc = thumbnailCover.src;
                                }
                            }
                            const tanggalMulai = moment(webinar.tanggal_mulai, "YYYY-MM-DD").format(
                                "DD-MM-YYYY");
                            const tanggalSelesai = moment(webinar.tanggal_selesai, "YYYY-MM-DD").format(
                                "DD-MM-YYYY");
                            const durasi = moment(webinar.tanggal_selesai, "YYYY-MM-DD").diff(
                                moment(webinar.tanggal_mulai, "YYYY-MM-DD"), "days") + 1;

                            $("#webinar").append(`
            <div class="col-md-4">
                <div class="blog-post-card wow fadeInUp" style="min-height: 350px;">
                    <div class="blog-post-thumbnail-wrapper">
                        <a href="${thumbnailSrc}" data-fancybox="single" data-caption="${webinar.judul}">
                            <img src="${thumbnailSrc}" alt="Cover Webinar" style="max-height: 200px; object-fit: cover;">
                        </a>
                    </div>
                    <p class="blog-post-date">${tanggalMulai} s/d ${tanggalSelesai} | ${durasi} Hari</p>
                    <h5 class="blog-post-title">${webinar.judul}</h5>
                    <p class="blog-post-address">${webinar.tempat} | ${webinar.skp} SKP</p>
                    <div class="blog-post-footer">
                        <a href="/w/${webinar.slug}" class="btn btn-primary" id="pilih">Lihat Selengkapnya</a>
                    </div>
                </div>
            </div>
        `);
                        }
                    });

                    Fancybox.bind('[data-fancybox]', {});
                    renderPagination(response);
                },
                error: function() {
                    $("#webinar").html('<p class="text-center text-danger">Failed to load data</p>');
                },
            });
        }

        function renderPagination(data) {
            $("#pagination").empty();
            if (data.prev_page_url) {
                $("#pagination").append(
                    `<button class="btn btn-secondary mx-1" onclick="loadWebinars(${data.current_page - 1})">Previous</button>`
                );
            }
            for (let i = 1; i <= data.last_page; i++) {
                $("#pagination").append(
                    `<button class="btn ${data.current_page === i ? 'btn-primary' : 'btn-secondary'} mx-1" onclick="loadWebinars(${i})">${i}</button>`
                );
            }
            if (data.next_page_url) {
                $("#pagination").append(
                    `<button class="btn btn-secondary mx-1" onclick="loadWebinars(${data.current_page + 1})">Next</button>`
                );
            }
        }

        function filterWebinars() {
            let searchValue = $("#search").val().toLowerCase();
            $("#webinar .blog-post-card").each(function() {
                let title = $(this).find(".blog-post-title").text().toLowerCase();
                let address = $(this).find(".blog-post-address").text().toLowerCase();
                if (title.includes(searchValue) || address.includes(searchValue)) {
                    $(this).closest(".col-md-4").show();
                } else {
                    $(this).closest(".col-md-4").hide();
                }
            });
            currentPage = 1;
        }

        $(document).ready(function() {
            loadWebinars();
            $("#search").on("input", function() {
                filterWebinars();
            });
        });
    </script>
@endsection

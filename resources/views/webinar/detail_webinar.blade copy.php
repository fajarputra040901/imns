@extends('partial.land')
@push('meta')
    @php
        // Decode data cover yang berupa JSON
        $coverImages = json_decode($data->cover, true);

        // Cari gambar yang memiliki thumbnail = true
        $mainImage = array_filter($coverImages, function ($item) {
            return $item['thumbnail'] === true;
        });

        $mainImageSrc = '';
        if (!empty($mainImage)) {
            $base64Image = reset($mainImage)['src']; // Base64 string
            $base64String = explode(',', $base64Image, 2);
            $base64Data = $base64String[1] ?? $base64String[0];

            // Decode base64
            $binaryData = base64_decode($base64Data);
            if ($binaryData === false) {
                die('Invalid base64 string.');
            }

            // Deteksi MIME type
            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $mimeType = $finfo->buffer($binaryData);

            // Tentukan ekstensi
            $extensions = [
                'image/jpeg' => 'jpg',
                'image/png' => 'png',
                'image/gif' => 'gif',
            ];
            $extension = $extensions[$mimeType] ?? 'jpg'; // Default ke jpg

            // Path file
            $hashName = md5($base64Image) . '.' . $extension;
            $filePath = 'thumbnail/' . $hashName;

            // Periksa apakah file sudah ada
            if (!file_exists(public_path($filePath))) {
                file_put_contents(public_path($filePath), $binaryData);
            }

            // Ambil URL file
            $mainImageSrc = url('public/' . $filePath);
        }
        function cleanText($text, $limit = 150)
        {
            // Decode entitas HTML
            $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
            // Hapus tag HTML
            $text = strip_tags($text);
            // Hapus spasi tambahan
            $text = str_replace("\u{00A0}", ' ', $text); // Menghapus non-breaking space (Unicode)
            $text = preg_replace('/\s+/', ' ', $text);
            // Potong teks jika lebih panjang dari batas
            if (strlen($text) > $limit) {
                $text = substr($text, 0, $limit) . '...';
            }
            return $text;
        }

        // Dapatkan deskripsi dari data
        $deskripsi = $data->deskripsi ?? '';
        // Buat ringkasan
        $summary = cleanText($deskripsi);
    @endphp




    <meta property="og:title" content="{{ $data->judul }}" />
    <meta property="og:description" content="{{ $summary }}" />
    <meta property="og:image" content="{{ $mainImageSrc }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="article" />

    <meta name="twitter:card" content="{{ $mainImageSrc }}">
    <meta name="twitter:title" content="{{ $data->judul }}">
    <meta name="twitter:description" content="{{ $summary }}">
    <meta name="twitter:image" content="{{ $mainImageSrc }}">
@endpush
@push('style')
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@7.2.96/css/materialdesignicons.min.css" rel="stylesheet">
    <style>
        .slider {
            width: 100%;
            max-width: 600px;
            /* Atur lebar sesuai dengan kebutuhan */
            margin: 0 auto;
            /* Agar berada di tengah */
        }

        /* CSS untuk gambar slider */
        .slider-image {
            width: 100%;
            height: auto;
            /* Mengatur agar gambar responsive */
        }
    </style>
    <style>
        .post-header {
            background-color: #f8f9fa;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }


        .post-title {
            font-size: 24px;
            font-weight: bold;
            margin-top: 20px;
        }

        .post-type,
        .post-skp,
        .post-location {
            font-size: 16px;
            color: #6c757d;
        }

        .post-description {
            margin-top: 15px;
            line-height: 1.6;
        }

        .comment-section {
            margin-top: 30px;
            margin-bottom: 30px;
            padding: 15px;
            background-color: #e9ecef;
            border-radius: 8px;
        }

        .form-control {
            border-radius: 5px;
            border: 1px solid #ced4da;
        }
    </style>
@endpush
@section('content')
    <main class="blog-post-single">
        <div class="container">
            <h1 class="post-title wow fadeInUp">{{ $data->judul }}</h1>
            <div class="row">
                <div class="col-md-8 blog-post-wrapper">
                    <div class="post-header wow fadeInUp">
                        @php
                            $covers = json_decode($data->cover, true);
                            $thumbnailImage = array_filter($covers, function ($item) {
                                return $item['thumbnail'] === true;
                            });
                            $nonThumbnailImages = array_filter($covers, function ($item) {
                                return $item['thumbnail'] === false;
                            });
                            $sortedImages = array_merge($thumbnailImage, $nonThumbnailImages);
                        @endphp

                        <div class="slider">
                            @foreach ($sortedImages as $cover)
                                @if (isset($cover['src']))
                                    <div class="slide">
                                        <a href="{{ $cover['src'] }}" data-fancybox="gallery"
                                            data-caption="{{ $data->judul }}">
                                            <img src="{{ $cover['src'] }}" alt="Cover Image" loading="lazy"
                                                class="slider-image">
                                        </a>
                                    </div>
                                @else
                                    <p>No image found</p>
                                @endif
                            @endforeach
                        </div>

                        @php
                            use Carbon\Carbon;
                            $tanggalMulai = Carbon::parse($data->tanggal_mulai);
                            $tanggalAkhir = Carbon::parse($data->tanggal_selesai);
                            $formattedTanggalMulai = $tanggalMulai->format('d/m/Y');
                            $formattedTanggalAkhir = $tanggalAkhir->format('d/m/Y');
                            if ($tanggalMulai->equalTo($tanggalAkhir)) {
                                $formattedTanggal = $formattedTanggalMulai;
                                $durasiHari = 1;
                            } else {
                                $formattedTanggal = $formattedTanggalMulai . ' s/d ' . $formattedTanggalAkhir;
                                $durasiHari = $tanggalMulai->diffInDays($tanggalAkhir) + 1;
                            }
                        @endphp

                        <p class="post-date">{{ $formattedTanggal }} | {{ $durasiHari }} Hari</p>
                    </div>

                    <div class="post-content wow fadeInUp">
                        <h2 class="post-title">{{ $data->judul }}</h2>
                        <h3>{{ $data->cakupan }}</h3>
                        <p class="post-type">Metode Pembelajaran: {{ $data->jenis }}</p>
                        <p class="post-skp">Perolehan SKP : {{ $data->skp }} </p>
                        <p class="post-location">Tempat Pelaksanaan : {{ $data->tempat }}</p>

                        <div class="post-description" style="text-align: justify">
                            {!! $data->deskripsi !!}
                        </div>

                        <div class="comment-section wow fadeInUp">
                            <button type="button" class="btn btn-primary" id="btnPanduanPendaftaran">Panduan
                                Pendaftaran</button>

                            <hr>

                            <div id="formdaftar mt-2">
                                <h4>Formulir Pendaftaran</h4>
                                <form action="" id="formulir_pendaftaran">
                                    @csrf
                                    <div id="formnya"></div>
                                    <select name="paket" id="biaya" class="form-control" data-validations="required">
                                        <option value="">Pilih Paket</option>
                                    </select>
                                    <div id="keterangan" class="mt-3"></div>
                                    <div class="mb-3">
                                        <label for="Harga" class="form-label">Harga</label>
                                        <input type="text" class="form-control harga" id="harga" name="harga">
                                        <span id="keterangan_harga"></span>
                                    </div>
                                    <div class="mb-3" id="donasikan">
                                        <label for="Harga" class="form-label">Donasi</label>
                                        <input type="text" class="form-control harga" id="donasi" name="donasi"
                                            placeholder="Donasi">
                                        <span><i>Optional</i></span>
                                    </div>
                                    <input type="hidden" name="id" id="id" value="{{ $data->id }}">
                                    <div class="mt-2">
                                        <button class="btn btn-primary" id="submit">Daftar</button>
                                    </div>
                                </form>
                            </div>
                            <div class="mb-2 mt-3">
                                <p>Apabila terdapat kesulitan dapat menghubungi Contact person :</p>
                                <div style="display: inline-flex; align-items: center; gap: 10px;">
                                    <i class="mdi mdi-headset mdi-24px" style="color: #007bff;"></i> <!-- Ikon Headset -->
                                    <h5 class="h5 mb-0">Contact Person</h5>
                                </div>


                                <!-- Tombol WhatsApp -->
                                <p class="mt-2">Butuh bantuan? Hubungi kami melalui WhatsApp:</p>
                                @php
                                    // Decode JSON menjadi array
                                    $contactPersons = json_decode($data->contact_person, true);
                                    $adminCount = 1; // Counter untuk anonim admin
                                @endphp

                                @foreach ($contactPersons as $contact)
                                    @php
                                        // Ubah nomor telepon menjadi format 62
                                        $phoneNumber = '62' . ltrim($contact['no_cp'], '0');
                                    @endphp

                                    @if ($contact['status'] === 'N')
                                        <!-- Jika status N, tampilkan nama asli -->
                                        <a href="https://wa.me/{{ $phoneNumber }}?text=Halo+Saya+butuh+bantuan"
                                            target="_blank" class="btn btn-success d-block mb-2">
                                            <i class="mdi mdi-whatsapp"></i> Hubungi {{ $contact['nama'] }}
                                        </a>
                                    @else
                                        <!-- Jika status Y, tampilkan anonim -->
                                        <a href="https://wa.me/{{ $phoneNumber }}?text=Halo+Saya+butuh+bantuan"
                                            target="_blank" class="btn btn-success d-block mb-2">
                                            <i class="mdi mdi-whatsapp"></i> Hubungi Admin {{ $adminCount }}
                                        </a>
                                        @php
                                            $adminCount++;
                                        @endphp
                                    @endif
                                @endforeach
                            </div>
                        </div>

                    </div>





                </div>
                <div class="col-md-4">
                    <div class="sidebar-widget wow fadeInUp">
                        <h5 class="widget-title">Cek Kepesertaan Webinar</h5>
                        <form id="cekKepesertaanForm" action="#">
                            <input type="hidden" value="{{ $data->id }}" name="id">
                            <div class="form-group mb-3">
                                <label for="noTransaksi">No Transaksi</label>
                                <input type="text" class="form-control" id="id_transaksi" name="id_transaksi"
                                    placeholder="Masukkan No Transaksi">
                            </div>
                            <hr>
                            <p style="text-align: center">Atau</p>
                            <hr>

                            @php
                                $formFields = json_decode($data->form, true);
                                $primaryFields = array_filter($formFields, fn($field) => $field['primary'] === 'true');
                            @endphp

                            @foreach ($primaryFields as $field)
                                <div class="form-group mb-3">
                                    <label for="{{ $field['id'] }}">{{ $field['label'] }}</label>

                                    @if ($field['type'] === 'select')
                                        <select class="form-control" id="{{ $field['id'] }}"
                                            name="{{ $field['name'] }}">
                                            @foreach ($field['options'] ?? [] as $option)
                                                <option value="{{ $option }}">{{ ucfirst($option) }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <input type="{{ $field['type'] }}" class="form-control"
                                            id="{{ $field['id'] }}" name="{{ $field['name'] }}"
                                            placeholder="{{ $field['placeholder'] }}">
                                    @endif


                                </div>
                            @endforeach
                            <button class="btn btn-primary w-100" id="cek">Cek Kepesertaan</button>
                        </form>

                    </div>
                </div>

            </div>
        </div>

    </main>
    <div class="modal fade" id="modal_panduan_pendaftaran" tabindex="-1" aria-labelledby="modalPanduanPendaftaranLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPanduanPendaftaranLabel">Panduan Pendaftaran</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <ul style="text-align: justify">
                        <li>Peserta membuka <strong>laman webinar</strong> yang tersedia.</li>
                        <li>
                            Harap membaca dengan seksama <span style="color: blue;">deskripsi webinar</span>, termasuk
                            cakupan, metode pembelajaran, jumlah SKP, dan informasi lainnya sebelum mendaftar.
                        </li>
                        <li>
                            Peserta mengisi <strong>Formulir Pendaftaran</strong> sesuai dengan input yang diminta.
                            <u>Pastikan semua input terisi</u> sebelum mengirimkan formulir.
                        </li>
                        <li>
                            Jika muncul notifikasi <span style="color: red;">"Terjadi Kesalahan Saat Mengirim Data"</span>,
                            lakukan langkah berikut:
                            <ol>
                                <li>Refresh halaman terlebih dahulu, lalu coba kembali.</li>
                                <li>Jika masalah berlanjut, hapus histori dan cache browser Anda.</li>
                                <li>Jika tetap bermasalah, coba gunakan browser lain seperti <strong>Chrome, Safari, atau
                                        Mozilla Firefox</strong>.</li>
                                <li>Jika masih mengalami kendala, laporkan kepada <strong>Contact Person</strong> yang
                                    tersedia.</li>
                            </ol>
                        </li>
                        <li>
                            Jika pendaftaran berhasil, peserta akan diarahkan ke <strong>laman pembayaran</strong>.
                            Untuk informasi lebih lanjut, silakan baca panduan di laman pembayaran.
                        </li>
                        <li>
                            <u>Peserta hanya dapat mendaftar satu kali</u>. Jika terdapat perubahan informasi atau paket
                            yang dipilih, segera laporkan kepada <strong>Contact Person</strong>.
                        </li>
                        <li>
                            Jika mengalami kesulitan, <span>jangan ragu untuk menghubungi Contact
                                Person</span> yang tersedia.
                        </li>
                    </ul>
                    <p>Terima kasih dan <strong>Salam Sehat</strong>.</p>
                </div>
                <div class="modal-footer">
                    <!-- Tombol Tutup dengan ID tombol_tutup -->
                    <button type="button" class="btn btn-secondary" id="tombol_tutup"
                        data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        var idnya = {{ $data->id }};
    </script>


    <script>
        $(document).ready(function() {
            $('#btnPanduanPendaftaran').click(function() {
                $('#modal_panduan_pendaftaran').modal('show'); // Menampilkan modal dengan animasi fade
            });
            $('.btn-close, .modal').click(function(event) {
                if ($(event.target).is('.btn-close') || $(event.target).is('.modal')) {
                    $('#modal_panduan_pendaftaran').modal('hide'); // Menutup modal dengan animasi fade
                }
            });
            $('#tombol_tutup').click(function(event) {
                $('#modal_panduan_pendaftaran').modal('hide');
            });

            function loader(show) {
                if (show) {
                    $('#preloader').show(); // Menampilkan preloader
                } else {
                    $('#preloader').hide(); // Menyembunyikan preloader
                }
            }
            var id = "{{ $data->id }}";

            $('.slider').slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                dots: true,
                autoplay: true,
                autoplaySpeed: 3000
            });
            Fancybox.bind('[data-fancybox="gallery"]', {
                // Customization if needed
            });

            $('#cek').on('click', function(event) {
                event.preventDefault();
                loader(true)

                const formData = $('#cekKepesertaanForm')
                    .serializeArray(); // Pastikan memilih form dengan id yang benar
                let formObject = {};

                formData.forEach(item => {
                    formObject[item.name] = item.value.trim();
                });

                // Cek apakah id_transaksi diisi atau ada input lain yang diisi
                let isValid = formObject.id_transaksi.trim() !== "" || Object.keys(formObject).some(key =>
                    key !== 'id_transaksi' && key !== '_token' && formObject[key] !== "");

                if (!isValid) {
                    loader(false)
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Harap isi No Transaksi atau salah satu form lainnya.',
                    });
                    return;
                }

                // Jika id_transaksi kosong dan ada input lain yang terisi, jadikan input lain required
                if (formObject.id_transaksi.trim() === "") {
                    $('#cekKepesertaanForm input, #cekKepesertaanForm select').each(function() {
                        const input = $(this);
                        if (input.val().trim() === "") {
                            input.prop('required',
                                true); // Menambahkan required ke input yang kosong
                        } else {
                            input.prop('required', false); // Hapus required jika input sudah terisi
                        }
                    });
                }

                $.ajax({
                    url: '/cek_peserta', // Ganti 1 dengan ID webinar yang sesuai
                    method: 'POST',
                    data: JSON.stringify(formObject), // Pastikan data dikirim dalam format JSON
                    contentType: 'application/json', // Tentukan tipe konten sebagai JSON
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        loader(false)
                        if (response.success) {
                            if (response.status === 'undangan') {
                                Swal.fire({
                                    icon: "success",
                                    title: "Success",
                                    text: "Anda terdaftar di webinar ini melalui jalur undangan. ",
                                });
                            } else {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sukses',
                                    text: response.message +
                                        " Menuju Detail Transaksi?",
                                    showCancelButton: true, // Tampilkan tombol Cancel
                                    confirmButtonText: 'Ya',
                                    cancelButtonText: 'Tidak',
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        // Jika memilih 'Ya', arahkan ke halaman detail transaksi
                                        window.location.href = '/w/t/' + response
                                            .id_transaksi;
                                    }
                                });
                            }



                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.message,
                            });
                        }
                    },
                    error: function(xhr) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Galat',
                            text: 'Terjadi kesalahan saat memproses data.',
                        });
                        console.error(xhr);
                    }
                });
            });


        });
    </script>

    <script src="/portal/js/tes_form.js"></script>
@endsection

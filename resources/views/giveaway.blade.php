@extends('partial.index')
@section('content')
    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">

                <ol>
                    <li><a href="/">Beranda</a></li>
                    <li>Giveaway Okuli</li>
                </ol>
                <h2> Giveaway Okuli</h2>

            </div>
        </section><!-- End Breadcrumbs -->

        <main id="main">

            <section class="inner-page">
                <div class="container">
                    <h3 style="text-align: center">Giveaway Okuli</h3>
                    <p>Syarat Dan Ketentuan</p>
                    <ul>
                        <li>Isi Semua Form DI Bawah Ini</li>
                        <li>Follow Instagram @indonesia.melihat <a
                                href="https://instagram.com/indonesia.melihat?igshid=MmU2YjMzNjRlOQ==">Klik Disini</a></li>
                        <li>Sertakan Bukti Follow Pada Form Di Bawah</li>
                        <li>Pastikan Akun mu Tidak Di Privat</li>
                        <li>Giveaway Dibuka Hingga 2 November 2023</li>

                    </ul>
                    <form id="form" action="giveaway_post" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-4 ">
                            <label for="nama" class="form-label">Nama</label>
                            <input required type="text" class="form-control" id="nama" placeholder="Masukan Nama"
                                name="nama">
                        </div>
                        <div class="mb-4 mt-4">
                            <label for="email" class="form-label">Email</label>
                            <input required type="email" class="form-control" id="email" placeholder="Masukan Email"
                                name="email">
                        </div>
                        <div class="mb-4 mt-4">
                            <label for="nohp" class="form-label">No Whatsapp</label>
                            <input required type="number" class="form-control" id="nohp"
                                placeholder="Masukan No Whatsapp" name="nohp">
                        </div>
                        <div class="mb-4 mt-4">
                            <label for="sumber" class="form-label">Anda Mengetahui Event OKULI Dari</label>
                            <input required type="text" class="form-control" id="sumber"
                                placeholder="etc: Instagram , Whatsapp, Atau Masukan Sumber Lain" name="sumber">
                        </div>
                        <div class="mb-4 mt-4">
                            <label for="follow" class="form-label">Screenshoot Bukti Foloow Instagram
                                @indonesia.melihat</label>
                            <input required type="file" class="form-control" id="follow" name="follow">
                        </div>

                        <div class="mb-4 mt-4">
                            <label for="ig" class="form-label">Username Instagram</label>
                            <input required type="text" class="form-control" id="ig"
                                placeholder="Masukan Username Instagram" name="ig">
                        </div>
                        <p>Jawab Pertanyaan Berikut :</p>
                        <div class="mb-4 mt-4">
                            <label for="per1" class="form-label">Apa Tagline Indonesia Melihat</label>
                            <input required type="text" class="form-control" id="per1" name="per1">
                        </div>
                        <div class="mb-4 mt-4">
                            <label for="per2" class="form-label">Siapa Ketua Dari Yayasan Indonesia Melihat</label>
                            <input required type="text" class="form-control" id="per2" name="per2">
                        </div>
                        <div class="mb-4 mt-4">
                            <label for="per3" class="form-label">Titik Terjauh Kegiatan Yang Pernah Diselenggarakan
                                Indonesia Melihat</label>
                            <input required type="text" class="form-control" id="per3" name="per3">
                        </div>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </form>
                </div>
            </section>

        </main><!-- End #main -->
        {{-- <script>
            $(document).ready(function() {
                $('#form').submit(function(e) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    $.ajax({
                        type: 'POST',
                        url: '/giveaway_post',
                        data: formData,
                        success: function(response) {
                            if (response.status === "error") {
                                alert(response.pesan);
                            } else if (response.status === "tidak") {
                                window.location.href = "/finish_giveaway_okuli";
                            } else {
                                alert(response.pesan);
                            }
                        }
                    });
                });
            });
        </script> --}}
    @endsection

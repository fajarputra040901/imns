<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate Builder</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.10.1/jszip.min.js"></script>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
            background-color: #f0f0f0;
        }

        .container {
            display: flex;
            gap: 20px;
        }

        .controls {
            flex: 1;
            background: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .preview {
            flex: 2;
            background: white;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
        }

        .canvas-container {
            position: relative;
            width: 800px;
            height: 600px;
            border: 1px solid #ccc;
            margin-top: 20px;
            overflow: hidden;
        }

        .text-box {
            position: absolute;
            background: rgba(255, 255, 255, 0.8);
            border: 1px dashed #666;
            padding: 5px;
            cursor: move;
            /* Menambahkan cursor move untuk indikasi bisa di-drag */
            user-select: none;
            /* Mencegah seleksi teks saat drag */
        }

        .text-box.dragging {
            opacity: 0.8;
            z-index: 1000;
        }

        .preview-container {
            width: 800px;
            height: 600px;
            position: relative;
            border: 1px solid #ccc;
            margin-top: 20px;
        }

        button {
            background: #4CAF50;
            color: white;
            border: none;
            padding: 10px 20px;
            border-radius: 4px;
            cursor: pointer;
            margin: 5px;
        }

        button:hover {
            background: #45a049;
        }

        input,
        select,
        textarea {
            margin: 5px 0;
            padding: 5px;
            width: 100%;
        }

        .text-box-controls {
            background: #f5f5f5;
            padding: 10px;
            margin: 10px 0;
            border-radius: 4px;
        }

        .number-input {
            width: 60px;
            margin-right: 10px;
        }

        label {
            display: inline-block;
            width: 80px;
            margin-right: 10px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="controls">
            <h2>Certificate Builder</h2>
            <div>
                <h3>Template Upload</h3>
                <input type="file" id="templateUpload" accept="image/*">
            </div>

            <div>
                <h3>Layout Controls</h3>
                <button onclick="addTextBox()">Add Text Box</button>
                <div id="textBoxControls"></div>
            </div>

            <div>
                <h3>Layout Configuration</h3>
                <textarea id="layoutConfig" rows="10" placeholder="Layout configuration will appear here..."></textarea>
                <button onclick="saveLayout()">Save Layout</button>
                <button onclick="loadLayout()">Load Layout</button>
            </div>

            <div>
                <h3>Data Upload</h3>
                <input type="file" id="jsonUpload" accept=".json">
                <button onclick="generateCertificates()">Generate All Certificates</button>
            </div>
        </div>
        <div class="preview">
            <h2>Preview</h2>
            <div class="canvas-container" id="canvasContainer">
                <img id="templateImage" style="width: 100%; height: 100%;">
            </div>
        </div>
    </div>

    <script>
        let textBoxes = [];
        let currentData = [];
        let draggedElement = null;
        let initialX = 0;
        let initialY = 0;
        let currentX = 0;
        let currentY = 0;

        // Handle template upload
        document.getElementById('templateUpload').addEventListener('change', function(e) {
            const file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = function(event) {
                document.getElementById('templateImage').src = event.target.result;
            };
            reader.readAsDataURL(file);
        });

        // Handle JSON upload
        document.getElementById('jsonUpload').addEventListener('change', function(e) {
            const file = e.target.files[0];
            const reader = new FileReader();
            reader.onload = function(event) {
                try {
                    currentData = JSON.parse(event.target.result);
                    updatePreview();
                } catch (error) {
                    alert('Invalid JSON file');
                }
            };
            reader.readAsText(file);
        });

        // Add new text box
        function addTextBox() {
            const box = {
                id: 'box_' + textBoxes.length,
                label: '',
                x: 50,
                y: 50,
                width: 100,
                height: 30,
                fontSize: 16,
                fontFamily: 'Arial'
            };
            textBoxes.push(box);
            createTextBox(box);
            updateControls();
            updateLayoutConfig();
        }

        // Create text box element
        function createTextBox(box) {
            const element = document.createElement('div');
            element.className = 'text-box';
            element.id = box.id;
            updateTextBoxStyle(element, box);

            // Tambahkan event listeners untuk drag
            element.addEventListener('mousedown', startDragging);
            document.getElementById('canvasContainer').appendChild(element);
        }

        // Fungsi untuk memulai drag
        function startDragging(e) {
            draggedElement = e.target;

            // Simpan posisi awal mouse
            initialX = e.clientX;
            initialY = e.clientY;

            // Simpan posisi awal elemen
            const style = window.getComputedStyle(draggedElement);
            currentX = parseInt(style.left);
            currentY = parseInt(style.top);

            draggedElement.classList.add('dragging');

            // Tambahkan event listeners untuk drag
            document.addEventListener('mousemove', drag);
            document.addEventListener('mouseup', stopDragging);

            // Prevent default untuk mencegah drag yang tidak diinginkan
            e.preventDefault();
        }

        // Fungsi untuk melakukan drag
        function drag(e) {
            if (!draggedElement) return;

            // Hitung perpindahan mouse
            const dx = e.clientX - initialX;
            const dy = e.clientY - initialY;

            // Update posisi elemen
            const container = document.getElementById('canvasContainer');
            const rect = container.getBoundingClientRect();

            let newX = currentX + dx;
            let newY = currentY + dy;

            // Batasi area drag dalam container
            newX = Math.max(0, Math.min(newX, container.offsetWidth - draggedElement.offsetWidth));
            newY = Math.max(0, Math.min(newY, container.offsetHeight - draggedElement.offsetHeight));

            draggedElement.style.left = newX + 'px';
            draggedElement.style.top = newY + 'px';

            // Update nilai di textBoxes array
            const boxId = draggedElement.id;
            const boxIndex = textBoxes.findIndex(box => box.id === boxId);
            if (boxIndex !== -1) {
                textBoxes[boxIndex].x = newX;
                textBoxes[boxIndex].y = newY;

                // Update input fields
                const controls = document.getElementById('textBoxControls');
                const xInput = controls.querySelector(`input[onchange="updateBoxProperty('${boxId}', 'x', this.value)"]`);
                const yInput = controls.querySelector(`input[onchange="updateBoxProperty('${boxId}', 'y', this.value)"]`);

                if (xInput) xInput.value = Math.round(newX);
                if (yInput) yInput.value = Math.round(newY);
            }

            // Update layout config
            updateLayoutConfig();
        }

        // Fungsi untuk menghentikan drag
        function stopDragging() {
            if (draggedElement) {
                draggedElement.classList.remove('dragging');
                draggedElement = null;
            }

            document.removeEventListener('mousemove', drag);
            document.removeEventListener('mouseup', stopDragging);
        }

        // Update text box style
        function updateTextBoxStyle(element, box) {
            element.style.left = box.x + 'px';
            element.style.top = box.y + 'px';
            element.style.width = box.width + 'px';
            element.style.height = box.height + 'px';
            element.style.fontSize = box.fontSize + 'px';
            element.style.fontFamily = box.fontFamily;
        }

        // Update controls panel
        function updateControls() {
            const container = document.getElementById('textBoxControls');
            container.innerHTML = '';

            textBoxes.forEach((box, index) => {
                const div = document.createElement('div');
                div.className = 'text-box-controls';
                div.innerHTML = `
                    <h4>Text Box ${index + 1}</h4>
                    <div>
                        <select onchange="updateBoxProperty('${box.id}', 'label', this.value)">
                            <option value="">Select Field</option>
                            <option value="no_surat" ${box.label === 'no_surat' ? 'selected' : ''}>No Surat</option>
                            <option value="nama" ${box.label === 'nama' ? 'selected' : ''}>Nama</option>
                            <option value="tanggal" ${box.label === 'tanggal' ? 'selected' : ''}>Tanggal</option>
                        </select>
                    </div>
                    <div>
                        <label>Position X:</label>
                        <input type="number" class="number-input" value="${box.x}"
                            onchange="updateBoxProperty('${box.id}', 'x', this.value)">
                        <label>Y:</label>
                        <input type="number" class="number-input" value="${box.y}"
                            onchange="updateBoxProperty('${box.id}', 'y', this.value)">
                    </div>
                    <div>
                        <label>Width:</label>
                        <input type="number" class="number-input" value="${box.width}"
                            onchange="updateBoxProperty('${box.id}', 'width', this.value)">
                        <label>Height:</label>
                        <input type="number" class="number-input" value="${box.height}"
                            onchange="updateBoxProperty('${box.id}', 'height', this.value)">
                    </div>
                    <div>
                        <label>Font Size:</label>
                        <input type="number" class="number-input" value="${box.fontSize}"
                            onchange="updateBoxProperty('${box.id}', 'fontSize', this.value)">
                    </div>
                    <button onclick="removeTextBox('${box.id}')">Remove</button>
                `;
                container.appendChild(div);
            });
        }

        // Update box property
        function updateBoxProperty(boxId, property, value) {
            const boxIndex = textBoxes.findIndex(box => box.id === boxId);
            if (boxIndex !== -1) {
                textBoxes[boxIndex][property] = Number(value) || value;
                const element = document.getElementById(boxId);
                if (element) {
                    updateTextBoxStyle(element, textBoxes[boxIndex]);
                }
                updatePreview();
                updateLayoutConfig();
            }
        }

        // Remove text box
        function removeTextBox(boxId) {
            const element = document.getElementById(boxId);
            if (element) element.remove();
            textBoxes = textBoxes.filter(box => box.id !== boxId);
            updateControls();
            updateLayoutConfig();
        }

        // Update preview with current data
        function updatePreview() {
            textBoxes.forEach(box => {
                const element = document.getElementById(box.id);
                if (element && box.label && currentData.length > 0) {
                    element.textContent = currentData[0][box.label] || '';
                }
            });
        }

        // Save layout configuration
        function saveLayout() {
            const config = {
                textBoxes: textBoxes.map(box => ({
                    label: box.label,
                    x: box.x,
                    y: box.y,
                    width: box.width,
                    height: box.height,
                    fontSize: box.fontSize,
                    fontFamily: box.fontFamily
                }))
            };
            document.getElementById('layoutConfig').value = JSON.stringify(config, null, 2);
        }

        // Load layout configuration
        function loadLayout() {
            try {
                const config = JSON.parse(document.getElementById('layoutConfig').value);
                // Clear existing text boxes
                textBoxes.forEach(box => {
                    const element = document.getElementById(box.id);
                    if (element) element.remove();
                });
                textBoxes = [];

                // Create new text boxes from config
                config.textBoxes.forEach(box => {
                    const newBox = {
                        ...box,
                        id: 'box_' + textBoxes.length
                    };
                    textBoxes.push(newBox);
                    createTextBox(newBox);
                });

                updateControls();
                updatePreview();
            } catch (error) {
                alert('Invalid layout configuration');
            }
        }

        // Update layout configuration in textarea
        function updateLayoutConfig() {
            saveLayout();
        }

        // Generate all certificates
        async function generateCertificates() {
            const zip = new JSZip();

            for (let i = 0; i < currentData.length; i++) {
                const data = currentData[i];

                // Update text boxes with current data
                textBoxes.forEach(box => {
                    const element = document.getElementById(box.id);
                    if (element && box.label) {
                        element.textContent = data[box.label] || '';
                    }
                });

                // Generate certificate image
                const canvas = await html2canvas(document.getElementById('canvasContainer'));
                const imageData = canvas.toDataURL('image/png');

                // Add to zip
                zip.file(`certificate_${i + 1}.png`, imageData.split('base64,')[1], {
                    base64: true
                });
            }

            // Download zip file
            zip.generateAsync({
                type: 'blob'
            }).then(function(content) {
                const url = window.URL.createObjectURL(content);
                const a = document.createElement('a');
                a.href = url;
                a.download = 'certificates.zip';
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                window.URL.revokeObjectURL(url);
            });
        }
    </script>
</body>

</html>

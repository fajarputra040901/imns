@extends('partial.sneat')
@push('style')
    <style>
        .nav-tabs {
            display: flex;
            flex-wrap: nowrap;
            overflow-x: hidden;
            overflow-y: hidden;
        }

        .nav-tabs .nav-item {
            flex: 0 0 auto;
        }

        @media (max-width: 768px) {
            .nav-tabs {
                overflow-x: auto;
                overflow-y: hidden;
                -webkit-overflow-scrolling: touch;
            }
        }

        .menu-card {
            cursor: pointer;
            transition: transform 0.2s;
        }

        .menu-card:hover {
            transform: scale(1.05);
        }

        @media (max-width: 768px) {
            .btn-group {
                flex-direction: column;
                /* Menjadikan tombol vertikal pada layar kecil */
                width: 100%;
                /* Tombol mengambil lebar penuh pada layar kecil */
            }

            .btn-group .btn {
                width: 100%;
                /* Tombol menjadi lebar penuh pada layar kecil */
                margin-bottom: 10px;
                /* Menambahkan jarak antar tombol */
            }
        }

        .ui-state-default {
            background-color: #f8f9fa;
            /* Warna latar belakang terang */
            border: 1px solid #ddd;
            /* Border ringan */
            border-radius: 5px;
            /* Membulatkan sudut */
            padding: 10px;
            /* Jarak dalam elemen */
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            /* Efek bayangan */
            cursor: move;
            /* Indikasi elemen bisa di-drag */
            transition: transform 0.2s ease;
            /* Animasi saat hover */
        }

        /* Hover efek untuk item */
        .ui-state-default:hover {
            transform: translateY(-3px);
            /* Efek naik sedikit */
            background-color: #e9ecef;
            /* Warna latar lebih terang */
        }

        /* Tombol Hapus */
        .ui-state-default .hapus-opsi {
            background-color: #dc3545;
            /* Warna merah */
            color: white;
            /* Teks putih */
            border: none;
            /* Hilangkan border */
            border-radius: 3px;
            /* Membulatkan sudut */
            padding: 5px 10px;
            /* Padding tombol */
            font-size: 0.875rem;
            /* Ukuran font lebih kecil */
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
            /* Efek bayangan */
            transition: background-color 0.2s ease;
            /* Animasi warna */
        }

        /* Hover efek untuk tombol Hapus */
        .ui-state-default .hapus-opsi:hover {
            background-color: #c82333;
            /* Warna merah lebih gelap */
        }

        .ui-state-highlight {
            background-color: #f0f8ff;
            border: 1px dashed #0099ff;
            height: 2.5rem;
            /* Sesuaikan dengan tinggi elemen */
            margin-bottom: 0.5rem;
        }
    </style>
    <style>
        #riview_cover {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            /* Membatasi maksimal 5 gambar dalam satu baris */
            gap: 10px;
            /* Jarak antar gambar */
            padding: 10px;
            overflow: auto;
            /* Membuat div scrollable jika gambar melebihi 5 per baris */
        }

        /* Menambahkan style untuk gambar jika hanya ada satu gambar */
        #riview_cover.single-image img {
            width: 200px;
            /* Ukuran gambar jika hanya satu */
            height: 200px;
            object-fit: contain
        }

        /* Kotak gambar dan tombol hapus */
        .image-preview-container {
            position: relative;
            overflow: hidden;
            width: 100%;
            height: 100%;
            box-sizing: border-box;
            border: 1px solid #ddd;
            border-radius: 8px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .image-preview-container img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 8px;
        }

        .delete-btn {
            position: absolute;
            top: 5px;
            right: 5px;
            background-color: rgba(255, 0, 0, 0.5);
            color: white;
            border: none;
            border-radius: 50%;
            width: 25px;
            height: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
            cursor: pointer;
        }



        .input-item {
            margin: 10px;
            padding: 10px;
            background-color: #f4f4f4;
            border: 1px solid #ccc;
            cursor: move;
        }

        .input-item button {
            margin-left: 2px;
        }

        .ui-state-highlight {
            border: 2px dashed #ccc;
        }

        .image-preview-container {
            position: relative;
            display: inline-block;
            margin-right: 10px;
        }

        .image-preview-container img {
            margin-right: 10px;
            margin-top: 10px;
        }

        .image-preview-container button {
            position: absolute;
            top: 0;
            right: 0;
            z-index: 10;
            border-radius: 50%;
            padding: 5px;
            font-size: 16px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">


        <div class="row g-4 mt-2">
            <!-- Peserta -->
            <div class="col-md-4">
                <div class="card text-center menu-card" id="info_peserta">
                    <div class="card-body">
                        <i class="bx bx-user fs-1 text-primary"></i>
                        <h5 class="card-title mt-3">Peserta</h5>
                    </div>
                </div>
            </div>
            <!-- Pembayaran -->
            <div class="col-md-4">
                <div class="card text-center menu-card" id="pembayaran">
                    <div class="card-body">
                        <i class="bx bx-wallet fs-1 text-success"></i>
                        <h5 class="card-title mt-3">Pembayaran</h5>
                    </div>
                </div>
            </div>
            <!-- Visabilitas -->
            <div class="col-md-4">
                <div class="card text-center menu-card">
                    <div class="card-body">
                        <i class="bx bx-show fs-1 text-danger"></i>
                        <h5 class="card-title mt-3">Surat Surat</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <p>Link Webinar : <a href="" id="link_webinar"></a></p>
            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <div class="nav-align-top mt-2">
                    <ul class="nav nav-tabs" role="tablist" style="overflow-x: auto; white-space: nowrap;">
                        <li class="nav-item" style="display: inline-block;">
                            <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab"
                                data-bs-target="#navs-konfigurasi" aria-controls="navs-konfigurasi"
                                aria-selected="true">Konfigurasi</button>
                        </li>
                        <li class="nav-item" style="display: inline-block;">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                data-bs-target="#navs-pendaftaran" aria-controls="navs-pendaftaran"
                                aria-selected="false">Formulir
                                Pendaftaran</button>
                        </li>
                        <li class="nav-item" style="display: inline-block;">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                data-bs-target="#navs-biaya" aria-controls="navs-biaya" aria-selected="false">Biaya</button>
                        </li>
                        <li class="nav-item" style="display: inline-block;">
                            <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                data-bs-target="#navs-sertifikat" aria-controls="navs-sertifikat"
                                aria-selected="false">Sertifikat</button>
                        </li>


                    </ul>

                    <div class="tab-content">

                        <div class="tab-pane fade show active" id="navs-konfigurasi" role="tabpanel">
                            <form id="form_edit_webinar">
                                <input type="hidden" name="id_webinar" id="id_webinar" value="{{ $data->id }}">
                                <!-- Tanggal Pelaksanaan -->
                                <textarea name="cover" id="cover" style="display: none" cols="30" rows="10"></textarea>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Cover</label>
                                    <div class="col-md-9">
                                        <button class="btn btn-primary" id="tambah_cover">
                                            Tambah Cover
                                        </button>
                                        <div id="riview_cover" class="mt-2" style="display: flex; overflow-x: auto;">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Tanggal Pelaksanaan</label>
                                    <div class="col-md-4 mt-1">
                                        <input type="text" class="form-control" name="tanggal_mulai"
                                            value="{{ $data->tanggal_mulai ? \Carbon\Carbon::parse($data->tanggal_mulai)->format('d-m-Y') : '' }}"
                                            placeholder="Tanggal Mulai">
                                    </div>
                                    <div class="col-md-4 mt-1">
                                        <input type="text" class="form-control" name="tanggal_selesai"
                                            value="{{ $data->tanggal_selesai ? \Carbon\Carbon::parse($data->tanggal_selesai)->format('d-m-Y') : '' }}"
                                            placeholder="Tanggal Selesai">
                                    </div>
                                </div>



                                <!-- Judul -->
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Judul</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="judul"
                                            placeholder="Judul Webinar" value="{{ $data->judul }}">
                                    </div>
                                </div>

                                <!-- Jenis Webinar -->
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Jenis Webinar</label>
                                    <div class="col-md-9">
                                        <select class="form-select" name="jenis">
                                            <option value="">Pilih</option>
                                            <option value="Online" {{ $data->jenis == 'Online' ? 'selected' : '' }}>
                                                Online</option>
                                            <option value="Klasikal / Offline"
                                                {{ $data->jenis == 'Klasikal / Offline' ? 'selected' : '' }}>Klasikal /
                                                Offline</option>
                                            <option value="Hybrid" {{ $data->jenis == 'Hybrid' ? 'selected' : '' }}>
                                                Hybrid</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Cakupan</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="cakupan" placeholder="Cakupan"
                                            value="{{ $data->cakupan }}">
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Tempat</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="tempat"
                                            placeholder="Tempat Webinar" value="{{ $data->tempat }}">
                                    </div>
                                </div>


                                <!-- Jumlah Peserta -->
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Jumlah Peserta</label>
                                    <div class="col-md-9">
                                        <input type="number" class="form-control" name="peserta"
                                            value="{{ $data->peserta }}" placeholder="Jumlah Peserta">
                                    </div>
                                </div>

                                <!-- Jumlah SKP -->
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Jumlah SKP</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="skp"
                                            value="{{ $data->skp }}" placeholder="Jumlah SKP">
                                        <span class="text-muted fst-italic">Masukan Sedang Diproses jika SKP belum
                                            terbit</span>
                                    </div>
                                </div>

                                <!-- Deskripsi -->
                                <div class="mb-3">
                                    <label class="form-label">Deskripsi</label>
                                    <textarea class="form-control" name="deskripsi" id="deskripsi">{{ $data->deskripsi }}</textarea>
                                </div>

                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Visabilitas</label>
                                    <div class="col-md-9">
                                        <div class="form-check form-check-inline mt-2">
                                            <input class="form-check-input" type="radio" name="visabilitas"
                                                id="visabilitas_y" value="Y"
                                                {{ $data->visabilitas == 'Y' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="visabilitas_y">Ditampilkan</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="visabilitas"
                                                id="visabilitas_n" value="N"
                                                {{ $data->visabilitas == 'N' ? 'checked' : '' }}>
                                            <label class="form-check-label" for="visabilitas_n">Disembunyikan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Link Grup WA</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="link_wa" value=""
                                            placeholder="Link WAG">
                                        <span class="text-muted fst-italic">WAG Peserta</span>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Link LMS Satu Sehat</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="lms" placeholder="Link LMS"
                                            value="{{ $data->lms }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Tanggal Akhir Pendaftaran</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="dedlen"
                                            placeholder="Tanggal Akhir Pendaftaran" value="{{ $data->dedlen }}">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Slug</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="slug" value=""
                                            placeholder="Slug">
                                        <span class="text-muted fst-italic">Shortlink Untuk Mempermudah Akses</span>
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Contact Person</label>
                                    <div class="col-md-9">
                                        <button class="btn btn-primary btn-sm" id="tambah_cp">Tambah CP</button>
                                        <div id="form_cp" style="display: none">
                                            <div class="d-flex align-items-center gap-1 mt-2">
                                                <input type="text" class="form-control" name="nama" id="nama"
                                                    value="" placeholder="Nama">
                                                <input type="text" class="form-control" name="no_cp" id="no_cp"
                                                    value="" placeholder="No Telp (08xxxx)">
                                                <div class="form-check d-flex align-items-center">
                                                    <input class="form-check-input" type="checkbox" name="anonim"
                                                        id="anonim" value="Y" checked>
                                                    <label class="form-check-label ms-2" for="anonim">Anonim</label>
                                                </div>
                                            </div>
                                            <div class="mt-2">
                                                <button class="btn btn-primary btn-sm" id="submit_cp">Tambahkan</button>
                                            </div>
                                        </div>


                                        <div class="table-responsive">
                                            <table class="table mt-2" id="tabel_cp">
                                                <thead>
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Nama</th>
                                                        <th>No Telp</th>
                                                        <th>Status</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                                <div class="mt-2">
                                    <button class="btn btn-primary" id="submit_form">
                                        Perbaharui
                                    </button>
                                </div>

                            </form>

                        </div>
                        <div class="tab-pane fade show " id="navs-pendaftaran" role="tabpanel">
                            <div class="btn-group gap-1 mt-2" role="group" aria-label="Button group">
                                <button class="btn btn-sm  btn-primary d-flex align-items-center" id="tambah_input">
                                    <i class="bx bx-plus-circle me-2"></i> Tambah Input
                                </button>
                                <button class="btn btn-sm  btn-secondary d-flex align-items-center" id="template_input">
                                    <i class="bx bx-brush me-2"></i> Gunakan Template
                                </button>
                                <button class="btn btn-sm  btn-success d-flex align-items-center" id="simpan_input">
                                    <i class="bx bx-save me-2"></i> Simpan
                                </button>
                            </div>


                            <div class="mt-2">
                                <textarea name="isi_form" style="display: none" id="isi_form" cols="30" rows="10"></textarea>
                                <p>Review Formulir</p>
                            </div>
                            <div class="mt-2" id="review_input_form">

                            </div>
                        </div>
                        <div class="tab-pane fade show " id="navs-biaya" role="tabpanel">
                            <div class="btn-group gap-1 mt-2" role="group" aria-label="Button group">
                                <button class="btn btn-sm  btn-primary d-flex align-items-center" id="tambah_input_biaya">
                                    <i class="bx bx-plus-circle me-2"></i> Tambah Paket
                                </button>

                                <button class="btn btn-sm  btn-success d-flex align-items-center" id="simpan_input_biaya">
                                    <i class="bx bx-save me-2"></i> Simpan
                                </button>
                            </div>
                            <div class="mt-2">
                                <textarea name="isi_form_biaya" style="display: none" id="isi_form_biaya" cols="30" rows="10"></textarea>
                                <p>Review Paket</p>
                            </div>
                            <div class="mt-2">

                            </div>
                            <div class="mt-2" id="review_input_biaya"></div>
                        </div>






                    </div>

                </div>
            </div>
        </div>

        <!-- Modal -->
        <!-- Modal -->
        <div class="modal fade" id="modalForm" tabindex="-1" aria-labelledby="modalFormLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalFormLabel">Tambah Input Formulir</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="formInputModal">
                            <div class="mb-3">
                                <label for="labelInput" class="form-label">Judul / Label Input</label>
                                <input type="text" class="form-control" id="labelInput"
                                    placeholder="Masukan Judul Untuk Label" required>
                            </div>
                            <div class="mb-3">
                                <label for="namaInput" class="form-label">Nama Input</label>
                                <input type="text" class="form-control" id="namaInput"
                                    placeholder="Masukan Input tanpa spasi" required>
                            </div>

                            <div class="mb-3">
                                <label for="jenisInput" class="form-label">Jenis Input</label>
                                <select class="form-select" id="jenisInput" required>
                                    <option value="" disabled selected>Pilih Jenis Input</option>
                                    <option value="text">Text</option>
                                    <option value="select">Select</option>
                                    <option value="email">Email</option>
                                    <option value="textarea">Textarea</option>
                                    <option value="date">Date</option>
                                    <option value="number">Number</option>
                                    <option value="file">File</option>

                                </select>
                            </div>
                            <div id="jenis_select" style="display: none">
                                <div class="mb-3 d-flex align-items-center">
                                    <label for="opsi" class="form-label me-2">Opsi Select</label>
                                    <input type="text" class="form-control me-2" id="opsi"
                                        placeholder="Masukan Opsi">
                                    <button class="btn btn-primary btn-sm" id="tambah_opsi">Tambah</button>
                                </div>

                                <div class="mt-2">
                                    <p>Opsi</p>
                                </div>
                                <div class="mt-2" id="list_opsi">


                                </div>
                            </div>

                            <div class="mb-3">
                                <label for="placeholderInput" class="form-label">Placeholder</label>
                                <input type="text" class="form-control" id="placeholderInput"
                                    placeholder="Bayangan Input" required>
                            </div>

                            <div class="mb-3">
                                <label for="keteranganInput" class="form-label">Keterangan</label>
                                <input type="text" class="form-control" id="keteranganInput"
                                    placeholder="Masukkan Keterangan Bila Ada">
                            </div>
                            <div class="form-check mb-3">
                                <input class="form-check-input" type="checkbox" value="primary" id="primary">
                                <label class="form-check-label" for="primary">Primary</label>
                            </div>
                            <div class="form-check mb-3">
                                <input class="form-check-input" type="checkbox" value="notif_wa" id="notif_wa">
                                <label class="form-check-label" for="notif_wa">Notifikasi Whatsapp</label>
                            </div>
                            <div class="form-check mb-3">
                                <input class="form-check-input" type="checkbox" value="notif_email" id="notif_email">
                                <label class="form-check-label" for="notif_email">Notifikasi Email</label>
                            </div>

                            <!-- Validasi Dinamis -->
                            <div class="mb-3">
                                <label for="validasiInput" class="form-label">Validasi</label><br>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="required"
                                        id="validasiRequired">
                                    <label class="form-check-label" for="validasiRequired">Tidak Boleh Kosong</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="number" id="validasiNumber">
                                    <label class="form-check-label" for="validasiNumber">Harus Angka</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="date" id="validasiDate">
                                    <label class="form-check-label" for="validasiDate">Harus Tanggal (dd-mm-yyyy)</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="file" id="validasiFile">
                                    <label class="form-check-label" for="validasiFile">Harus File</label>
                                </div>
                            </div>

                            <!-- Validasi File (jika dipilih) -->
                            <div id="fileValidationFields" style="display: none;">
                                <div class="mb-3">
                                    <label for="ukuranFile" class="form-label">Ukuran File (MB)</label>
                                    <input type="text" class="form-control" id="ukuranFile"
                                        placeholder="Ukuran dalam MB">
                                </div>

                                <div class="mb-3">
                                    <label for="jenisFile" class="form-label">Jenis File</label><br>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="jpg" id="fileJPG">
                                        <label class="form-check-label" for="fileJPG">JPG</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="png" id="filePNG">
                                        <label class="form-check-label" for="filePNG">PNG</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="jpeg" id="fileJPEG">
                                        <label class="form-check-label" for="fileJPEG">JPEG</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="checkbox" value="pdf" id="filePDF">
                                        <label class="form-check-label" for="filePDF">PDF</label>
                                    </div>
                                    <!-- Add more file types here if needed -->
                                </div>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id="tambah_input_form">Simpan</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modalpaket" tabindex="-1" aria-labelledby="modalFormLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalFormLabel">Tambah Paket</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="formpaket">
                            <div class="mb-3">
                                <label for="namapaket" class="form-label">Nama Paket</label>
                                <input type="text" class="form-control" id="namapaket" placeholder="Nama Paket">
                            </div>
                            <div class="mb-3">
                                <label for="hargapaket" class="form-label">Harga</label>
                                <input type="text" class="form-control harga" id="hargapaket"
                                    placeholder="Masukan Harga">
                                <input class="form-check-input" type="checkbox" value="minimalharga" id="minimalharga">
                                <label class="form-check-label" for="minimalharga">Minimal Harga</label>
                            </div>

                            <div class="mb-3">
                                <label for="baksos" class="form-label">Baksos (Maintenance)</label>
                                <input type="text" class="form-control" id="baksos" readonly
                                    placeholder="Masukan Harga">
                                <input class="form-check-input" type="checkbox" disabled value="" id="">
                                <label class="form-check-label" for="minimalharga">Volunter</label>
                            </div>

                            <div class="mb-3">
                                <label for="benefit" class="form-label">Benefit</label>
                                <textarea name="benefit" id="benefit"></textarea>
                            </div>


                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id="tambah_input_form_biaya">Simpan</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="modalcover" tabindex="-1" aria-labelledby="modalcoverLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalcoverLabel">Tambah Cover</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="formCover">
                            <!-- Input File untuk Upload -->
                            <div class="mb-3">
                                <label for="coverFile" class="form-label">Pilih Cover</label>
                                <input type="file" class="form-control" id="coverFile" name="coverFile"
                                    accept="image/*" required>
                            </div>

                            <!-- Checkbox untuk Thumbnails -->
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="thumbnailsCheck" name="thumbnails">
                                <label class="form-check-label" for="thumbnailsCheck">Thumbnails</label>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id="simpanCover">Simpan</button>
                    </div>
                </div>
            </div>
        </div>

        @push('scripts')
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
            <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
            <script>
                CKEDITOR.replace('deskripsi');
                CKEDITOR.replace('benefit');
            </script>
            <script>
                var id_webinar = {{ $data->id }};
            </script>

            <script src="/portal/js/config_webinar.js"></script>
            <script src="/portal/js/config_webinar_biaya.js"></script>
            <script src="/portal/js/config_webinar_cover.js"></script>

            <script>
                $('#info_peserta').click(function() {
                    var id = $(this).data('id'); // Ambil id dari data-id
                    window.location.href = '/peserta_webinar/' + id_webinar; // Arahkan ke URL
                });
                $('#pembayaran').click(function() {
                    var id = $(this).data('id'); // Ambil id dari data-id
                    window.location.href = '/keuangan_webinar/' + id_webinar; // Arahkan ke URL
                });
            </script>
        @endpush
    @endsection

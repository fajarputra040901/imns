@extends('partial.sneat')
@push('style')
    <style>
        .nav-tabs {
            display: flex;
            flex-wrap: nowrap;
            overflow-x: hidden;
            overflow-y: hidden;
        }

        .nav-tabs .nav-item {
            flex: 0 0 auto;
        }

        @media (max-width: 768px) {
            .nav-tabs {
                overflow-x: auto;
                overflow-y: hidden;
                -webkit-overflow-scrolling: touch;
            }
        }

        .menu-card {
            cursor: pointer;
            transition: transform 0.2s;
        }

        .menu-card:hover {
            transform: scale(1.05);
        }

        @media (max-width: 768px) {
            .btn-group {
                flex-direction: column;
                /* Menjadikan tombol vertikal pada layar kecil */
                width: 100%;
                /* Tombol mengambil lebar penuh pada layar kecil */
            }

            .btn-group .btn {
                width: 100%;
                /* Tombol menjadi lebar penuh pada layar kecil */
                margin-bottom: 10px;
                /* Menambahkan jarak antar tombol */
            }
        }

        .ui-state-default {
            background-color: #f8f9fa;
            /* Warna latar belakang terang */
            border: 1px solid #ddd;
            /* Border ringan */
            border-radius: 5px;
            /* Membulatkan sudut */
            padding: 10px;
            /* Jarak dalam elemen */
            box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
            /* Efek bayangan */
            cursor: move;
            /* Indikasi elemen bisa di-drag */
            transition: transform 0.2s ease;
            /* Animasi saat hover */
        }

        /* Hover efek untuk item */
        .ui-state-default:hover {
            transform: translateY(-3px);
            /* Efek naik sedikit */
            background-color: #e9ecef;
            /* Warna latar lebih terang */
        }

        /* Tombol Hapus */
        .ui-state-default .hapus-opsi {
            background-color: #dc3545;
            /* Warna merah */
            color: white;
            /* Teks putih */
            border: none;
            /* Hilangkan border */
            border-radius: 3px;
            /* Membulatkan sudut */
            padding: 5px 10px;
            /* Padding tombol */
            font-size: 0.875rem;
            /* Ukuran font lebih kecil */
            box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
            /* Efek bayangan */
            transition: background-color 0.2s ease;
            /* Animasi warna */
        }

        /* Hover efek untuk tombol Hapus */
        .ui-state-default .hapus-opsi:hover {
            background-color: #c82333;
            /* Warna merah lebih gelap */
        }

        .ui-state-highlight {
            background-color: #f0f8ff;
            border: 1px dashed #0099ff;
            height: 2.5rem;
            /* Sesuaikan dengan tinggi elemen */
            margin-bottom: 0.5rem;
        }
    </style>
    <style>
        #riview_cover {
            display: grid;
            grid-template-columns: repeat(5, 1fr);
            /* Membatasi maksimal 5 gambar dalam satu baris */
            gap: 10px;
            /* Jarak antar gambar */
            padding: 10px;
            overflow: auto;
            /* Membuat div scrollable jika gambar melebihi 5 per baris */
        }

        /* Menambahkan style untuk gambar jika hanya ada satu gambar */
        #riview_cover.single-image img {
            width: 200px;
            /* Ukuran gambar jika hanya satu */
            height: 200px;
            object-fit: contain
        }

        /* Kotak gambar dan tombol hapus */
        .image-preview-container {
            position: relative;
            overflow: hidden;
            width: 100%;
            height: 100%;
            box-sizing: border-box;
            border: 1px solid #ddd;
            border-radius: 8px;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .image-preview-container img {
            width: 100%;
            height: 100%;
            object-fit: cover;
            border-radius: 8px;
        }

        .delete-btn {
            position: absolute;
            top: 5px;
            right: 5px;
            background-color: rgba(255, 0, 0, 0.5);
            color: white;
            border: none;
            border-radius: 50%;
            width: 25px;
            height: 25px;
            display: flex;
            justify-content: center;
            align-items: center;
            cursor: pointer;
        }



        .input-item {
            margin: 10px;
            padding: 10px;
            background-color: #f4f4f4;
            border: 1px solid #ccc;
            cursor: move;
        }

        .input-item button {
            margin-left: 2px;
        }

        .ui-state-highlight {
            border: 2px dashed #ccc;
        }

        .image-preview-container {
            position: relative;
            display: inline-block;
            margin-right: 10px;
        }

        .image-preview-container img {
            margin-right: 10px;
            margin-top: 10px;
        }

        .image-preview-container button {
            position: absolute;
            top: 0;
            right: 0;
            z-index: 10;
            border-radius: 50%;
            padding: 5px;
            font-size: 16px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">


        <div class="row g-4 mt-2">
            <!-- Peserta -->
            <div class="col-md-4">
                <div class="card text-center menu-card" id="import_card">
                    <div class="card-body">
                        <i class="bx bx-import fs-1 text-primary"></i> <!-- Ikon untuk Import Kontak -->
                        <h5 class="card-title mt-3">Import Kontak</h5>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center menu-card" id="kirim_pesan">
                    <div class="card-body">
                        <i class="bx bx-message fs-1 text-success"></i> <!-- Ikon untuk Kirim Pesan -->
                        <h5 class="card-title mt-3">Kirim Pesan</h5>
                    </div>
                </div>
            </div>



        </div>
        <div class="card mt-2">
            <div class="card-body">

            </div>
        </div>
        <div class="card mt-2">
            <div class="card-body">
                <form id="form_blast">
                <div class="mb-3">
                    <label for="jenis" class="form-label">Pilih Jenis Pesan</label>
                    <select name="jenis" id="jenis" class="form-control">
                        <option value="">Pilih</option>
                        <option value="wa">Whatsapp</option>
                        <option value="email">Email</option>
                    </select>
                </div>
                <div class="mb-3"  style="display: none;" id="div_email">
                    <label for="name_email" class="form-label">Email</label>
                    <select id="name_email" name="email" class="form-select">
                        <!-- Option akan diisi menggunakan AJAX -->
                    </select>
                </div>
                <div class="mb-3" id="fileInputWrapperEmail" style="display: none;">
                    <label for="file_email" class="form-label">File</label>
                    <input type="file" name="file_email[]" id="file_email" class="form-control"
                        multiple>
                </div>
                <div class="mb-3" id="subjek_email" style="display: none;">
                    <label for="subjek" class="form-label">Subjek</label>
                    <input type="text" class="form-control" id="subjek" name="subjek">
                </div>
                <div class="mb-3">
                    <textarea name="isi_pesan" class="form-control" id="isi_pesan"></textarea>
                </div>


            </div>
        </form>
        </div>
        <div class="card mt-2">
            <div class="card-body table-responsive" >
                <p>List Kontak</p>
                <div class="mb-3">
                    <input type="text" id="search" class="form-control"
                        placeholder="Cari Nama atau No Telp atau Email">
                </div>
                <div id="list_kontak"></div>
                <div id="pagination" class="mt-2"></div>
            </div>
        </div>

        <div class="modal fade" id="modal_import" tabindex="-1" aria-labelledby="modalImportLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalImportLabel">Import Kontak</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="card">
                            <div class="card-body">
                                <label for="file" class="form-label">Masukan File</label>
                                <input type="file" class="form-control" id="file" name="file">
                            </div>
                        </div>
                        <div class="card mt-2">
                            <div class="card-body">
                                <button class="btn btn-success mt-1" id="template_whatsapp">Download Template
                                    Whastapp</button>
                                <button class="btn btn-danger mt-1" id="template_email">Download Template Email</button>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                        <button type="button" class="btn btn-primary" id="upload">Upload</button>
                    </div>
                </div>
            </div>
        </div>

        @push('scripts')
            <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.18.5/xlsx.full.min.js"></script>
            <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
            <script>
                CKEDITOR.replace('isi_pesan');
            </script>
            <script src="/portal/js/config_blast.js"></script>
        @endpush
    @endsection

@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Peserta -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="info_peserta">
                    <i class="bx bx-user menu-icons"></i>
                    <div class="menu-title">Info Peserta</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pencarian -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="pencarianCard">
                    <i class="bx bx-search menu-icons"></i>
                    <div class="menu-title">Pencarian</div>
                </div>
            </div>

            <!-- Card Menu 3 - Kirim Pesan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="kirimPesanCard">
                    <i class="bx bx-message-dots menu-icons"></i>
                    <div class="menu-title">Kirim Pesan</div>
                </div>
            </div>

            <!-- Card Menu 4 - Export Excel -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="exportExcelCard">
                    <i class="bx bx-file menu-icons"></i>
                    <div class="menu-title">Export Excel</div>
                </div>
            </div>
        </div>


        <div class="mt-3">
            <div class="card">
                <div class="card-body table-responsive">
                    <table class="table table-striped" id="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                @foreach ($formData as $field)
                                    <th>{{ $field['label'] }}</th>
                                @endforeach
                                <th>Paket</th>
                                <th>ID Transaksi</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Data akan di-load oleh DataTables -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>



    <div class="modal fade" id="menuModal" tabindex="-1" aria-labelledby="menuModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="menuModalLabel">Pencarian</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="" id="pencarianForm">
                        <div id="isian">
                            <!-- Form fields akan dimuat secara dinamis -->
                        </div>
                        <div id="paket">
                            <!-- Paket Select akan dimuat secara dinamis -->
                        </div>
                        <div id="lain">
                            <label for="status">Status</label>
                            <select class="form-select" id="status">
                                <option value="">Semua</option>
                                <option value="paid">Paid</option>
                                <option value="unpaid">Unpaid</option>
                                <option value="expired">Expire</option>
                                <option value="pending">Pending</option>
                                <option value="undangan">Undangan</option>
                            </select>
                        </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary" data-bs-dismiss="modal">Cari</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="detail_modal" tabindex="-1" aria-labelledby="detailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detailModalLabel">Detail Peserta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="peserta_id">
                    <form action="" id="form_edit_peserta">
                        @csrf
                    </form>
                    <div class="mt-2">
                        <button class="btn btn-danger" id="hapus_peserta">
                            Hapus Peserta
                        </button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-success" id="edit_peserta" data-bs-dismiss="modal">Edit</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Info Peserta -->
    <div class="modal fade" id="info_peserta_modal" tabindex="-1" aria-labelledby="infoPesertaModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="infoPesertaModalLabel">Info Peserta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Konten modal akan diisi dengan AJAX -->
                    <div id="info_awal"></div>
                    <div id="table_info_peserta" class="table-responsive mt-2"></div>
                    <div class="card mt-2">
                        <div class="card-header">
                            Tautan Peserta Via Undangan
                        </div>
                        <div class="card-body">
                            <p>Link Webinar : <a href="" id="link_webinar_undangan"></a></p>
                            <div class="mt-2">
                                <p>Konfigurasi</p>
                                <div class="row mb-3">
                                    <label class="col-md-3 col-form-label">Visabilitas Tautan Undangan</label>
                                    <div class="col-md-9">
                                        <div class="form-check form-check-inline mt-2">
                                            <input class="form-check-input" type="radio" name="visabilitas"
                                                id="visabilitas_y" value="Y">
                                            <label class="form-check-label" for="visabilitas_y">Ditampilkan</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="visabilitas"
                                                id="visabilitas_n" value="N">
                                            <label class="form-check-label" for="visabilitas_n">Disembunyikan</label>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-primary" id="update_undangan">Update</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="kirim_pesan_modal" tabindex="-1" aria-labelledby="kirimPesanModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="kirimPesanModalLabel">Kirim Pesan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="nav-align-top mt-2">
                        <ul class="nav nav-tabs" role="tablist" style="overflow-x: auto; white-space: nowrap;">
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-konfigurasi" aria-controls="navs-konfigurasi"
                                    aria-selected="true">Whatsapp</button>
                            </li>
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-pendaftaran" aria-controls="navs-pendaftaran"
                                    aria-selected="false">Email</button>
                            </li>



                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane fade show active" id="navs-konfigurasi" role="tabpanel">
                                <form id="form_wa_blast">
                                    <div class="mb-3">
                                        <label for="status" class="form-label">Status</label>
                                        <select class="form-select" name="status" id="status" required>
                                            <option value="paid">Paid</option>
                                            <option value="unpaid">Unpaid</option>
                                            <option value="expired">Expire</option>
                                            <option value="pending">Pending</option>
                                            <option value="undangan">Undangan</option>
                                        </select>
                                    </div>
                                    {{-- <div class="mb-3">
                                        <label for="paket_wa" class="form-label">Paket</label>
                                        <select class="form-select" name="paket_wa" id="paket_wa" required>
                                            <option value="semua">Semua</option>

                                        </select>
                                    </div> --}}
                                    <div class="mb-3">
                                        <label for="jenis_pesan" class="form-label">Jenis Pesan</label>
                                        <select class="form-select" name="jenis_pesan" id="jenis_pesan" required>
                                            <option value="">Pilih jenis pesan</option>
                                            <option value="Teks">Teks</option>
                                            <option value="File">File</option>
                                        </select>
                                    </div>
                                    <div class="mb-3" id="fileInputWrapper" style="display: none;">
                                        <label for="file" class="form-label">File</label>
                                        <input type="file" name="file[]" id="file" class="form-control"
                                            multiple>
                                    </div>
                                    <div class="mb-3">
                                        <label for="isi_pesan" class="form-label">Isi Pesan</label>
                                        <textarea class="form-control" name="isi_pesan" id="isi_pesan" rows="4" required></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Kirim</button>
                                </form>


                            </div>
                            <div class="tab-pane fade show " id="navs-pendaftaran" role="tabpanel">
                                <form id="kirim_pesan_email">
                                    <div class="mb-3">
                                        <label for="name_email" class="form-label">Email</label>
                                        <select id="name_email" name="email" class="form-select">
                                            <!-- Option akan diisi menggunakan AJAX -->
                                        </select>
                                    </div>
                                    <div class="mb-3"> 
                                        <label for="status_email" class="form-label">Status</label>
                                        <select class="form-select" name="status_email" id="status_email" required>
                                            <option value="paid">Paid</option>
                                            <option value="unpaid">Unpaid</option>
                                            <option value="expired">Expire</option>
                                            <option value="pending">Pending</option>
                                            <option value="undangan">Undangan</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="jenis_pesan_email" class="form-label">Jenis Pesan</label>
                                        <select id="jenis_pesan_email" name="jenis_pesan_email" class="form-select">
                                            <option value="Teks">Teks</option>
                                            <option value="File">File</option>
                                        </select>
                                    </div>
                                    <div class="mb-3" id="fileInputWrapperEmail" style="display: none;">
                                        <label for="file_email" class="form-label">File</label>
                                        <input type="file" name="file_email[]" id="file_email" class="form-control"
                                            multiple>
                                    </div>
                                    <div class="mb-3">
                                        <label for="subject" class="form-label">Subjek</label>
                                        <input type="text" id="subject" name="subject" class="form-control">
                                    </div>
                                    <div class="mb-3">
                                        <label for="isi_pesan_email" class="form-label">Isi Pesan</label>
                                        <textarea id="isi_pesan_email" name="isi_pesan_email" class="form-control"></textarea>
                                    </div>
                                    <div class="mb-3">
                                        <button type="submit" class="btn btn-primary">Kirim</button>
                                    </div>
                                </form>
                            </div>







                        </div>

                    </div>

                </div>
                <div class="modal-footer">


                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_export_excel" tabindex="-1" aria-labelledby="modal_export_excelLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_export_excelLabel">Export Excel</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="konfig_export">
                        <div class="mb-3">
                            <label for="status_excel" class="form-label">Status</label>
                            <select class="form-select" id="status_excel" name="status_excel">
                                <option value="semua">Semua</option>
                                <option value="paid">Paid</option>
                                <option value="unpaid">Unpaid</option>
                                <option value="expired">Expired</option>
                                <option value="pending">Pending</option>
                                <option value="undangan">Undangan</option>
                            </select>
                        </div>
                        <button type="submit" id="buat_excel" class="btn btn-primary">Buat Excel</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
        <script>
            CKEDITOR.replace('isi_pesan');
            CKEDITOR.replace('isi_pesan_email');

            function loader(show) {
                if (show) {
                    $(".overlay").show(); // Menampilkan preloader
                    $(".preloader").show();
                } else {
                    $(".overlay").hide(); // Menyembunyikan preloader
                    $(".preloader").hide(); // Menyembunyikan preloader
                }
            }
        </script>
        {{-- <script src="/portal/js/peserta_webinar.js"></script> --}}
        <script>
            var id = {{ $webinar->id }};
            console.log(id);
            let tableInstance;

            function table(url) {
                tableInstance = $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: url,
                        type: 'GET',
                        dataSrc: function(json) {
                            return json.data; // Data langsung dari server
                        }
                    },
                    columns: [{
                            data: null, // Kolom untuk nomor urut
                            render: function(data, type, row, meta) {
                                // Gunakan meta.settings._iDisplayStart untuk menghitung nomor urut yang berkelanjutan
                                return meta.settings._iDisplayStart + meta.row +
                                    1; // Menampilkan nomor urut yang berkelanjutan
                            }
                        },
                        @foreach ($formData as $field)
                            {
                                data: 'form.{{ $field['name'] }}', // Mengakses kolom sesuai dengan nama field pada form
                                title: '{{ $field['label'] ?? ucfirst($field['name']) }}', // Menampilkan label atau nama field
                                searchable: true
                            },
                        @endforeach {
                            data: 'paket',
                            searchable: true
                        },
                        {
                            data: 'id_transaksi',
                            searchable: true
                        },
                        {
                            data: 'jumlah',
                            searchable: true
                        },
                        {
                            data: 'status',
                            searchable: true
                        },
                        {
                            data: 'aksi',
                            searchable: false // Nonaktifkan pencarian untuk kolom 'aksi'
                        }
                    ],
                    language: {
                        search: "Cari: "
                    }
                });
            }

            function reloadDataTable() {
                if (tableInstance) {
                    tableInstance.ajax.reload(null, false); // Reload tabel tanpa reset pagination
                } else {
                    console.warn('DataTable belum diinisialisasi.');
                }
            }


            $(document).ready(function() {


                table('/webinars/' + id + '/data');



                $('#pencarianCard').click(function() {

                    $('#menuModal').modal('show');
                });
                $('#pencarianForm').on('submit', function(e) {
                    e.preventDefault(); // Mencegah submit form default

                    // Ambil data dari form
                    var status = $('#status').val();

                    // Tambahkan parameter ke URL
                    let newUrl = `/webinars/${id}/data?status=${encodeURIComponent(status)}`;

                    // Ganti URL DataTables dan reload
                    tableInstance.ajax.url(newUrl).load();
                });

                $(document).on('click', '.detail-btn', function() {
                    // Ambil ID dari atribut data-id
                    var id = $(this).data('id');
                    $('#peserta_id').val(id);

                    // Tampilkan modal terlebih dahulu
                    $('#detail_modal').modal('show');

                    // Ubah isi modal menjadi status memuat


                    // Lakukan AJAX request ke endpoint
                    $.ajax({
                        url: "/detail_peserta/" + id,
                        method: "GET",
                        success: function(response) {
                            if (response.success) {
                                const formEditPeserta = document.querySelector(
                                    "#form_edit_peserta");
                                formEditPeserta.innerHTML = ''; // Kosongkan form sebelumnya

                                // Fungsi untuk membuat input form
                                function generateForm(formConfig, formData) {
                                    formConfig.forEach(field => {
                                        const label = document.createElement('label');
                                        label.setAttribute('for', field.id);
                                        label.textContent = field.label;

                                        let input;
                                        if (field.type === 'select') {
                                            input = document.createElement('select');
                                            input.setAttribute('id', field.id);
                                            input.setAttribute('name', field.name);
                                            input.classList.add('form-control');

                                            field.options.forEach(option => {
                                                const optionElement = document
                                                    .createElement('option');
                                                optionElement.value = option;
                                                optionElement.textContent = option;
                                                if (formData[field.name] ===
                                                    option) {
                                                    optionElement.selected = true;
                                                }
                                                input.appendChild(optionElement);
                                            });
                                        } else {
                                            input = document.createElement('input');
                                            input.setAttribute('type', field.type);
                                            input.setAttribute('id', field.id);
                                            input.setAttribute('name', field.name);
                                            input.setAttribute('placeholder', field
                                                .placeholder || '');
                                            input.value = formData[field.name] || '';
                                            input.classList.add('form-control');
                                        }

                                        if (field.validations.includes('required')) {
                                            input.setAttribute('required', 'true');
                                        }

                                        const helperText = document.createElement('small');
                                        helperText.textContent = field.keterangan || '';
                                        helperText.classList.add('form-text', 'text-muted');

                                        const div = document.createElement('div');
                                        div.classList.add('mb-3');
                                        div.appendChild(label);
                                        div.appendChild(input);
                                        div.appendChild(helperText);

                                        formEditPeserta.appendChild(div);
                                    });
                                }

                                // Tambahkan elemen form dari formConfig dan formData
                                generateForm(response.formconfig, JSON.parse(response.data.form));

                                // Tambahkan input select untuk Paket
                                const paketDiv = document.createElement('div');
                                paketDiv.classList.add('mb-3');

                                const paketLabel = document.createElement('label');
                                paketLabel.textContent = "Paket";
                                paketDiv.appendChild(paketLabel);

                                const paketSelect = document.createElement('select');
                                paketSelect.classList.add('form-control');
                                paketSelect.setAttribute('name', 'paket');

                                response.formcost?.forEach(paket => {
                                    const option = document.createElement('option');
                                    option.value = paket.id;
                                    option.textContent = paket.namapaket;
                                    if (response.data.paket === paket.id) {
                                        option.selected = true;
                                    }
                                    paketSelect.appendChild(option);
                                });
                                paketDiv.appendChild(paketSelect);
                                formEditPeserta.appendChild(paketDiv);

                                // Tambahkan select untuk Status
                                const statusDiv = document.createElement('div');
                                statusDiv.classList.add('mb-3');

                                const statusLabel = document.createElement('label');
                                statusLabel.textContent = "Status";
                                statusDiv.appendChild(statusLabel);

                                const statusSelect = document.createElement('select');
                                statusSelect.classList.add('form-control');
                                statusSelect.setAttribute('name', 'status');

                                ["", "paid", "unpaid", "expired", "pending", "undangan"].forEach(
                                    status => {
                                        const option = document.createElement('option');
                                        option.value = status;
                                        option.textContent = status.charAt(0).toUpperCase() +
                                            status.slice(1);
                                        if (response.data.status === status) {
                                            option.selected = true;
                                        }
                                        statusSelect.appendChild(option);
                                    });
                                statusDiv.appendChild(statusSelect);
                                formEditPeserta.appendChild(statusDiv);

                                // Tambahkan input ID Transaksi (readonly)
                                const idTransaksiDiv = document.createElement('div');
                                idTransaksiDiv.classList.add('mb-3');

                                const idTransaksiLabel = document.createElement('label');
                                idTransaksiLabel.textContent = "ID Transaksi";
                                idTransaksiDiv.appendChild(idTransaksiLabel);

                                const idTransaksiInput = document.createElement('input');
                                idTransaksiInput.classList.add('form-control');
                                idTransaksiInput.setAttribute('name', 'id_transaksi');
                                idTransaksiInput.setAttribute('readonly', true);
                                idTransaksiInput.value = response.data.id_transaksi;
                                idTransaksiDiv.appendChild(idTransaksiInput);
                                formEditPeserta.appendChild(idTransaksiDiv);

                                // Tambahkan span untuk Harga, Donasi, dan Jumlah
                                const financialData = ["harga", "donasi", "jumlah"];
                                financialData.forEach(field => {
                                    const financialDiv = document.createElement('div');
                                    financialDiv.classList.add('mb-3');

                                    const label = document.createElement('label');
                                    label.textContent = field.charAt(0).toUpperCase() +
                                        field.slice(1);
                                    financialDiv.appendChild(label);

                                    const span = document.createElement('p');
                                    span.textContent = response.data[field];
                                    financialDiv.appendChild(span);

                                    formEditPeserta.appendChild(financialDiv);
                                });

                                // Tampilkan modal
                                $("#detail_modal").modal("show");
                            } else {
                                alert("Gagal memuat detail peserta.");
                            }
                        },
                        error: function() {
                            alert("Terjadi kesalahan saat memuat data.");
                        }
                    });


                });

                $('#hapus_peserta').on('click', function() {
                    let pesertaId = $('#peserta_id').val(); // Ambil nilai ID peserta

                    if (!pesertaId) {
                        Swal.fire({
                            icon: 'warning',
                            title: 'ID Peserta Tidak Ditemukan',
                            text: 'Harap pilih peserta sebelum menghapus.',
                        });
                        return;
                    }

                    Swal.fire({
                        title: 'Konfirmasi',
                        text: 'Apakah Anda yakin ingin menghapus peserta ini?',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#d33',
                        cancelButtonColor: '#3085d6',
                        confirmButtonText: 'Ya, Hapus',
                        cancelButtonText: 'Batal',
                    }).then((result) => {
                        if (result.isConfirmed) {
                            $.ajax({
                                type: 'POST',
                                url: '/hapus_peserta/' + pesertaId,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                        'content'), // Tambahkan CSRF Token jika diperlukan
                                },
                                success: function(response) {
                                    if (response.success) {
                                        reloadDataTable()
                                        $('#detail_modal').modal('hide');
                                        Swal.fire({
                                            icon: 'success',
                                            title: 'Peserta Dihapus',
                                            text: response.message ||
                                                'Peserta berhasil dihapus.',
                                        });
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            title: 'Gagal',
                                            text: 'Terjadi kesalahan saat menghapus peserta.',
                                        });
                                    }
                                },
                                error: function(xhr) {
                                    Swal.fire({
                                        icon: 'error',
                                        title: 'Gagal',
                                        text: xhr.responseJSON?.message ||
                                            'Terjadi kesalahan saat menghapus peserta.',
                                    });
                                },
                            });
                        }
                    });
                });
                $('#edit_peserta').on('click', function() {
                    // Ambil ID peserta dari input hidden
                    var pesertaId = $('#peserta_id').val();

                    // Kirim permintaan AJAX ke URL yang sesuai
                    $.ajax({
                        url: '/edit_peserta/' + encodeURIComponent(pesertaId), // URL dengan peserta_id
                        type: 'POST', // Gunakan metode POST (atau sesuai kebutuhan)
                        data: $('#form_edit_peserta').serialize(), // Kirim data dari form
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr(
                                'content'),
                        },
                        success: function(response) {
                            // Tindakan setelah sukses
                            if (response.success) {
                                reloadDataTable();
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Sukses',
                                    text: 'Peserta berhasil diubah!',
                                });
                            }

                            // Lakukan tindakan tambahan jika diperlukan, seperti reload tabel

                        },
                        error: function(xhr) {
                            // Tindakan jika terjadi error
                            Swal.fire({
                                icon: 'error',
                                title: 'Galat',
                                text: xhr.responseJSON?.message ||
                                    'Terjadi kesalahan saat mengedit peserta.',
                            });
                        },
                    });
                });

                $(document).on('click', '#info_peserta', function() {
                    // Ganti dengan ID Webinar yang sesuai, bisa diambil dari elemen atau variabel lain

                    // Kirim AJAX ke endpoint /count_peserta dengan parameter id_webinar
                    $.ajax({
                        url: '/count_peserta/' + id, // Ganti dengan URL sesuai
                        type: 'GET', // Atau POST sesuai kebutuhan
                        success: function(response) {
                            if (response.success) {
                                // Mengosongkan div sebelum menambahkan konten baru
                                $('#table_info_peserta').empty();
                                $('#info_awal').empty();

                                // Menampilkan informasi kuota dan peserta mendaftar
                                var infoHTML = `
                    <p>Kuota: <strong>${response.kuota}</strong></p>
                    <p>Peserta Mendaftar: <strong>${response.peserta}</strong></p>
                `;

                                // Menambahkan informasi di atas ke dalam div #table_info
                                $('#info_awal').append(infoHTML);

                                // Membuat tabel
                                var tableHTML = `
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Jumlah Peserta Undangan</td>
                                <td>${response.peserta_undangan}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Peserta Paid</td>
                                <td>${response.peserta_paid}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Peserta Unpaid</td>
                                <td>${response.peserta_unpaid}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Peserta Pending</td>
                                <td>${response.peserta_pending}</td>
                            </tr>
                            <tr>
                                <td>Jumlah Peserta Expired</td>
                                <td>${response.peserta_expired}</td>
                            </tr>
                            <tr>
                                <td><strong>PESERTA VALID</strong></td>
                               <td><strong>${response.peserta_paid} / ${response.kuota}</strong></td>

                            </tr>
                            <tr>
                                <td><strong>PESERTA TIDAK VALID</strong></td>
                                <td><strong>${(response.peserta_unpaid + response.peserta_pending + response.peserta_expired)}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                `;

                                var fullUrl =
                                    window.location.origin + "/w/u/" + response.slug;
                                $("#link_webinar_undangan")
                                    .attr("href", fullUrl) // Set href ke domain + /w/slug
                                    .text(fullUrl) // Set teks dari link sesuai dengan URL lengkap
                                    .attr("target", "_blank");
                                $('#table_info_peserta').append(tableHTML);
                                if (response.undangan === 'N') {
                                    $('#visabilitas_n').prop('checked', true);
                                } else if (response.undangan === 'Y') {
                                    $('#visabilitas_y').prop('checked', true);
                                }
                                $('#info_peserta_modal').modal('show');
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: 'Tidak dapat memuat data peserta.'
                                });
                            }



                        },
                        error: function(xhr, status, error) {
                            console.error('Error:', error);
                            Swal.fire({
                                icon: 'error',
                                title: 'Gagal',
                                text: 'Terjadi kesalahan saat mengambil data peserta.'
                            });
                        }
                    });
                });
                $('#update_undangan').click(function() {
                    // Ambil nilai radio button yang dipilih
                    var visabilitas = $('input[name="visabilitas"]:checked').val();

                    // Ambil ID (misalnya dari variabel id_webinar atau lainnya)


                    // Kirim AJAX POST request
                    $.ajax({
                        url: '/update_undangan/' + id, // Endpoint yang sesuai
                        type: 'POST',
                        data: {
                            undangan: visabilitas,
                            _token: $('meta[name="csrf-token"]').attr(
                                'content') // Menambahkan CSRF token
                        },
                        success: function(response) {
                            if (response.success) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Berhasil',
                                    text: 'Visabilitas tautan undangan berhasil diperbarui.'
                                });
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Gagal',
                                    text: 'Tidak dapat memperbarui visabilitas tautan undangan.'
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            console.error('Error:', error);
                            Swal.fire({
                                icon: 'error',
                                title: 'Gagal',
                                text: 'Terjadi kesalahan saat memperbarui visabilitas.'
                            });
                        }
                    });
                });

                $('#exportExcelCard').click(function() {
                    $('#modal_export_excel').modal('show');
                });

                $('#konfig_export').submit(function(event) {
                    event.preventDefault();

                    var status = $('#status_excel').val();
                    var exportUrl = '/export_excel_peserta_webinar?status=' + status + '&id=' + id;
                    var link = document.createElement('a');
                    link.href = exportUrl;
                    link.target = '_blank'; // Membuka file di tab baru
                    link.click();
                });


            });
        </script>
        <script src="/portal/js/config_webinar_blast.js"></script>
    @endpush
@endsection

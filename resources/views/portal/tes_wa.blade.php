<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>WA Blast</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    <div class="container mt-5">
        <h1 class="mb-4">WA Blast</h1>

        <!-- Input Form -->
        <div class="row mb-3">
            <div class="col-md-5">
                <label for="name" class="form-label">Nama</label>
                <input type="text" id="name" class="form-control" placeholder="Masukkan nama">
            </div>
            <div class="col-md-5">
                <label for="number" class="form-label">Nomor</label>
                <input type="text" id="number" class="form-control" placeholder="Masukkan nomor">
            </div>
            <div class="col-md-2 d-flex align-items-end">
                <button id="addButton" class="btn btn-primary w-100">Tambah</button>
            </div>
        </div>

        <!-- Message Textarea -->
        <div class="mb-4">
            <label for="message" class="form-label">Isi Pesan</label>
            <textarea id="message" class="form-control" rows="4" placeholder="Tulis pesan Anda di sini..."></textarea>
            <button id="sendButton" class="btn btn-success mt-3">Kirim Pesan</button>
        </div>

        <!-- Table List -->
        <h5>List WA</h5>
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Nomor</th>
                </tr>
            </thead>
            <tbody id="waList">
                <!-- Rows will be added dynamically -->
            </tbody>
        </table>
    </div>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script>
        let counter = 1;

        $("#addButton").on("click", function() {
            const name = $("#name").val().trim();
            const number = $("#number").val().trim();

            if (!name || !number) {
                alert("Nama dan nomor tidak boleh kosong!");
                return;
            }

            const row = `
                <tr>
                    <td>${counter++}</td>
                    <td>${name}</td>
                    <td>${number}</td>
                </tr>
            `;

            $("#waList").append(row);

            // Clear input fields
            $("#name").val('');
            $("#number").val('');
        });

        $("#sendButton").on("click", function() {
            const message = $("#message").val().trim();
            const numbers = [];

            $("#waList tr").each(function() {
                const number = $(this).find("td:nth-child(3)").text().trim();
                if (number) {
                    numbers.push(number);
                }
            });

            if (!message || numbers.length === 0) {
                alert("Pesan atau nomor tidak boleh kosong!");
                return;
            }

            $.ajax({
                url: "/kirim_wa",
                method: "POST",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    numbers,
                    message
                },
                success: function(response) {
                    alert("Pesan berhasil dikirim!");
                },
                error: function(error) {
                    console.error(error);
                    alert("Terjadi kesalahan saat mengirim pesan.");
                }
            });
        });
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
</body>

</html>

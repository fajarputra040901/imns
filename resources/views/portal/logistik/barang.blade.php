@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }

        .barcode {
            width: 100%;
            /* Pastikan lebar diatur */
            height: auto;
            display: flex;
            /* Atur sebagai flex untuk penataan */
            justify-content: center;
            /* Pusatkan barcode */
            align-items: center;
            overflow: visible;
            /* Pastikan tidak terpotong */
            padding: 10px;
            /* Tambahkan ruang untuk memastikan tampilan */
            background-color: #f9f9f9;
            /* Opsional: latar belakang */
            border: 1px solid #ddd;
            /* Opsional: garis pembatas */
        }

        .barcode svg {
            width: 100%;
            /* Sesuaikan ukuran */
            max-width: 400px;
            /* Batas maksimum */
            height: auto;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Permintaan Pencairan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="tambah_barang">
                    <i class="bx bx-plus-circle menu-icons"></i> <!-- Ikon tambah -->
                    <div class="menu-title">Tambah Barang</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pencarian -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPemasukanCard">
                    <i class="bx bx-search menu-icons"></i> <!-- Ikon pencarian -->
                    <div class="menu-title">Pencarian</div>
                </div>
            </div>

            <!-- Card Menu 3 - Import Excel -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="import">
                    <i class='bx bx-import menu-icons'></i>
                    <div class="menu-title">Import Excel</div>
                </div>
            </div>

            {{-- <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPengeluaranCard">
                    <i class="bx bx-chart menu-icons"></i> <!-- Ikon grafik pengeluaran -->
                    <div class="menu-title"></div>
                </div>
            </div> --}}

            <!-- Card Menu 4 - Export Excel -->

        </div>




        <div class="mt-3">



            <div class="card ">
                <div class="card-body table-responsive">

                    <table class="table table-striped" id="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>

                                <th>Code</th>
                                <th>Nama</th>
                                <th>Detail</th>
                                <th>keterangan</th>
                                <th>Jenis</th>
                                <th>Stok</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>

    <div class="modal fade" id="modal_tambah_barang" tabindex="-1" aria-labelledby="modalTambahBarangLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="form_tambah_barang">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTambahBarangLabel">Tambah Barang</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3">
                            <!-- Input Code -->
                            <div class="col-md-6">
                                <label for="code" class="form-label">Code</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" readonly id="code" name="code">
                                    <div class="input-group-text">
                                        <input type="checkbox" id="checkbox_otomatis" checked>
                                        <label for="checkbox_otomatis" class="form-label">Otomatis</label>
                                    </div>
                                </div>
                            </div>
                            <!-- Input Nama Barang -->
                            <div class="col-md-6">
                                <label for="nama" class="form-label">Nama Barang</label>
                                <input type="text" class="form-control" id="nama" name="nama">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="jenis" class="form-label">Jenis</label>
                                <select name="jenis" id="jenis" class="form-control">
                                    <option value="">Pilih</option>
                                    <option value="frame">Frame</option>
                                    <option value="lensa">Lensa</option>
                                    <option value="lainnya">Lainnya</option>
                                </select>
                                <input type="text" name="jenis_lainnya" id="jenis_lainnya" class="mt-2 form-control"
                                    placeholder="Masukan Jenis Lainnya" style="display: none">
                            </div>
                            <div class="col-md-6">
                                <label for="stok" class="form-label">Stok</label>
                                <input type="number" name="stok" id="stok" class="mt-2 form-control"
                                    placeholder="Masukan Stok">

                            </div>
                            <!-- Textarea Detail Barang -->
                            <div class="col-md-6">
                                <label for="detail" class="form-label">Detail Barang</label>
                                <textarea class="form-control" id="detail" name="detail" rows="3"></textarea>
                            </div>
                            <!-- Textarea Keterangan -->
                            <div class="col-md-6">
                                <label for="keterangan" class="form-label">Keterangan</label>
                                <textarea class="form-control" id="keterangan" name="keterangan" rows="3"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary me-2">Submit</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_detail" tabindex="-1" aria-labelledby="modalTambahBarangLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form id="form_edit_barang">
                    <input type="hidden" id="id_barang">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTambahBarangLabel">Detail Barang</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="code_edit" class="form-label">Code</label>
                                <div class="input-group">
                                    <input type="text" readonly class="form-control" id="code_edit" name="code_edit">
                                    <div class="input-group-text">
                                        <input type="checkbox" id="checkbox_otomatis_edit" checked>
                                        <label for="checkbox_otomatis_edit" class="form-label">Otomatis</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="nama_edit" class="form-label">Nama Barang</label>
                                <input type="text" class="form-control" id="nama_edit" name="nama_edit">
                            </div>
                        </div>
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="jenis_edit" class="form-label">Jenis</label>
                                <select name="jenis_edit" id="jenis_edit" class="form-control">
                                    <option value="">Pilih</option>
                                    <option value="frame">Frame</option>
                                    <option value="lensa">Lensa</option>
                                    <option value="lainnya">Lainnya</option>
                                </select>
                                <input type="text" name="jenis_lainnya_edit" id="jenis_lainnya_edit"
                                    class="mt-2 form-control" placeholder="Masukan Jenis Lainnya" style="display: none">
                            </div>
                            <div class="col-md-6">
                                <label for="stok_edit" class="form-label">Stok</label>
                                <input type="number" name="stok_edit" id="stok_edit" class="mt-2 form-control"
                                    placeholder="Masukan Stok">
                            </div>
                            <div class="col-md-6">
                                <label for="detail_edit" class="form-label">Detail Barang</label>
                                <textarea class="form-control" id="detail_edit" name="detail_edit" rows="3"></textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="keterangan_edit" class="form-label">Keterangan</label>
                                <textarea class="form-control" id="keterangan_edit" name="keterangan_edit" rows="3"></textarea>
                            </div>
                        </div>
                        <div class="card mt-2">
                            <div class="card-body">
                                <canvas id="barcode"></canvas>

                                <div class="mt-2">
                                    <button class="btn btn-primary" id="cetak_label">Cetak Label</button>
                                </div>
                                <div class="mt-2">
                                    <button class="btn btn-danger" id="hapus_barang">Hapus Barang</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary me-2">Submit</button>
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_import" tabindex="-1" aria-labelledby="modalImportLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalImportLabel">Import Excel</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="form_import" enctype="multipart/form-data">
                        <div class="mb-3">
                            <label for="file_import" class="form-label">Pilih File Excel</label>
                            <input type="file" class="form-control" id="file_import" name="file_import"
                                accept=".xlsx, .xls" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Upload</button>
                    </form>
                    <div class="card mt-2">
                        <div class="card-body">
                            <button class="btn btn-success" id="template">Template Import</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/jsbarcode@3.11.5/dist/JsBarcode.all.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bwip-js/dist/bwip-js-min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.18.5/xlsx.full.min.js"></script>


        <script>
            $(document).on('click', '#permintaanPencairanCard', function() {
                window.location.href = '/permintaan_pencairan';

            });
        </script>
        <script src="/portal/js/logistik_barang.js"></script>
    @endpush
@endsection

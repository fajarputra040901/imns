@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - List Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="list_barang">
                    <i class="bx bx-list-ul menu-icons"></i> <!-- Ikon daftar/list -->
                    <div class="menu-title">List Barang</div>
                </div>
            </div>

            <!-- Card Menu 2 - List Inventaris -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="list_inventaris">
                    <i class="bx bx-box menu-icons"></i> <!-- Ikon inventaris/kotak -->
                    <div class="menu-title">List Inventaris</div>
                </div>
            </div>

            <!-- Card Menu 3 - Permintaan Pengadaan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPengeluaranCard">
                    <i class="bx bx-receipt menu-icons"></i> <!-- Ikon dokumen/permintaan -->
                    <div class="menu-title">Permintaan Pengadaan</div>
                </div>
            </div>

            <!-- Card Menu 4 - Stokis Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="stokis_barang">
                    <i class='bx bx-home-circle menu-icons'></i>
                    <div class="menu-title">Stokis Barang</div>
                </div>
            </div>
        </div>





        <div class="mt-3">
            <div class="card">
                <div class="card-body">
                    Permintaan Pengadaan Barang
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body table-responsive">

                    <table class="table table-striped" id="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tanggal Pengajuan</th>
                                <th>Tanggal Validasi</th>
                                <th>Judul</th>
                                <th>Detail</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Data akan di-load oleh DataTables -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="modal_detal_permintaan" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <input type="hidden" name="id_pengajuan" id="id_pengajuan">
                    <h5 class="modal-title" id="modalLabel">Detail Permintaan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="div_detal"></div>
                    <div id="tombol_validasi">

                    </div>

                    <div class="mt-2" style="display: none">
                        <button id="ajukan_rab_barang" class="btn btn-warning">Ajukan Sebagai RAB</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Fullscreen -->
    <div class="modal fade" id="modal_validasi" tabindex="-1" aria-labelledby="modalValidasiLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalValidasiLabel">Validasi Barang</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Isi modal -->
                    <div class="card">
                        <div class="card-body">
                            <div class="btn-group" role="group" aria-label="Button group">
                                <button type="button" class="btn btn-primary btn-md me-2 validator"
                                    data-indikator='validasi'>
                                    <i class="bx bx-check-circle"></i> Validasi Semua
                                </button>
                                <button type="button" class="btn btn-danger btn-md me-2 validator" data-indikator='tolak'>
                                    <i class="bx bx-x-circle"></i> Tolak Semua
                                </button>
                                <button type="button" class="btn btn-secondary btn-md validator" data-indikator='reset'>
                                    <i class="bx bx-reset"></i> Reset
                                </button>
                            </div>
                        </div>

                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-header">
                                    Permintaan Barang
                                </div>
                                <div class="card-body table-responsive">

                                    <table id="table_permintaan" class="table table-stripped" style="font-size: 12px">
                                        <thead>
                                            <th>Code</th>
                                            <th>Nama Barang</th>
                                            <th>Detail</th>
                                            <th>Jumlah</th>
                                            <th>Aksi</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-header">
                                    Validasi Barang
                                </div>
                                <div class="card-body table-responsive">

                                    <table id="table_validasi" class="table table-stripped" style="font-size: 12px">
                                        <thead>
                                            <th>Code</th>
                                            <th>Nama Barang</th>
                                            <th>Detail</th>
                                            <th>Jumlah</th>
                                            <th>Status</th>
                                            <th>Keterangan</th>
                                            <th>Aksi</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="simpan_semua_validasi">Simpan</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_validasi_barang" tabindex="-1" aria-labelledby="validasiBarangLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="validasiBarangLabel">Validasi Barang</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="form_validasi_barang">
                        <div class="mb-3">
                            <label for="status" class="form-label">Status</label>
                            <select class="form-select" name="status" id="status">
                                <option value="" selected>Pilih</option>
                                <option value="diajukan">Diajukan</option>

                                <option value="divalidasi">Divalidasi</option>
                                <option value="ditolak">Ditolak</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="keterangan" class="form-label">Keterangan</label>
                            <textarea class="form-control" name="keterangan" id="keterangan" rows="3"></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="simpan_validasi">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalValidator" tabindex="-1" aria-labelledby="modalValidatorLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalValidatorLabel">Masukkan Keterangan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formValidator">
                        <div class="mb-3">
                            <label for="keterangan_validator" class="form-label">Keterangan</label>
                            <textarea class="form-control" id="keterangan_validator" name="keterangan_validator" rows="3"
                                placeholder="Masukkan keterangan"></textarea>
                        </div>
                        <input type="hidden" id="indikator_validator" name="indikator_validator">
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="submitValidator">Lanjutkan</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalKeterangan" tabindex="-1" aria-labelledby="modalKeteranganLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalKeteranganLabel">Konfirmasi dan Keterangan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="formKeterangan">
                        <div class="mb-3">
                            <label for="status_final" class="form-label">Status Pengajuan</label>
                            <select class="form-select" name="status_final" id="status_final">
                                <option value="" selected>Pilih</option>

                                <option value="divalidasi">Divalidasi</option>
                                <option value="ditolak">Ditolak</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="keterangan_final" class="form-label">Keterangan</label>
                            <textarea class="form-control" id="keterangan_final" name="keterangan_final" rows="4"
                                placeholder="Masukkan keterangan..."></textarea>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="submitKeterangan">Lanjutkan</button>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script src="/portal/js/logistik_permintaan_pengadaan.js"></script>
    @endpush
@endsection

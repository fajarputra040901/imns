@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - List Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="list_barang">
                    <i class="bx bx-list-ul menu-icons"></i> <!-- Ikon daftar/list -->
                    <div class="menu-title">List Barang</div>
                </div>
            </div>

            <!-- Card Menu 2 - List Inventaris -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="list_inventaris">
                    <i class="bx bx-box menu-icons"></i> <!-- Ikon inventaris/kotak -->
                    <div class="menu-title">List Inventaris</div>
                </div>
            </div>

            <!-- Card Menu 3 - Permintaan Pengadaan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPengeluaranCard">
                    <i class="bx bx-receipt menu-icons"></i> <!-- Ikon dokumen/permintaan -->
                    <div class="menu-title">Permintaan Pengadaan</div>
                </div>
            </div>

            <!-- Card Menu 4 - Stokis Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="stokis_barang">
                    <i class='bx bx-home-circle menu-icons'></i>
                    <div class="menu-title">Stokis Barang</div>
                </div>
            </div>
        </div>





        {{-- <div class="mt-3">
            <div class="card">
                <div class="card-body">
                    Riwayat Pencairan
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body table-responsive">

                    <table class="table table-striped" id="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>

                                <th>Tanggal Pengajuan</th>
                                <th>Tanggal Validasi</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Data akan di-load oleh DataTables -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div> --}}


    </div>





    @push('scripts')
        <script>
            $(document).on('click', '#list_barang', function() {
                window.open('/logistik/barang', '_blank');
            });
            $(document).on('click', '#list_inventaris', function() {
                window.open('/logistik/inventaris', '_blank');
            });
        </script>
    @endpush
@endsection

@extends('partial.sneat')
@push('style')
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">

        <div>

            <form action="" id="formulir_pendaftaran">
                @csrf
                <div id="formnya"></div>
                <select name="paket" id="biaya" class="form-control" data-validations="required">
                    <option value="">Pilih Paket</option>
                </select>
                <div id="keterangan" class="mt-3"></div>
                <div class="mb-3">
                    <label for="Harga" class="form-label">Harga</label>
                    <input type="text" class="form-control" id="harga" name="harga">
                    <span id="keterangan_harga"></span>
                </div>
                <div class="mb-3" id="donasikan">
                    <label for="Harga" class="form-label">Donasi</label>
                    <input type="text" class="form-control" id="donasi" name="donasi" placeholder="Donasi">
                    <span><i>Optional</i></span>
                </div>
                <input type="hidden" name="id" id="id" value="{{ $data->id }}">
                <div class="mt-2">
                    <button class="btn btn-primary" id="submit">Submit</button>
                </div>
            </form>

        </div>
    </div>




    @push('scripts')
        <script>
            var idnya = {{ $data->id }};
        </script>
        <script src="/portal/js/tes_form.js"></script>
    @endpush
@endsection

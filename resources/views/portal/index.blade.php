@extends('partial.sneat')
@section('content')
    <link href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css" rel="stylesheet">

    <style>
        .sidebar {
            position: fixed;
            top: 0;
            right: -350px;
            z-index: 9999;
            width: 350px;
            height: 100vh;
            background: #f8f9fa;
            box-shadow: -3px 0 5px rgba(0, 0, 0, 0.2);
            transition: all 0.3s;
            overflow-y: auto;
            padding: 20px;
        }

        .sidebar.open {
            right: 0;
        }

        .radio-group {
            display: flex;
            flex-direction: column;
            gap: 10px;
        }

        .radio-label {
            display: flex;
            align-items: center;
            gap: 10px;
            padding: 5px 10px;
            border-radius: 20px;
            color: #fff;
            font-weight: bold;
            cursor: pointer;
        }

        .radio-label.webinar {
            background-color: blue;
        }

        .radio-label.baksos {
            background-color: green;
        }

        .radio-label.konten {
            background-color: red;
        }

        .radio-label.event {
            background-color: darkred;
        }

        .radio-label input {
            margin: 0;
            position: relative;
            top: 2px;
        }

        .radio-group {
            display: flex;
            flex-direction: column;
            gap: 10px;
        }

        .radio-label {
            display: flex;
            align-items: center;
            gap: 10px;
            padding: 5px 10px;
            border-radius: 20px;
            color: #fff;
            font-weight: bold;
            cursor: pointer;
        }

        .radio-label.webinar {
            background-color: blue;
        }

        .radio-label.baksos {
            background-color: green;
        }

        .radio-label.konten {
            background-color: red;
        }

        .radio-label.event {
            background-color: darkred;
        }

        .radio-label input {
            margin: 0;
            position: relative;
            top: 2px;
        }

        #calendar {
            width: 100%;
            margin: 0 auto;
        }

        /* Styling for the header */
        .fc-toolbar {
            font-size: 1rem;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        /* Make toolbar buttons and text smaller on mobile */
        @media (max-width: 768px) {
            .fc-toolbar {
                font-size: 0.8rem;
                flex-direction: column;
                /* Stack the buttons vertically */
                align-items: flex-start;
            }

            .fc-toolbar button {
                font-size: 0.8rem;
                padding: 8px 12px;
            }

            .fc-toolbar .fc-button-group {
                width: 100%;
                display: flex;
                justify-content: space-between;
            }

            .fc-toolbar .fc-button {
                width: 48%;
                /* Make the buttons smaller and stack them */
            }

            .fc-toolbar .fc-title {
                font-size: 1.1rem;
                margin-bottom: 10px;
            }
        }

        /* Styling for calendar cells and header */
        @media (max-width: 576px) {
            .fc-dayGridMonth-view .fc-day-top {
                height: 80px;
                /* Smaller height for day cells */
                font-size: 10px;
            }
        }
    </style>
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h1 class="text-center">Timeline </h1>
                        <div id="calendar"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="sidebar" id="sidebar">
        <button class="btn btn-danger btn-sm float-end" id="closeSidebar">Close</button>
        <h4 class="mt-3">Tambah Peringatan</h4>
        <form id="eventForm">
            <div class="mb-3">
                <label for="title" class="form-label">Peringatan</label>
                <input type="text" class="form-control" id="title" placeholder="Masukkan Peringatan">
            </div>
            <div class="mb-3">
                <label class="form-label">Label</label>
                <div>
                    <label class="radio-label webinar">
                        <input type="radio" name="label" value="webinar" class="form-check-input"> Webinar
                    </label>
                    <label class="radio-label baksos">
                        <input type="radio" name="label" value="baksos" class="form-check-input"> Baksos
                    </label>
                    <label class="radio-label konten">
                        <input type="radio" name="label" value="konten" class="form-check-input"> Konten
                    </label>
                    <label class="radio-label event">
                        <input type="radio" name="label" value="event" class="form-check-input"> Event
                    </label>
                </div>
            </div>
            <div class="mb-3">
                <label for="dateRange" class="form-label">Range Tanggal</label>
                <input type="date" id="startDate" class="form-control mb-2" placeholder="Tanggal Mulai">
                <input type="date" id="endDate" class="form-control" placeholder="Tanggal Selesai">
            </div>
            <div class="mb-3">
                <label for="description" class="form-label">Keterangan</label>
                <textarea id="description" class="form-control" rows="3" placeholder="Masukkan Keterangan"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div> --}}
    <div id="sidebar" class="sidebar">
        <div class="sidebar-content">
            <h4 id="event-title"></h4>
            <p id="event-description"></p>
            <button class="btn btn-secondary mt-3" onclick="closeSidebar()">Tutup</button>
        </div>
    </div>
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>

        <script>
            document.addEventListener('DOMContentLoaded', function() {
                const calendarEl = document.getElementById('calendar');

                const calendar = new FullCalendar.Calendar(calendarEl, {
                    initialView: 'dayGridMonth', // Tampilan grid
                    headerToolbar: {
                        left: 'prev,next',
                        center: 'title',
                        right: 'dayGridMonth,timeGridWeek'
                    },
                    events: [{
                            title: 'Webinar',
                            start: '2024-12-15',
                            end: '2024-12-21', // FullCalendar membutuhkan end sebagai eksklusif
                            description: 'Webinar mengenai teknologi terbaru.',
                            color: 'blue',
                        },
                        {
                            title: 'Baksos',
                            start: '2024-12-15',
                            end: '2024-12-17',
                            description: 'Bakti sosial untuk masyarakat sekitar.',
                            color: 'green',
                        },
                        {
                            title: 'Event',
                            start: '2024-12-16',
                            end: '2024-12-18',
                            description: 'Event pameran seni.',
                            color: 'red',
                        }
                    ],
                    eventClick: function(info) {
                        // Tampilkan sidebar saat event di klik
                        document.getElementById('event-title').textContent = info.event.title;
                        document.getElementById('event-description').textContent = info.event.extendedProps
                            .description;
                        openSidebar();
                    },
                });

                calendar.render();

                // Sidebar control
                window.openSidebar = function() {
                    document.getElementById('sidebar').classList.add('open');
                };

                window.closeSidebar = function() {
                    document.getElementById('sidebar').classList.remove('open');
                };
            });
        </script>
    @endpush
@endsection

@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Permintaan Pencairan -->


            <!-- Card Menu 2 - Laporan Pemasukan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPemasukanCard">
                    <i class="bx bx-search menu-icons"></i> <!-- Ikon grafik pemasukan -->
                    <div class="menu-title">Pencarian</div>
                </div>
            </div>

            <!-- Card Menu 3 - Laporan Pengeluaran -->

        </div>




        <div class="mt-3">

            <div class="card mt-2">
                <div class="card-body table-responsive">

                    <table class="table table-striped" id="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>

                                <th>Tanggal Pengajuan</th>
                                <th>Tanggal Validasi</th>
                                <th>Jenis</th>
                                <th>Parameter</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Keterangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Data akan di-load oleh DataTables -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>


    <div class="modal fade" id="modal_detail_pengajuan" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Detail Pengajuan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Konten detail pengajuan akan dimuat di sini -->
                    <div id="detailPengajuanContent" class="table-responsive"></div>
                    <div id="konfigurasi">
                        <form action="" id="form_validasi"></form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        <script src="/portal/js/permintaan_pencairan.js"></script>
    @endpush
@endsection

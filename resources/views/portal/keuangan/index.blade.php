@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Permintaan Pencairan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="permintaanPencairanCard">
                    <i class="bx bx-dollar-circle menu-icons"></i> <!-- Ikon dolar -->
                    <div class="menu-title">Permintaan Pencairan</div>
                </div>
            </div>

            <!-- Card Menu 2 - Laporan Pemasukan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPemasukanCard">
                    <i class="bx bx-bar-chart-alt-2 menu-icons"></i> <!-- Ikon grafik pemasukan -->
                    <div class="menu-title">Laporan Pemasukan</div>
                </div>
            </div>

            <!-- Card Menu 3 - Laporan Pengeluaran -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="laporanPengeluaranCard">
                    <i class="bx bx-chart menu-icons"></i> <!-- Ikon grafik pengeluaran -->
                    <div class="menu-title">Laporan Pengeluaran</div>
                </div>
            </div>

            <!-- Card Menu 4 - Export Excel -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="exportExcelCard">
                    <i class="bx bx-file menu-icons"></i> <!-- Ikon file -->
                    <div class="menu-title">Export Excel</div>
                </div>
            </div>
        </div>




        {{-- <div class="mt-3">
            <div class="card">
                <div class="card-body">
                    Riwayat Pencairan
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body table-responsive">

                    <table class="table table-striped" id="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>

                                <th>Tanggal Pengajuan</th>
                                <th>Tanggal Validasi</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Data akan di-load oleh DataTables -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div> --}}


    </div>





    @push('scripts')
        <script>
            $(document).on('click', '#permintaanPencairanCard', function() {
                window.location.href = '/permintaan_pencairan';
            });
        </script>
    @endpush
@endsection

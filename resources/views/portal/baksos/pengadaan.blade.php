@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }

        /* Kontainer khusus untuk form scanner */
        .custom-scanner-container {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-top: 20px;
            padding: 20px;
            border: 2px solid #3498db;
            border-radius: 10px;
            background-color: #f7f9fc;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            width: 100%;
            max-width: 400px;
            margin-left: auto;
            margin-right: auto;
        }

        /* Label untuk scanner */
        .custom-scanner-label {
            font-size: 18px;
            font-weight: bold;
            color: #2c3e50;
            margin-bottom: 10px;
            text-align: center;
        }

        /* Input khusus scanner */
        .custom-scanner-input {
            width: 100%;
            padding: 15px;
            font-size: 20px;
            border: 2px solid #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #2c3e50;
            background-color: #ffffff;
            transition: all 0.3s ease;
        }

        /* Efek hover dan focus untuk input */
        .custom-scanner-input:hover,
        .custom-scanner-input:focus {
            border-color: #2980b9;
            background-color: #ecf0f1;
            outline: none;
            box-shadow: 0 0 5px rgba(41, 128, 185, 0.8);
        }

        /* Responsif untuk perangkat kecil */
        @media (max-width: 768px) {
            .custom-scanner-container {
                padding: 15px;
                max-width: 90%;
            }

            .custom-scanner-input {
                font-size: 18px;
                padding: 12px;
            }

            .custom-scanner-label {
                font-size: 16px;
            }
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Pengajuan Barang Tersedia -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="barang_tersedia">
                    <i class="bx bx-box menu-icons"></i> <!-- Ikon kotak (barang) -->
                    <div class="menu-title">Pengajuan Barang Tersedia</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pengajuan Barang Tidak Tersedia -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="tambah_baksos">
                    <i class="bx bx-cart-add menu-icons"></i> <!-- Ikon menambah keranjang (barang) -->
                    <div class="menu-title">Pengajuan Barang Tidak Tersedia</div>
                </div>
            </div>

            <!-- Card Menu 3 - Peminjaman Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="peminjaman_barang">
                    <i class="bx bx-credit-card menu-icons"></i> <!-- Ikon kartu kredit (peminjaman) -->
                    <div class="menu-title">Peminjaman Barang</div>
                </div>
            </div>

            <!-- Card Menu 4 - Pengembalian Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="pengembalian_barang">
                    <i class="bx bx-undo menu-icons"></i> <!-- Ikon pengembalian (undo) -->
                    <div class="menu-title">Pengembalian Barang</div>
                </div>
            </div>
        </div>



        <div class="mt-3">

            <div class="card mt-2">
                <div class="card-header">
                    Riwayat Pengajuan Pengadaan Barang
                </div>
                <div class="card-body table-responsive">

                    <table class="table table-hover" id="table_riwayat" style="width:100%;font-size:14px">
                        <thead>
                            <tr>
                                <th style="width: 5%">No</th>
                                <th style="width: 5%">Tanggal Pengajuan</th>
                                <th style="width: 10%">Tanggal Validasi</th>
                                <th style="width: 10%">Judul</th>

                                <th style="width: 15%">Status</th>
                                <th style="width: 10%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


    </div>

    <div class="modal fade" id="modal_barang_tersedia" tabindex="-1" aria-labelledby="modal_barang_tersediaLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-fullscreen modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_barang_tersediaLabel">Pengajuan Barang Tersedia</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <form id="scanner_form">
                                <input type="hidden" name="parameter" id="parameter" value="{{ $baksos['id'] }}">
                                <div class="custom-scanner-container">
                                    <label for="input_scanner" class="custom-scanner-label">Scan Barcode:</label>
                                    <input type="text" id="input_scanner" name="input_scanner"
                                        class="custom-scanner-input" placeholder="Arahkan scanner ke sini..."
                                        autocomplete="off">
                                </div>
                            </form>

                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-header">
                                    Pilih Barang
                                </div>
                                <div class="card-body">
                                    <table class="table" id="table_barang" style="font-size: 12px">
                                        <thead>
                                            <th>Code</th>
                                            <th>Nama</th>
                                            <th>Jenis</th>
                                            <th>Detail</th>
                                            <th>Stok</th>
                                            <th>Aksi</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="card">
                                <div class="card-header">
                                    Barang Terpilih
                                </div>
                                <div class="card-body">
                                    <table class="table" id="table_barang_terpilih" style="font-size: 12px">
                                        <thead>
                                            <th>Code</th>
                                            <th>Nama</th>
                                            <th>Jenis</th>
                                            <th>Detail</th>
                                            <th>Stok</th>
                                            <th>Aksi</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" id="simpan_pengadaan" class="btn btn-primary">Simpan</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalPilihBarang" tabindex="-1" aria-labelledby="modalPilihBarangLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalPilihBarangLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Tabel untuk menampilkan detail barang -->
                    <table class="table table-bordered">
                        <tr>
                            <th>Nama</th>
                            <td id="namaBarang"></td>
                        </tr>
                        <tr>
                            <th>Jenis</th>
                            <td id="jenisBarang"></td>
                        </tr>
                        <tr>
                            <th>Detail</th>
                            <td id="detailBarang"></td>
                        </tr>
                        <tr>
                            <th>Keterangan</th>
                            <td id="keteranganBarang"></td>
                        </tr>
                        <tr>
                            <th>Stok</th>
                            <td id="stokBarang"></td>
                        </tr>
                    </table>

                    <!-- Form untuk memasukkan jumlah stok -->
                    <form id="formPilihBarang">
                        <div class="mb-3">
                            <label for="jumlahStok" class="form-label" id="labeljumlah"></label>
                            <input type="number" class="form-control" id="jumlahStok" name="jumlah_stok"
                                placeholder="Jumlah stok" required>
                        </div>
                        <input type="hidden" id="barangId" name="id_barang">
                        <input type="hidden" id="jenis_form" name="jenis_form">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Detail Pengajuan -->
    <div class="modal fade" id="modal_detail_pengajuan" tabindex="-1" role="dialog"
        aria-labelledby="modal_detail_pengajuanLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_detail_pengajuanLabel">Detail Pengajuan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="detail_pengajuan_div"></div>
                    <div class="mt-2">
                        <input type="hidden" id="id_pengajuan_barang">
                        <button id="detail_barang_pengajuan" class="btn btn-primary">Detail Barang</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_barang_tersedia_detail" tabindex="-1"
        aria-labelledby="modal_barang_tersediaLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_barang_tersediaLabel">Detail Pengajuan Barang Tersedia</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-body">
                            <p>Barang yang sudah diajukan tidak dapat diubah, hanya dapat dihapus sebelum di sahkan oleh KA
                                Div Logistik, apabila sudah disahkan barang tidak dapat dihapus, hanya dapat dikembalikan di
                                menu pengembalian barang, barang yang memiliki status ditolak maka tidak bisa masuk ke
                                daftar barang bawaan kegiatan</p>
                        </div>
                    </div>

                    <div class="card mt-2">
                        <div class="card-header">
                            Barang Yang Diajukan
                        </div>
                        <div class="card-body table-responsive">
                            <table class="table" id="table_barang_diajukan" style="font-size: 12px">
                                <thead>
                                    <th>Code</th>
                                    <th>Nama</th>
                                    <th>Jenis</th>
                                    <th>Detail</th>
                                    <th>Stok</th>
                                    <th>Keterangan</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>

                </div>
            </div>
        </div>
    </div>






    @push('scripts')
        <script>
            var id_kegiatan = "{{ $baksos['id'] }}";
            console.log(id_kegiatan);
        </script>
        <script src="/portal/js/config_baksos_pengadaan.js"></script>
    @endpush
@endsection

@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Isi RAB -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="buat_rab">
                    <i class="bx bx-calculator menu-icons"></i> <!-- Ikon kalkulator -->
                    <div class="menu-title">Buat RAB</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pengajuan Pencairan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="pengajuan_pencairan">
                    <i class="bx bx-money menu-icons"></i> <!-- Ikon uang -->
                    <div class="menu-title">Pengajuan Pencairan</div>
                </div>
            </div>

            <!-- Card Menu 3 - Laporan -->

        </div>




        <div class="mt-3">
            <div class="card">
                <div class="card-header">
                    Rencana Anggaran Biaya
                </div>
                <div class="card-body table-responsive">
                    <table class="table table-stripped" id="table_rab">
                        <thead>
                            <th>No</th>
                            <th>Code</th>
                            <th>Pembuatan</th>
                            <th>Judul</th>
                            <th>Biaya</th>
                            <th>Status</th>
                            <th>Aksi</th>
                        </thead>
                    </table>

                </div>
            </div>
            <div class="card mt-2">
                <div class="card-header">
                    Riwayat Pencairan
                </div>
                <div class="card-body table-responsive">

                </div>
            </div>

        </div>


    </div>
    <div class="modal fade" id="modal_isi_rab" tabindex="-1" aria-labelledby="modalIsiRABLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <div class="modal-header">
                    <input type="hidden" id="id_rab" name="id_rab">
                    <h5 class="modal-title" id="modalIsiRABLabel">Isi RAB</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-header">
                            Formulir RAB
                        </div>
                        <div class="card-body">

                            <!-- Input Anggaran -->
                            <div class="mb-3">
                                <label for="anggaran" class="form-label">Anggaran</label>
                                <textarea class="form-control" id="anggaran" name="anggaran" rows="3" placeholder="Masukkan anggaran"></textarea>
                            </div>

                            <!-- Input Biaya -->
                            <div class="mb-3">
                                <label for="biaya" class="form-label">Biaya</label>
                                <input type="text" class="form-control" id="biaya" name="biaya"
                                    placeholder="Masukkan biaya">
                            </div>

                            <!-- Tombol Simpan -->
                            <button class="btn btn-primary" id="simpanRab">Simpan</button>


                        </div>
                    </div>
                    <div class="card mt-2">
                        <div class="card-body table-responsive">
                            <table class="table table-stripped" id="table_rencana_rab">
                                <thead>
                                    <th>No</th>
                                    <th>Anggaran</th>
                                    <th>Biaya</th>
                                    <th>status</th>
                                    <th>Aksi</th>
                                </thead>
                            </table>

                        </div>
                        <div class="card mt-2">
                            <div class="card-header">
                                Resume RAB
                            </div>
                            <div class="card-body">
                                <div id="div_rencana_rab_baksos"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary" id="fix_rab">Simpan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_konfirmasi" tabindex="-1" aria-labelledby="modalKonfirmasiLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalKonfirmasiLabel">Konfirmasi Pengajuan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="pengajuan" name="pengajuan" value="N">
                        <label class="form-check-label" for="pengajuan">Ajukan Pencairan Dana</label>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Batal</button>
                    <button type="button" class="btn btn-primary" id="lanjutkan_pengajuan">Lanjutkan</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_detail_rab" tabindex="-1" aria-labelledby="modal_detail_rabLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal_detail_rabLabel">Detail RAB</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Card Structure with Form -->
                    <div class="card">
                        <div class="card-header" id="header" style="text-align: center">
                            
                        </div>
                        <div class="card-body">
                            <p class="mt-2">Informasi RAB</p>
                           
                                <div class="form-group">
                                    <label for="anggaranedit">Anggaran</label>
                                    <input type="text" id="anggaranedit" class="form-control" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="biayaedit">Biaya</label>
                                    <input type="text" id="biayaedit" class="form-control" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="statusedit">Status</label>
                                    <div id="statusedit" class="badge badge-pill"></div>
                                </div>
                                <button type="button" class="btn btn-primary" id="edit-btn">Edit</button>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    @push('scripts')
        {{-- <script src="/portal/js/peserta_webinar.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
        <script>
            var id_kegiatan = "{{ $baksos['id_kegiatan'] }}"
            console.log(id_kegiatan)
            var id_baksos = "{{ $baksos['id'] }}";
        </script>
        <script src="/portal/js/baksos_config_rab.js"></script>
    @endpush
@endsection
 
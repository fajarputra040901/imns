@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - RAB -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="rab">
                    <i class="bx bx-calculator menu-icons"></i> <!-- Ikon kalkulator -->
                    <div class="menu-title">RAB</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pengadaan Barang -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="pengadaan">
                    <i class="bx bx-cart-alt menu-icons"></i> <!-- Ikon keranjang belanja -->
                    <div class="menu-title">Pengadaan Barang</div>
                </div>
            </div>

            <!-- Card Menu 3 - Laporan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="">
                    <i class="bx bx-file menu-icons"></i> <!-- Ikon file -->
                    <div class="menu-title">Laporan</div>
                </div>
            </div>

            <!-- Card Menu 4 - Dokumentasi -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="">
                    <i class="bx bx-camera menu-icons"></i> <!-- Ikon kamera -->
                    <div class="menu-title">Dokumentasi</div>
                </div>
            </div>
        </div>




        <div class="mt-3">

            <div class="card mt-2">
                <div class="card-body ">
                    <div class="nav-align-top mt-2">
                        <ul class="nav nav-tabs" role="tablist" style="overflow-x: auto; white-space: nowrap;">
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link active" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-konfigurasi" aria-controls="navs-konfigurasi"
                                    aria-selected="true">Konfigurasi</button>
                            </li>
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-pendaftaran" aria-controls="navs-pendaftaran"
                                    aria-selected="false">Volunteer</button>
                            </li>
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-biaya" aria-controls="navs-biaya"
                                    aria-selected="false">Provider</button>
                            </li>
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-sertifikat" aria-controls="navs-sertifikat"
                                    aria-selected="false">Open Donasi</button>
                            </li>
                            <li class="nav-item" style="display: inline-block;">
                                <button type="button" class="nav-link" role="tab" data-bs-toggle="tab"
                                    data-bs-target="#navs-sertifikat" aria-controls="navs-sertifikat"
                                    aria-selected="false">Artikel</button>
                            </li>


                        </ul>

                        <div class="tab-content">

                            <div class="tab-pane fade show active" id="navs-konfigurasi" role="tabpanel">
                                <form action="" style="font-size: 12px" id="formedit" enctype="multipart/form-data">

                                    <div class="mb-3 row">
                                        <label for="idkegiatan" class="col-sm-2 col-form-label">Nomor
                                            Kegiatan</label>
                                        <div class="col-sm-8">
                                            <input type="text" readonly class="form-control" id="idkegiatan">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="tanggaledit" class="col-sm-2 col-form-label">Tanggal
                                            Kegiatan</label>
                                        <div class="col-sm-8">

                                            <input type="text" autocomplete="off" data-toggle="tanggaledit"
                                                class="form-control" id="tanggaledit" placeholder="DD-MM-YYYY">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="namaedit" class="col-sm-2 col-form-label">Nama
                                            Kegiatan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="namaedit">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="tempatedit" class="col-sm-2 col-form-label">Tempat
                                            Kegiatan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="tempatedit">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="desaedit" class="col-sm-2 col-form-label">Desa</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="desaedit">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="kabedit" class="col-sm-2 col-form-label">Kabupaten</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="kabedit">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="provedit" class="col-sm-2 col-form-label">Provinsi</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="provedit">
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="deskripsiedit" class="col-sm-2 col-form-label">Deskripsi
                                            Kegiatan</label>
                                        <div class="col-sm-8">
                                            <textarea class="form-control" id="deskripsiedit" rows="3"></textarea>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="coveredit" class="col-sm-2 col-form-label">Cover
                                            Kegiatan</label><br>
                                        <div class="col-sm-8">
                                            <input type="file" name="coveredit" id="coveredit" class="form-control">
                                            <div class="invalid-feedback">Field ini harus diisi.</div>
                                            <span>Untuk Cover Gunakan Resolusi 16:9</span>
                                        </div>
                                    </div>


                                    <div id="image_preview_edit" class="mb-3"></div>
                                    <div class="mb-3 row">
                                        <label for="jenis_kegiatanedit" class="col-sm-2 col-form-label">Jenis
                                            Kegiatan</label>
                                        <div class="col-sm-8">
                                            <select name="jenis_kegiatanedit" id="jenis_kegiatanedit"
                                                class="form-control">
                                                <option value="">Pilih Jenis Kegiatan</option>
                                                <option value="Gratis">Gratis</option>
                                                <option value="Berbayar">Berbayar</option>
                                            </select>
                                            <div class="invalid-feedback">Field ini harus diisi.</div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row" style="display: none" id="div_biayaedit">
                                        <label for="biayaedit" class="col-sm-2 col-form-label">Biaya Kegiatan</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="biayaedit">
                                            <div class="invalid-feedback">Field ini harus diisi.</div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row align-items-center">
                                        <label for="pesertaedit" class="col-sm-2 col-form-label">Kuota Peserta</label>
                                        <div class="col-sm-6">
                                            <input type="number" class="form-control" id="pesertaedit">
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="tanpaBatasanedit">
                                                <label class="form-check-label" for="tanpaBatasanedit">
                                                    Tanpa Batasan
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <label for="batasan_usia" class="col-sm-2 col-form-label">Batasan Usia</label>
                                        <div class="col-sm-10">
                                            <div class="row mb-2">
                                                <label for="min_usiaedit" class="col-sm-2 col-form-label">Min
                                                    Usia</label>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="min_usiaedit">
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="tanpaMinimaledit">
                                                        <label class="form-check-label" for="tanpaMinimaledit">
                                                            Tanpa Minimal
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-2">
                                                <label for="max_usiaedit" class="col-sm-2 col-form-label">Max
                                                    Usia</label>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="max_usiaedit">
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="tanpaMaksimaledit">
                                                        <label class="form-check-label" for="tanpaMaksimaledit">
                                                            Tanpa Maksimal
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="tanpaBatasanUsiaedit">
                                                        <label class="form-check-label" for="tanpaBatasanUsiaedit">
                                                            Tanpa Batasan Usia
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                                <button type="button" id="edit" class="btn btn-success">Update</button>


                            </div>
                            <div class="tab-pane fade show " id="navs-pendaftaran" role="tabpanel">

                            </div>
                            <div class="tab-pane fade show " id="navs-biaya" role="tabpanel">

                            </div>






                        </div>

                    </div>

                </div>
            </div>
        </div>


    </div>


    @push('scripts')
        {{-- <script src="/portal/js/peserta_webinar.js"></script> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>
        <script>
            var id_kegiatan = "{{ $baksos['id_kegiatan'] }}"
            console.log(id_kegiatan)
            var id = "{{ $baksos['id'] }}";
        </script>
        <script src="/portal/js/baksos_config.js"></script>
    @endpush
@endsection

@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Info Keuangan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="tambah_baksos">
                    <i class="bx bx-wallet menu-icons"></i> <!-- Ikon dompet -->
                    <div class="menu-title">Tambah Baksos</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pencarian -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="pencarianCard">
                    <i class="bx bx-search-alt-2 menu-icons"></i> <!-- Ikon pencarian alternatif -->
                    <div class="menu-title">Pencarian</div>
                </div>
            </div>

            <!-- Card Menu 3 - Pengajuan Pencairan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="">
                    <i class="bx bx-envelope-open menu-icons"></i> <!-- Ikon surat terbuka -->
                    <div class="menu-title">Jumlah Baksos</div>

                </div>
            </div>
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="">
                    <i class="bx bx-envelope-open menu-icons"></i> <!-- Ikon surat terbuka -->
                    <div class="menu-title">Reload</div>
                </div>
            </div>

            <!-- Card Menu 4 - Export Excel -->

        </div>



        <div class="mt-3">

            <div class="card mt-2">
                <div class="card-body table-responsive">

                    <table class="table table-hover" id="table" style="width:100%;font-size:14px">
                        <thead>
                            <tr>
                                <th style="width: 5%">No</th>
                                <th style="width: 5%">ID</th>
                                <th style="width: 10%">Tanggal</th>
                                <th style="width: 10%">Nama Kegiatan</th>
                                <th style="width: 15%">Tempat</th>
                                <th style="width: 15%">Region</th>
                                <th style="width: 10%">Jenis Kegiatan</th>
                                <th style="width: 10%">Aksi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>


    </div>


    @push('scripts')
        {{-- <script src="/portal/js/peserta_webinar.js"></script> --}}

        <script src="/portal/js/baksos_index.js"></script>
    @endpush
@endsection

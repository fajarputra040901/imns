<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Certificate Editor</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css">
    <style>
        #certificate-container {
            position: relative;
            width: 100%;
            height: 600px;
            border: 1px solid #ddd;
            background-color: lightgray; /* Placeholder for certificate background */
        }

        .text-box {
            position: absolute;
            padding: 10px;
            resize: both;
            overflow: auto;
            border: 1px solid black;
            background-color: rgba(255, 255, 255, 0.5);
            font-family: Arial, sans-serif;
            color: black;
            box-sizing: border-box;
        }

        .draggable {
            cursor: move;
        }

        .remove-btn {
            position: absolute;
            top: 0;
            right: 0;
            background-color: red;
            color: white;
            border: none;
            padding: 5px;
            cursor: pointer;
            font-size: 14px;
        }

        .panel {
            margin-top: 20px;
        }

        .upload-container {
            margin-bottom: 15px;
        }

        #upload-btn {
            margin-top: 15px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h3>Certificate Editor</h3>

                <!-- Upload Template Certificate -->
                <div class="upload-container">
                    <input type="file" id="upload-image" class="form-control" accept="image/*">
                </div>

                <button id="add-text-box" class="btn btn-primary mb-3">Add Text Box</button>

                <div id="settings-panel" class="panel" style="display: none;">
                    <h5>Text Box Settings</h5>
                    <label for="text-content">Text:</label>
                    <input type="text" id="text-content" class="form-control mb-3">
                    <label for="font-family">Font:</label>
                    <select id="font-family" class="form-control mb-3">
                        <option value="Arial">Arial</option>
                        <option value="Courier New">Courier New</option>
                        <option value="Verdana">Verdana</option>
                    </select>
                    <label for="font-size">Size:</label>
                    <input type="number" id="font-size" class="form-control mb-3" value="20">
                    <label for="font-color">Font Color:</label>
                    <input type="color" id="font-color" class="form-control mb-3" value="#000000">
                    <label for="text-align">Horizontal Align:</label>
                    <select id="text-align" class="form-control mb-3">
                        <option value="left">Left</option>
                        <option value="center">Center</option>
                        <option value="right">Right</option>
                    </select>
                    <label for="vertical-align">Vertical Align:</label>
                    <select id="vertical-align" class="form-control mb-3">
                        <option value="top">Top</option>
                        <option value="middle">Middle</option>
                        <option value="bottom">Bottom</option>
                    </select>
                    <label for="background-color">Background Color:</label>
                    <input type="color" id="background-color" class="form-control mb-3" value="#ffffff">
                    <label for="border-toggle">Border:</label>
                    <input type="checkbox" id="border-toggle">
                    <button id="apply-settings" class="btn btn-success mt-3">Apply Settings</button>
                </div>
            </div>

            <div class="col-md-8">
                <h3>Preview</h3>
                <div id="certificate-container">
                    <!-- Sertifikat Template (gambar) akan dimuat di sini -->
                </div>
                <textarea id="config_all" class="form-control mt-3" rows="10" readonly></textarea>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/interactjs@1.9.20/dist/interact.min.js"></script>
    <script>
        let configData = [];
        let selectedTextBox = null;

        // Menambahkan kotak teks baru
        document.getElementById('add-text-box').addEventListener('click', function() {
            const textBoxId = 'text-box-' + Math.random().toString(36).substr(2, 9); // ID acak untuk setiap kotak teks

            // Membuat kotak teks baru
            const textBoxDiv = document.createElement('div');
            textBoxDiv.classList.add('text-box');
            textBoxDiv.classList.add('draggable');
            textBoxDiv.id = textBoxId;
            textBoxDiv.style.left = '50px';
            textBoxDiv.style.top = '50px';
            textBoxDiv.style.width = '200px';
            textBoxDiv.style.height = '50px';
            textBoxDiv.textContent = 'Enter text here';

            // Menambahkan tombol hapus
            const removeButton = document.createElement('button');
            removeButton.classList.add('remove-btn');
            removeButton.textContent = 'X';
            removeButton.onclick = function() {
                textBoxDiv.remove();
                const index = configData.findIndex(b => b.id === textBoxId);
                if (index > -1) configData.splice(index, 1); // Menghapus data konfigurasi
                updateConfigJson();
            };
            textBoxDiv.appendChild(removeButton);

            // Menambahkan kotak teks ke dalam container sertifikat
            document.getElementById('certificate-container').appendChild(textBoxDiv);

            // Menambahkan data konfigurasi kotak teks ke array configData
            const boxData = {
                id: textBoxId,
                label: 'Enter text here',
                font: '20px Arial',
                color: '#000000',
                textAlign: 'left',
                verticalAlign: 'top',
                backgroundColor: '#ffffff',
                border: false,
            };
            configData.push(boxData);

            // Menambahkan interaktivitas drag dan resize
            interact(`#${textBoxId}`)
                .draggable({
                    onmove(event) {
                        textBoxDiv.style.left = (parseFloat(textBoxDiv.style.left) + event.dx) + 'px';
                        textBoxDiv.style.top = (parseFloat(textBoxDiv.style.top) + event.dy) + 'px';
                    }
                })
                .resizable({
                    edges: { left: true, right: true, bottom: true, top: true },
                    onmove(event) {
                        textBoxDiv.style.width = event.rect.width + 'px';
                        textBoxDiv.style.height = event.rect.height + 'px';
                    }
                });

            // Menampilkan panel pengaturan
            document.getElementById('settings-panel').style.display = 'block';
            selectedTextBox = textBoxDiv;
        });

        // Terapkan pengaturan
        document.getElementById('apply-settings').addEventListener('click', function() {
            if (selectedTextBox) {
                const textContent = document.getElementById('text-content').value;
                const fontFamily = document.getElementById('font-family').value;
                const fontSize = document.getElementById('font-size').value;
                const fontColor = document.getElementById('font-color').value;
                const textAlign = document.getElementById('text-align').value;
                const verticalAlign = document.getElementById('vertical-align').value;
                const border = document.getElementById('border-toggle').checked;
                const backgroundColor = document.getElementById('background-color').value;

                selectedTextBox.style.fontFamily = fontFamily;
                selectedTextBox.style.fontSize = fontSize + 'px';
                selectedTextBox.style.color = fontColor;
                selectedTextBox.style.textAlign = textAlign;
                selectedTextBox.style.verticalAlign = verticalAlign;
                selectedTextBox.style.backgroundColor = backgroundColor;
                selectedTextBox.style.border = border ? '2px solid black' : 'none';
                selectedTextBox.textContent = textContent;

                // Update data konfigurasi
                const box = configData.find(b => b.id === selectedTextBox.id);
                if (box) {
                    box.label = textContent;
                    box.font = fontSize + 'px ' + fontFamily;
                    box.color = fontColor;
                    box.textAlign = textAlign;
                    box.verticalAlign = verticalAlign;
                    box.backgroundColor = backgroundColor;
                    box.border = border;
                }

                updateConfigJson();
            }
        });

        // Memperbarui JSON konfigurasi di textarea
        function updateConfigJson() {
            document.getElementById('config_all').value = JSON.stringify(configData, null, 2);
        }

        // Menangani upload gambar sertifikat
        document.getElementById('upload-image').addEventListener('change', function(event) {
            const file = event.target.files[0];
            if (file) {
                const reader = new FileReader();
                reader.onload = function(e) {
                    const img = new Image();
                    img.src = e.target.result;
                    img.onload = function() {
                        const certificateContainer = document.getElementById('certificate-container');
                        certificateContainer.style.backgroundImage = `url(${img.src})`;
                        certificateContainer.style.backgroundSize = 'cover';
                        certificateContainer.style.backgroundPosition = 'center';
                    };
                };
                reader.readAsDataURL(file);
            }
        });
    </script>
</body>
</html>

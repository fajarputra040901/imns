<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Pembayaran</title>
    <link rel="stylesheet" href="{{ asset('portal/') }}/style/loader.css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> <!-- Link ke jQuery -->
    <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js"
        data-client-key="SB-Mid-client-Y9aC7PgVbtnRWcK0"></script>
    {{-- <script type="text/javascript" src="https://app.midtrans.com/snap/snap.js"
        data-client-key="Mid-client-KepuzyjzCbw4njAa"></script> --}}
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@7.2.96/css/materialdesignicons.min.css" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script>




    <style>
        #invoice {
            display: none;
            /* Sembunyikan invoice */
        }

        .card {
            border: none;
            border-radius: 10px;
            box-shadow: 0 4px 20px rgba(0, 0, 0, 0.1);
            display: flex;
            flex-direction: column;
            height: 100%;
            /* Memastikan kartu mengisi tinggi penuh */
        }

        .btn-custom {
            background-color: #007bff;
            color: white;
        }

        .btn-custom:hover {
            background-color: #0056b3;
        }

        .card-body {
            flex-grow: 1;
            /* Membuat konten kartu mengisi ruang yang tersedia */
        }
    </style>
</head>

<body>
    <div id="preloader" style="display: none" class="preloader">

        <div class="loader"></div>

    </div>



    <div class="container mt-5">
        <div class="row d-flex align-items-stretch">
            <div class="col-md-6 mb-3"> <!-- Tambahkan mb-3 di sini untuk tampilan mobile -->
                <div class="card p-4 h-100"> <!-- Tambahkan h-100 untuk memastikan tinggi kartu sama -->
                    <div class="card-body">
                        <h2 class="h5">Informasi Pendaftaran</h2>
                        <table class="table">

                            <tbody>
                                @php
                                    $form_config = json_decode($webinar->form, true);
                                    $formData = json_decode($data->form, true);

                                    // Filter untuk mendapatkan field dengan primary: true
                                    $primaryFields = array_filter($form_config, function ($field) {
                                        return isset($field['primary']) && $field['primary'] === 'true';
                                    });

                                    // Filter untuk mendapatkan field tanpa primary: true
                                    $nonPrimaryFields = array_filter($form_config, function ($field) {
                                        return !isset($field['primary']) || $field['primary'] !== 'true';
                                    });

                                    // Membuat nilai dengan sensor
                                    function sensorValue($value)
                                    {
                                        $length = strlen($value);
                                        if ($length <= 4) {
                                            return str_repeat('*', $length); // Semua disensor jika panjang <= 4
                                        }
                                        return substr($value, 0, 2) . str_repeat('*', $length - 4) . substr($value, -2); // Sensor tengah
                                    }
                                @endphp


                                @foreach ($primaryFields as $field)
                                    @php
                                        $name = $field['name'];
                                        $label = $field['label'];
                                        $value = isset($formData[$name]) ? $formData[$name] : null;
                                    @endphp
                                    @if ($value)
                                        <tr>
                                            <td>{{ $label }}</td>
                                            <td>{{ sensorValue($value) }}</td>
                                        </tr>
                                    @endif
                                @endforeach

                                <!-- Menampilkan field tanpa primary: true -->
                                @foreach ($nonPrimaryFields as $field)
                                    @php
                                        $name = $field['name'];
                                        $label = $field['label'];
                                        $value = isset($formData[$name]) ? $formData[$name] : null;
                                    @endphp
                                    @if ($value)
                                        <tr>
                                            <td>{{ $label }}</td>
                                            <td>{{ $value }}</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>

                        <hr>
                        <p>Disarankan untuk menggunakan Qris dalam metode pembayaran untuk menghindari biaya admin</p>
                        <ul style="text-align: justify">
                            <li>
                                <strong>Periksa kembali rincian pembayaran</strong> untuk memastikan total jumlah yang
                                tertera sesuai dengan pembayaran Anda, baik menggunakan <em>QRIS</em> maupun <em>Virtual
                                    Account</em>.
                            </li>
                            <li>
                                Setelah transaksi <strong>berhasil</strong>, status pembayaran akan berubah menjadi
                                <strong>Sudah Dibayar</strong>. Jangan lupa untuk kembali ke halaman ini dan melakukan
                                <strong>refresh</strong> untuk memperbarui status pembayaran.
                            </li>
                            <li>
                                Jika pembayaran telah selesai, <strong>unduh invoice</strong> di bagian bawah rincian
                                pembayaran, dan pastikan Anda <strong>bergabung dengan grup WhatsApp</strong> yang
                                tersedia setelah menyelesaikan pembayaran.
                            </li>
                            <li>
                                Jika mengalami kesulitan atau masalah, segera <strong>hubungi Contact Person</strong>
                                yang tersedia. Harap lampirkan <strong>nama</strong> dan <strong>email</strong> yang
                                telah Anda daftarkan untuk mempermudah proses bantuan.
                            </li>
                            <li>
                                Jika terdapat kesalahan pada data identitas yang diisi, tidak perlu khawatir. Setelah
                                pembayaran selesai, <strong>laporkan</strong> kepada Contact Person dengan melampirkan
                                <strong>nama</strong>, <strong>email</strong>, dan <strong>data yang perlu
                                    diperbarui</strong>.
                            </li>
                            <li>
                                <strong>Jika metode pembayaran yang dipilih adalah Other Bank / Bank Lainnya, cara
                                    membayarnya:</strong>
                                <ul>
                                    <li>Buka aplikasi <strong>m-banking</strong> bank Anda di smartphone.</li>
                                    <li>Masukkan <strong>username</strong> dan <strong>password</strong>.</li>
                                    <li>Pilih menu <strong>Transfer </strong>.</li>
                                    <li>Masukkan <strong>Kode Bank 009</strong> atau pilih <strong>Bank Negara
                                            Indonesia (BNI)</strong>, kemudian masukkan <strong>Nomor Virtual
                                            Account</strong> yang sudah didapatkan.</li>
                                    <li>Pastikan <strong>nama penerima</strong> adalah <strong>Midtrans - (Nama yang
                                            didaftarkan dari formulir)</strong>.</li>
                                    <li>Masukkan <strong>nominal</strong> yang harus dibayarkan (samakan dengan jumlah
                                        yang tertera di rincian pembayaran).</li>
                                    <li>Layar akan menunjukkan <strong>konfirmasi</strong>. Apabila telah sesuai, pilih
                                        <strong>Lanjutkan</strong> / <strong>Kirim</strong>.
                                    </li>
                                    <li>Masukkan <strong>PIN transaksi</strong>.</li>
                                    <li>Selesai.</li>

                                </ul>
                            </li>
                            <li>
                                <strong>Penting:</strong> Untuk sementara, pembayaran <span style="color: red;">via BCA
                                    Mobile</span> <strong><i><u>Tidak dapat dilakukan</u></i></strong>.
                                Harap <span>memilih metode pembayaran lain</span> atau menggunakan <span>bank
                                    lain</span> saat melakukan transaksi.
                                Terima kasih atas pengertiannya.
                            </li>
                        </ul>






                    </div>
                </div>
            </div>
            <div class="col-md-6 mb-3">
                <div class="card p-4 h-100"> <!-- Tambahkan h-100 di sini juga -->
                    <div class="card-body">
                        <div class="py-2">
                            <h5 class="font-size-15">Rincian Pembayaran</h5>
                            <h6>No Transaksi : {{ $data->id_transaksi }} </h6>

                            <div class="table-responsive">
                                <table class="table align-middle table-nowrap table-centered mb-0">
                                    <thead>
                                        <tr>
                                            <th>Deskripsi</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{{ $paket['namapaket'] }}</td>
                                            <td> Rp {{ number_format($data->harga, 0, ',', '.') }} </td>
                                        </tr>
                                        @if (!is_null($data->donasi) && $data->donasi !== '')
                                            <tr>
                                                <td>Donasi</td>
                                                <td>Rp {{ number_format($data->donasi, 0, ',', '.') }}</td>
                                            </tr>
                                        @endif

                                        <tr>
                                            <td>Biaya Layanan</td>
                                            <td>Rp 2.000</td>
                                        </tr>
                                        <tr>
                                            <th>Total</th>
                                            <td class="fw-bold text-primary"> Rp
                                                {{ number_format($data->jumlah, 0, ',', '.') }} </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Status
                                            </td>
                                            <td>
                                                <span id="status"></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="text-end mt-4" id="action">



                        </div>
                        <div class="mt-2" style="display: none" id="grup">
                            <p>Terima kasih, pembayaran sudah di konfirmasi, berikut link grup whatsapp untuk
                                peserta. silahkan bergabung untuk mendapatkan informasi lebih lanjut</p>
                            @php
                                use App\Models\Webinar;

                                // Ambil data webinar berdasarkan webinar_id
                                $webinar = Webinar::where('id', $data->webinar_id)->first();

                                // Ambil link WhatsApp dari webinar
                                $whatsappLink = $webinar ? $webinar->link_wa : '#'; // Gunakan '#' jika link tidak ditemukan
                            @endphp

                            <a href="{{ $whatsappLink }}" target="_blank" class="btn btn-success d-block mb-2">
                                <i class="mdi mdi-whatsapp"></i> Masuk Grup Whatsapp
                            </a>
                        </div>
                        <p>Apabila terdapat kesulitan dapat menghubungi Contact person :</p>
                        <div style="display: inline-flex; align-items: center; gap: 10px;">
                            <i class="mdi mdi-headset mdi-24px" style="color: #007bff;"></i> <!-- Ikon Headset -->
                            <h2 class="h5 mb-0">Contact Person</h2>
                        </div>



                        <p class="mt-3">Butuh bantuan? Hubungi kami melalui WhatsApp:</p>
                        @php
                            // Decode JSON menjadi array
                            $contactPersons = json_decode($webinar->contact_person, true);
                            $adminCount = 1; // Counter untuk anonim admin
                        @endphp

                        @foreach ($contactPersons as $contact)
                            @php
                                // Ubah nomor telepon menjadi format 62
                                $phoneNumber = '62' . ltrim($contact['no_cp'], '0');
                            @endphp

                            @if ($contact['status'] === 'N')
                                <!-- Jika status N, tampilkan nama asli -->
                                <a href="https://wa.me/{{ $phoneNumber }}?text=Saya+butuh+bantuan+terkait+pembayaran"
                                    target="_blank" class="btn btn-success d-block mb-2">
                                    <i class="mdi mdi-whatsapp"></i> Hubungi {{ $contact['nama'] }}
                                </a>
                            @else
                                <!-- Jika status Y, tampilkan anonim -->
                                <a href="https://wa.me/{{ $phoneNumber }}?text=Saya+butuh+bantuan+terkait+pembayaran"
                                    target="_blank" class="btn btn-success d-block mb-2">
                                    <i class="mdi mdi-whatsapp"></i> Hubungi Admin {{ $adminCount }}
                                </a>
                                @php
                                    $adminCount++;
                                @endphp
                            @endif
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container mt-5" id="invoice">

        <div class="card">

            <div class="card-header text-center">

                <h4>Invoice</h4>

                <p>No Transaksi: {{ $data->id_transaksi }}</p>

            </div>

            <div class="card-body">

                <h5 class="card-title">Detail Transaksi</h5>

                <table class="table ">

                    <tbody>




                        @foreach ($primaryFields as $field)
                            @php
                                $name = $field['name'];
                                $label = $field['label'];
                                $value = isset($formData[$name]) ? $formData[$name] : null;
                            @endphp
                            @if ($value)
                                <tr>
                                    <td>{{ $label }}</td>
                                    <td>{{ sensorValue($value) }}</td>
                                </tr>
                            @endif
                        @endforeach

                        <!-- Menampilkan field tanpa primary: true -->
                        @foreach ($nonPrimaryFields as $field)
                            @php
                                $name = $field['name'];
                                $label = $field['label'];
                                $value = isset($formData[$name]) ? $formData[$name] : null;
                            @endphp
                            @if ($value)
                                <tr>
                                    <td>{{ $label }}</td>
                                    <td>{{ $value }}</td>
                                </tr>
                            @endif
                        @endforeach

                    </tbody>

                </table>


                <h5 class="card-title">Rincian Pembayaran</h5>

                <table class="table  align-middle table-nowrap table-centered mb-0">

                    <thead>

                        <tr>

                            <th>Deskripsi</th>

                            <th>Jumlah</th>

                        </tr>

                    </thead>

                    <tbody>

                        <tr>

                            <td>{{ $paket['namapaket'] }}</td>

                            <td>Rp {{ number_format($data->harga, 0, ',', '.') }}</td>

                        </tr>

                        @if (!is_null($data->donasi) && $data->donasi !== '')
                            <tr>

                                <td>Donasi</td>

                                <td>Rp {{ number_format($data->donasi, 0, ',', '.') }}</td>

                            </tr>
                        @endif

                        <tr>

                            <td>Biaya Layanan</td>

                            <td>Rp 2.000</td>

                        </tr>

                        <tr>

                            <th>Total</th>

                            <td class="fw-bold text-primary">Rp {{ number_format($data->jumlah, 0, ',', '.') }}</td>

                        </tr>

                        <tr>

                            <td>Status</td>

                            <td><span id="status_invoice"></span></td>

                        </tr>

                    </tbody>

                </table>

            </div>

            <div class="card-footer text-muted text-center">

                Terima kasih!

            </div>

        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/portal/js/loader.js"></script>

    <script>
        $(document).ready(function() {
            var id = "{{ $data->id_transaksi }}";

            getData(id)


            var snapToken = 0;


            function getData(id) {
                loader(true);
                $.ajax({
                    type: "GET", // Menentukan metode HTTP sebagai GET
                    url: "/get_tr/" + id, // URL tujuan, menambahkan id ke dalam URL
                    success: function(response) { // Callback jika sukses
                        loader(false);
                        console.log("Response:", response); // Menampilkan respons dari server
                        $('#action').empty();
                        // Menangani status berdasarkan nilai yang ada di response
                        var status = response.data.status; // Asumsikan status ada di dalam response
                        snapToken = response.data.snaptoken;
                        // Menentukan tindakan berdasarkan status
                        if (status === "unpaid") {
                            $('#status').text('Belum Dibayar').css('color',
                                'red'); // Update text dan warna
                            $('#action').html(
                                '<button class="btn btn-custom w-100" id="pay-button">Bayar</button>'
                            ); // Menambahkan tombol Bayar

                        } else if (status === "pending") {
                            $('#status').text('Belum Dibayar').css('color',
                                'red'); // Update text dan warna
                            $('#action').html(
                                '<button class="btn btn-custom w-100" id="pay-button">Cek Status Pembayaran</button>'
                            ); // Tombol disable
                        } else if (status === "expired") {
                            $('#status').text('Kadaluarsa').css('color',
                                'red'); // Update text dan warna
                            $('#action').html(
                                '<button class="btn btn-custom w-100" id="re-pay">Ulangi Pembayaran</button>'
                            ); // Tombol ulangi pembayaran
                            $('#re-pay').click(function() {
                                loader(true);
                                $.ajax({
                                    url: '/re_payment/' + id, // U RL dengan ID
                                    method: 'GET', // Atau 'POST' jika diperlukan
                                    success: function(response) {
                                        // loader(false);
                                        window.location.href = '/w/t/' +
                                            response.id; // Arahkan ke URL
                                    },
                                    error: function(xhr, status, error) {
                                        // Tangani error jika ada
                                        loader(false);
                                        alert(
                                            'Terjadi kesalahan saat mengulangi pembayaran'
                                        );
                                    }
                                });
                            });
                        } else if (status === "paid") {
                            $('#status').text('Sudah Dibayar').css('color',
                                'green'); // Update text dan warna
                            $('#status_invoice').text('Sudah Dibayar').css('color',
                                'green');
                            $('#grup').show();
                            $('#action').html(
                                '<button class="btn btn-custom w-100" id="invoice-button">Unduh Invoice</button>'
                            ); // Tombol unduh invoice
                            $('#invoice-button').click(function() {
                                var invoiceDiv = $('#invoice')[
                                    0]; // Mengambil elemen div dengan ID invoice
                                $(invoiceDiv).show();
                                // Menggunakan html2canvas untuk menangkap screenshot dari elemen div
                                html2canvas(invoiceDiv, {
                                    onrendered: function(canvas) {
                                        var link = document.createElement('a');
                                        link.href = canvas.toDataURL('image/png');
                                        link.download =
                                            'invoice.png'; // Nama file gambar
                                        link
                                            .click(); // Menyimulasikan klik untuk memulai unduhan
                                        $(invoiceDiv).hide();
                                    }

                                });
                            });
                        }
                    },
                    error: function(xhr, status, error) { // Callback jika ada error
                        loader(false);
                        console.error("Error:", error); // Menampilkan pesan error
                        alert("Terjadi kesalahan saat memuat data.");
                    }
                });
            }
            $(document).on('click', '#pay-button', function() {

                snap.pay(snapToken, {
                    onSuccess: function(result) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Pembayaran Berhasil',
                            text: 'Status: Berhasil',
                            confirmButtonText: 'Tutup'
                        });
                        getData(id);
                    },
                    onPending: function(result) {
                        Swal.fire({
                            icon: 'info',
                            title: 'Pembayaran Pending',
                            text: 'Status: Pending',
                            confirmButtonText: 'Tutup'
                        });
                        getData(id);
                    },
                    onError: function(result) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Pembayaran Gagal',
                            text: 'Status: Gagal',
                            confirmButtonText: 'Tutup'
                        });
                        getData(id);
                    }
                });
            });



            function loader(show) {
                if (show) {
                    $('#preloader').show(); // Menampilkan preloader
                } else {
                    $('#preloader').hide(); // Menyembunyikan preloader
                }
            }




        });
    </script>
    <script>
        // Ambil snapToken dari response backend
    </script>
</body>

</html>

@extends('partial.sneat')
@push('style')
    <style>
        /* CSS untuk card menu */
        .menu-card {
            border: 1px solid #ddd;
            background-color: #fff;
            border-radius: 10px;
            padding: 20px;
            text-align: center;
            transition: transform 0.3s ease;

        }

        .menu-card:hover {
            transform: scale(1.05);
            box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.1);
            cursor: pointer;
        }

        .menu-icons {
            font-size: 2.5rem;
            color: #007bff;
            transition: color 0.3s;
        }

        .menu-card:hover .menu-icons {
            color: #0056b3;
        }

        .menu-title {
            font-size: 1.2rem;
            margin-top: 10px;
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">
        <div class="row">
            <!-- Card Menu 1 - Info Keuangan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="info_peserta">
                    <i class="bx bx-wallet menu-icons"></i> <!-- Ikon dompet -->
                    <div class="menu-title">Info Keuangan</div>
                </div>
            </div>

            <!-- Card Menu 2 - Pencarian -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="pencarianCard">
                    <i class="bx bx-search-alt-2 menu-icons"></i> <!-- Ikon pencarian alternatif -->
                    <div class="menu-title">Pencarian</div>
                </div>
            </div>

            <!-- Card Menu 3 - Pengajuan Pencairan -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="kirimPesanCard">
                    <i class="bx bx-envelope-open menu-icons"></i> <!-- Ikon surat terbuka -->
                    <div class="menu-title">Pengajuan Pencairan</div>
                </div>
            </div>

            <!-- Card Menu 4 - Export Excel -->
            <div class="col-md-3 mb-4">
                <div class="menu-card" id="exportExcelCard">
                    <i class="bx bx-spreadsheet menu-icons"></i> <!-- Ikon spreadsheet -->
                    <div class="menu-title">Export Excel</div>
                </div>
            </div>
        </div>



        <div class="mt-3">
            <div class="card">
                <div class="card-body">
                    Riwayat Pencairan
                </div>
            </div>
            <div class="card mt-2">
                <div class="card-body table-responsive">

                    <table class="table table-striped" id="table" style="width: 100%">
                        <thead>
                            <tr>
                                <th>#</th>

                                <th>Tanggal Pengajuan</th>
                                <th>Tanggal Validasi</th>
                                <th>Jumlah</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <!-- Data akan di-load oleh DataTables -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>


    </div>
    <div class="modal fade" id="info_keuangan_modal" tabindex="-1" aria-labelledby="infoKeuanganModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="infoKeuanganModalLabel">Info Keuangan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Konten dari AJAX akan diisi di sini -->
                    <div id="infonya"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="pengajuan_penarikan_modal" tabindex="-1" aria-labelledby="pengajuanPenarikanLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="pengajuanPenarikanLabel">Pengajuan Pencairan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="form_pengajuan_penarikan">
                        <div class="mb-3">
                            <label for="jumlah" class="form-label">Jumlah Penarikan</label>
                            <input type="text" id="jumlah" name="jumlah" class="form-control"
                                placeholder="Masukkan jumlah">
                        </div>
                        <div class="mb-3">
                            <label for="keterangan" class="form-label">Keterangan</label>
                            <textarea id="keterangan" name="keterangan" class="form-control" rows="3" placeholder="Masukkan keterangan"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary">Ajukan</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal_detail_pengajuan" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel">Detail Pengajuan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Konten detail pengajuan akan dimuat di sini -->
                    <div id="detailPengajuanContent" class="table-responsive"></div>
                    <div id="konfigurasi">
                        <form action="" id="form_validasi"></form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @push('scripts')
        {{-- <script src="/portal/js/peserta_webinar.js"></script> --}}
        <script>
            var id = {{ $data->id }};
        </script>
        <script src="/portal/js/keuangan_webinar.js"></script>
    @endpush
@endsection

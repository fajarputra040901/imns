@extends('partial.sneat')
@push('style')
    <style>
        .webinar-card {
            border: 1px solid #ddd;
            /* Menambahkan border ringan di sekeliling card */
            border-radius: 8px;
            /* Membuat sudut card lebih membulat */
            box-shadow: 0 2px 8px rgba(0, 0, 0, 0.1);
            /* Efek bayangan ringan untuk card */
            transition: transform 0.3s ease, box-shadow 0.3s ease;
        }

        .webinar-card:hover {
            transform: translateY(-5px);
            /* Efek hover: card sedikit terangkat */
            box-shadow: 0 4px 12px rgba(0, 0, 0, 0.15);
            /* Menambah bayangan lebih besar saat hover */
        }

        .webinar-card-body {
            padding: 16px;
        }

        .webinar-card-title {
            font-size: 20px;
            font-weight: bold;
            color: #333;
        }

        .webinar-card-text {
            font-size: 14px;
            color: #777;
        }

        .text-muted {
            color: #999 !important;
        }

        .webinar-card-header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            /* Vertically center the content */
        }

        .webinar-card-header p {
            margin-bottom: 0.5rem;
        }

        .webinar-card-icon {
            display: flex;
            /* Horizontally center the icon */
        }
    </style>
@endpush
@section('content')
    <div class="container-xxl flex-grow-1 container-p-y">

        <div class="card mt-2">
            <div class="card-body">
                <div class="d-flex flex-wrap align-items-center gap-2">
                    <!-- Tombol Tambah -->
                    <button class="btn btn-sm btn-primary" id="btn_tambah_webinar">
                        <i class='bx bx-plus'></i> Tambah Event
                    </button>
                    <!-- Input Pencarian -->
                    <div class="flex-grow-1">
                        <div class="input-group">
                            <span class="input-group-text" id="search-icon">
                                <i class='bx bx-search'></i>
                            </span>
                            <input type="text" id="pencarian_webinar" class="form-control"
                                placeholder="Cari Webinar / Seminar ..." aria-label="Cari" aria-describedby="search-icon">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="list_webinar" class="mt-2">
            <!-- Daftar webinar akan dimuat di sini -->
        </div>

        <div id="pagination" class="mt-3">
            <!-- Pagination controls (Next, Previous) akan muncul di sini -->
        </div>
        <div class="modal fade" id="modal_tambah_webinar" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
            aria-labelledby="staticBackdropLabel" aria-hidden="true">

            <div class="modal-dialog modal-dialog-scrollable " style="max-width: 90%;font-size:12px">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalTambahWebinarLabel">Tambah Webinar</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form id="form_tambah_webinar">
                            <!-- Tanggal Pelaksanaan -->
                            <div class="row mb-3">
                                <label class="col-md-3 col-form-label">Tanggal Pelaksanaan</label>
                                <div class="col-md-4 mt-1">
                                    <input type="text" class="form-control" name="tanggal_mulai"
                                        placeholder="Tanggal Mulai">
                                </div>
                                <div class="col-md-4 mt-1">
                                    <input type="text" class="form-control" name="tanggal_selesai"
                                        placeholder="Tanggal Selesai">
                                </div>
                            </div>


                            <!-- Judul -->
                            <div class="row mb-3">
                                <label class="col-md-3 col-form-label">Judul</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="judul" placeholder="Judul Webinar">
                                </div>
                            </div>

                            <!-- Jenis Webinar -->
                            <div class="row mb-3">
                                <label class="col-md-3 col-form-label">Jenis Webinar</label>
                                <div class="col-md-9">
                                    <select class="form-select" name="jenis">
                                        <option value="Online">Online</option>
                                        <option value="Klasikal / Offline">Klasikal / Offline</option>
                                        <option value="Hybrid">Hybrid</option>
                                    </select>
                                </div>
                            </div>

                            <!-- Jumlah Peserta -->
                            <div class="row mb-3">
                                <label class="col-md-3 col-form-label">Jumlah Peserta</label>
                                <div class="col-md-9">
                                    <input type="number" class="form-control" name="peserta" placeholder="Jumlah Peserta">
                                </div>
                            </div>

                            <!-- Jumlah SKP -->
                            <div class="row mb-3">
                                <label class="col-md-3 col-form-label">Jumlah SKP</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="skp" placeholder="Jumlah SKP">
                                    <span class="text-muted fst-italic">Masukan Sedang Diproses jika SKP belum terbit</span>
                                </div>
                            </div>

                            <!-- Deskripsi -->
                            <div class="mb-3">
                                <label class="form-label">Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="deskripsi"></textarea>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary me-2" data-bs-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary" form="form_tambah_webinar">Simpan</button>
                    </div>

                </div>
            </div>
        </div>

    </div>
    @push('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdn.ckeditor.com/4.22.1/standard/ckeditor.js"></script>


        <script src="/portal/js/webinar.js"></script>
    @endpush
@endsection

@extends('partial.index')
@push('meta')
    {{-- <meta property="og:title" content="{{ $data->judul }}" />
    <meta property="og:description" content="{{ $summary }}" />
    <meta property="og:image" content="{{ $mainImageSrc }}" />
    <meta property="og:url" content="{{ url()->current() }}" />
    <meta property="og:type" content="article" />

    <meta name="twitter:card" content="{{ $mainImageSrc }}">
    <meta name="twitter:title" content="{{ $data->judul }}">
    <meta name="twitter:description" content="{{ $summary }}">
    <meta name="twitter:image" content="{{ $mainImageSrc }}"> --}}
    <script type="text/javascript" src="https://app.midtrans.com/snap/snap.js"
        data-client-key="Mid-client-KepuzyjzCbw4njAa"></script>
@endpush
@push('style')
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@7.2.96/css/materialdesignicons.min.css" rel="stylesheet">

    <style>
        @media (min-width: 768px) {
            .carousel .content img {
                height: auto;
                max-height: 100%;
                width: 100%;
                object-fit: contain;

            }
        }


        @media (max-width: 767px) {
            .carousel .content img {
                height: 300px;
                max-width: none;
                width: 100%;
                object-fit: cover;
            }
        }

        .slider {
            width: 100%;
            max-width: 600px;
            /* Atur lebar sesuai dengan kebutuhan */
            margin: 0 auto;
            /* Agar berada di tengah */
        }

        /* CSS untuk gambar slider */
        .slider-image {
            width: 100%;
            height: auto;
            /* Mengatur agar gambar responsive */
        }

        .donation-container {
            max-width: 800px;
            margin: 0 auto;
            background: white;
            padding: 30px;
            border-radius: 15px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
        }

        .form-header {
            text-align: center;
            color: #2c3e50;
            margin-bottom: 30px;
        }

        .form-header h1 {
            font-size: 2.5em;
            margin-bottom: 10px;
        }

        .form-header p {
            color: #7f8c8d;
        }

        .input-group {
            margin-bottom: 25px;
            position: relative;
        }

        .input-group label {
            display: block;
            margin-bottom: 8px;
            color: #2c3e50;
            font-weight: 500;
        }

        .input-group input,
        .input-group textarea {
            width: 100%;
            padding: 12px;
            border: 2px solid #e0e0e0;
            border-radius: 8px;
            font-size: 16px;
            transition: all 0.3s ease;
        }

        .input-group input:focus,
        .input-group textarea:focus {
            border-color: #3498db;
            box-shadow: 0 0 0 3px rgba(52, 152, 219, 0.2);
            outline: none;
        }

        .input-group i {
            position: absolute;
            right: 15px;
            top: 45px;
            color: #95a5a6;
        }

        .donation-amount {
            display: flex;
            gap: 10px;
            flex-wrap: wrap;
            margin-bottom: 20px;
        }

        .amount-btn {
            padding: 10px 20px;
            border: 2px solid #3498db;
            border-radius: 25px;
            background: white;
            color: #3498db;
            cursor: pointer;
            transition: all 0.3s ease;
        }

        .amount-btn:hover,
        .amount-btn.active {
            background: #3498db;
            color: white;
        }

        .anonymous-wrapper {
            margin-top: 8px;
            padding-left: 12px;
        }

        .anonymous-check {
            display: flex;
            align-items: center;
            gap: 8px;
            color: #666;
            font-size: 0.9em;
        }

        .anonymous-check input[type="checkbox"] {
            width: 16px;
            height: 16px;
            margin: 0;
        }

        .submit-btn {
            width: 100%;
            padding: 15px;
            background: #3498db;
            color: white;
            border: none;
            border-radius: 8px;
            font-size: 18px;
            font-weight: 600;
            cursor: pointer;
            transition: all 0.3s ease;
        }

        .submit-btn:hover {
            background: #2980b9;
            transform: translateY(-2px);
        }

        .secure-badge {
            text-align: center;
            margin-top: 20px;
            color: #7f8c8d;
            font-size: 14px;
        }

        .input-group select {
            width: 100%;
            padding: 12px;
            border: 2px solid #e0e0e0;
            border-radius: 8px;
            font-size: 16px;
            background-color: white;
            color: #2c3e50;
            transition: all 0.3s ease;
            appearance: none;
            /* Menghilangkan tanda panah default browser */
            -webkit-appearance: none;
            /* Untuk browser berbasis WebKit */
            -moz-appearance: none;
            /* Untuk Firefox */
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20' fill='%2395a5a6'%3E%3Cpath fill-rule='evenodd' d='M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z' clip-rule='evenodd'/%3E%3C/svg%3E");
            background-repeat: no-repeat;
            background-position: right 12px center;
            background-size: 16px;
        }

        .input-group select:focus {
            border-color: #3498db;
            box-shadow: 0 0 0 3px rgba(52, 152, 219, 0.2);
            outline: none;
        }
    </style>
    <style>
        .post-header {
            background-color: #f8f9fa;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        }


        .post-title {
            font-size: 24px;
            font-weight: bold;
            margin-top: 20px;
        }

        .post-type,
        .post-skp,
        .post-location {
            font-size: 16px;
            color: #6c757d;
        }

        .post-description {
            margin-top: 15px;
            line-height: 1.6;
        }

        .comment-section {
            margin-top: 30px;
            margin-bottom: 30px;
            padding: 15px;
            background-color: #e9ecef;
            border-radius: 8px;
        }

        .form-control {
            border-radius: 5px;
            border: 1px solid #ced4da;
        }

        .intl-tel-input {
            display: table-cell;
        }

        .intl-tel-input .selected-flag {
            z-index: 4;
        }

        .intl-tel-input .country-list {
            z-index: 5;
        }

        .input-group .intl-tel-input .form-control {
            border-top-left-radius: 4px;
            border-top-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-bottom-right-radius: 0;
        }
    </style>
@endpush
@section('content')
    <main class="main">

        <!-- Page Title -->
        <div class="page-title dark-background" data-aos="fade">
            <div class="container">
                <nav class="breadcrumbs">
                    <ol>
                        <li><a href="index.html">Home</a></li>
                        <li class="current">Donasi</li>
                    </ol>
                </nav>

            </div>
        </div><!-- End Page Title -->

        <!-- Starter Section Section -->
        <section id="starter-section" class="starter-section section">




            <!-- Section Title -->
            <div class="container section-title" data-aos="fade-up">
                <span>Donasi<br></span>
                <h2>Donasi</h2>
                <p>Kesehatan penglihatan adalah bagian penting dari kehidupan yang berkualitas. Yayasan Indonesia
                    Melihat
                    percaya bahwa setiap individu berhak memiliki kesempatan untuk melihat dunia dengan lebih jelas.
                    Halaman
                    ini adalah ruang bagi Anda yang ingin berkontribusi dalam menciptakan masa depan di mana
                    penglihatan
                    yang sehat menjadi hak semua orang.</p>
            </div><!-- End Section Title -->
            <div class="container" data-aos="fade-up">
                <div class="post-header wow fadeInUp">


                    <div id="carouselExampleCaptions" class="carousel slide carousel-fade" data-bs-ride="carousel">
                        <div class="carousel-indicators">
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0"
                                class="active" aria-current="true" aria-label="Slide 1"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                                aria-label="Slide 2"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                                aria-label="Slide 3"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3"
                                aria-label="Slide 4"></button>
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="4"
                                aria-label="Slide 5"></button>
                        </div>
                        <div class="carousel-inner">
                            {{-- <div class="carousel-item active content">
                                <img src="{{ asset('assets/img/baner/okuli-baner.png') }}" class="d-block w-100 fit " alt="..."
                                    loading="lazy">
                                <div class="overlay"></div>
                                <div class="carousel-caption ">
                                    <h3>Ikuti Event OKULI</h3>

                                    <a href="/okuli"><button type="button" class="btn btn-danger">Daftar</button></a>
                                </div>
                            </div> --}}
                            <div class="carousel-item active content">
                                <img src="{{ asset('assets/img/baner/anak.webp') }}" class="d-block w-100 fit "
                                    alt="..." loading="lazy">
                                <div class="overlay"></div>

                            </div>
                            <div class="carousel-item content">
                                <img src="{{ asset('assets/img/baner/ibu.webp') }}" class="d-block w-100 fit" alt="..."
                                    loading="lazy">
                                <div class="overlay"></div>

                            </div>
                            <div class="carousel-item content">
                                <img src="{{ asset('assets/img/baner/periksa.webp') }}" class="d-block w-100 fit"
                                    alt="..." loading="lazy">
                                <div class="overlay"></div>

                            </div>
                            <div class="carousel-item content">
                                <img src="{{ asset('assets/img/baner/ibu2.webp') }}" class="d-block w-100 fit"
                                    alt="..." loading="lazy">
                                <div class="overlay"></div>

                            </div>
                        </div>
                        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                        </button>
                        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                        </button>
                    </div>


                </div>

                <div class="post-content wow fadeInUp">

                    <div class="donation-container mt-3">
                        <div class="form-header">
                            <h1 style="font-size: 2rem">🤝 Mari Berdonasi</h1>
                            <p>Bantuan Anda akan membuat perbedaan yang berarti</p>
                        </div>

                        <form id="donation-form">
                            <div class="input-group">
                                <label for="name">Nama Lengkap</label>
                                <input type="text" id="name" placeholder="Masukkan nama Anda">
                                <i class='bx bx-user'></i>
                                <div class="anonymous-wrapper">
                                    <label class="anonymous-check">
                                        <input type="checkbox" id="anonymous">
                                        <span>Donasi sebagai Anonim</span>
                                    </label>
                                </div>
                            </div>

                            <div class="input-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" placeholder="contoh@email.com">
                                <i class="fas fa-envelope"></i>
                            </div>

                            <div class="mb-3 row align-items-center">
                                <label for="phone" class="col-sm-2 col-form-label">No. Telepon</label>
                                <div class=" col-sm-10">
                                    <input type="tel" style="width: 100%" id="nohp" class="form-control">

                                </div>
                            </div>

                            <div class="input-group">
                                <label class="me-4">Jumlah Donasi</label>

                                <div class="donation-amount">
                                    <button type="button" class="amount-btn">Rp 50.000</button>
                                    <button type="button" class="amount-btn">Rp 100.000</button>
                                    <button type="button" class="amount-btn">Rp 200.000</button>
                                    <button type="button" class="amount-btn">Rp 500.000</button>
                                </div>
                                <input type="text" id="donation" placeholder="Masukkan nominal lain">
                                <i class="bx bx-money"></i>
                            </div>

                            <div class="input-group">
                                <label for="message">Pesan / Doa / Keterangan</label>
                                <textarea name="keterangan" id="keterangan" rows="4" placeholder="Tuliskan pesan atau doa Anda (opsional)"></textarea>
                            </div>

                            <button type="submit" class="submit-btn">
                                <i class='bx bxs-heart-circle'></i> Kirim Donasi
                            </button>

                            <div class="secure-badge">
                                <i class='bx bx-lock-alt'></i> Pembayaran Aman & Terenkripsi
                            </div>
                        </form>
                    </div>
                    <div class="donation-container mt-3">
                        <div class="form-header">
                            <h1 style="font-size: 2rem">🎁 Donasi Barang atau Jasa</h1>
                            <p>Bagikan barang atau jasa Anda untuk membantu sesama</p>
                        </div>

                        <form id="non-monetary-donation-form">
                            <div class="input-group">
                                <label for="name-nonmoney">Nama Lengkap</label>
                                <input type="text" id="name-nonmoney" placeholder="Masukkan nama Anda">
                                <i class='bx bx-user'></i>
                            </div>

                            <div class="input-group">
                                <label for="email-nonmoney">Email</label>
                                <input type="email" id="email-nonmoney" placeholder="contoh@email.com">
                                <i class="fas fa-envelope"></i>
                            </div>

                            <div class="input-group">
                                <label for="donation-type">Jenis Donasi</label>
                                <select id="donation-type">
                                    <option value="barang">Barang</option>
                                    <option value="jasa">Jasa</option>
                                </select>
                                <i class="bx bx-box"></i>
                            </div>

                            <div class="input-group">
                                <label for="description">Deskripsi Donasi</label>
                                <textarea id="description" rows="4" placeholder="Jelaskan barang atau jasa yang ingin didonasikan"></textarea>
                            </div>

                            <div class="input-group">
                                <label for="pickup-location">Lokasi Pengambilan / Pengiriman</label>
                                <input type="text" id="pickup-location"
                                    placeholder="Masukkan lokasi pengambilan atau pengiriman">
                                <i class="bx bx-map"></i>
                            </div>

                            <div class="input-group">
                                <label for="availability">Ketersediaan (Jika Jasa)</label>
                                <input type="datetime-local" id="availability">
                                <i class="bx bx-calendar"></i>
                            </div>



                            <button type="submit" class="submit-btn">
                                <i class='bx bxs-heart-circle'></i> Kirim Donasi
                            </button>

                            <div class="secure-badge">
                                <i class='bx bx-lock-alt'></i> Data Anda Aman & Terenkripsi
                            </div>
                        </form>
                    </div>

                    <div class="comment-section wow fadeInUp">


                        {{-- <form id="donation-form">
                            <div class="mb-3 row">
                                <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="name"
                                        placeholder="Masukkan nama Anda">
                                    <div class="form-check mt-2">
                                        <input class="form-check-input" type="checkbox" id="anonymous">
                                        <label class="form-check-label" for="anonymous">Anonim</label>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3 row align-items-center">
                                <label for="email" class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-6">
                                    <input type="email" class="form-control" id="email"
                                        placeholder="Masukkan email Anda">
                                </div>
                            </div>
                            <div class="mb-3 row align-items-center">
                                <label for="phone" class="col-sm-2 col-form-label">No. Telepon</label>
                                <div class=" col-sm-8">
                                    <input type="tel" id="nohp" class="form-control">

                                </div>
                            </div>



                            <div class="mb-3 row align-items-center">
                                <label for="donation" class="col-sm-2 col-form-label">Donasi</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="donation"
                                        placeholder="Masukkan nominal donasi">
                                </div>
                            </div>
                            <div class="mb-3 row align-items-center">
                                <label for="keterangan" class="col-sm-2 col-form-label">Keterangan</label>
                                <div class="col-sm-6">
                                    <textarea name="keterangan" id="keterangan" class="form-control"></textarea>
                                </div>
                            </div>

                            <button type="submit" class="btn btn-primary">Kirim Donasi</button>
                        </form> --}}
                    </div>

                </div>
            </div>
            <!-- Section Title -->
            <div class="container section-title" data-aos="fade-up">
                <span>Zakat untuk Cahaya Masa Depan<br></span>
                <h2>Zakat untuk Cahaya Masa Depan</h2>
                <p>Melalui kolaborasi dengan Unit Pengumpul Zakat (UPZ), Yayasan Indonesia Melihat menyediakan
                    ruang bagi
                    Anda untuk berkontribusi lebih luas melalui zakat, infak, dan sedekah. Setiap langkah ini
                    mendukung
                    program-program yang bertujuan meningkatkan kesehatan penglihatan dan mencegah kebutaan di
                    Indonesia.
                    Bersama, kita wujudkan dunia yang lebih terang, baik secara jasmani maupun rohani, untuk
                    mereka yang
                    membutuhkan.</p>
            </div><!-- End Section Title -->

            <div class="container" data-aos="fade-up">
                <div class="post-header wow fadeInUp">


                    <div class="slider">


                        <div class="slide">
                            <a href="/" data-fancybox="gallery" data-caption="baner">
                                <img src="/" alt="Cover Image" loading="lazy" class="slider-image">
                            </a>
                        </div>


                    </div>


                </div>

                <div class="post-content wow fadeInUp">


                    <div class="comment-section wow fadeInUp">

                    </div>

                </div>
            </div>





        </section>
    </main>


    @push('script')
        <script src="https://cdn.jsdelivr.net/npm/intl-tel-input@25.2.1/build/js/intlTelInput.min.js"></script>
        <script>
            const input = document.querySelector("#nohp");
            const iti = window.intlTelInput(input, {
                initialCountry: "id", // Set default ke Indonesia
                loadUtils: () => import("https://cdn.jsdelivr.net/npm/intl-tel-input@25.2.1/build/js/utils.js"),
            });
        </script>
        <script>
            function negara() {
                const apiURL = '/get-country-codes';

                // Tampilkan preloader selama data dimuat
                $('#country_code').html('<option>Loading...</option>');

                $.ajax({
                    url: apiURL,
                    method: 'GET',
                    dataType: 'json', // Pastikan ini sesuai dengan format response
                    success: function(response) {
                        if (Array.isArray(response)) {
                            // Kosongkan dropdown dan tambahkan opsi default
                            $('#country_code').empty().append(
                                '<option value="62" selected>Indonesia (+62)</option>');

                            // Urutkan data negara berdasarkan nama (A-Z)
                            response.sort((a, b) => a.name.localeCompare(b.name));

                            // Tambahkan opsi lainnya ke dropdown
                            response.forEach(country => {
                                if (country.name && country.code) {
                                    $('#country_code').append(
                                        `<option value="${country.code}">${country.name} (${country.code})</option>`
                                    );
                                }
                            });
                        } else {
                            console.error('Response is not an array:', response);
                            alert('Gagal memuat daftar kode negara.');
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error('AJAX Error:', textStatus, errorThrown);
                        alert('Gagal memuat daftar kode negara. Silakan coba lagi.');
                    }
                });
            }
            $(document).ready(function() {


                function loader(show) {
                    if (show) {
                        $("#preloader").show(); // Menampilkan preloader
                    } else {
                        $("#preloader").hide(); // Menyembunyikan preloader
                    }
                }
                // Format input donasi sebagai Rupiah
                const formatter = new Intl.NumberFormat('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0,
                });

                $('#donation').on('input', function() {
                    let value = $(this).val().replace(/[^\d]/g, ''); // Hanya angka
                    if (value) {
                        $(this).val(formatter.format(value)); // Format sebagai Rupiah
                    } else {
                        $(this).val('');
                    }
                });

                // Opsional: Reset nama jika anonim dicentang
                $('#anonymous').on('change', function() {
                    if ($(this).is(':checked')) {
                        $('#name').val('').prop('disabled', true);
                    } else {
                        $('#name').prop('disabled', false);
                    }
                });

                // Form submission (contoh)
                $('#donation-form').on('submit', function(e) {
                    e.preventDefault();
                    loader(true);
                    const countryData = iti.getSelectedCountryData();
                    const countryCode = countryData.dialCode; // Misalnya, 62 untuk Indonesia
                    let phone = input.value.trim();

                    // Hapus awalan 0 jika ada
                    if (phone.startsWith("0")) {
                        phone = phone.slice(1);
                    }

                    // Tambahkan kode negara ke nomor telepon
                    phone = countryCode + phone;

                    // Debugging untuk nomor telepon lengkap

                    const name = $('#anonymous').is(':checked') ? 'Anonim' : $('#name').val();
                    const keterangan = $('#keterangan').val();

                    const email = $('#email').val(); // Ambil email dari input
                    const donation = $('#donation').val().replace(/[^0-9]/g, '');
                    const csrfToken = $('meta[name="csrf-token"]').attr(
                        'content'); // Pastikan CSRF token tersedia dalam meta tag


                    $.ajax({
                        url: '/submit_donasi',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        data: {
                            nama: name,
                            nohp: phone,
                            keterangan: keterangan,
                            email: email, // Kirim email ke server
                            donasi: donation
                        },
                        success: function(response) {
                            if (response.success) {
                                // Tampilkan pesan sukses
                                window.location.href = "/d/" + response.data.id_transaksi;

                                // Swal.fire({
                                //     icon: "success",
                                //     title: "Success",
                                //     text: "Form successfully submitted.",
                                // });
                            } else {
                                // Tampilkan pesan error jika sudah ada yang terdaftar
                                loader(false);
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Gagal!',
                                    text: 'Gagal mengirim donasi. Silakan coba lagi.',
                                    confirmButtonText: 'OK'
                                });
                            }
                        },
                        error: function(xhr, status, error) {
                            loader(false);
                            console.error('Error:', error);
                            Swal.fire({
                                icon: 'error',
                                title: 'Gagal!',
                                text: 'Gagal mengirim donasi. Silakan coba lagi.',
                                confirmButtonText: 'OK'
                            });
                        }
                    });
                });

            });
        </script>
        <script>
            var myCarousel = document.querySelector('#carouselExampleCaptions')
            var carousel = new bootstrap.Carousel(myCarousel, {
                interval: 5000

            })
        </script>
        <script>
            // Handle amount button selection
            document.querySelectorAll('.amount-btn').forEach(button => {
                button.addEventListener('click', () => {
                    document.querySelectorAll('.amount-btn').forEach(btn => btn.classList.remove('active'));
                    button.classList.add('active');
                    document.getElementById('donation').value = button.textContent.trim();
                });
            });

            // Handle anonymous checkbox


            // Simple form validation
        </script>
    @endpush
@endsection

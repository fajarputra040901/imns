<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Pembayaran</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script type="text/javascript" src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="SB-Mid-client-Y9aC7PgVbtnRWcK0"></script>
    
    <style>
        :root {
            --primary-color: #2563eb;
            --secondary-color: #1e40af;
            --success-color: #16a34a;
            --warning-color: #ca8a04;
            --danger-color: #dc2626;
            --background-color: #f8fafc;
            --card-bg: #ffffff;
            --text-primary: #1e293b;
            --text-secondary: #64748b;
        }

        body {
            font-family: 'Inter', sans-serif;
            background-color: var(--background-color);
            color: var(--text-primary);
            line-height: 1.6;
        }

        .payment-container {
            max-width: 1200px;
            margin: 2rem auto;
            padding: 0 1rem;
        }

        .card {
            background: var(--card-bg);
            border-radius: 1rem;
            border: none;
            box-shadow: 0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1);
            transition: transform 0.2s ease;
        }

        .card:hover {
            transform: translateY(-2px);
        }

        .card-header {
            background: transparent;
            border-bottom: 1px solid #e2e8f0;
            padding: 1.5rem;
        }

        .status-badge {
            padding: 0.5rem 1rem;
            border-radius: 9999px;
            font-weight: 600;
            font-size: 0.875rem;
        }

        .status-unpaid {
            background-color: #fee2e2;
            color: var(--danger-color);
        }

        .status-paid {
            background-color: #dcfce7;
            color: var(--success-color);
        }

        .status-pending {
            background-color: #fef9c3;
            color: var(--warning-color);
        }

        .payment-details {
            padding: 1.5rem;
        }

        .payment-amount {
            font-size: 2rem;
            font-weight: 700;
            color: var(--primary-color);
        }

        .payment-id {
            color: var(--text-secondary);
            font-size: 0.875rem;
        }

        .payment-method {
            margin-top: 2rem;
        }

        .method-option {
            padding: 1rem;
            border: 2px solid #e2e8f0;
            border-radius: 0.75rem;
            margin-bottom: 1rem;
            cursor: pointer;
            transition: all 0.2s ease;
        }

        .method-option:hover {
            border-color: var(--primary-color);
            background-color: #f8fafc;
        }

        .method-option.selected {
            border-color: var(--primary-color);
            background-color: #eff6ff;
        }

        .btn-pay {
            background-color: var(--primary-color);
            color: white;
            padding: 1rem 2rem;
            border-radius: 0.75rem;
            border: none;
            font-weight: 600;
            width: 100%;
            transition: background-color 0.2s ease;
        }

        .btn-pay:hover {
            background-color: var(--secondary-color);
        }

        .contact-section {
            margin-top: 2rem;
            padding: 1.5rem;
            background-color: #f8fafc;
            border-radius: 0.75rem;
        }

        .contact-icon {
            font-size: 1.5rem;
            color: var(--primary-color);
        }

        .whatsapp-button {
            background-color: var(--success-color);
            color: white;
            padding: 0.75rem 1.5rem;
            border-radius: 0.75rem;
            display: inline-flex;
            align-items: center;
            gap: 0.5rem;
            text-decoration: none;
            transition: background-color 0.2s ease;
        }

        .whatsapp-button:hover {
            background-color: #15803d;
            color: white;
        }

        .info-alert {
            background-color: #eff6ff;
            border-left: 4px solid var(--primary-color);
            padding: 1rem;
            margin: 1rem 0;
            border-radius: 0.5rem;
        }

        .payment-steps {
            counter-reset: step;
            margin: 2rem 0;
        }

        .step-item {
            display: flex;
            align-items: flex-start;
            margin-bottom: 1rem;
            padding: 1rem;
            background-color: #f8fafc;
            border-radius: 0.75rem;
        }

        .step-number {
            background-color: var(--primary-color);
            color: white;
            width: 2rem;
            height: 2rem;
            border-radius: 9999px;
            display: flex;
            align-items: center;
            justify-content: center;
            font-weight: 600;
            margin-right: 1rem;
            flex-shrink: 0;
        }

        .step-content {
            flex-grow: 1;
        }

        @media (max-width: 768px) {
            .payment-container {
                margin: 1rem auto;
            }

            .card {
                margin-bottom: 1rem;
            }

            .payment-amount {
                font-size: 1.5rem;
            }
        }

        /* Loader styles */
        .preloader {
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: rgba(255, 255, 255, 0.9);
            display: flex;
            justify-content: center;
            align-items: center;
            z-index: 9999;
        }

        .loader {
            width: 48px;
            height: 48px;
            border: 5px solid var(--primary-color);
            border-bottom-color: transparent;
            border-radius: 50%;
            animation: rotation 1s linear infinite;
        }

        @keyframes rotation {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
    </style>
</head>
<body>
    <div id="preloader" class="preloader" style="display: none;">
        <div class="loader"></div>
    </div>

    <div class="payment-container">
        <div class="row">
            <div class="col-lg-7 mb-4">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="mb-0">Detail Pembayaran</h5>
                            <span class="status-badge" id="status"></span>
                        </div>
                    </div>
                    <div class="payment-details">
                        <div class="payment-id mb-4">
                            ID Transaksi: {{ $data->id_transaksi }}
                        </div>
                        <div class="payment-amount mb-4">
                            Rp {{ number_format($data->jumlah, 0, ',', '.') }}
                        </div>
                        
                        <div class="info-alert mb-4">
                            <i class='bx bx-info-circle me-2'></i>
                            Disarankan untuk menggunakan QRIS dalam metode pembayaran untuk menghindari biaya admin
                        </div>

                        <div class="payment-steps">
                            <div class="step-item">
                                <div class="step-number">1</div>
                                <div class="step-content">
                                    <h6>Pilih Metode Pembayaran</h6>
                                    <p>Tersedia berbagai metode pembayaran yang dapat Anda pilih</p>
                                </div>
                            </div>
                            <div class="step-item">
                                <div class="step-number">2</div>
                                <div class="step-content">
                                    <h6>Lakukan Pembayaran</h6>
                                    <p>Ikuti instruksi pembayaran yang diberikan</p>
                                </div>
                            </div>
                            <div class="step-item">
                                <div class="step-number">3</div>
                                <div class="step-content">
                                    <h6>Konfirmasi Otomatis</h6>
                                    <p>Status pembayaran akan diperbarui secara otomatis</p>
                                </div>
                            </div>
                        </div>

                        <div id="action" class="mt-4"></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-5">
                <div class="card mb-4">
                    <div class="card-header">
                        <h5 class="mb-0">Informasi Donatur</h5>
                    </div>
                    <div class="payment-details">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td><strong>Nama</strong></td>
                                    <td>{{ $data->nama }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Email</strong></td>
                                    <td>{{ $data->email }}</td>
                                </tr>
                                <tr>
                                    <td><strong>No. Telepon</strong></td>
                                    <td>{{ $data->nohp }}</td>
                                </tr>
                                <tr>
                                    <td><strong>Keterangan</strong></td>
                                    <td>{{ $data->keterangan }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header">
                        <h5 class="mb-0">Butuh Bantuan?</h5>
                    </div>
                    <div class="payment-details">
                        <p>Jika Anda mengalami kesulitan dalam proses pembayaran, silakan hubungi tim support kami:</p>
                        
                        <a href="https://wa.me/6285603530711?text=Saya+butuh+bantuan+terkait+pembayaran" 
                           class="whatsapp-button">
                            <i class='bx bxl-whatsapp'></i>
                            Hubungi via WhatsApp
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Your existing scripts -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js"></script>
    <script src="/portal/js/loader.js"></script>

    <script>
        $(document).ready(function() {
            var id = "{{ $data->id_transaksi }}";

            getData(id)


            var snapToken = 0;


            function getData(id) {
                loader(true);
                $.ajax({
                    type: "GET",
                    url: "/get_tr_donasi/" + id,
                    success: function(response) {
                        loader(false);
                        $('#action').empty();
                        var status = response.data.status;
                        snapToken = response.data.snaptoken;

                        updateStatus(status);
                    },
                    error: function(xhr, status, error) {
                        loader(false);
                        console.error("Error:", error);
                        alert("Terjadi kesalahan saat memuat data.");
                    }
                });
            }
            function updateStatus(status) {
                const statusElement = $('#status');
                const actionElement = $('#action');

                switch(status) {
                    case "unpaid":
                        statusElement.text('Belum Dibayar').addClass('status-unpaid');
                        actionElement.html('<button class="btn-pay" id="pay-button"><i class="bx bx-credit-card me-2"></i>Bayar Sekarang</button>');
                        break;
                    case "pending":
                        statusElement.text('Menunggu Pembayaran').addClass('status-pending');
                        actionElement.html('<button class="btn-pay" id="pay-button">Cek Status Pembayaran</button>');
                        break;
                    case "paid":
                        statusElement.text('Pembayaran Berhasil').addClass('status-paid');
                        actionElement.html('<button class="btn-pay" id="invoice-button"><i class="bx bx-download me-2"></i>Unduh Invoice</button>');
                        $('#grup').show();
                        break;
                    case "expired":
                        statusElement.text('Kadaluarsa').addClass('status-unpaid');
                        actionElement.html('<button class="btn-pay" id="re-pay"><i class="bx bx-refresh me-2"></i>Ulangi Pembayaran</button>');
                        break;
                }
            }
            $(document).on('click', '#pay-button', function() {

                snap.pay(snapToken, {
                    onSuccess: function(result) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Pembayaran Berhasil',
                            text: 'Status: Berhasil',
                            confirmButtonText: 'Tutup'
                        });
                        getData(id);
                    },
                    onPending: function(result) {
                        Swal.fire({
                            icon: 'info',
                            title: 'Pembayaran Pending',
                            text: 'Status: Pending',
                            confirmButtonText: 'Tutup'
                        });
                        getData(id);
                    },
                    onError: function(result) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Pembayaran Gagal',
                            text: 'Status: Gagal',
                            confirmButtonText: 'Tutup'
                        });
                        getData(id);
                    }
                });
            });



            function loader(show) {
                if (show) {
                    $('#preloader').show(); // Menampilkan preloader
                } else {
                    $('#preloader').hide(); // Menyembunyikan preloader
                }
            }




        });
    </script>
    <script>
        // Ambil snapToken dari response backend
    </script>
</body>
</html>
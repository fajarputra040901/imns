@extends('partial.index')
@section('content')
    <main id="main">

        <!-- ======= Breadcrumbs ======= -->
        <section id="breadcrumbs" class="breadcrumbs">
            <div class="container">

                <ol>
                    <li><a href="/">Beranda</a></li>
                    <li>Giveaway Okuli</li>
                </ol>
                <h2> Giveaway Okuli</h2>

            </div>
        </section><!-- End Breadcrumbs -->

        <main id="main">

            <section class="inner-page">
                <div class="container">
                    <div style="text-align:center">
                        <h3>Selamat {{ $data->nama }} Anda Menjadi Kandidat Pemenang Giveaway</h3>
                        <img src="{{ asset('assets/img/instastory-okuli.png') }}" alt="">
                    </div><br>


                    <div class="mt-3">
                        <p>Syarat Dan Ketentuan</p>
                        <ul>
                            <li>Pastikan Akun Instagram Tidak Di Privat</li>
                            <li>Pastikan Tidak Melakukan Unfollow Akun Instagram @indonesia.melihat</li>
                            <li>Giveaway Tidak Memungut Biaya Apapun</li>
                            <li>Pastikan Anda Mengikuti Semua Syarat Giveaway</li>
                            <li>Pemenang Akan Di Diberitahu Melalui Whatsapp Dan Story Di Akun @indonesia.melihat</li>
                            <li>Giveaway Dibuka Hingga 2 November 2023</li>
                            <li>Anda Memiliki Kesempatan Juga Mendapatkan Merchandise Khusus Dari Event OKULI 2023</li>
                            <li>Terakhir Post Di InstaStory Mu Sebagai Tanda Telah Mengikuti Giveaway Dan Tag 3 Temanmu
                                Serta
                                @indonesia.melihat</li>
                            <li>Gambar Postingan InstaStory Dapat Di Unduh Pada Tombol Dibawah</li>
                        </ul>
                        <p>Unduh Gambar InstaStory</p>
                        <a href="/download_instastory"><button class="btn btn-danger">Unduh</button></a>

                    </div>
                </div>
            </section>

        </main><!-- End #main -->
        {{-- <script>
            $(document).ready(function() {
                $('#form').submit(function(e) {
                    e.preventDefault();
                    var formData = new FormData(this);
                    $.ajax({
                        type: 'POST',
                        url: '/giveaway_post',
                        data: formData,
                        success: function(response) {
                            if (response.status === "error") {
                                alert(response.pesan);
                            } else if (response.status === "tidak") {
                                window.location.href = "/finish_giveaway_okuli";
                            } else {
                                alert(response.pesan);
                            }
                        }
                    });
                });
            });
        </script> --}}
    @endsection

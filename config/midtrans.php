<?php

return [
    'server_key' => env('MIDTRANS_SERVER_KEY'),
    'client_key' => env('MIDTRANS_CLIENT_KEY'),
    'is_production' => env('MIDTRANS_IS_PRODUCTION'), // false for sandbox, true for production
    'expiry' => env('MIDTRANS_EXPIRY', 3600), // Default expiry time for payment in seconds
];
